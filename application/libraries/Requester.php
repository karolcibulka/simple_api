<?php


class Requester
{

    protected $ci;
    protected $required_fields;
    protected $required_fields_login;
    protected $required_password_reset_fields;
    protected $required_update_fields;
    protected $required_update_password_fields;

    protected static $errors;
    protected static $user;

    protected $lang = null;
    protected $password_length = 6;
    protected $data;
    protected $property_id;
    protected $user_token;
    protected $property_settings;
    protected $property;
    protected $salt;
    protected $user_reservations;

    protected static $user_data;
    protected $forgot_password_url;
    protected $referer;
    protected $filters;
    protected $bill_id;
    protected $titan_id;
    protected $user_id;

    protected $property_has_group = false;
    protected $group_property_ids = array();


    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->model('ApiUser_model','user_model');

        $this->required_fields = array('first_name','last_name','zip','city','email','country','phone','street','street_number','password','password_confirmation');
        $this->required_fields_login = array('email','password');
        $this->required_password_reset_fields = array('password','password_confirmation');
        $this->required_update_fields = array('first_name','last_name','zip','city','email','country','phone','street','street_number');
        $this->required_update_password_fields = array('old_password','password','password_confirmation');
        $this->required_get_user_data_fields = array('user_token','user_id');
        $this->filters = array('first_name','last_name','phone','email','country','street','street_number','zip','city','lang','token','id');

        $this->salt = generateToken(20);
        $this->user_token = generateToken(50);
        $this->forgot_password_url = '/loyaltySystem/forgotPassword/';
        $this->referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'http://localhost/bookingfront';
        $this->referer = rtrim($this->referer,'/');
    }

    public static function init(){
        return new self;
    }

    //setters start
    public function setLang($lang){
        $this->lang = $lang;
        $this->ci->lang->load('api',getLangFromCode($this->lang));

        return $this;
    }

    public function setTitanID($titan_id){
        $this->titan_id = $titan_id;
        self::$user['titan_id'] = $titan_id;
        return $this;
    }

    public function setBillID($bill_id){
        $this->bill_id = $bill_id;
        return $this;
    }

    public function setData($data){
        $this->data = $data;

        return $this;
    }

    public function setUserToken($token){
        $this->user_token = $token;

        return $this;
    }

    public function setProperty($propertyID){
        $this->property_id = $propertyID;
        if(!$this->property_settings = $this->ci->user_model->getPropertySettings($this->property_id)){
            self::$errors[] = lang('api.errors.property');
        }

        return $this;
    }

    public function setUserID($userID){
        $this->user_id = $userID;

        if(!self::$user = $this->ci->user_model->getUserByID($this->property_id,$userID)){
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_id'),$userID);
        }


        return $this;
    }

    private function setSalt($salt) {
        $this->salt = $salt;
    }
    //setters end

    //main start
    public function updateUser(){
        $this->validateUpdateFields();
        if(!self::$user = $this->ci->user_model->getUserRaw($this->property_id,$this->user_token)){
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_token'),$this->user_token);
        }
        if($update_password = $this->updatePassword()){
            $this->validateUpdatePasswordFields();
            if(empty(self::$errors)){
                $this->validateUpdatePassword();
            }
        }

        if(empty(self::$errors)){
            $update_user_data = array();

            foreach($this->required_update_fields as $required_field){
                $update_user_data[$required_field] = $this->data[$required_field];
            }

            if($update_password){
                $update_user_data['password'] = $this->hashPassword();
            }

            $this->ci->user_model->updateUser($this->user_token,$update_user_data);

            self::$user_data = array(
                'user_id' => self::$user['id'],
                'user_token' => $this->user_token
            );

            if(empty(self::$user['titan_id'])){
                $this->doTitanOperations('titan');
            }

            if(empty(self::$user['ewallet_id'])){
                $this->doTitanOperations('ewallet');
            }

            $this->titanUpdateUser();
        }
    }

    public function login(){
        $this->validateRequiredFieldsLogin();
        if(self::$user = $this->ci->user_model->getUsersByEmail($this->data['email'],$this->property_id)) {

            $this->validateLoginPassword();

            if(empty(self::$errors)){
                self::$user_data = array(
                    'user_id' => self::$user['id'],
                    'user_token' => self::$user['token']
                );

                $this->user_token = self::$user['token'];

                if(empty(self::$user['titan_id'])){
                    $this->doTitanOperations('titan');
                }
                if(empty(self::$user['ewallet_id'])){
                    $this->doTitanOperations('ewallet');
                }
            }
        }
        else{
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist'),$this->data['email']);
        }
    }

    public function resetPassword(){
        $this->validateResetPasswordFields();

        if(!self::$user = $this->ci->user_model->getUserRaw($this->property_id,$this->user_token)) {
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_token'),$this->user_token);
        }

        if(empty(self::$errors)){
            $this->validateUpdatePassword(false);
        }

        if(empty(self::$errors)){
            $this->salt = self::$user['salt'];

            $update_user_data = array(
                'password' => $this->hashPassword()
            );

            $this->ci->user_model->updateUser($this->user_token,$update_user_data);

            self::$user_data = array(
                'user_id' => self::$user['id'],
                'user_token' => $this->user_token
            );
        }

    }

    private function updatePassword(){
        $state = false;
        foreach($this->required_update_password_fields as $required_field){
            if(isset($this->data[$required_field]) && !empty($this->data[$required_field])){
                $state = true;
                break;
            }
        }

        return $state;
    }

    private function createUserProcess(){
        $this->data['password'] = $this->hashPassword();
        $this->data['created_at'] = date('Y-m-d H:i:s');
        $this->data['token'] = $this->user_token;
        $this->data['lang'] = $this->lang;
        $this->data['property_id'] = $this->property_id;
        $this->data['salt'] = $this->salt;

        unset($this->data['password_confirmation']);

        if($user_id = $this->ci->user_model->storeUser($this->data)){

            self::$user_data = array(
                'user_id' => $user_id,
                'user_token' => $this->user_token
            );

            self::$user = $this->data;
            self::$user['id'] = $user_id;
            $this->doTitanOperations();
        }
    }

    public function createUser(){
        $this->validateRequiredFields();
        $this->validatePassword();
        if(empty(self::$errors)){
            if(!self::$user = $this->ci->user_model->getUsersByEmail($this->data['email'],$this->property_id)){
                $this->createUserProcess();
            }
            else{
                self::$errors[] = line_with_argument(lang('api.errors.user_exist'),$this->data['email']);
            }
        }
    }

    public function getUserWithValidation(){
        $this->validateRequiredFieldsUserData();
        if(empty(self::$errors)){
            $this->user_token = $this->data['user_token'];
            if(!self::$user = $this->ci->user_model->getUser($this->property_id,$this->user_token)){
                self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_token'),$this->user_token);
            }

            if($categories = $this->doTitanOperations('marketing_categories')){
                $this->ci->user_model->updateUser($this->user_token,array('marketing_categories'=>$categories));
            }

        }
    }

    public function getUserBookings(){
        $this->loadTitanLibrary();
        $this->getUserRaw();
        if($bookings = $this->ci->titan->listGuestBookings(self::$user['titan_id'])){
            return $bookings;
        }

        self::$errors[] = lang('api.errors.no_bookings');
    }

    public function getUserAccommodations(){
        $this->loadTitanLibrary();
        $this->getUserRaw();
        if($accommodations = $this->ci->titan->listGuestAccommodations(self::$user['titan_id'])){
            return $accommodations;
        }

        self::$errors[] = lang('api.errors.no_accommodations');
    }

    public function getWallet($only_wallet = false){
        $this->loadWallet();
        if(!$only_wallet) {
            $this->getUserRaw();
        }

        if($wallet = $this->ci->eWallet->getEwallet(self::$user['ewallet_id'])){
            return $wallet;
        }

        self::$errors[] = lang('api.errors.wallet');
    }

    public function getWalletActivities(){
        $this->loadWallet();
        $this->getUserRaw();

        if($activities = $this->ci->eWallet->listEwalletActivities(self::$user['ewallet_id'])){
            return $activities;
        }

        self::$errors[] = lang('api.errors.activities');
    }
    //main end

    //validation start
    private function validateResetPasswordFields(){
        foreach($this->required_password_reset_fields as $required_field){
            if(!isset($this->data[$required_field]) || $this->data[$required_field]){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }
    }

    private function validateUpdatePassword($validate_old_password = true){
        $password = self::$user['password'];
        $this->salt = self::$user['salt'];
        if($validate_old_password){
            if($password !== $this->hashPassword($this->data['old_password'])){
                self::$errors[] = lang('api.errors.old_password_doesnt_match');
            }
        }

        if($this->data['password'] !== $this->data['password_confirmation']){
            self::$errors[] = lang('api.errors.password_match');
        }

    }

    private function validateUpdateFields(){
        foreach($this->required_update_fields as $required_field){
            if(!isset($this->data[$required_field]) || empty($this->data[$required_field])){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }
    }

    private function validateUpdatePasswordFields(){
        foreach($this->required_update_password_fields as $required_field){
            if(!isset($this->data[$required_field]) || empty($this->data[$required_field])){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }
    }

    private function validateRequiredFieldsUserData(){
        foreach($this->required_get_user_data_fields as $required_field){
            if(!isset($this->data[$required_field]) || empty($this->data[$required_field])){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }
    }

    private function validateLoginPassword(){
        $this->salt = self::$user['salt'];
        if(self::$user['password'] !== $this->hashPassword()){
            self::$errors[] = lang('api.errors.wrong_password');
        }

    }

    private function validatePassword(){

        if(!empty(self::$errors)) return false;

        if($this->data['password'] !== $this->data['password_confirmation']){
            self::$errors[] = lang('api.errors.password_match');
        }

        if(strlen($this->data['password']) < $this->password_length ){
            self::$errors[] = line_with_argument(lang('api.errors.password_length'),$this->password_length);
        }
    }

    private function validateRequiredFieldsLogin(){
        foreach($this->required_fields_login as $required_field){
            if(!isset($this->data[$required_field]) || empty($this->data[$required_field])){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }

        if(isset($this->data) && !empty($this->data)){
            foreach($this->data as $key => $value){
                if(!in_array($key,$this->required_fields_login)){
                    unset($this->data[$key]);
                }
            }
        }

    }

    private function validateRequiredFields(){
        foreach($this->required_fields as $required_field){
            if(!isset($this->data[$required_field]) || empty($this->data[$required_field])){
                self::$errors[] = lang('api.errors.'.$required_field);
            }
        }

        if(isset($this->data) && !empty($this->data)){
            foreach($this->data as $key => $value){
                if(!in_array($key,$this->required_fields) && $key !== 'newsletter'){
                    unset($this->data[$key]);
                }
            }
        }
    }
    //validation end

    //getters start

    public static function getUserData(){
        return self::$user_data;
    }

    public static function getErrors(){
        return self::$errors;
    }

    public static function getUser(){
        return self::$user;
    }

    public function getUserCategories(){
        if(empty(self::$errors)){
            if(!$categories = $this->doTitanOperations('marketing_categories')){
                self::$errors[] = lang('api.errors.no_categories');
            }

            return $categories;
        }
    }

    public function getForgotPasswordUrl(){
        if(isset($this->data['email']) && !empty($this->data['email'])){
            if(self::$user = $this->ci->user_model->getUserByEmail($this->data['email'],$this->property_id)){
                return array(
                    'url' => $this->referer.'/'.self::$user['lang'].$this->forgot_password_url.self::$user['token'].'?email='.self::$user['email'],
                    'user' => $this->filterResponse(self::$user)
                );
            }

            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist'),$this->data['email']);

        }
        else{
            self::$errors[] = lang('api.errors.email');
        }
    }

    public function getUserRaw(){
        if(!self::$user = $this->ci->user_model->getUserRaw($this->property_id,$this->user_token)){
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_token'),$this->user_token);
        }
    }

    public function getBill(){
        $this->loadTitanLibrary();
        $this->getUserRaw();

        if($bill = $this->ci->titan->getBillDetail(self::$user['titan_id'],$this->bill_id)){
            return $bill;
        }

        self::$errors[] = lang('api.errors.bill');
    }

    public function getBills(){
        $this->loadTitanLibrary();
        $this->getUserRaw();

        $response = array();

        if(isset($this->data) && !empty($this->data)){
            foreach($this->data as $bill_id_key => $bill_id){
                $response[$bill_id_key] = $this->ci->titan->getBillDetail(self::$user['titan_id'],$bill_id);
            }
        }

        if(!empty($response)){
            return $response;
        }

        return array();
    }
    //getters end

    //titan start
    private function doTitanOperations($type = null){
        switch($type){
            case 'ewallet':
                $this->ewalletOperation();
                break;
            case 'titan':
                $this->titanOperation();
                break;
            case 'marketing_categories':
                return $this->titanMarketingCategories();
                break;
            default:
                $this->titanOperation();
                $this->ewalletOperation();
                break;
        }
    }

    private function titanMarketingCategories(){
        $this->loadTitanLibrary();

        $check_more = false;
        $titan_id = null;

        $guest_data = array();

        if($titan_user = $this->ci->titan->getGuestByEmail(self::$user['email'])){
            if(!isset($titan_user['guest']) || empty($titan_user['guest'])){
                $check_more = true;
            }
            else{
                $guest_data = $titan_user;
            }
        }

        if($check_more){
            if($titan_users = $this->ci->titan->findGuestViaEmail(self::$user['email'])){
                if(isset($titan_users['guests']) && !empty($titan_users['guests'])){
                    foreach($titan_users['guests'] as $guest){
                        if($guest['postCode'] === self::$user['zip']){
                            $guest_data = $guest;
                            break;
                        }
                    }
                }
            }
        }

        return $guest_data;


    }

    private function titanOperation(){
        $this->loadTitanLibrary();

        $check_more = false;
        $titan_id = null;


        if($titan_user = $this->ci->titan->getGuestByEmail(self::$user['email'])){
            if(!isset($titan_user['guest']) || empty($titan_user['guest'])){
                $check_more = true;
            }
            else{
                $titan_id = $titan_user['guest']['id'];
            }
        }

        if($check_more){
            if($titan_users = $this->ci->titan->findGuestViaEmail(self::$user['email'])){
                if(isset($titan_users['guests']) && !empty($titan_users['guests'])){
                    foreach($titan_users['guests'] as $guest){
                        if($guest['postCode'] === self::$user['zip']){
                            $titan_id = $guest['id'];
                            break;
                        }
                    }
                }
            }
        }


        if($titan_id){
            self::$user['titan_id'] = $titan_id;
        }
        else{
            $data = array(
                'id' => null,
                'name' => self::$user['first_name'],
                'surname' => self::$user['last_name'],
                'address' => array(
                    'street' => array(
                        'name' => self::$user['street']
                    ),
                    'city' => array(
                        'name' => self::$user['city']
                    ),
                    'state'	=> array(
                        'code' => self::$user['country'],
                        'name' => self::$user['country']
                    ),
                    'postcode' => self::$user['zip']
                ),
                'telephoneNumber' => isset(self::$user['phone']) ? self::$user['phone'] : null,
                'email' => self::$user['email']
            );

            if($response = $this->ci->titan->storeGuest($data)){
               if(isset($response['guest']) && !empty($response['guest'])){
                   self::$user['titan_id'] = $titan_id = $response['guest']['id'];
               }
            }
        }

        if($titan_id && isset(self::$user_data['user_token']) && !empty(self::$user_data['user_token'])){
            $this->ci->user_model->updateUser(self::$user_data['user_token'],array('titan_id'=>$titan_id));
        }

    }

    private function ewalletOperation(){
        $this->loadWallet();

        $eWallet_id = null;

        if(isset(self::$user['titan_id']) && !empty(self::$user['titan_id'])){
            if($eWallets = $this->ci->eWallet->searchEwallet(self::$user['titan_id'])){
                foreach($eWallets as $eWallet){
                    if($eWallet['ewalletType']['code'] === 'POINTS'){
                        $eWallet_id = $eWallet['id'];
                        break;
                    }
                }
            }
            else{
                if($eWallet = $this->ci->eWallet->createEwallet(self::$user['titan_id'])){
                    if(isset($eWallet['ewalletType']['code']) && $eWallet['ewalletType']['code'] === 'POINTS'){
                        $eWallet_id = $eWallet['id'];
                    }
                }
            }
        }

        if($eWallet_id){
            self::$user['ewallet_id'] = $eWallet_id;
            if(isset(self::$user_data['user_token']) && !empty(self::$user_data['user_token'])){
                $this->ci->user_model->updateUser(self::$user_data['user_token'],array('ewallet_id'=>$eWallet_id));
            }
        }
    }

    private function titanUpdateUser(){
        $this->getUserRaw();
        $this->loadTitanLibrary();

        $update_user_data = array(
            'id' => self::$user['titan_id'],
            'name' => self::$user['first_name'],
            'surname' => self::$user['last_name'],
            'address' => array(
                'street' => array(
                    'name' => self::$user['street']
                ),
                'city' => array(
                    'name' => self::$user['city']
                ),
                'state'	=> array(
                    'code' => self::$user['country'],
                    'name' => self::$user['country']
                ),
                'postcode' => self::$user['zip']
            ),
            'telephoneNumber' => self::$user['phone'],
            'email' => self::$user['email']
        );

        $this->ci->titan->storeGuest($update_user_data);

    }

    //titan end

    //other methods start
    private function hashPassword($password = false){
        if($password){
            return md5($password.$this->salt);
        }
        return md5($this->data['password'].$this->salt);
    }

    private function filterResponse($user_data){

        if(!empty($user_data)){
            foreach($user_data as $field => $item){
                if(!in_array($field,$this->filters)){
                    unset($user_data[$field]);
                }
            }
        }

        return $user_data;
    }
    //other methods end

    //points start
    public function storePoints(){
        if(isset($this->data['price']) && !empty($this->data['price'])){
            $this->pointOperation('INCREASE');
        }
        else{
            self::$errors[] = lang('api.errors.price');
        }
    }

    public function drawPoints(){
        if(isset($this->data['points']) && !empty($this->data['points'])){
            $this->pointOperation('DRAWING');
        }
        else{
            self::$errors[] = lang('api.errors.points');
        }
    }

    private function pointOperation($type){
        if(self::$user = $this->ci->user_model->getUser($this->property_id,$this->user_token)){

            if(empty(self::$user['titan_id'])){
                $this->doTitanOperations('titan');
            }

            if(empty(self::$user['ewallet_id'])){
                $this->doTitanOperations('ewallet');
            }

            $old_value = 0;
            if($wallet = $this->getWallet(true)){
                if(isset($wallet['actualBalance']) && !empty($wallet['actualBalance'])){
                    $old_value = $wallet['actualBalance'];
                }
            }


            if(!empty(self::$user['titan_id']) && !empty(self::$user['ewallet_id'])){
                $this->loadWallet();
                switch($type){
                    case 'INCREASE':
                        if(isset($this->property_settings['increase_points']) && !empty($this->property_settings['increase_points'])) {
                            $this->ci->eWallet->createNewActivity(self::$user['ewallet_id'], self::$user['titan_id'], $this->data['price'], 0, 'INCREASE', $old_value);
                        }
                        else{
                            self::$errors[] = lang('api.errors.cannot_increase_points');
                        }
                        break;
                    case 'DRAWING':
                        if(isset($this->property_settings['draw_points']) && !empty($this->property_settings['draw_points'])) {
                            $this->ci->eWallet->createNewActivity(self::$user['ewallet_id'], self::$user['titan_id'], 0, $this->data['points'], 'DRAWING', $old_value);
                        }
                        else{
                            self::$errors[] = lang('api.errors.cannot_draw_points');
                        }
                        break;
                }
            }
        }
        else{
            self::$errors[] = line_with_argument(lang('api.errors.user_doesnt_exist_token'),$this->user_token);
        }
    }

    public function pointOperationRaw($type){
        $old_value = null;

        if(empty(self::$errors)){
            if(isset(self::$user['titan_id']) && !empty(self::$user['titan_id'])){
                if($wallet = $this->getWallet(true)){
                    if(isset($wallet['actualBalance']) && !empty($wallet['actualBalance'])){
                        $old_value = $wallet['actualBalance'];
                    }
                }

                $this->ewalletOperation();

                if(isset(self::$user['ewallet_id']) && !empty(self::$user['ewallet_id'])){
                    switch($type){
                        case 'INCREASE':
                            if(isset($this->property_settings['increase_points']) && !empty($this->property_settings['increase_points'])){
                                $this->ci->eWallet->createNewActivity(self::$user['ewallet_id'],self::$user['titan_id'],$this->data['price'],0,'INCREASE',$old_value);
                            }
                            else{
                                self::$errors[] = lang('api.errors.cannot_increase_points');
                            }
                            break;
                        case 'DRAWING':
                            if(isset($this->property_settings['draw_points']) && !empty($this->property_settings['draw_points'])) {
                                $this->ci->eWallet->createNewActivity(self::$user['ewallet_id'], self::$user['titan_id'], 0, $this->data['points'], 'DRAWING', $old_value);
                            }
                            else{
                                self::$errors[] = lang('api.errors.cannot_draw_points');
                            }
                            break;
                    }
                }
            }
            else{
                self::$errors[] = lang('api.errors.titan_id');
            }
        }
    }

    public function getUserTitanID(){
        $titan_id = null;

        if(self::$user = $this->ci->user_model->getUserByEmail($this->data['email'],$this->property_id)){
            $titan_id = self::$user['titan_id'];
        }
        else{
            self::$user = $this->data;
            $this->doTitanOperations('titan');
            if(isset(self::$user['titan_id']) && !empty(self::$user['titan_id'])){
                $titan_id = self::$user['titan_id'];
            }
        }

        return $titan_id;
    }


    //loaders start
    private function loadWallet(){
        $this->ci->load->library('ExternalSystems/EWallet',$this->property_settings,'eWallet');
    }

    private function loadTitanLibrary(){
        $this->ci->load->library('ExternalSystems/Titan',$this->property_settings,'titan');
    }
    //loeaders end
}