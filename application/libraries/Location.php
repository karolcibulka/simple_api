<?php

class Location {

    protected $ci;
    protected static $ip;
    protected static $response;

    public function __construct($ip = null)
    {
        $this->ci = & get_instance();
        self::$ip = $ip;
    }

    public static function setIP($ip){
        self::$ip = $ip;
        return new self;
    }

    public function getLocation(){
        self::$response = json_decode(file_get_contents('http://ip-api.com/json/'.self::$ip));
    }

    public function getCountryCode(){
        if(isset(self::$response['status'],self::$response['countryCode']) && self::$response['status'] === 'success'){
            return self::$response['countryCode'];
        }

        return null;
    }

}
