<?php

class PriceProfiles {

    private static $price;
    private static $price_profile;
    private static $price_profiles;

    public static function init($price_profiles){
        self::$price_profiles = $price_profiles;
        return new self;
    } 

    public static function setPrice($price){
        self::$price = $price;
        return self::init(self::$price_profiles);
    }

    public function setPriceProfile($price_profile){
        self::$price_profile = $price_profile;
        return $this;
    }

    public function getPrice(){
        
    }
}