<?php


class Traveldata
{

    protected $ci;
    protected $endpoint = 'https://api.softsolutions.sk';
    protected $api_key = 'JksU6wl5doo5qrWMi5gnifoHBeTUHO';

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->library('curl');
    }

    public function getRooms($hotelID){
        $this->ci->curl->create($this->endpoint.'/export/gastro/getRooms/'.$hotelID);
        $this->ci->curl->http_header('API-KEY',$this->api_key);
        $response = json_decode($this->ci->curl->execute(),TRUE);

        if(isset($response['status'],$response['data']) && $response['status'] === TRUE && !empty($response['data'])){
            return $response['data'];
        }

        return array();
    }
}