<?php


class EWallet
{

    private $ci;

    private $property_id;
    private $titan_end_point;
    private $uach;
    private $cgin;
    private $ratio_point;

    private $data;


    public function __construct($serverData)
    {
        $this->ci            = &get_instance();

        $this->property_id       = $serverData['id'];
        $this->titan_end_point = 'http://' . $serverData['server'] . ':' . $serverData['port'] . '/TitanAPI/rest/ewallet-service/';

        $this->uach   = $serverData['uach'];
        $this->cgin   = $serverData['cgin'];
        $this->ratio_point   = $serverData['point_ratio'];
        $this->data = array();

        include_once APPPATH . '/libraries/phpjwx/libjsoncrypto.php';
        $this->ci->load->library('curl');
    }

    private function setEncryptedData()
    {
        return jwt_encrypt(json_encode($this->data), APPPATH . '/config/property/' . $this->property_id . '/titan_public.key', false, null, null, null, 'RSA1_5', 'A256GCM', true);
    }

    private function callTitan($method ,$post = true)
    {
        $this->ci->curl->create($this->titan_end_point . $method);
        $this->ci->curl->http_header('UACH', $this->uach);
        $this->ci->curl->http_header('Content-Type', 'application/json');
        if($post){
            $this->ci->curl->post($this->setEncryptedData());
        }
        $encryptedResponse = $this->ci->curl->execute();
        return jwt_decrypt($encryptedResponse, APPPATH . '/config/property/' . $this->property_id . '/titan_private.key');
    }

    public function createEwallet($guestID)
    {
        $this->data = $this->getData($guestID);

        if ($decryptedResponse = $this->callTitan('createEwallet/'.$this->cgin.'/'.$guestID)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function modifyEwallet()
    {
        if ($decryptedResponse = $this->callTitan('modifyEwallet/'.$this->cgin)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function removeEwallet($eWalletID)
    {
        if ($decryptedResponse = $this->callTitan('removeEwallet/'.$this->cgin.'/'.$eWalletID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function getEwallet($eWalletID)
    {
        if ($decryptedResponse = $this->callTitan('getEwallet/'.$this->cgin.'/'.$eWalletID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function searchEwallet($guestID)
    {
        if ($decryptedResponse = $this->callTitan('searchEwallet/'.$this->cgin.'?guestId='.$guestID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;

    }

    public function stripEwallet($guestID, $eWalletID)
    {
        if ($decryptedResponse = $this->callTitan('stripEwallet/'.$this->cgin.'/'.$guestID.'/'.$eWalletID , false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function addEwallet($guestID, $eWalletID)
    {
        if ($decryptedResponse = $this->callTitan('addEwallet/'.$this->cgin.'/'.$guestID.'/'.$eWalletID)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function listEwallets($guestID)
    {
        if ($decryptedResponse = $this->callTitan('listEwallets/'.$this->cgin.'/'.$guestID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }
        return false;

    }

    public function newEwalletActivity($titan_id, $ewallet_id, $post)
    {
        $this->data = $post;


        if ($decryptedResponse = $this->callTitan('newEwalletActivity/'.$this->cgin.'/'.$titan_id.'/'.$ewallet_id)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function listEwalletActivities($eWalletID, $offset = 0, $limit = 1000)
    {

        if ($decryptedResponse = $this->callTitan('listEwalletActivities/'.$this->cgin.'/'.$eWalletID.'/'.$offset.'/'.$limit,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;

    }

    public function confirmPreauthorization($eWalletActivityID)
    {
        if ($decryptedResponse = $this->callTitan('confirmPreauthorization/'.$this->cgin.'/'.$eWalletActivityID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function cancelPreauthorization($eWalletActivityID)
    {
        if ($decryptedResponse = $this->callTitan('cancelPreauthorization/'.$this->cgin.'/'.$eWalletActivityID,false)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function benefits()
    {
        if ($decryptedResponse = $this->callTitan('benefits/'.$this->cgin)) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    private function getData($titan_id)
    {
        return array(
            'id' => null,
            'ewalletType' => array(
                'id' => 2,
                'code' => 'POINTS',
                'name' => 'eWallet_' . $titan_id . '_' . $this->property_id . '_pointsEWallet',
            ),
            'name' => 'eWallet_' . $titan_id . '_' . $this->property_id . '_pointsEWallet',
        );
    }

    public function createNewActivity($ewallet_id,$titan_id,$price,$points,$type,$old_value)
    {
        if (!empty($ewallet_id)) {
            switch ($type) {
                case 'INCREASE':
                    $request = $this->createIncreaseRequest($ewallet_id, $price, $this->ratio_point,$old_value);
                    break;
                case 'DRAWING':
                    $request = $this->createDrawingRequest($ewallet_id, $points,$old_value);
                    break;
            }

            return $this->newEwalletActivity($titan_id, $ewallet_id, $request);
        }

        return true;
    }

    private function createIncreaseRequest($ewallet_id, $price, $pointRatio,$old_value)
    {
        return array(
            'id' => null,
            'ewallet' => array(
                'id' => $ewallet_id,
                'ewalletType' => array(
                    'id' => 2,
                    'code' => 'POINTS',
                    'name' => 'points'
                )
            ),
            'activityType' => array(
                'id' => 1,
                'code' => 'INCREASE',
                'name' => 'increase'
            ),
            'originalValue' => $old_value,
            'activityValue' => floor($price / $pointRatio),
            'vat' => 0,
            'created' => strtotime(date('Y-m-d H:i:s')) * 1000,
            'createdBy' => 'ja',
            'createdById' => 0,
        );
    }

    private function createDrawingRequest($ewallet_id, $points,$old_value)
    {
        return array(
            'id' => null,
            'ewallet' => array(
                'id' => $ewallet_id,
                'ewalletType' => array(
                    'id' => 2,
                    'code' => 'POINTS',
                    'name' => 'points'
                )
            ),
            'activityType' => array(
                'id' => 2,
                'code' => 'DRAWING',
                'name' => 'drawing'
            ),
            'originalValue' => $old_value,
            'activityValue' => $points,
            'vat' => 0,
            'created' => strtotime(date('Y-m-d H:i:s')) * 1000,
            'createdBy' => 'ja',
            'createdById' => 0,
        );
    }



}