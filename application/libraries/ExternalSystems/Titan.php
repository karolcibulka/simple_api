<?php

/**
 * Class Titan
 */

class Titan
{

    private $ci;
    private $property_id;
    private $titan_end_point;
    private $uach;
    private $cgin;

    private $data;

    public function __construct($serverData)
    {
        $this->ci = &get_instance();

        $this->property_id        = $serverData['id'];
        $this->titan_end_point = 'http://' . $serverData['server'] . ':' . $serverData['port'] . '/TitanAPI/rest/guests-service/';
        $this->uach            = $serverData['uach'];
        $this->cgin            = $serverData['cgin'];

        include_once APPPATH . '/libraries/phpjwx/libjsoncrypto.php';
        $this->ci->load->library('curl');

    }

    private function setEncryptedData()
    {
        return jwt_encrypt(json_encode($this->data), APPPATH . '/config/property/' . $this->property_id . '/titan_public.key', false, null, null, null, 'RSA1_5', 'A256GCM', true);
    }

    private function callTitan($method)
    {
        $this->ci->curl->create($this->titan_end_point . $method);
        $this->ci->curl->http_header('UACH', $this->uach);
        $this->ci->curl->http_header('Content-Type', 'application/json');
        $this->ci->curl->post($this->setEncryptedData());
        $encryptedResponse = $this->ci->curl->execute();
        return jwt_decrypt($encryptedResponse, APPPATH . '/config/property/' . $this->property_id . '/titan_private.key');
    }

    public function getGuestByEmail($email)
    {
        $this->data = array('cgin' => $this->cgin, 'emailAddress' => $email);

        if ($decryptedResponse = $this->callTitan('getGuestViaEmail')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function getGuest($guestId)
    {
        $this->data = array('cgin' => $this->cgin, 'guestId' => $guestId);

        if ($decryptedResponse = $this->callTitan('getGuest')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function getGuestHistory($guestId)
    {
        $this->data = array('cgin' => $this->cgin, 'guestId' => $guestId);

        if ($decryptedResponse = $this->callTitan('getGuestHistory')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function storeGuest($guestData)
    {

        $this->data = array('cgin' => $this->cgin, 'guest' => $guestData);

        if ($decryptedResponse = $this->callTitan('storeGuest')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function listBillHeaders($guestId, $offset, $limit)
    {
        $this->data = array( 'cgin' => $this->cgin, 'guestId' => $guestId, 'offset' => $offset, 'limit' => $limit );

        if ($decryptedResponse = $this->callTitan('listBillHeaders')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function getBillDetail($guestId, $billId)
    {
        $this->data = array('cgin' => $this->cgin, 'guestId' => $guestId, 'billId' => $billId);

        if ($decryptedResponse = $this->callTitan('getBillDetail')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function listGuestAccommodations($guestId)
    {
        $this->data = array('cgin' => $this->cgin, 'guestId' => $guestId);

        if ($decryptedResponse = $this->callTitan('listGuestAccommodations')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function listGuestBookings($guestId)
    {
        $this->data = array('cgin' => $this->cgin, 'guestId' => $guestId);

        if ($decryptedResponse = $this->callTitan('listGuestBookings')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }

    public function findGuestViaEmail($email)
    {
        $this->data = array('cgin' => $this->cgin, 'emailAddress' => $email);

        if ($decryptedResponse = $this->callTitan('findGuestViaEmail')) {
            return json_decode($decryptedResponse, TRUE);
        }

        return false;
    }
}
