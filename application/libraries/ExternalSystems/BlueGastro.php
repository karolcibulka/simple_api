<?php


class BlueGastro
{

    public $ci;
    public $server;
    public $port;
    public $cash_desk_id;
    public $system_id;
    public $property_id;

    public function __construct($data)
    {
        $this->ci = & get_instance();
        $this->server = $data['server'];
        $this->port = $data['port'];
        $this->cash_desk_id = $data['cash_desk_id'];
        $this->system_id = $data['system_id'];
        $this->property_id = $data['id'];

        $this->ci->load->library('curl');
    }

    public function checkConnection(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/test/test');
        $response = $this->ci->curl->execute();

        if($response == 'tset'){
            return true;
        }

        return false;

    }

    public function getPlu(){

        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getPluList/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);
        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getOrderTypes(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getOrderTypeList/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);

        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getStatuses(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getOrderStatusList/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);

        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getReservationStatus($order_id){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getStatusOfOrder/'.$this->system_id.'/'.$order_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);

        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }



    public function getPaymentTypes(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getPaymentTypeList/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);
        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getDiscountTypes(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getDiscountList/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);
        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getListTable(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getListTable/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);
        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function getListAccommodatedGuest(){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getListAccommodatedGuest/'.$this->system_id);
        $response = json_decode($this->ci->curl->execute(),TRUE);

        //pre_r($response);exit;
        if(isset($response['code']) && !empty($response['code'])){
            return false;
        }
        else{
            return $response;
        }
    }

    public function storeReservation($requestData,$version = '1'){
        $end_method = 'newOrder';
        switch($version){
            case '1':
                $end_method = 'newOrder';
                break;
            case '2':
                $end_method = 'newOrder_2';
                break;
        }
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/'.$end_method.'/'.$this->system_id);
        $this->ci->curl->http_header('Content-Type','application/json');
        $this->ci->curl->post(json_encode($requestData));
        $response = json_decode($this->ci->curl->execute(),TRUE);

        return $response;
    }

    public function getStatusesOfAllOrdersByTime($requestData){
        $this->ci->curl->create('http://'.$this->server.':'.$this->port.'/BlueGastroWS/rest/ExternalOrders/getStatusesOfAllOrdersByTime/'.$this->system_id);
        $this->ci->curl->http_header('Content-Type','application/json');
        $this->ci->curl->post(json_encode($requestData));
        return json_decode($this->ci->curl->execute(),TRUE);
    }
}