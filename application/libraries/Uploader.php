<?php

Class Uploader {

    protected $ci;
    protected $upload_dir;
    protected $property_id;
    protected $sizes;
    protected $favicon_sizes;

    public function __construct($property_id = null)
    {
        $this->ci = & get_instance();
        $this->property_id = $property_id;

        $this->sizes = imageSizes();
        $this->favicon_sizes = faviconSizes();
    }

    public function resizeImage($upload_dir,$name,$extension,$sizes){

        $this->ci->load->library('resize');
        if(!empty($sizes)){
            foreach($sizes as $prefix => $size){
                $newName = $name.'-'.$prefix.'.'.$extension;
                $resizeObjThumb = new resize($upload_dir.$name.'.'.$extension);
                $resizeObjThumb->resizeImage($size, 0, 'landscape');
                $resizeObjThumb->saveImage($upload_dir . '' . $newName, 85);
            }
        }
    }

    public function removeImages($type,$name,$favicon = false,$logo = false){
          $this->upload_dir = getUploadPaths($type);

          if($favicon){
              foreach($this->favicon_sizes as $prefix => $size){
                  $name = str_replace('-'.$prefix,'',$name);
              }

              foreach($this->favicon_sizes as $prefix => $size){
                  if(file_exists($this->upload_dir . implode('-'.$prefix.'.',explode('.',$name)))){
                      unlink($this->upload_dir . implode('-'.$prefix.'.',explode('.',$name)));
                  }
              }
          }
          elseif ($logo){
              if(file_exists($this->upload_dir . $name)){
                  unlink($this->upload_dir . $name);
              }
          }
          else{
              foreach($this->sizes as $prefix => $size){
                  $name = str_replace('-'.$prefix,'',$name);
              }

              foreach($this->sizes as $prefix => $size){
                  if(file_exists($this->upload_dir . implode('-'.$prefix.'.',explode('.',$name)))){
                      unlink($this->upload_dir . implode('-'.$prefix.'.',explode('.',$name)));
                  }
              }
          }
    }

    public function uploadImages($type,$file_name){
        $this->upload_dir = getUploadPaths($type);

        $response = array();
        if (isset($_FILES[$file_name]['name'][0]) && !empty($_FILES[$file_name]['name'][0])) {
            foreach($_FILES[$file_name]['name'] as $key => $name){

                $temp_name  = $_FILES[$file_name]['name'][$key];
                $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
                $image_name = generateToken(30);


                if (!file_exists($this->upload_dir)) {
                    mkdir($this->upload_dir, 0775, true);
                }

                $image = $image_name . '.' . $extension;

                copy($_FILES[$file_name]['tmp_name'][$key], $this->upload_dir . $image);


                $this->resizeImage($this->upload_dir,$image_name,$extension,$this->sizes);


                if(file_exists($this->upload_dir . $image)){
                    unlink($this->upload_dir . $image);
                }

                $response[] = $image;

            }
        }

        return $response;

    }

    public function getImages($type,$images){
        $response = array();

        if(!empty($images)){
            foreach($images as $key => $image){
                $response[] = array(
                    'id' => $image,
                    'src' => getUploadPath($type,$image,true)
                    );
            }
        }

        return $response;
    }

    public function uploadImage($type,$file_name,$favicon = false,$logo = false){

        $this->upload_dir = getUploadPaths($type);

        if($logo){
            if (isset($_FILES[$file_name]['name']) && !empty($_FILES[$file_name]['name'])) {
                $temp_name  = $_FILES[$file_name]['name'];
                $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
                $image_name = generateToken(30);

                if (!file_exists($this->upload_dir)) {
                    mkdir($this->upload_dir, 0775, true);
                }

                $image = $image_name . '.' . $extension;

                copy($_FILES[$file_name]['tmp_name'], $this->upload_dir . $image);

                return $image;
            }

            return null;
        }

        if (isset($_FILES[$file_name]['name']) && !empty($_FILES[$file_name]['name'])) {
            $temp_name  = $_FILES[$file_name]['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $image_name = generateToken(30);


            if (!file_exists($this->upload_dir)) {
                mkdir($this->upload_dir, 0775, true);
            }

            $image = $image_name . '.' . $extension;

            copy($_FILES[$file_name]['tmp_name'], $this->upload_dir . $image);

            if($favicon){
                if($extension === 'svg'){
                    foreach($this->favicon_sizes as $prefix => $size){
                        $newName = $image_name.'-'.$prefix.'.'.$extension;
                        copy($_FILES[$file_name]['tmp_name'], $this->upload_dir . $newName);
                    }
                }
                else{
                    $this->resizeImage($this->upload_dir,$image_name,$extension,$this->favicon_sizes);
                }
            }
            else{
                $this->resizeImage($this->upload_dir,$image_name,$extension,$this->sizes);
            }

            if(file_exists($this->upload_dir . $image)){
                unlink($this->upload_dir . $image);
            }

            return $image;
        }

        return null;
    }
}
