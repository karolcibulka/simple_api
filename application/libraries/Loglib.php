<?php


class Loglib
{
    public $ci;
    public $controller;
    public $data;
    public $user_id;
    public $property_id;
    public $exceptions;

    public function __construct(){
        $this->ci = & get_instance();
        $this->ci->load->model('Log_model','log_model');
        $this->exceptions = array('index','loadData','getStats','getQRCode');
    }

    public function storeLog($controller,$method,$item_id=null){
        //return false;
    }

    private function setExceptions($exceptions){
        if(isset($exceptions) && !empty($exceptions)){
            if(is_array($exceptions)){
                $this->exceptions = array_merge($this->exceptions,$exceptions);
            }
            else{
                $this->exceptions[] = $exceptions;
            }
        }
    }

    public function storeLogApi($exceptions = array(),$type = null){
        $this->setExceptions($exceptions);

        $controller = $this->ci->uri->segment(2);
        $method = $this->ci->uri->segment(3);
        $property_id = $this->ci->uri->segment(4);
        $item = $this->ci->uri->segment(5);

        if($type){
            $item = $this->ci->uri->segment(6);
        }


        if(isset($method) && !empty($method) && !$this->isInExceptions($method)) {
            $request = array();

            if (isset($_POST) && !empty($_POST)) {
                $request = $_POST;
            }
            if (isset($_GET) && !empty($_GET)) {
                $request = $_GET;
            }

            if($type && !empty($request)){
                $request = $this->hidePasswords($request);
            }

            $insertLogData = array(
                'property_id' => $property_id,
                'controller' => $controller,
                'method' => $method,
                'item_id' => isset($item) && !empty($item) ? $item : null,
                'request' => !empty($request) ? json_encode($request) : null,
                'created_at' => date('Y-m-d H:i:s')
            );

            $this->ci->log_model->storeApiLog($insertLogData);

        }
    }

    private function hidePasswords($request){
        if(isset($request['password']) && !empty($request['password'])){
            $request['password'] = '******';
        }
        if(isset($request['password_confirmation']) && !empty($request['password_confirmation'])){
            $request['password_confirmation'] = '******';
        }
        if(isset($request['old_password']) && !empty($request['old_password'])){
            $request['old_password'] = '******';
        }

        return $request;
    }

    public function storeLogMyController($exceptions = array()){

        $this->setExceptions($exceptions);

        $controller = $this->ci->uri->segment(2);
        $method = $this->ci->uri->segment(3);
        $item_id = $this->ci->uri->segment(4);

        if(isset($method) && !empty($method) && !$this->isInExceptions($method)){
            $request = array();

            if(isset($_POST) && !empty($_POST)){
                $request = $_POST;
            }
            if(isset($_GET) && !empty($_GET)){
                $request = $_GET;
            }


            $insertLogData = array(
                'property_id' => $this->ci->session->userdata('active_property'),
                'user_id' => $this->ci->session->userdata('user_id'),
                'controller' => $controller,
                'method' => $method,
                'item_id' => isset($item_id) && !empty($item_id) ? $item_id : null,
                'request' => !empty($request) ? json_encode($request) : null,
                'created_at' => date('Y-m-d H:i:s')
            );

            $this->ci->log_model->storeLog($insertLogData);

        }
    }

    private function isInExceptions($method){

        if(in_array($method,$this->exceptions)){
            return true;
        }

        return false;
    }
}