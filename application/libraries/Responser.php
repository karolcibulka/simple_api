<?php


class Responser
{
    protected $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public static function wallet($wallet){
        $response = array();

        if(!empty($wallet)){
            $response['balance'] = $wallet['actualBalance'];
            $response['id'] = $wallet['id'];
        }

        return $response;
    }

    public static function walletActivities($activities){
        $response = array();

        if(!empty($activities)){
            foreach($activities as $activity){
                $response['actual_balance'] = $activity['ewallet']['actualBalance'];
                $response['usable_balance'] = $activity['ewallet']['usableBalance'];
                $response['activities'][$activity['id']]['id'] = $activity['id'];
                $response['activities'][$activity['id']]['value'] = $activity['activityValue'];
                $response['activities'][$activity['id']]['created_at'] = date('d.m.Y H:i:s',($activity['created']/1000));
                $response['activities'][$activity['id']]['type'] = $activity['activityType']['code'];
                $response['activities'][$activity['id']]['note'] = $activity['activityType']['name'];
                $response['activities'][$activity['id']]['old_value'] = $activity['originalValue'];
            }

            usort($response['activities'],function($a,$b){
                return $a['id'] < $b['id'];
            });

            $response['activities'] = array_values($response['activities']);
        }

        return $response;
    }

    public static function bookings($bookings){
        $response = array();

        if(isset($bookings['bookings']) && !empty($bookings['bookings'])){
            foreach($bookings['bookings'] as $booking_key => $booking){
                if(isset($booking['reservationNumber']) && !empty($booking['reservationNumber'])){
                    $dayDiff = date_diff(new DateTime(date('d.m.Y', $booking['dateTo'] / 1000)), new DateTime(date('d.m.Y', $booking['dateFrom'] / 1000)));
                    $response[$booking['reservationNumber']][$booking['id']]['id']                     = $booking['id'];
                    $response[$booking['reservationNumber']][$booking['id']]['reservation_number']     = $booking['reservationNumber'];
                    $response[$booking['reservationNumber']][$booking['id']]['order_id']               = $booking['orderId'];
                    $response[$booking['reservationNumber']][$booking['id']]['id']                     = $booking['id'];
                    $response[$booking['reservationNumber']][$booking['id']]['branch_name']            = $booking['branchName'];
                    $response[$booking['reservationNumber']][$booking['id']]['accommodation_name']     = $booking['accommodationName'];
                    $response[$booking['reservationNumber']][$booking['id']]['guest_count']            = $booking['guestCount'];
                    $response[$booking['reservationNumber']][$booking['id']]['nights']                 = $dayDiff->days;
                    $response[$booking['reservationNumber']][$booking['id']]['date_from']              = date('Y-m-d H:i:s', $booking['dateFrom'] / 1000);
                    $response[$booking['reservationNumber']][$booking['id']]['date_to']                = date('Y-m-d H:i:s', $booking['dateTo'] / 1000);
                    $response[$booking['reservationNumber']][$booking['id']]['created_at']             = !empty($booking['booking']['createdDate']) ? date('Y-m-d H:i:s', $booking['booking']['createdDate'] / 1000) : '';
                    $response[$booking['reservationNumber']][$booking['id']]['reserved_until']         = !empty($booking['booking']['reservedUntil']) ? date('Y-m-d H:i:s', $booking['booking']['reservedUntil'] / 1000) : '';
                    $response[$booking['reservationNumber']][$booking['id']]['price']['type']          = $booking['priceType'];
                    $response[$booking['reservationNumber']][$booking['id']]['price']['accommodation'] = $booking['price'];
                    $response[$booking['reservationNumber']][$booking['id']]['price']['total']         = $booking['price'];
                    $response[$booking['reservationNumber']][$booking['id']]['booking']['type']        = $booking['bookedType'];
                    $response[$booking['reservationNumber']][$booking['id']]['booking']['id']          = date('Y-m-d H:i:s', $booking['createdDate']);
                    $response[$booking['reservationNumber']][$booking['id']]['booking']['number']      = $booking['number'];
                    $response[$booking['reservationNumber']][$booking['id']]['guest']['name']          = $booking['ownerName'];
                    $response[$booking['reservationNumber']][$booking['id']]['guest']['surname']       = $booking['ownerSurname'];
                    $response[$booking['reservationNumber']][$booking['id']]['state']                  = self::getState($booking['state']);
                    $response[$booking['reservationNumber']][$booking['id']]['guest']['email']         = $booking['ownerEmail'];
                }
            }
        }
        return $response;
    }

    public static function accommodations($accommodations){
        $response = array();
        if(isset($accommodations['guestAccommodations']) && !empty($accommodations['guestAccommodations'])){
            foreach($accommodations['guestAccommodations'] as $accommodation_key => $accommodation){
                $response[$accommodation['id']]['id']                     = $accommodation['id'];
                $response[$accommodation['id']]['branch_name']            = $accommodation['branchName'];
                $response[$accommodation['id']]['nights']                 = $accommodation['numOfNights'];
                $response[$accommodation['id']]['name']                   = $accommodation['name'];
                $response[$accommodation['id']]['date_from']              = date('Y-m-d H:i:s', $accommodation['dateFrom'] / 1000);
                $response[$accommodation['id']]['date_to']                = date('Y-m-d H:i:s', $accommodation['dateTo'] / 1000);
                $response[$accommodation['id']]['price']['accommodation'] = $accommodation['priceAccomm'];
                $response[$accommodation['id']]['price']['food']          = $accommodation['priceFood'];
                $response[$accommodation['id']]['price']['other']         = $accommodation['priceOther'];
                $response[$accommodation['id']]['price']['total']         = $accommodation['priceSum'];
                $response[$accommodation['id']]['done']                   = $accommodation['done'];
                $response[$accommodation['id']]['bill']                   = $accommodation['billsInfo'];
                $response[$accommodation['id']]['bills_price']            = 0;
                if (isset($accommodation['billsInfo']) && !empty($accommodation['billsInfo'])) {
                    foreach ($accommodation['billsInfo'] as $bill) {
                        $response[$accommodation['id']]['bills_price'] += $bill['priceSumBill'];
                    }
                }
            }
        }
        return $response;
    }


    public static function marketingCategories($categories){
        //TODO::prerobit na vhodny response
        return $categories;
    }
    public static function bill($bill){
        //TODO::prerobit na vhodny response
        return $bill;
    }

    public static function bills($bills){
        //TODO::prerobit na vhodny response
        return $bills;
    }

    private static function getState($s){
        switch ($s) {
            case 'R':
                return 'Aktívna';
                break;
            case 'T':
                return 'Aktívna, na typ izby';
                break;
            case 'K':
                return 'Aktívna, kontrakt';
                break;
            case 'Z':
                return 'Zrušená';
                break;
            case 'V':
                return 'Vyradená';
                break;
            case 'N':
                return 'Nastúpená';
                break;
        }
    }
}