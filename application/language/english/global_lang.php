<?php

$lang['message.warning'] = '<b> You do not have permission to access this page! </b> <b> Contact </b> an administrator to resolve this issue!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Submit';
$lang['menu.icon'] = 'Icon';
$lang['menu.parent'] = 'Parent';
$lang['menu.type'] = 'Type';
$lang['menu.active'] = 'Active';
$lang['menu.nonactive'] = 'Inactive';
$lang['menu.activeHeader'] = 'Activity';
$lang['menu.path'] = 'Path';
$lang['menu.pathPlaceholder'] = 'Choose path: controller / method';
$lang['menu.pathLinkPlaceholder'] = 'Choose path to website';
$lang['menu.chooseType'] = 'Select Type';
$lang['menu.navigationName'] = 'Name of the element in the navigation';
$lang['menu.addMenuItem'] = 'Add menu item';
$lang['menu.iAmParent'] = 'I am a parent!';
$lang['menu.logout'] = 'Logout';
$lang['menu.chooseType'] = 'Choose type';
$lang['menu.controllerType'] = 'Type';
$lang['menu.controllerDescription'] = 'Controller Name';
$lang['menu.addController'] = 'Add Controller';
$lang['menu.editController'] = 'Edit Controller';
$lang['menu.controllerName'] = 'Controller';
$lang['menu.addControllerToGroups'] = 'Add controller to all groups';
$lang['menu.delete'] = 'Delete';
$lang['menu.edit'] = 'Edit';
$lang['menu.switchOn'] = 'Enable';
$lang['menu.switchOff'] = 'Disable';
$lang['menu.showing'] = 'Display';
$lang['menu.creating'] = 'Creating';
$lang['menu.editing'] = 'Editing';
$lang['menu.deleting'] = 'Delete';
$lang['menu.backToAdd'] = 'Back to add';


$lang['day.monday'] = 'Monday';
$lang['day.tuesday'] = 'Tuesday';
$lang['day.wednesday'] = 'Wednesday';
$lang['day.thursday'] = 'Thursday';
$lang['day.friday'] = 'Friday';
$lang['day.saturday'] = 'Saturday';
$lang['day.sunday'] = 'Sunday';
$lang['day.workingDay'] = 'Work Day';
$lang['day.workingDays'] = 'Working Days';
$lang['day.weekend'] = 'Weekend';
$lang['day.eachDay'] = 'Every day';

$lang['segment.dashboard'] = 'Dashboard';

$lang['segment.propertySettings'] = 'Device Settings';
$lang['segment.propertySettings.propertySettings'] = 'device settings';

$lang['segment.properties'] = 'Devices';
$lang['segment.properties.properties'] = 'device settings';

$lang['segment.services'] = 'Services';
$lang['segment.services.services'] = 'create services for your customers';

$lang['segment.categories'] = 'Categories';
$lang['segment.categories.edit'] = 'Edit Categories';
$lang['segment.categories.create'] = 'Create Categories';
$lang['segment.categories.categories'] = 'category management';

$lang['segment.products'] = 'Products';
$lang['segment.products.edit'] = 'Product Modification';
$lang['segment.products.create'] = 'Product Creation';
$lang['segment.products.products'] = 'Product Management';

$lang['segment.variations'] = 'Variations';
$lang['segment.variations.edit'] = 'Edit Variations';
$lang['segment.variations.create'] = 'Create Variation';
$lang['segment.variations.variations'] = 'Variations Management';

$lang['segment.facilities'] = 'Properties';
$lang['segment.facilities.edit'] = 'Edit Properties';
$lang['segment.facilities.create'] = 'Create Properties';
$lang['segment.facilities.facilities'] = 'Property Management';

$lang['segment.dashboard'] = 'Dashboard';
$lang['segment.dashboard.edit'] = 'Edit Dashboard';
$lang['segment.dashboard.create'] = 'Dashboard Creation';
$lang['segment.dashboard.dashboard'] = 'dashboard management';

$lang['segment.variabilities'] = 'Variability';
$lang['segment.variabilities.edit'] = 'Adjust variability';
$lang['segment.variabilities.create'] = 'Create Variability';
$lang['segment.variabilities.variabilities'] = 'Variability Management';

$lang['segment.availability'] = 'Availability';
$lang['segment.availability.edit'] = 'Adjust Availability';
$lang['segment.availability.create'] = 'Create Availability';
$lang['segment.availability.availability'] = 'availability management';

$lang['segment.navigation'] = 'Navigation';
$lang['segment.developerAuth'] = 'Users';
$lang['segment.permission'] = 'Powers';
$lang['segment.program'] = 'Programs';
$lang['segment.apiLogs'] = 'API Logs';
$lang['segment.apiKeys'] = 'API Keys';
$lang['segment.apiPermissions'] = 'API Privileges';

$lang['segment.stock'] = 'Stock';
$lang['segment.offer'] = 'Offer';
$lang['segment.settings'] = 'Settings';
$lang['segment.developer_zone'] = 'Developer Zone';


$lang['segment.import'] = 'Imports';

$lang['app.internal_name'] = 'Internal Name';
$lang['app.change_password'] = 'Change Password';
$lang['app.no_property'] = 'UNREADED DEVICE';
$lang['app.edit_user'] = 'Edit User';
$lang['app.property'] = 'Device';
$lang['app.role'] = 'Role';
$lang['app.surname'] = 'Last Name';
$lang['app.name'] = 'Name';
$lang['app.email'] = 'Email';
$lang['app.create_user_property'] = 'Create user for device';
$lang['app.password'] = 'Password';
$lang['app.save_new_user'] = 'Save User';
$lang['app.password_confirmation'] = 'Password Confirmation';
$lang['app.action'] = 'Action';
$lang['app.no_permission_edit_user'] = 'You do not have permission to change user';
$lang['app.edit'] = 'Edit';
$lang['app.users'] = 'Users';
$lang['app.name_in_language'] = 'Language Name';
$lang['app.description_in_language'] = 'Language Description';
$lang['app.perex_in_language'] = 'Perex in Language';
$lang['app.users'] = 'Users';
$lang['app.facilities'] = 'Properties';
$lang['app.no_facilities'] = 'No Features';
$lang['app.parent'] = 'Parent';
$lang['app.no_parent'] = 'No Parent';
$lang['app.delete'] = 'Delete';
$lang['app.new'] = 'New Feature';
$lang['app.value_in_language'] = 'Language Value';
$lang['app.save'] = 'Save';
$lang['app.create_new'] = 'Create New Feature';
$lang['app.active'] = 'Active';
$lang['app.no_active'] = 'Inactive';
$lang['app.step'] = 'Step';
$lang['app.minimal_value'] = 'Minimum Value';
$lang['app.maximal_value'] = 'Maximum Value';
$lang['app.count'] = 'Quantity';
$lang['app.no_count'] = 'Do not show quantity';
$lang['app.weight'] = 'Weight';
$lang['app.pieces'] = 'Count';
$lang['app.category'] = 'Category';
$lang['app.name_p'] = 'Name';
$lang['app.value'] = 'Value';
$lang['app.variation'] = 'Variation';
$lang['app.translation'] = 'Translations';
$lang['app.gallery'] = 'Gallery';
$lang['app.variability'] = 'Variability';
$lang['app.no_variability'] = 'No variability';
$lang['app.unit_price'] = 'Single Price';
$lang['app.price_profile'] = 'Pricing';
$lang['app.discounts'] = 'Appropriate Discounts';
$lang['app.no_facilities_defined'] = 'You have no properties defined';
$lang['app.product'] = 'Product';
$lang['app.use_variability'] = 'Use Variability';
$lang['app.images'] = 'Images';
$lang['app.no'] = 'No';
$lang['app.yes'] = 'Yes';

$lang['app.phone'] = 'Phone Number';
$lang['app.primary_color'] = 'Main Color';
$lang['app.secundary_color'] = 'Secondary Color';
$lang['app.default_language'] = 'Default Language';
$lang['app.available_languages'] = 'Available Languages';
$lang['app.orders'] = 'Basic Layout';
$lang['app.selling'] = 'Sales Allowed';
$lang['app.price_definition'] = 'Price Definition';
$lang['app.price_definition_delimiter'] = 'Price Separator from Cents';
$lang['app.allowed_currencies'] = 'Allowed Currencies';
$lang['app.currency'] = 'Default Currency';

$lang['app.price_up'] = 'Lowest Price';
$lang['app.price_down'] = 'Price from highest';
$lang['app.letter_up'] = 'A to Z';
$lang['app.letter_down'] = 'Z to A';
$lang['app.rating_up'] = 'From least favorite';
$lang['app.rating_down'] = 'From Most Popular';

$lang['app.no_decimals'] = 'No decimal place';
$lang['app.one_decimals'] = '1 decimal place';
$lang['app.two_decimals'] = '2 decimal places';

$lang['app.comma'] = 'Comma';
$lang['app.dot'] = 'Dot';















$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';