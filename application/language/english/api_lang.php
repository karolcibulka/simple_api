<?php

$lang['api.errors.password_match'] = 'Passwords entered do not match';
$lang['api.errors.password_length'] = 'Minimum password length is% s characters';


// required fields
$lang['api.errors.first_name'] = 'Name is a required parameter';
$lang['api.errors.last_name'] = 'Last Name is a required parameter';
$lang['api.errors.zip'] = 'Postcode is a required parameter';
$lang['api.errors.city'] = 'City is a required parameter';
$lang['api.errors.street_number'] = 'State is a required parameter';
$lang['api.errors.password'] = 'Password is a required parameter';
$lang['api.errors.password_confirmation'] = 'Password confirmation is a required parameter';
$lang['api.errors.street'] = 'Street is a required parameter';
$lang['api.errors.email'] = 'Email is a required parameter';
$lang['api.errors.phone'] = 'Phone is a required parameter';
$lang['api.errors.old_password'] = 'Old password is a required parameter';

$lang['api.errors.email_is_in_use'] = 'The email you entered is already in use';
$lang['api.errors.property'] = 'Device with ID% s does not exist';
$lang['api.errors.user_doesnt_exist'] = 'Requester with email% s does not exist';
$lang['api.errors.old_password_doesnt_match'] = 'Old password is incorrect!';
$lang['api.errors.user_doesnt_exist_token'] = 'Requester with token% s does not exist';
$lang['api.errors.wrong_password'] = 'The password you entered is incorrect';
$lang['api.errors.empty_property_group'] = 'The specified device group contains no devices';


$lang['api.errors.no_reservations'] = 'Requester has no reservations';