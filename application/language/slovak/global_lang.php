<?php

$lang['message.warning'] = '<b>Pre prístup ku tejto stránke nemáte právomoc!</b> Pre vyriešenie tohto problému <b>kontaktujte</b> administrátora!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Odoslať';
$lang['menu.icon'] = 'Ikona';
$lang['menu.parent'] = 'Rodič';
$lang['menu.type'] = 'Typ';
$lang['menu.active'] = 'Aktívne';
$lang['menu.nonactive'] = 'Neaktívne';
$lang['menu.activeHeader'] = 'Aktivita';
$lang['menu.path'] = 'Cesta';
$lang['menu.pathPlaceholder'] = 'Zvoľ cestu: controller/metóda';
$lang['menu.pathLinkPlaceholder'] = 'Zvoľ cestu ku webovej stránke';
$lang['menu.chooseType'] = 'Vyber typ';
$lang['menu.navigationName'] = 'Názov prvku v navigácii';
$lang['menu.addMenuItem'] = 'Pridaj prvok do menu';
$lang['menu.iAmParent'] = 'Ja som rodič!';
$lang['menu.logout'] = 'Odhlásiť';
$lang['menu.chooseType'] = 'Zvoľ typ';
$lang['menu.controllerType'] = 'Typ';
$lang['menu.controllerDescription'] = 'Názov controlleru';
$lang['menu.addController'] = 'Pridanie controlleru';
$lang['menu.editController'] = 'Úprava controlleru';
$lang['menu.controllerName'] = 'Controller';
$lang['menu.addControllerToGroups'] = 'Pridať controller do všetkých skupín';
$lang['menu.delete'] = 'Zmazať';
$lang['menu.edit'] = 'Upraviť';
$lang['menu.switchOn'] = 'Zapnúť';
$lang['menu.switchOff'] = 'Vypnúť';
$lang['menu.showing'] = 'Zobrazovanie';
$lang['menu.creating'] = 'Vytváranie';
$lang['menu.editing'] = 'Upravovanie';
$lang['menu.deleting'] = 'Mazanie';
$lang['menu.backToAdd'] = ' Späť na pridanie';


$lang['day.monday'] = 'Pondelok';
$lang['day.tuesday'] = 'Utorok';
$lang['day.wednesday'] = 'Streda';
$lang['day.thursday'] = 'Štvrtok';
$lang['day.friday'] = 'Piatok';
$lang['day.saturday'] = 'Sobota';
$lang['day.sunday'] = 'Nedeľa';
$lang['day.workingDay'] = 'Pracovný deň';
$lang['day.workingDays'] = 'Pracovné dni';
$lang['day.weekend'] = 'Víkend';
$lang['day.eachDay'] = 'Každý deň';

$lang['segment.dashboard'] = 'Dashboard';

$lang['segment.propertySettings'] = 'Nastavenie zariadenia';
$lang['segment.propertySettings.propertySettings'] = 'nastavnie zariadenia';

$lang['segment.properties'] = 'Zariadenia';
$lang['segment.properties.properties'] = 'nastavenia zariadení';

$lang['segment.services'] = 'Služby';
$lang['segment.services.services'] = 'vytvorte služby pre vaších zákazníkov';

$lang['segment.categories'] = 'Kategórie';
$lang['segment.categories.edit'] = 'Úprava kategórie';
$lang['segment.categories.create'] = 'Vytvorenie kategórie';
$lang['segment.categories.categories'] = 'správa kategórií';

$lang['segment.products'] = 'Produkty';
$lang['segment.products.edit'] = 'Úprava produktu';
$lang['segment.products.create'] = 'Vytvorenie produktu';
$lang['segment.products.products'] = 'správa produktov';

$lang['segment.variations'] = 'Variácie';
$lang['segment.variations.edit'] = 'Úprava variácie';
$lang['segment.variations.create'] = 'Vytvorenie variácie';
$lang['segment.variations.variations'] = 'správa variácií';

$lang['segment.facilities'] = 'Vlastnosti';
$lang['segment.facilities.edit'] = 'Úprava vlastností';
$lang['segment.facilities.create'] = 'Vytvorenie vlastností';
$lang['segment.facilities.facilities'] = 'správa vlastností';

$lang['segment.dashboard'] = 'Dashboard';
$lang['segment.dashboard.edit'] = 'Úprava dashboardu';
$lang['segment.dashboard.create'] = 'Vytvorenie dashboardu';
$lang['segment.dashboard.dashboard'] = 'správa dashboardu';

$lang['segment.variabilities'] = 'Variabilita';
$lang['segment.variabilities.edit'] = 'Úprava variability';
$lang['segment.variabilities.create'] = 'Vytvorenie variability';
$lang['segment.variabilities.variabilities'] = 'správa variabilít';

$lang['segment.availability'] = 'Dostupnosť';
$lang['segment.availability.edit'] = 'Úprava dostupnosti';
$lang['segment.availability.create'] = 'Vytvorenie dostupnosti';
$lang['segment.availability.availability'] = 'správa dostupnosti';

$lang['segment.navigation'] = 'Navigácia';
$lang['segment.developerAuth'] = 'Užívatelia';
$lang['segment.permission'] = 'Právomoci';
$lang['segment.program'] = 'Programy';
$lang['segment.apiLogs'] = 'API Logy';
$lang['segment.apiKeys'] = 'API kľúče';
$lang['segment.apiPermissions'] = 'API právomoci';

$lang['segment.stock'] = 'Sklad';
$lang['segment.offer'] = 'Ponuka';
$lang['segment.settings'] = 'Nastavenia';
$lang['segment.developer_zone'] = 'Developerská zóna';

$lang['segment.import'] = 'Importy';

$lang['app.internal_name'] = 'Interný názov';
$lang['app.change_password'] = 'Zmeniť heslo';
$lang['app.no_property'] = 'NEPRIRADENÉ ZARIADENIE';
$lang['app.edit_user'] = 'Upraviť užívateľa';
$lang['app.property'] = 'Zariadenie';
$lang['app.role'] = 'Rola';
$lang['app.surname'] = 'Priezvisko';
$lang['app.name'] = 'Meno';
$lang['app.email'] = 'Email';
$lang['app.create_user_property'] = 'Vytvorenie užívateľa pre zariadenie';
$lang['app.password'] = 'Heslo';
$lang['app.save_new_user'] = 'Uložiť užívateľa';
$lang['app.password_confirmation'] = 'Potvrdenie hesla';
$lang['app.action'] = 'Akcia';
$lang['app.no_permission_edit_user'] = 'Nemáš právo meniť užívateľa';
$lang['app.edit'] = 'Upraviť';
$lang['app.users'] = 'Užívatelia';
$lang['app.name_in_language'] = 'Názov v jazyku';
$lang['app.description_in_language'] = 'Popis v jazyku';
$lang['app.perex_in_language'] = 'Perex v jazyku';
$lang['app.users'] = 'Užívatelia';
$lang['app.facilities'] = 'Vlastnosti';
$lang['app.no_facilities'] = 'Žiadne vlastnosti';
$lang['app.parent'] = 'Rodič';
$lang['app.no_parent'] = 'Žiadny rodič';
$lang['app.delete'] = 'Zmazať';
$lang['app.new'] = 'Nový prvok';
$lang['app.value_in_language'] = 'Hodnota v jazyku';
$lang['app.save'] = 'Uložiť';
$lang['app.create_new'] = 'Vytvoriť nový prvok';
$lang['app.active'] = 'Aktívne';
$lang['app.no_active'] = 'Neaktívne';
$lang['app.step'] = 'Krok';
$lang['app.minimal_value'] = 'Minimálna hodnota';
$lang['app.maximal_value'] = 'Maximálna hodnota';
$lang['app.count'] = 'Množstvo';
$lang['app.no_count'] = 'Nezobrazovať množstvo';
$lang['app.weight'] = 'Váha';
$lang['app.pieces'] = 'Počet';
$lang['app.category'] = 'Kategória';
$lang['app.name_p'] = 'Názov';
$lang['app.value'] = 'Hodnota';
$lang['app.variation'] = 'Variácia';
$lang['app.translation'] = 'Preklady';
$lang['app.gallery'] = 'Galéria';
$lang['app.variability'] = 'Variabilita';
$lang['app.no_variability'] = 'Bez variability';
$lang['app.unit_price'] = 'Jendotková cena';
$lang['app.price_profile'] = 'Cenotvorba';
$lang['app.discounts'] = 'Uplatniteľné zľavy';
$lang['app.no_facilities_defined'] = 'Nemáte zadefinované žiadne vlastnosti';
$lang['app.product'] = 'Produkt';
$lang['app.use_variability'] = 'Používať variabilitu';
$lang['app.images'] = 'Obrázky';
$lang['app.no'] = 'Nie';
$lang['app.yes'] = 'Áno';

$lang['app.phone'] = 'Telefónne číslo';
$lang['app.primary_color'] = 'Hlavná farba';
$lang['app.secundary_color'] = 'Sekundárna farba';
$lang['app.default_language'] = 'Prednastavený jazyk';
$lang['app.available_languages'] = 'Dostupné jazyky';
$lang['app.orders'] = 'Základné zoradzovanie';
$lang['app.selling'] = 'Predaj povolený';
$lang['app.price_definition'] = 'Definícia ceny';
$lang['app.price_definition_delimiter'] = 'Oddelovač ceny od centov';
$lang['app.allowed_currencies'] = 'Povolené meny';
$lang['app.currency'] = 'Prednastavená mena';

$lang['app.price_up'] = 'Cena od najnižšej';
$lang['app.price_down'] = 'Cena od najvyššej';
$lang['app.letter_up'] = 'od A do Z';
$lang['app.letter_down'] = 'od Z do A';
$lang['app.rating_up'] = 'Od najmenej obľúbeného';
$lang['app.rating_down'] = 'Od Najviac oblúbeného';

$lang['app.no_decimals'] = 'Bez desatinného miesta';
$lang['app.one_decimals'] = '1 desatinné miesto';
$lang['app.two_decimals'] = '2 desatinné miesta';

$lang['app.comma'] = 'Čiarka';
$lang['app.dot'] = 'Bodka';



$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';

//tu nove
$lang['app.login'] = 'Prihlásiť sa';
$lang['app.register'] = 'Registrovať';
$lang['app.token_explain'] = 'Tento parameter nie je povinný v prípade ak sa nechcete pripojiť už ku existujúcemu zariadeniu';
$lang['app.property_token'] = 'Token zariadenia';

$lang['segment.your_zone'] = 'Vaša zóna';
$lang['segment.public_api'] = 'Verejná API';

$lang['segment.donation'] = 'Podpora';
$lang['segment.donation.donation'] = 'správa podpory';
$lang['segment.donation.create'] = 'vytvoriť podporu';
$lang['segment.donation.edit'] = 'úprava podpory';

$lang['segment.endpoints'] = 'Dokumentácia';
$lang['segment.endpoints.endpoints'] = 'správa dokumentácie';
$lang['segment.endpoints.create'] = 'Vytvorenie dokumentácie';
$lang['segment.endpoints.edit'] = 'Úprava dokumentácie';

$lang['segment.transportation'] = 'Doprava';
$lang['segment.transportation.transportation'] = 'správa dopravy';
$lang['segment.transportation.create'] = 'Vytvorenie dopravy';
$lang['segment.transportation.edit'] = 'Úprava dopravy';

$lang['segment.payments'] = 'Platobné metódy';
$lang['segment.payments.payments'] = 'správa platobných metód';
$lang['segment.payments.create'] = 'Vytvorenie platobnej metódy';
$lang['segment.payments.edit'] = 'Úprava platobnej metódy';

$lang['segment.prices'] = 'Cenotvorba';
$lang['segment.prices.prices'] = 'správa cenotvorby';
$lang['segment.prices.create'] = 'Vytvorenie cenotvorby';
$lang['segment.prices.edit'] = 'Úprava cenotvorby';


$lang['segment.packing'] = 'Balné';
$lang['segment.packing.packing'] = 'správa balného';
$lang['segment.packing.create'] = 'Vytvorenie balného';
$lang['segment.packing.edit'] = 'Úprava balného';

$lang['app.payment'] = 'Typ platby';
$lang['app.image'] = 'Obrázok';
$lang['app.show_from_price'] = 'Zobraziť od';
$lang['app.show_estimated_date'] = 'Zobraziť predpokladaný dátum';
$lang['app.show_estimated_time'] = 'Zobraziť predpokladaný čas';
$lang['app.is_delivery'] = 'Je táto doprava rozvoz?';
$lang['app.cities'] = 'Mesto/ Mestské časti';
$lang['app.min_days'] = 'Minimálny počet dní od aktúalneho (dnešného) dňa';
$lang['app.max_days'] = 'Maximálny počet dní od aktúalneho (dnešného) dňa';

