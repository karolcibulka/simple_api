<?php

//passwords
$lang['api.errors.password_match'] = 'Zadané heslá sa nezhodujú';
$lang['api.errors.password_length'] = 'Minimálna dĺžka hesla je %s znakov';


//required fields
$lang['api.errors.first_name'] = 'Meno je povinný parameter';
$lang['api.errors.last_name'] = 'Priezvisko je povinný parameter';
$lang['api.errors.zip'] = 'PSČ je povinný parameter';
$lang['api.errors.city'] = 'Mesto je povinný parameter';
$lang['api.errors.street_number'] = 'Číslo ulice je povinný parameter';
$lang['api.errors.password'] = 'Heslo je povinný parameter';
$lang['api.errors.password_confirmation'] = 'Potvdenie hesla je povinný parameter';
$lang['api.errors.street'] = 'Ulica je povinný parameter';
$lang['api.errors.email'] = 'Email je povinný parameter';
$lang['api.errors.phone'] = 'Telefón je povinný parameter';
$lang['api.errors.old_password'] = 'Staré heslo je povinný parameter';
$lang['api.errors.user_id'] = 'user_id je povinný parameter';
$lang['api.errors.user_token'] = 'user_token je povinný parameter';

$lang['api.errors.email_is_in_use'] = 'Zadaný email sa už používa';
$lang['api.errors.property'] = 'Zariadenie s ID %s neexistuje';
$lang['api.errors.user_doesnt_exist'] = 'Užívateľ s emailom %s neexistuje';
$lang['api.errors.user_exist'] = 'Užívateľ s emailom %s už existuje';
$lang['api.errors.old_password_doesnt_match'] = 'Staré heslo nie je správne!';
$lang['api.errors.user_doesnt_exist_token'] = 'Užívateľ s tokenom %s neexistuje';
$lang['api.errors.user_doesnt_exist_id'] = 'Užívateľ s id %s neexistuje';
$lang['api.errors.wrong_password'] = 'Zadané heslo nie je správne';
$lang['api.errors.empty_property_group'] = 'Zadaná skupina zariadení neobsahuje žiadne zariadenia';


$lang['api.errors.no_reservations'] = 'Užívateľ nemá žiadne rezervácie';
$lang['api.errors.property'] = 'Zariadenie neexistuje';
$lang['api.errors.country'] = 'Krajina je povinný parameter';
$lang['api.errors.wallet'] = 'Žiadna penaženka';
$lang['api.errors.activities'] = 'Žiadne aktivity v peňaženke';
$lang['api.errors.no_bookings'] = 'Žiadne rezervácie pre užívateľa';
$lang['api.errors.no_accommodations'] = 'Žiadne ubytovania pre užívateľa';
$lang['api.errors.bill'] = 'Žiadne blok pre rezerváciu';
$lang['api.errors.titan_id'] = 'Titan ID neexistuje';
$lang['api.errors.cannot_increase_points'] = 'Nie je možné pripočítavať body';
$lang['api.errors.cannot_draw_points'] = 'Nie je možné čerpať body';
$lang['api.errors.no_categories'] = 'Nemá žiadne kategórie';