<?php

$lang['api.errors.password_match'] = 'A megadott jelszavak nem egyeznek';
$lang['api.errors.password_length'] = 'A jelszó minimális hossza% s karakter';


// Szükséges Mezők
$lang['api.errors.first_name'] = 'A név kötelező paraméter';
$lang['api.errors.last_name'] = 'A vezetéknév kötelező paraméter';
$lang['api.errors.zip'] = 'Az irányítószám kötelező paraméter';
$lang['api.errors.city'] = 'A város kötelező paraméter';
$lang['api.errors.street_number'] = 'Az állapot kötelező paraméter';
$lang['api.errors.password'] = 'A jelszó kötelező paraméter';
$lang['api.errors.password_confirmation'] = 'A jelszó megerősítése kötelező paraméter';
$lang['api.errors.street'] = 'Az utca kötelező paraméter';
$lang['api.errors.email'] = 'Az e-mail kötelező paraméter';
$lang['api.errors.phone'] = 'A telefon kötelező paraméter';
$lang['api.errors.old_password'] = 'A régi jelszó kötelező paraméter';

$lang['api.errors.email_is_in_use'] = 'A megadott e-mail már használatban van';
$lang['api.errors.property'] = '% s azonosítójú eszköz nem létezik';
$lang['api.errors.user_doesnt_exist'] = '% s e-mail felhasználó nem létezik';
$lang['api.errors.old_password_doesnt_match'] = 'Helytelen a régi jelszó!';
$lang['api.errors.user_doesnt_exist_token'] = '% s tokennel rendelkező felhasználó nem létezik';
$lang['api.errors.wrong_password'] = 'Helytelen a megadott jelszó';
$lang['api.errors.empty_property_group'] = 'A megadott eszközcsoport nem tartalmaz eszközt';


$lang['api.errors.no_reservations'] = 'A felhasználónak nincsenek fenntartásai';