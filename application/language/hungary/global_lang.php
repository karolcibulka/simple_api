<?php
$lang['message.warning'] = '<b> Nincs engedélye az oldal elérésére! </b> <b> A probléma megoldásához lépjen kapcsolatba </b> egy rendszergazdával!';

$lang['menu.menu'] = 'Menü';
$lang['menu.submit'] = 'Küldés';
$lang['menu.icon'] = 'Ikon';
$lang['menu.parent'] = 'Szülő';
$lang['menu.type'] = 'Típus';
$lang['menu.active'] = 'Aktív';
$lang['menu.nonactive'] = 'Inaktív';
$lang['menu.activeHeader'] = 'Tevékenység';
$lang['menu.path'] = 'Útvonal';
$lang['menu.pathPlaceholder'] = 'Útvonal kiválasztása: vezérlő / módszer';
$lang['menu.pathLinkPlaceholder'] = 'Válassza ki a webhely elérési útját';
$lang['menu.chooseType'] = 'Típus kiválasztása';
$lang['menu.navigationName'] = 'A navigáció elemének neve';
$lang['menu.addMenuItem'] = 'Menü hozzáadása';
$lang['menu.iAmParent'] = 'Szülő vagyok!';
$lang['menu.logout'] = 'Kijelentkezés';
$lang['menu.chooseType'] = 'Válasszon típust';
$lang['menu.controllerType'] = 'Típus';
$lang['menu.controllerDescription'] = 'Vezérlő neve';
$lang['menu.addController'] = 'Vezérlő hozzáadása';
$lang['menu.editController'] = 'Vezérlő szerkesztése';
$lang['menu.controllerName'] = 'Vezérlő';
$lang['menu.addControllerToGroups'] = 'Vezérlő hozzáadása az összes csoporthoz';
$lang['menu.delete'] = 'Törlés';
$lang['menu.edit'] = 'Szerkesztés';
$lang['menu.switchOn'] = 'Engedélyezés';
$lang['menu.switchOff'] = 'Letiltás';
$lang['menu.showing'] = 'Megjelenítés';
$lang['menu.creating'] = 'Létrehozás';
$lang['menu.editing'] = 'Szerkesztés';
$lang['menu.deleting'] = 'Törlés';
$lang['menu.backToAdd'] = 'Vissza a hozzáadáshoz';


$lang['day.monday'] = 'hétfő';
$lang['day.tuesday'] = 'kedd';
$lang['day.wednesday'] = 'szerda';
$lang['day.thursday'] = 'csütörtök';
$lang['day.friday'] = 'péntek';
$lang['day.saturday'] = 'szombat';
$lang['day.sunday'] = 'vasárnap';
$lang['day.workingDay'] = 'Munkanap';
$lang['day.workingDays'] = 'Munkanapok';
$lang['day.weekend'] = 'Hétvége';
$lang['day.eachDay'] = 'Minden nap';

$lang['segment.dashboard'] = 'Irányítópult';

$lang['segment.propertySettings'] = 'Eszközbeállítások';
$lang['segment.propertySettings.propertySettings'] = 'eszközbeállítások';

$lang['segment.properties'] = 'Eszközök';
$lang['segment.properties.properties'] = 'eszközbeállítások';

$lang['segment.services'] = 'Szolgáltatások';
$lang['segment.services.services'] = 'szolgáltatások létrehozása az ügyfelek számára';

$lang['segment.categories'] = 'Kategóriák';
$lang['segment.categories.edit'] = 'Kategóriák szerkesztése';
$lang['segment.categories.create'] = 'Kategóriák létrehozása';
$lang['segment.categories.categories'] = 'kategória kezelése';

$lang['segment.products'] = 'Termékek';
$lang['segment.products.edit'] = 'Termékmódosítás';
$lang['segment.products.create'] = 'Termék létrehozása';
$lang['segment.products.products'] = 'Termékkezelés';

$lang['segment.variations'] = 'Változatok';
$lang['segment.variations.edit'] = 'Változatok szerkesztése';
$lang['segment.variations.create'] = 'Változat létrehozása';
$lang['segment.variations.variations'] = 'Változatok kezelése';

$lang['segment.facilities'] = 'Tulajdonságok';
$lang['segment.facilities.edit'] = 'Tulajdonságok szerkesztése';
$lang['segment.facilities.create'] = 'Tulajdonságok létrehozása';
$lang['segment.facilities.facilities'] = 'Vagyonkezelés';

$lang['segment.dashboard'] = 'Irányítópult';
$lang['segment.dashboard.edit'] = 'Irányítópult szerkesztése';
$lang['segment.dashboard.create'] = 'Irányítópult létrehozása';
$lang['segment.dashboard.dashboard'] = 'irányítópult kezelése';

$lang['segment.varencies'] = 'Változékonyság';
$lang['segment.varencies.edit'] = 'A változékonyság beállítása';
$lang['segment.varencies.create'] = 'Változó létrehozása';
$lang['segment.varencies.varencies'] = 'Változáskezelés';

$lang['segment.availability'] = 'Elérhetőség';
$lang['segment.availability.edit'] = 'Rendelkezésre állás';
$lang['segment.availability.create'] = 'Elérhetőség létrehozása';
$lang['segment.availability.availability'] = 'Elérhetőség kezelése';

$lang['segment.navigation'] = 'Navigáció';
$lang['segment.developerAuth'] = 'Felhasználók';
$lang['segment.permission'] = 'Hatalmak';
$lang['segment.program'] = 'Programok';
$lang['segment.apiLogs'] = 'API naplók';
$lang['segment.apiKeys'] = 'API kulcsok';
$lang['segment.apiPermissions'] = 'API-jogosultságok';

$lang['segment.stock'] = 'Részvény';
$lang['segment.offer'] = 'Ajánlat';
$lang['segment.settings'] = 'Beállítások';
$lang['segment.developer_zone'] = 'Fejlesztői zóna';

$lang['segment.import'] = 'Importálás';

$lang['app.internal_name'] = 'Belső név';
$lang['app.change_password'] = 'Jelszó módosítása';
$lang['app.no_property'] = 'OLVASATLAN ESZKÖZ';
$lang['app.edit_user'] = 'Felhasználó szerkesztése';
$lang['app.property'] = 'Eszköz';
$lang['app.role'] = 'Szerep';
$lang['app.surname'] = 'Vezetéknév';
$lang['app.name'] = 'Név';
$lang['app.email'] = 'E-mail';
$lang['app.create_user_property'] = 'Felhasználó létrehozása az eszközhöz';
$lang['app.password'] = 'Jelszó';
$lang['app.save_new_user'] = 'Felhasználó mentése';
$lang['app.password_confirmation'] = 'Jelszó megerősítése';
$lang['app.action'] = 'Művelet';
$lang['app.no_permission_edit_user'] =' Nincs engedélye a felhasználó megváltoztatására ';
$lang['app.edit'] = 'Szerkesztés';
$lang['app.users'] = 'Felhasználók';
$lang['app.name_in_language'] = 'Nyelv neve';
$lang['app.description_in_language'] = 'Nyelv leírása';
$lang['app.perex_in_language'] = 'Perex a nyelvben';
$lang['app.users'] = 'Felhasználók';
$lang['app.facilities'] = 'Tulajdonságok';
$lang['app.no_facilities'] = 'Nincsenek funkciók';
$lang['app.parent'] = 'Szülő';
$lang['app.no_parent'] = 'Nincs szülő';
$lang['app.delete'] = 'Törlés';
$lang['app.new'] = 'Új szolgáltatás';
$lang['app.value_in_language'] = 'Nyelvérték';
$lang['app.save'] = 'Mentés';
$lang['app.create_new'] = 'Új szolgáltatás létrehozása';
$lang['app.active'] = 'Aktív';
$lang['app.no_active'] = 'Inaktív';
$lang['app.step'] = 'Lépés';
$lang['app.minimal_value'] = 'Minimális érték';
$lang['app.maximal_value'] = 'Maximális érték';
$lang['app.count'] = 'Mennyiség';
$lang['app.no_count'] = 'Ne mutassa a mennyiséget';
$lang['app.weight'] = 'Súly';
$lang['app.pieces'] = 'Számlálás';
$lang['app.category'] = 'Kategória';
$lang['app.name_p'] = 'Név';
$lang['app.value'] = 'Érték';
$lang['app.variation'] = 'Változat';
$lang['app.translation'] = 'Fordítások';
$lang['app.gallery'] = 'Galéria';
$lang['app.variability'] = 'Változékonyság';
$lang['app.no_variability'] = 'Nincs változás';
$lang['app.unit_price'] = 'Egységes ár';
$lang['app.price_profile'] = 'Árképzés';
$lang['app.discounts'] = 'Megfelelő kedvezmények';
$lang['app.no_facilities_defined'] = 'Nincsenek meghatározva tulajdonságok';
$lang['app.product'] = 'Termék';
$lang['app.use_variability'] = 'A változó használata';
$lang['app.images'] = 'Képek';
$lang['app.no'] = 'Nem';
$lang['app.yes'] = 'Igen';

$lang['app.phone'] = 'Telefonszám';
$lang['app.primary_color'] = 'Fő szín';
$lang['app.secundary_color'] = 'Másodlagos szín';
$lang['app.default_language'] = 'Alapértelmezett nyelv';
$lang['app.available_languages'] = 'Elérhető nyelvek';
$lang['app.orders'] = 'Alapvető elrendezés';
$lang['app.selling'] = 'Értékesítés megengedett';
$lang['app.price_definition'] = 'Árdefiníció';
$lang['app.price_definition_delimiter'] = 'Árelválasztó centektől';
$lang['app.allowed_currencies'] = 'Engedélyezett pénznemek';
$lang['app.currency'] = 'Alapértelmezett pénznem';

$lang['app.price_up'] = 'Legalacsonyabb ár';
$lang['app.price_down'] = 'Ár a legmagasabbtól';
$lang['app.letter_up'] = 'A-tól Z-ig';
$lang['app.letter_down'] = 'Z-től A-ig';
$lang['app.rating_up'] = 'A legkevésbé kedveltektől';
$lang['app.rating_down'] = 'A legnépszerűbbektől';

$lang['app.no_decimals'] = 'Nincs tizedesjegy';
$lang['app.one_decimals'] = '1 tizedesjegy';
$lang['app.two_decimals'] = '2 tizedesjegy';

$lang['app.comma'] = 'vessző';
$lang['app.dot'] = 'Pont';


$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';