<?php

$lang['message.warning'] = '<b> Pro přístup ke stránce nemáte pravomoc! </ B> Pro vyřešení tohoto problému <b> kontaktujte </ b> administrátora!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Odeslat';
$lang['menu.icon'] = 'Ikona';
$lang['menu.parent'] = 'Rodič';
$lang['menu.type'] = 'Typ';
$lang['menu.active'] = 'Aktivní';
$lang['menu.nonactive'] = 'Neaktivní';
$lang['menu.activeHeader'] = 'Aktivita';
$lang['menu.path'] = 'Cesta';
$lang['menu.pathPlaceholder'] = 'Zvolit cestu: controller / metoda';
$lang['menu.pathLinkPlaceholder'] = 'Zvolit cestu ke webové stránce';
$lang['menu.chooseType'] = 'Vyber typ';
$lang['menu.navigationName'] = 'Název prvku v navigaci';
$lang['menu.addMenuItem'] = 'Přidej prvek do menu';
$lang['menu.iAmParent'] = 'Já jsem rodič!';
$lang['menu.logout'] = 'Odhlásit';
$lang['menu.chooseType'] = 'Zvolit typ';
$lang['menu.controllerType'] = 'Typ';
$lang['menu.controllerDescription'] = 'Název controlleru';
$lang['menu.addController'] = 'Přidání controlleru';
$lang['menu.editController'] = 'Úprava controlleru';
$lang['menu.controllerName'] = 'Controller';
$lang['menu.addControllerToGroups'] = 'Přidat controller do všech skupin';
$lang['menu.delete'] = 'Smazat';
$lang['menu.edit'] = 'Upravit';
$lang['menu.switchOn'] = 'Zapnout';
$lang['menu.switchOff'] = 'Vypnout';
$lang['menu.showing'] = 'Zobrazování';
$lang['menu.creating'] = 'Vytváření';
$lang['menu.editing'] = 'Upravování';
$lang['menu.deleting'] = 'Mazání';
$lang['menu.backToAdd'] = 'Zpět na přidání';


$lang['day.monday'] = 'Pondělí';
$lang['day.tuesday'] = 'Úterý';
$lang['day.wednesday'] = 'Středa';
$lang['day.thursday'] = 'Čtvrtek';
$lang['day.friday'] = 'Pátek';
$lang['day.saturday'] = 'Sobota';
$lang['day.sunday'] = 'Neděle';
$lang['day.workingDay'] = 'Pracovní den';
$lang['day.workingDays'] = 'Pracovní dny';
$lang['day.weekend'] = 'Víkend';
$lang['day.eachDay'] = 'Každý den';

$lang['segment.dashboard'] = 'Dashboard';

$lang['segment.propertySettings'] = 'Nastavení zařízení';
$lang['segment.propertySettings.propertySettings'] = 'nástavných zařízení';

$lang['segment.properties'] = 'Zařízení';
$lang['segment.properties.properties'] = 'nastavení zařízení';

$lang['segment.services'] = 'Služby';
$lang['segment.services.services'] = 'vytvořte služby pro vašich zákazníků';

$lang['segment.categories'] = 'Kategorie';
$lang['segment.categories.edit'] = 'Úprava kategorie';
$lang['segment.categories.create'] = 'Vytvoření kategorie';
$lang['segment.categories.categories'] = 'zpráva kategorií';

$lang['segment.products'] = 'Produkty';
$lang['segment.products.edit'] = 'Úprava produktu';
$lang['segment.products.create'] = 'Vytvoření produktu';
$lang['segment.products.products'] = 'zpráva produktů';

$lang['segment.variations'] = 'Variace';
$lang['segment.variations.edit'] = 'Úprava variace';
$lang['segment.variations.create'] = 'Vytvoření variace';
$lang['segment.variations.variations'] = 'zpráva variací';

$lang['segment.facilities'] = 'Vlastnosti';
$lang['segment.facilities.edit'] = 'Úprava vlastností';
$lang['segment.facilities.create'] = 'Vytvoření vlastností';
$lang['segment.facilities.facilities'] = 'zpráva vlastností';

$lang['segment.dashboard'] = 'Dashboard';
$lang['segment.dashboard.edit'] = 'Úprava dashboardu';
$lang['segment.dashboard.create'] = 'Vytvoření dashboardu';
$lang['segment.dashboard.dashboard'] = 'zpráva dashboardu';

$lang['segment.variabilities'] = 'Variabilita';
$lang['segment.variabilities.edit'] = 'Úprava variability';
$lang['segment.variabilities.create'] = 'Vytvoření variability';
$lang['segment.variabilities.variabilities'] = 'zpráva Variabilní';

$lang['segment.availability'] = 'Dostupnost';
$lang['segment.availability.edit'] = 'Úprava dostupnosti';
$lang['segment.availability.create'] = 'Vytvoření dostupnosti';
$lang['segment.availability.availability'] = 'zpráva dostupnosti';

$lang['segment.navigation'] = 'Navigace';
$lang['segment.developerAuth'] = 'Uživatelé';
$lang['segment.permission'] = 'Pravomoci';
$lang['segment.program'] = 'Programy';
$lang['segment.apiLogs'] = 'API Logy';
$lang['segment.apiKeys'] = 'API klíče';
$lang['segment.apiPermissions'] = 'API pravomoci';

$lang['segment.stock'] = 'Sklad';
$lang['segment.offer'] = 'Nabídka';
$lang['segment.settings'] = 'Nastavení';
$lang['segment.developer_zone'] = 'Developerská zóna';

$lang['segment.import'] = 'Importy';

$lang['app.internal_name'] = 'Interní název';
$lang['app.change_password'] = 'Změnit heslo';
$lang['app.no_property'] = 'NEZAŘAZENÉ ZAŘÍZENÍ';
$lang['app.edit_user'] = 'Upravit uživatele';
$lang['app.property'] = 'Zařízení';
$lang['app.role'] = 'Role';
$lang['app.surname'] = 'Příjmení';
$lang['app.name'] = 'Jméno';
$lang['app.email'] = 'Email';
$lang['app.create_user_property'] = 'Vytvoření uživatele pro zařízení';
$lang['app.password'] = 'Heslo';
$lang['app.save_new_user'] = 'Uložit uživatele';
$lang['app.password_confirmation'] = 'Potvrzení hesla';
$lang['app.action'] = 'Akce';
$lang['app.no_permission_edit_user '] =' Nemáš právo měnit uživatele ';
$lang['app.edit'] = 'Upravit';
$lang['app.users'] = 'Uživatelé';
$lang['app.name_in_language'] = 'Název v jazyce';
$lang['app.description_in_language'] = 'Popis v jazyce';
$lang['app.perex_in_language'] = 'Perex v jazyce';
$lang['app.users'] = 'Uživatelé';
$lang['app.facilities'] = 'Vlastnosti';
$lang['app.no_facilities'] = 'Žádné vlastnosti';
$lang['app.parent'] = 'Rodič';
$lang['app.no_parent'] = 'Žádný rodič';
$lang['app.delete'] = 'Smazat';
$lang['app.new'] = 'Nový prvek';
$lang['app.value_in_language'] = 'Hodnota v jazyce';
$lang['app.save'] = 'Uložit';
$lang['app.create_new'] = 'Vytvořit nový prvek';
$lang['app.active'] = 'Aktivní';
$lang['app.no_active'] = 'Neaktivní';
$lang['app.step'] = 'Krok';
$lang['app.minimal_value'] = 'Minimální hodnota';
$lang['app.maximal_value'] = 'Maximální hodnota';
$lang['app.count'] = 'Množství';
$lang['app.no_count'] = 'Nezobrazovat množství';
$lang['app.weight'] = 'Váha';
$lang['app.pieces'] = 'Počet';
$lang['app.category'] = 'Kategorie';
$lang['app.name_p'] = 'Název';
$lang['app.value'] = 'Hodnota';
$lang['app.variation'] = 'Variace';
$lang['app.translation'] = 'Překlady';
$lang['app.gallery'] = 'Galerie';
$lang['app.variability'] = 'Variabilita';
$lang['app.no_variability'] = 'Bez variability';
$lang['app.unit_price'] = 'Jendotková cena';
$lang['app.price_profile'] = 'Cenotvorba';
$lang['app.discounts'] = 'Použitelné slevy';
$lang['app.no_facilities_defined'] = 'Nemáte definovány žádné vlastnosti';
$lang['app.product'] = 'Produkt';
$lang['app.use_variability'] = 'Používat variabilitu';
$lang['app.images'] = 'Obrázky';
$lang['app.no'] = 'Ne';
$lang['app.yes'] = 'Ano';

$lang['app.phone'] = 'Telefonní číslo';
$lang['app.primary_color'] = 'Hlavní barva';
$lang['app.secundary_color'] = 'Sekundární barva';
$lang['app.default_language'] = 'Přednastavený jazyk';
$lang['app.available_languages'] = 'Jazyky';
$lang['app.orders'] = 'Základní zoradzovanie';
$lang['app.selling'] = 'Prodej povolen';
$lang['app.price_definition'] = 'Definice ceny';
$lang['app.price_definition_delimiter'] = 'Oddelovač ceny od centů';
$lang['app.allowed_currencies'] = 'Povolené měny';
$lang['app.currency'] = 'Přednastavené jména';

$lang['app.price_up'] = 'Cena od nejnižší';
$lang['app.price_down'] = 'Cena od nejvyšší';
$lang['app.letter_up'] = 'od A do Z';
$lang['app.letter_down'] = 'od Z do A';
$lang['app.rating_up'] = 'Od nejméně oblíbeného';
$lang['app.rating_down'] = 'Od Nejvíce oblíbeného';

$lang['app.no_decimals'] = 'Bez desetinného místa';
$lang['app.one_decimals'] = '1 desetinné místo';
$lang['app.two_decimals'] = '2 desetinná místa';

$lang['app.comma'] = 'Čárka';
$lang['app.dot'] = 'Tečka';








$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';