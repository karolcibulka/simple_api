<?php

//passwords
$lang['api.errors.password_match'] = 'Zadaná hesla se neshodují';
$lang['api.errors.password_length'] = 'Minimální délka hesla je% s znaků';


// required fields
$lang['api.errors.first_name'] = 'Jméno je povinný parametr';
$lang['api.errors.last_name'] = 'Příjmení je povinný parametr';
$lang['api.errors.zip'] = 'PSČ je povinen parametr';
$lang['api.errors.city'] = 'Město je povinný parametr';
$lang['api.errors.street_number'] = 'Stát je povinen parametr';
$lang['api.errors.password'] = 'Heslo je povinen parametr';
$lang['api.errors.password_confirmation'] = 'Potvrzení hesla je povinen parametr';
$lang['api.errors.street'] = 'Ulice je povinen parametr';
$lang['api.errors.email'] = 'Email je povinný parametr';
$lang['api.errors.phone'] = 'Telefon je povinný parametr';
$lang['api.errors.old_password'] = 'Staré heslo je povinen parametr';

$lang['api.errors.email_is_in_use'] = 'Zadaný email je již používán';
$lang['api.errors.property'] = 'Zařízení s ID% s neexistuje';
$lang['api.errors.user_doesnt_exist'] = 'Uživatel s emailem% s neexistuje';
$lang['api.errors.old_password_doesnt_match'] = 'Staré heslo není správné!';
$lang['api.errors.user_doesnt_exist_token'] = 'Uživatel s tokenem% s neexistuje';
$lang['api.errors.wrong_password'] = 'Zadané heslo není správné';
$lang['api.errors.empty_property_group'] = 'Zadaná skupina zařízení neobsahuje žádné zařízení';


$lang['api.errors.no_reservations'] = 'Uživatel nemá žádné rezervace';