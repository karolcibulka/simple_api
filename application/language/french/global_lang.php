<?php

$lang['message.warning'] = '<b> Vous n’avez pas l’autorisation d’accéder à cette page! </b> <b> Contactez </b> un administrateur pour résoudre ce problème!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Soumettre';
$lang['menu.icon'] = 'Icône';
$lang['menu.parent'] = 'Parent';
$lang['menu.type'] = 'Type';
$lang['menu.active'] = 'Actif';
$lang['menu.nonactive'] = 'Inactif';
$lang['menu.activeHeader'] = 'Activité';
$lang['menu.path'] = 'Chemin';
$lang['menu.pathPlaceholder'] = 'Choisissez le chemin: contrôleur / méthode';
$lang['menu.pathLinkPlaceholder'] = 'Choisissez le chemin vers le site Web';
$lang['menu.chooseType'] = 'Sélectionnez le type';
$lang['menu.navigationName'] = 'Nom de l\'élément dans la navigation';
$lang['menu.addMenuItem'] = 'Ajouter un élément de menu';
$lang['menu.iAmParent'] = 'Je suis parent!';
$lang['menu.logout'] = 'Déconnexion';
$lang['menu.chooseType'] = 'Choisissez le type';
$lang['menu.controllerType'] = 'Type';
$lang['menu.controllerDescription'] = 'Nom du contrôleur';
$lang['menu.addController'] = 'Ajouter un contrôleur';
$lang['menu.editController'] = 'Modifier le contrôleur';
$lang['menu.controllerName'] = 'Contrôleur';
$lang['menu.addControllerToGroups'] = 'Ajouter un contrôleur à tous les groupes';
$lang['menu.delete'] = 'Supprimer';
$lang['menu.edit'] = 'Modifier';
$lang['menu.switchOn'] = 'Activer';
$lang['menu.switchOff'] = 'Désactiver';
$lang['menu.showing'] = 'Afficher';
$lang['menu.creating'] = 'Création';
$lang['menu.editing'] = 'Édition';
$lang['menu.deleting'] = 'Supprimer';
$lang['menu.backToAdd'] = 'Retour à ajouter';


$lang['day.monday'] = 'Lundi';
$lang['day.tuesday'] = 'Mardi';
$lang['day.wednesday'] = 'Mercredi';
$lang['day.thursday'] = 'Jeudi';
$lang['day.friday'] = 'Vendredi';
$lang['day.saturday'] = 'Samedi';
$lang['day.sunday'] = 'Dimanche';
$lang['day.workingDay'] = 'Jour de travail';
$lang['day.workingDays'] = 'Jours ouvrables';
$lang['day.weekend'] = 'Week-end';
$lang['day.eachDay'] = 'Tous les jours';

$lang['segment.dashboard'] = 'Tableau de bord';

$lang['segment.propertySettings'] = 'Paramètres du périphérique';
$lang['segment.propertySettings.propertySettings'] = 'paramètres de l\'appareil';

$lang['segment.properties'] = 'Périphériques';
$lang['segment.properties.properties'] = 'paramètres de l\'appareil';

$lang['segment.services'] = 'Services';
$lang['segment.services.services'] = 'créez des services pour vos clients';

$lang['segment.categories'] = 'Catégories';
$lang['segment.categories.edit'] = 'Modifier les catégories';
$lang['segment.categories.create'] = 'Créer des catégories';
$lang['segment.categories.categories'] = 'gestion des catégories';

$lang['segment.products'] = 'Produits';
$lang['segment.products.edit'] = 'Modification du produit';
$lang['segment.products.create'] = 'Création de produit';
$lang['segment.products.products'] = 'Gestion des produits';

$lang['segment.variations'] = 'Variations';
$lang['segment.variations.edit'] = 'Modifier les variantes';
$lang['segment.variations.create'] = 'Créer une variante';
$lang['segment.variations.variations'] = 'Gestion des variantes';

$lang['segment.facilities'] = 'Propriétés';
$lang['segment.facilities.edit'] = 'Modifier les propriétés';
$lang['segment.facilities.create'] = 'Créer des propriétés';
$lang['segment.facilities.facilities'] = 'Gestion de la propriété';

$lang['segment.dashboard'] = 'Tableau de bord';
$lang['segment.dashboard.edit'] = 'Modifier le tableau de bord';
$lang['segment.dashboard.create'] = 'Création de tableau de bord';
$lang['segment.dashboard.dashboard'] = 'gestion du tableau de bord';

$lang['segment.variabilities'] = 'Variabilité';
$lang['segment.variabilities.edit'] = 'Ajuster la variabilité';
$lang['segment.variabilities.create'] = 'Créer une variabilité';
$lang['segment.variabilities.variabilities'] = 'Gestion de la variabilité';

$lang['segment.availability'] = 'Disponibilité';
$lang['segment.availability.edit'] = 'Ajuster la disponibilité';
$lang['segment.availability.create'] = 'Créer une disponibilité';
$lang['segment.availability.availability'] = 'gestion de la disponibilité';

$lang['segment.navigation'] = 'Navigation';
$lang['segment.developerAuth'] = 'Utilisateurs';
$lang['segment.permission'] = 'Pouvoirs';
$lang['segment.program'] = 'Programmes';
$lang['segment.apiLogs'] = 'Journaux API';
$lang['segment.apiKeys'] = 'Clés API';
$lang['segment.apiPermissions'] = 'Privilèges API';

$lang['segment.stock'] = 'Stock';
$lang['segment.offer'] = 'Offre';
$lang['segment.settings'] = 'Paramètres';
$lang['segment.developer_zone'] = 'Zone développeur';

$lang['segment.import'] = 'Importations';

$lang['app.internal_name'] = 'Nom interne';
$lang['app.change_password'] = 'Changer le mot de passe';
$lang['app.no_property'] = 'APPAREIL NON LECTURE';
$lang['app.edit_user'] = 'Modifier l\'utilisateur';
$lang['app.property'] = 'Appareil';
$lang['app.role'] = 'Rôle';
$lang['app.surname'] = 'Nom de famille';
$lang['app.name '] =' Nom ';
$lang['app.email'] = 'E-mail';
$lang['app.create_user_property'] = 'Créer un utilisateur pour le périphérique';
$lang['app.password'] = 'Mot de passe';
$lang['app.save_new_user'] = 'Enregistrer l\'utilisateur';
$lang['app.password_confirmation'] = 'Confirmation du mot de passe';
$lang['app.action'] = 'Action';
$lang['app.no_permission_edit_user'] = 'Vous n \' avez pas la permission de changer d \ \'utilisateur';
$lang['app.edit'] = 'Modifier';
$lang['app.users'] = 'Utilisateurs';
$lang['app.name_in_language'] = 'Nom de la langue';
$lang['app.description_in_language'] = 'Description de la langue';
$lang['app.perex_in_language'] = 'Perex dans la langue';
$lang['app.users'] = 'Utilisateurs';
$lang['app.facilities'] = 'Propriétés';
$lang['app.no_facilities'] = 'Aucune fonctionnalité';
$lang['app.parent'] = 'Parent';
$lang['app.no_parent'] = 'Aucun parent';
$lang['app.delete'] = 'Supprimer';
$lang['app.new'] = 'Nouvelle fonctionnalité';
$lang['app.value_in_language'] = 'Valeur de la langue';
$lang['app.save'] = 'Enregistrer';
$lang['app.create_new'] = 'Créer une nouvelle fonctionnalité';
$lang['app.active'] = 'Actif';
$lang['app.no_active'] = 'Inactif';
$lang['app.step'] = 'Étape';
$lang['app.minimal_value'] = 'Valeur minimale';
$lang['app.maximal_value'] = 'Valeur maximale';
$lang['app.count'] = 'Quantité';
$lang['app.no_count'] = 'Ne pas afficher la quantité';
$lang['app.weight'] = 'Poids';
$lang['app.pieces'] = 'Count';
$lang['app.category'] = 'Catégorie';
$lang['app.name_p'] = 'Nom';
$lang['app.value'] = 'Valeur';
$lang['app.variation'] = 'Variation';
$lang['app.translation'] = 'Traductions';
$lang['app.gallery'] = 'Galerie';
$lang['app.variability'] = 'Variabilité';
$lang['app.no_variability'] = 'Aucune variabilité';
$lang['app.unit_price'] = 'Prix unique';
$lang['app.price_profile'] = 'Prix';
$lang['app.discounts'] = 'Remises appropriées';
$lang['app.no_facilities_defined'] = 'Vous n\'avez pas de propriétés définies';
$lang['app.product'] = 'Produit';
$lang['app.use_variability'] = 'Utiliser la variabilité';
$lang['app.images'] = 'Images';
$lang['app.no'] = 'Non';
$lang['app.yes'] = 'Oui';

$lang['app.phone'] = 'Numéro de téléphone';
$lang['app.primary_color'] = 'Couleur principale';
$lang['app.secundary_color'] = 'Couleur secondaire';
$lang['app.default_language'] = 'Langue par défaut';
$lang['app.available_languages'] = 'Langues disponibles';
$lang['app.orders'] = 'Disposition de base';
$lang['app.selling'] = 'Application autorisée';
$lang['app.price_definition'] = 'Définition du prix';
$lang['app.price_definition_delimiter'] = 'Séparateur de prix des cents';
$lang['app.allowed_currency'] = 'Devises autorisées';
$lang['app.currency'] = 'Devise par défaut';

$lang['app.price_up'] = 'Prix le plus bas';
$lang['app.price_down'] = 'Prix du plus élevé';
$lang['app.letter_up'] = 'A à Z';
$lang['app.letter_down'] = 'Z à A';
$lang['app.rating_up'] = 'Du moins favori';
$lang['app.rating_down'] = 'Du plus populaire';

$lang['app.no_decimals'] = 'Pas de décimale';
$lang['app.one_decimals'] = '1 décimale';
$lang['app.two_decimals'] = '2 décimales';

$lang['app.comma'] = 'Virgule';
$lang['app.dot'] = 'Point';















$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';