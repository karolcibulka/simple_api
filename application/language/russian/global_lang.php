<?php

$lang['message.warning'] = '<b> У вас нет разрешения на доступ к этой странице! </b> <b> Свяжитесь </b> с администратором для решения этой проблемы!';

$lang['menu.menu'] = 'Меню';
$lang['menu.submit'] = 'Отправить';
$lang['menu.icon'] = 'Значок';
$lang['menu.parent'] = 'Родитель';
$lang['menu.type'] = 'Тип';
$lang['menu.active'] = 'Активный';
$lang['menu.nonactive'] = 'Неактивен';
$lang['menu.activeHeader'] = 'Активность';
$lang['menu.path'] = 'Путь';
$lang['menu.pathPlaceholder'] = 'Выбрать путь: контроллер / метод';
$lang['menu.pathLinkPlaceholder'] = 'Выбрать путь к веб-сайту';
$lang['menu.chooseType'] = 'Выбрать тип';
$lang['menu.navigationName'] = 'Имя элемента в навигации';
$lang['menu.addMenuItem'] = 'Добавить пункт меню';
$lang['menu.iAmParent'] = 'Я родитель!';
$lang['menu.logout'] = 'Выйти';
$lang['menu.chooseType'] = 'Выбрать тип';
$lang['menu.controllerType'] = 'Тип';
$lang['menu.controllerDescription'] = 'Имя контроллера';
$lang['menu.addController'] = 'Добавить контроллер';
$lang['menu.editController'] = 'Редактировать контроллер';
$lang['menu.controllerName'] = 'Контроллер';
$lang['menu.addControllerToGroups'] = 'Добавить контроллер во все группы';
$lang['menu.delete'] = 'Удалить';
$lang['menu.edit'] = 'Редактировать';
$lang['menu.switchOn'] = 'Включить';
$lang['menu.switchOff'] = 'Отключить';
$lang['menu.showing'] = 'Показать';
$lang['menu.creating'] = 'Создание';
$lang['menu.editing'] = 'Редактирование';
$lang['menu.deleting'] = 'Удалить';
$lang['menu.backToAdd'] = 'Вернуться к добавлению';


$lang['day.monday'] = 'Понедельник';
$lang['day.tuesday'] = 'вторник';
$lang['day.wednesday'] = 'Среда';
$lang['day.thursday'] = 'Четверг';
$lang['day.friday'] = 'Пятница';
$lang['day.saturday'] = 'Суббота';
$lang['day.sunday'] = 'Воскресенье';
$lang['day.workingDay'] = 'Рабочий день';
$lang['day.workingDays'] = 'Рабочие дни';
$lang['day.weekend'] = 'Выходные';
$lang['day.eachDay'] = 'Каждый день';

$lang['segment.dashboard'] = 'Панель управления';

$lang['segment.propertySettings'] = 'Настройки устройства';
$lang['segment.propertySettings.propertySettings'] = 'настройки устройства';

$lang['segment.properties'] = 'Устройства';
$lang['segment.properties.properties'] = 'настройки устройства';

$lang['segment.services'] = 'Услуги';
$lang['segment.services.services'] = 'создавать услуги для ваших клиентов';

$lang['segment.categories'] = 'Категории';
$lang['segment.categories.edit'] = 'Редактировать категории';
$lang['segment.categories.create'] = 'Создать категории';
$lang['segment.categories.categories'] = 'управление категориями';

$lang['segment.products'] = 'Продукты';
$lang['segment.products.edit'] = 'Модификация продукта';
$lang['segment.products.create'] = 'Создание продукта';
$lang['segment.products.products'] = 'Управление продуктом';

$lang['segment.variations'] = 'Варианты';
$lang['segment.variations.edit'] = 'Редактировать варианты';
$lang['segment.variations.create'] = 'Создать вариант';
$lang['segment.variations.variations'] = 'Управление вариациями';

$lang['segment.facilities'] = 'Свойства';
$lang['segment.facilities.edit'] = 'Изменить свойства';
$lang['segment.facilities.create'] = 'Создать свойства';
$lang['segment.facilities.facilities'] = 'Управление недвижимостью';

$lang['segment.dashboard'] = 'Панель управления';
$lang['segment.dashboard.edit'] = 'Редактировать информационную панель';
$lang['segment.dashboard.create'] = 'Создание информационной панели';
$lang['segment.dashboard.dashboard'] = 'управление панелью инструментов';

$lang['segment.variabilities'] = 'Изменчивость';
$lang['segment.variabilities.edit'] = 'Настроить изменчивость';
$lang['segment.variabilities.create'] = 'Создать вариативность';
$lang['segment.variabilities.variabilities'] = 'Управление изменчивостью';

$lang['segment.availability'] = 'Доступность';
$lang['segment.availability.edit'] = 'Настроить доступность';
$lang['segment.availability.create'] = 'Создать доступность';
$lang['segment.availability.availability'] = 'управление доступностью';

$lang['segment.navigation'] = 'Навигация';
$lang['segment.developerAuth'] = 'Пользователи';
$lang['segment.permission'] = 'Полномочия';
$lang['segment.program'] = 'Программы';
$lang['segment.apiLogs'] = 'Журналы API';
$lang['segment.apiKeys'] = 'Ключи API';
$lang['segment.apiPermissions'] = 'Привилегии API';

$lang['segment.stock'] = 'Акция';
$lang['segment.offer'] = 'Предложение';
$lang['segment.settings'] = 'Настройки';
$lang['segment.developer_zone'] = 'Зона разработчика';

$lang['segment.import'] = 'Импорт';

$lang['app.internal_name'] = 'Внутреннее имя';
$lang['app.change_password'] = 'Сменить пароль';
$lang['app.no_property'] = 'НЕЧИТЫВАЕМОЕ УСТРОЙСТВО';
$lang['app.edit_user'] = 'Редактировать пользователя';
$lang['app.property'] = 'Устройство';
$lang['app.role'] = 'Роль';
$lang['app.surname'] = 'Фамилия';
$lang['app.name'] = 'Имя';
$lang['app.email'] = 'Электронная почта';
$lang['app.create_user_property'] = 'Создать пользователя для устройства';
$lang['app.password'] = 'Пароль';
$lang['app.save_new_user'] = 'Сохранить пользователя';
$lang['app.password_confirmation'] = 'Подтверждение пароля';
$lang['app.action'] = 'Действие';
$lang['app.no_permission_edit_user '] =' У вас нет разрешения на смену пользователя ';
$lang['app.edit'] = 'Изменить';
$lang['app.users'] = 'Пользователи';
$lang['app.name_in_language'] = 'Название языка';
$lang['app.description_in_language'] = 'Описание языка';
$lang['app.perex_in_language'] = 'Переекс на языке';
$lang['app.users'] = 'Пользователи';
$lang['app.facilities'] = 'Свойства';
$lang['app.no_facilities'] = 'Нет функций';
$lang['app.parent'] = 'Родитель';
$lang['app.no_parent'] = 'Без родителя';
$lang['app.delete'] = 'Удалить';
$lang['app.new'] = 'Новая функция';
$lang['app.value_in_language'] = 'Значение языка';
$lang['app.save'] = 'Сохранить';
$lang['app.create_new'] = 'Создать новую функцию';
$lang['app.active'] = 'Активный';
$lang['app.no_active'] = 'Неактивен';
$lang['app.step'] = 'Шаг';
$lang['app.minimal_value'] = 'Минимальное значение';
$lang['app.maximal_value'] = 'Максимальное значение';
$lang['app.count'] = 'Количество';
$lang['app.no_count'] = 'Не показывать количество';
$lang['app.weight'] = 'Вес';
$lang['app.pieces'] = 'Подсчитать';
$lang['app.category'] = 'Категория';
$lang['app.name_p'] = 'Имя';
$lang['app.value'] = 'Значение';
$lang['app.variation'] = 'Вариант';
$lang['app.translation'] = 'Переводы';
$lang['app.gallery'] = 'Галерея';
$lang['app.variability'] = 'Изменчивость';
$lang['app.no_variability'] = 'Без изменений';
$lang['app.unit_price'] = 'Единая цена';
$lang['app.price_profile'] = 'Цена';
$lang['app.discounts'] = 'Соответствующие скидки';
$lang['app.no_facilities_defined'] = 'У вас нет определенных свойств';
$lang['app.product'] = 'Продукт';
$lang['app.use_variability'] = 'Использовать изменчивость';
$lang['app.images'] = 'Изображения';
$lang['app.no'] = 'Нет';
$lang['app.yes'] = 'Да';

$lang['app.phone'] = 'Номер телефона';
$lang['app.primary_color'] = 'Основной цвет';
$lang['app.secundary_color'] = 'Дополнительный цвет';
$lang['app.default_language'] = 'Язык по умолчанию';
$lang['app.available_languages'] = 'Доступные языки';
$lang['app.orders'] = 'Базовый макет';
$lang['app.selling'] = 'Продажи разрешены';
$lang['app.price_definition'] = 'Определение цены';
$lang['app.price_definition_delimiter'] = 'Разделитель цен от центов';
$lang['app.allowed_currencies'] = 'Разрешенные валюты';
$lang['app.currency'] = 'Валюта по умолчанию';

$lang['app.price_up'] = 'Самая низкая цена';
$lang['app.price_down'] = 'Цена от максимальной';
$lang['app.letter_up'] = 'от А до Я';
$lang['app.letter_down'] = 'от Я до А';
$lang['app.rating_up'] = 'Из нелюбимых';
$lang['app.rating_down'] = 'Из самых популярных';

$lang['app.no_decimals'] = 'Без десятичного разряда';
$lang['app.one_decimals'] = '1 десятичный разряд';
$lang['app.two_decimals'] = '2 десятичных знака';

$lang['app.comma'] = 'Запятая';
$lang['app.dot'] = 'Точка';









$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';