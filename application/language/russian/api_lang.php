<?php

$lang['api.errors.password_match'] = 'Введенные пароли не совпадают';
$lang['api.errors.password_length'] = 'Минимальная длина пароля составляет% s символов';


// обязательные поля
$lang['api.errors.first_name'] = 'Имя является обязательным параметром';
$lang['api.errors.last_name'] = 'Фамилия является обязательным параметром';
$lang['api.errors.zip'] = 'Почтовый индекс является обязательным параметром';
$lang['api.errors.city'] = 'Город - обязательный параметр';
$lang['api.errors.street_number'] = 'Состояние является обязательным параметром';
$lang['api.errors.password'] = 'Пароль является обязательным параметром';
$lang['api.errors.password_confirmation'] = 'Подтверждение пароля является обязательным параметром';
$lang['api.errors.street'] = 'Улица является обязательным параметром';
$lang['api.errors.email'] = 'Электронная почта является обязательным параметром';
$lang['api.errors.phone'] = 'Телефон является обязательным параметром';
$lang['api.errors.old_password'] = 'Старый пароль является обязательным параметром';

$lang['api.errors.email_is_in_use'] = 'Введенный вами адрес электронной почты уже используется';
$lang['api.errors.property'] = 'Устройство с идентификатором% s не существует';
$lang['api.errors.user_doesnt_exist'] = 'Пользователь с адресом электронной почты% s не существует';
$lang['api.errors.old_password_doesnt_match'] = 'Старый пароль неверен!';
$lang['api.errors.user_doesnt_exist_token'] = 'Пользователь с токеном% s не существует';
$lang['api.errors.wrong_password'] = 'Введенный вами пароль неверен';
$lang['api.errors.empty_property_group'] = 'Указанная группа устройств не содержит устройств';


$lang['api.errors.no_reservations'] = 'У пользователя нет бронирований';