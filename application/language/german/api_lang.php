<?php

$lang['api.errors.password_match'] = 'Eingegebene Passwörter stimmen nicht überein';
$lang['api.errors.password_length'] = 'Die minimale Passwortlänge beträgt% s Zeichen';


// Benötigte Felder
$lang['api.errors.first_name'] = 'Name ist ein erforderlicher Parameter';
$lang['api.errors.last_name'] = 'Nachname ist ein erforderlicher Parameter';
$lang['api.errors.zip'] = 'Postleitzahl ist ein erforderlicher Parameter';
$lang['api.errors.city'] = 'Stadt ist ein erforderlicher Parameter';
$lang['api.errors.street_number'] = 'Status ist ein erforderlicher Parameter';
$lang['api.errors.password'] = 'Passwort ist ein erforderlicher Parameter';
$lang['api.errors.password_confirmation'] = 'Passwortbestätigung ist ein erforderlicher Parameter';
$lang['api.errors.street'] = 'Straße ist ein erforderlicher Parameter';
$lang['api.errors.email'] = 'E-Mail ist ein erforderlicher Parameter';
$lang['api.errors.phone'] = 'Telefon ist ein erforderlicher Parameter';
$lang['api.errors.old_password'] = 'Altes Passwort ist ein erforderlicher Parameter';

$lang['api.errors.email_is_in_use'] = 'Die von Ihnen eingegebene E-Mail wird bereits verwendet';
$lang['api.errors.property'] = 'Gerät mit ID% s existiert nicht';
$lang['api.errors.user_doesnt_exist'] = 'Benutzer mit E-Mail% s existiert nicht';
$lang['api.errors.old_password_doesnt_match'] = 'Altes Passwort ist falsch!';
$lang['api.errors.user_doesnt_exist_token'] = 'Benutzer mit Token% s existiert nicht';
$lang['api.errors.wrong_password'] = 'Das eingegebene Passwort ist falsch';
$lang['api.errors.empty_property_group'] = 'Die angegebene Gerätegruppe enthält keine Geräte';


$lang['api.errors.no_reservations'] = 'Benutzer hat keine Reservierungen';