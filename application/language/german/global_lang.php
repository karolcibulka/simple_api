<?php
$lang['message.warning'] = '<b> Sie haben keine Berechtigung zum Zugriff auf diese Seite! </ b> <b> Wenden Sie sich </ b> an einen Administrator, um dieses Problem zu beheben!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Submit';
$lang['menu.icon'] = 'Icon';
$lang['menu.parent'] = 'Parent';
$lang['menu.type'] = 'Type';
$lang['menu.active'] = 'Active';
$lang['menu.nonactive'] = 'Inaktiv';
$lang['menu.activeHeader'] = 'Aktivität';
$lang['menu.path'] = 'Path';
$lang['menu.pathPlaceholder'] = 'Pfad auswählen: controller / method';
$lang['menu.pathLinkPlaceholder'] = 'Pfad zur Website auswählen';
$lang['menu.chooseType'] = 'Typ auswählen';
$lang['menu.navigationName'] = 'Name des Elements in der Navigation';
$lang['menu.addMenuItem'] = 'Menüpunkt hinzufügen';
$lang['menu.iAmParent'] = 'Ich bin ein Elternteil!';
$lang['menu.logout'] = 'Abmelden';
$lang['menu.chooseType'] = 'Typ auswählen';
$lang['menu.controllerType'] = 'Type';
$lang['menu.controllerDescription'] = 'Controller Name';
$lang['menu.addController'] = 'Controller hinzufügen';
$lang['menu.editController'] = 'Controller bearbeiten';
$lang['menu.controllerName'] = 'Controller';
$lang['menu.addControllerToGroups'] = 'Controller zu allen Gruppen hinzufügen';
$lang['menu.delete'] = 'Löschen';
$lang['menu.edit'] = 'Bearbeiten';
$lang['menu.switchOn'] = 'Enable';
$lang['menu.switchOff'] = 'Deaktivieren';
$lang['menu.showing'] = 'Display';
$lang['menu.creating'] = 'Erstellen';
$lang['menu.editing'] = 'Bearbeiten';
$lang['menu.deleting'] = 'Löschen';
$lang['menu.backToAdd'] = 'Zurück zum Hinzufügen';


$lang['day.monday'] = 'Montag';
$lang['day.tuesday'] = 'Tuesday';
$lang['day.wednesday'] = 'Wednesday';
$lang['day.thursday'] = 'Thursday';
$lang['day.friday'] = 'Friday';
$lang['day.saturday'] = 'Saturday';
$lang['day.sunday'] = 'Sonntag';
$lang['day.workingDay'] = 'Arbeitstag';
$lang['day.workingDays'] = 'Arbeitstage';
$lang['day.weekend'] = 'Weekend';
$lang['day.eachDay'] = 'Jeden Tag';

$lang['segment.dashboard'] = 'Dashboard';

$lang['segment.propertySettings'] = 'Geräteeinstellungen';
$lang['segment.propertySettings.propertySettings'] = 'Geräteeinstellungen';

$lang['segment.properties'] = 'Geräte';
$lang['segment.properties.properties'] = 'Geräteeinstellungen';

$lang['segment.services'] = 'Services';
$lang['segment.services.services'] = 'Services für Ihre Kunden erstellen';

$lang['segment.categories'] = 'Categories';
$lang['segment.categories.edit'] = 'Kategorien bearbeiten';
$lang['segment.categories.create'] = 'Kategorien erstellen';
$lang['segment.categories.categories'] = 'Kategoriemanagement';

$lang['segment.products'] = 'Produkte';
$lang['segment.products.edit'] = 'Produktänderung';
$lang['segment.products.create'] = 'Produkterstellung';
$lang['segment.products.products'] = 'Produktmanagement';

$lang['segment.variations'] = 'Variationen';
$lang['segment.variations.edit'] = 'Variationen bearbeiten';
$lang['segment.variations.create'] = 'Variation erstellen';
$lang['segment.variations.variations'] = 'Variations Management';

$lang['segment.facilities'] = 'Eigenschaften';
$lang['segment.facilities.edit'] = 'Eigenschaften bearbeiten';
$lang['segment.facilities.create'] = 'Eigenschaften erstellen';
$lang['segment.facilities.facilities'] = 'Immobilienverwaltung';

$lang['segment.dashboard'] = 'Dashboard';
$lang['segment.dashboard.edit'] = 'Dashboard bearbeiten';
$lang['segment.dashboard.create'] = 'Dashboard-Erstellung';
$lang['segment.dashboard.dashboard'] = 'Dashboard-Verwaltung';

$lang['segment.variabilities'] = 'Variabilität';
$lang['segment.variabilities.edit'] = 'Variabilität anpassen';
$lang['segment.variabilities.create'] = 'Variabilität erstellen';
$lang['segment.variabilities.variabilities'] = 'Variabilitätsmanagement';

$lang['segment.availability'] = 'Verfügbarkeit';
$lang['segment.availability.edit'] = 'Verfügbarkeit anpassen';
$lang['segment.availability.create'] = 'Verfügbarkeit erstellen';
$lang['segment.availability.availability'] = 'Verfügbarkeitsmanagement';

$lang['segment.navigation'] = 'Navigation';
$lang['segment.developerAuth'] = 'Benutzer';
$lang['segment.permission'] = 'Powers';
$lang['segment.program'] = 'Programme';
$lang['segment.apiLogs'] = 'API-Protokolle';
$lang['segment.apiKeys'] = 'API-Schlüssel';
$lang['segment.apiPermissions'] = 'API-Berechtigungen';

$lang['segment.stock'] = 'Stock';
$lang['segment.offer'] = 'Angebot';
$lang['segment.settings'] = 'Einstellungen';
$lang['segment.developer_zone'] = 'Developer Zone';

$lang['segment.import'] = 'Importe';

$lang['app.internal_name'] = 'Interner Name';
$lang['app.change_password'] = 'Passwort ändern';
$lang['app.no_property'] = 'UNREADED DEVICE';
$lang['app.edit_user'] = 'Benutzer bearbeiten';
$lang['app.property'] = 'Gerät';
$lang['app.role'] = 'Rolle';
$lang['app.surname'] = 'Nachname';
$lang['app.name'] = 'Name';
$lang['app.email'] = 'Email';
$lang['app.create_user_property'] = 'Benutzer für Gerät erstellen';
$lang['app.password'] = 'Passwort';
$lang['app.save_new_user'] = 'Benutzer speichern';
$lang['app.password_confirmation'] = 'Passwortbestätigung';
$lang['app.action'] = 'Aktion';
$lang['app.no_permission_edit_user '] =' Sie haben keine Berechtigung zum Benutzerwechsel ';
$lang['app.edit'] = 'Bearbeiten';
$lang['app.users'] = 'Benutzer';
$lang['app.name_in_language'] = 'Sprachname';
$lang['app.description_in_language'] = 'Sprachbeschreibung';
$lang['app.perex_in_language'] = 'Perex in Language';
$lang['app.users'] = 'Benutzer';
$lang['app.facilities'] = 'Eigenschaften';
$lang['app.no_facilities'] = 'Keine Funktionen';
$lang['app.parent'] = 'Parent';
$lang['app.no_parent'] = 'Kein Elternteil';
$lang['app.delete'] = 'Löschen';
$lang['app.new'] = 'Neues Feature';
$lang['app.value_in_language'] = 'Sprachwert';
$lang['app.save'] = 'Speichern';
$lang['app.create_new'] = 'Neues Feature erstellen';
$lang['app.active'] = 'Active';
$lang['app.no_active'] = 'Inaktiv';
$lang['app.step'] = 'Step';
$lang['app.minimal_value'] = 'Minimalwert';
$lang['app.maximal_value'] = 'Maximalwert';
$lang['app.count'] = 'Menge';
$lang['app.no_count'] = 'Menge nicht anzeigen';
$lang['app.weight'] = 'Weight';
$lang['app.pieces'] = 'Count';
$lang['app.category'] = 'Category';
$lang['app.name_p'] = 'Name';
$lang['app.value'] = 'Wert';
$lang['app.variation'] = 'Variation';
$lang['app.translation'] = 'Übersetzungen';
$lang['app.gallery'] = 'Galerie';
$lang['app.variability'] = 'Variabilität';
$lang['app.no_variability'] = 'Keine Variabilität';
$lang['app.unit_price'] = 'Einzelpreis';
$lang['app.price_profile'] = 'Preisgestaltung';
$lang['app.discounts'] = 'Angemessene Rabatte';
$lang['app.no_facilities_defined'] = 'Sie haben keine Eigenschaften definiert';
$lang['app.product'] = 'Produkt';
$lang['app.use_variability'] = 'Variabilität verwenden';
$lang['app.images'] = 'Bilder';
$lang['app.no'] = 'Nein';
$lang['app.yes'] = 'Ja';

$lang['app.phone'] = 'Telefonnummer';
$lang['app.primary_color'] = 'Hauptfarbe';
$lang['app.secundary_color'] = 'Sekundärfarbe';
$lang['app.default_language'] = 'Standardsprache';
$lang['app.available_languages'] = 'Verfügbare Sprachen';
$lang['app.orders'] = 'Grundlayout';
$lang['app.selling'] = 'App erlaubt';
$lang['app.price_definition'] = 'Preisdefinition';
$lang['app.price_definition_delimiter'] = 'Preistrenner von Cent';
$lang['app.allowed_currencies'] = 'Zulässige Währungen';
$lang['app.currency'] = 'Standardwährung';

$lang['app.price_up'] = 'Niedrigster Preis';
$lang['app.price_down'] = 'Preis vom höchsten';
$lang['app.letter_up'] = 'A bis Z';
$lang['app.letter_down'] = 'Z bis A';
$lang['app.rating_up'] = 'Vom kleinsten Favoriten';
$lang['app.rating_down'] = 'From Most Popular';

$lang['app.no_decimals'] = 'Keine Dezimalstelle';
$lang['app.one_decimals'] = '1 Dezimalstelle';
$lang['app.two_decimals'] = '2 Dezimalstellen';

$lang['app.comma'] = 'Komma';
$lang['app.dot'] = 'Dot';







$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';