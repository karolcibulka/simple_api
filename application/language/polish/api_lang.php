<?php

$lang['api.errors.password_match'] = 'Wprowadzone hasła nie pasują';
$lang['api.errors.password_length'] = 'Minimalna długość hasła to% s znaków';


// Wymagane pola
$lang['api.errors.first_name'] = 'Nazwa jest wymaganym parametrem';
$lang['api.errors.last_name'] = 'Nazwisko jest wymaganym parametrem';
$lang['api.errors.zip'] = 'Kod pocztowy jest wymaganym parametrem';
$lang['api.errors.city'] = 'Miasto jest wymaganym parametrem';
$lang['api.errors.street_number'] = 'Stan jest wymaganym parametrem';
$lang['api.errors.password'] = 'Hasło jest wymaganym parametrem';
$lang['api.errors.password_confirmation'] = 'Potwierdzenie hasła jest wymaganym parametrem';
$lang['api.errors.street'] = 'Ulica jest wymaganym parametrem';
$lang['api.errors.email'] = 'Email jest wymaganym parametrem';
$lang['api.errors.phone'] = 'Telefon jest wymaganym parametrem';
$lang['api.errors.old_password'] = 'Stare hasło jest wymaganym parametrem';

$lang['api.errors.email_is_in_use'] = 'Podany adres e-mail jest już używany';
$lang['api.errors.property'] = 'Urządzenie o identyfikatorze% s nie istnieje';
$lang['api.errors.user_doesnt_exist'] = 'Użytkownik z adresem e-mail% s nie istnieje';
$lang['api.errors.old_password_doesnt_match'] = 'Stare hasło jest nieprawidłowe!';
$lang['api.errors.user_doesnt_exist_token'] = 'Użytkownik z tokenem% s nie istnieje';
$lang['api.errors.wrong_password'] = 'Wprowadzone hasło jest nieprawidłowe';
$lang['api.errors.empty_property_group'] = 'Określona grupa urządzeń nie zawiera żadnych urządzeń';


$lang['api.errors.no_reservations'] = 'Użytkownik nie ma rezerwacji';