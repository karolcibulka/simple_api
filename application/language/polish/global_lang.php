<?php

$lang['message.warning'] = '<b> Nie masz pozwolenia na dostęp do tej strony! </b> <b> Skontaktuj się </b> z administratorem, aby rozwiązać ten problem!';

$lang['menu.menu'] = 'Menu';
$lang['menu.submit'] = 'Prześlij';
$lang['menu.icon'] = 'Ikona';
$lang['menu.parent'] = 'Rodzic';
$lang['menu.type'] = 'Typ';
$lang['menu.active'] = 'Aktywne';
$lang['menu.nonactive'] = 'Nieaktywne';
$lang['menu.activeHeader'] = 'Aktywność';
$lang['menu.path'] = 'Ścieżka';
$lang['menu.pathPlaceholder'] = 'Wybierz ścieżkę: kontroler / metoda';
$lang['menu.pathLinkPlaceholder'] = 'Wybierz ścieżkę do strony internetowej';
$lang['menu.chooseType'] = 'Wybierz typ';
$lang['menu.navigationName'] = 'Nazwa elementu w nawigacji';
$lang['menu.addMenuItem'] = 'Dodaj element menu';
$lang['menu.iAmParent'] = 'Jestem rodzicem!';
$lang['menu.logout'] = 'Wyloguj się';
$lang['menu.chooseType'] = 'Wybierz typ';
$lang['menu.controllerType'] = 'Typ';
$lang['menu.controllerDescription'] = 'Nazwa kontrolera';
$lang['menu.addController'] = 'Dodaj kontroler';
$lang['menu.editController'] = 'Edytuj kontroler';
$lang['menu.controllerName'] = 'Kontroler';
$lang['menu.addControllerToGroups'] = 'Dodaj kontroler do wszystkich grup';
$lang['menu.delete'] = 'Usuń';
$lang['menu.edit'] = 'Edytuj';
$lang['menu.switchOn'] = 'Włącz';
$lang['menu.switchOff'] = 'Wyłącz';
$lang['menu.showing'] = 'Wyświetlacz';
$lang['menu.creating'] = 'Tworzenie';
$lang['menu.editing'] = 'Edycja';
$lang['menu.deleting'] = 'Usuń';
$lang['menu.backToAdd'] = 'Powrót do dodania';


$lang['day.monday'] = 'poniedziałek';
$lang['day.tuesday'] = 'Wtorek';
$lang['day.wed Wednesday'] = 'Środa';
$lang['day.th Thursday'] = 'Czwartek';
$lang['day.friday'] = 'piątek';
$lang['day.saturday'] = 'sobota';
$lang['day.sunday'] = 'Niedziela';
$lang['day.workingDay'] = 'Dzień roboczy';
$lang['day.workingDays'] = 'Dni robocze';
$lang['day.weekend'] = 'Weekend';
$lang['day.eachDay'] = 'Codziennie';

$lang['segment.dashboard'] = 'Dashboard';

$lang['segment.propertySettings'] = 'Ustawienia urządzenia';
$lang['segment.propertySettings.propertySettings'] = 'ustawienia urządzenia';

$lang['segment.properties'] = 'Urządzenia';
$lang['segment.properties.properties'] = 'ustawienia urządzenia';

$lang['segment.services'] = 'Usługi';
$lang['segment.services.services'] = 'twórz usługi dla swoich klientów';

$lang['segment.categories'] = 'Kategorie';
$lang['segment.categories.edit'] = 'Edytuj kategorie';
$lang['segment.categories.create'] = 'Utwórz kategorie';
$lang['segment.categories.categories'] = 'zarządzanie kategoriami';

$lang['segment.products'] = 'Produkty';
$lang['segment.products.edit'] = 'Modyfikacja produktu';
$lang['segment.products.create'] = 'Tworzenie produktu';
$lang['segment.products.products'] = 'Zarządzanie produktem';

$lang['segment.variations'] = 'Odmiany';
$lang['segment.variations.edit'] = 'Edytuj odmiany';
$lang['segment.variations.create'] = 'Utwórz odmianę';
$lang['segment.variations.variations'] = 'Zarządzanie zmianami';

$lang['segment.facilities'] = 'Właściwości';
$lang['segment.facilities.edit'] = 'Edytuj właściwości';
$lang['segment.facilities.create'] = 'Utwórz właściwości';
$lang['segment.facilities.facilities'] = 'Zarządzanie nieruchomościami';

$lang['segment.dashboard'] = 'Pulpit nawigacyjny';
$lang['segment.dashboard.edit'] = 'Edytuj pulpit nawigacyjny';
$lang['segment.dashboard.create'] = 'Tworzenie pulpitu nawigacyjnego';
$lang['segment.dashboard.dashboard'] = 'zarządzanie pulpitem nawigacyjnym';

$lang['segment.variabilities'] = 'Zmienność';
$lang['segment.variabilities.edit'] = 'Dostosuj zmienność';
$lang['segment.variabilities.create'] = 'Utwórz zmienność';
$lang['segment.variabilities.variabilities'] = 'Zarządzanie zmiennością';

$lang['segment.availability'] = 'Dostępność';
$lang['segment.availability.edit'] = 'Dostosuj dostępność';
$lang['segment.availability.create'] = 'Utwórz dostępność';
$lang['segment.availability.availability'] = 'zarządzanie dostępnością';

$lang['segment.navigation'] = 'Nawigacja';
$lang['segment.developerAuth'] = 'Użytkownicy';
$lang['segment.permission'] = 'Uprawnienia';
$lang['segment.program'] = 'Programy';
$lang['segment.apiLogs'] = 'Dzienniki API';
$lang['segment.apiKeys'] = 'Klucze API';
$lang['segment.apiPermissions'] = 'Uprawnienia API';

$lang['segment.stock'] = 'Stock';
$lang['segment.offer'] = 'Oferta';
$lang['segment.settings'] = 'Ustawienia';
$lang['segment.developer_zone'] = 'Strefa dewelopera';

$lang['segment.import'] = 'Importuje';

$lang['app.internal_name'] = 'Nazwa wewnętrzna';
$lang['app.change_password'] = 'Zmień hasło';
$lang['app.no_property'] = 'NIEOCZYTANE URZĄDZENIE';
$lang['app.edit_user'] = 'Edytuj użytkownika';
$lang['app.property'] = 'Urządzenie';
$lang['app.role'] = 'Rola';
$lang['app.sename'] = 'Last Name';
$lang['app.name'] = 'Imię';
$lang['app.email'] = 'E-mail';
$lang['app.create_user_property'] = 'Utwórz użytkownika dla urządzenia';
$lang['app.password'] = 'Hasło';
$lang['app.save_new_user'] = 'Zapisz użytkownika';
$lang['app.password_confirmation'] = 'Potwierdzenie hasła';
$lang['app.action'] = 'Action';
$lang['app.no_permission_edit_user '] =' Nie masz uprawnień do zmiany użytkownika ';
$lang['app.edit'] = 'Edytuj';
$lang['app.users'] = 'Użytkownicy';
$lang['app.name_in_language'] = 'Nazwa języka';
$lang['app.description_in_language'] = 'Opis języka';
$lang['app.perex_in_language'] = 'Perex w języku';
$lang['app.users'] = 'Użytkownicy';
$lang['app.facilities'] = 'Właściwości';
$lang['app.no_facilities'] = 'Brak funkcji';
$lang['app.parent'] = 'Rodzic';
$lang['app.no_parent'] = 'Brak rodzica';
$lang['app.delete'] = 'Usuń';
$lang['app.new'] = 'Nowa funkcja';
$lang['app.value_in_language'] = 'Wartość języka';
$lang['app.save'] = 'Zapisz';
$lang['app.create_new'] = 'Utwórz nową funkcję';
$lang['app.active'] = 'Active';
$lang['app.no_active'] = 'Nieaktywne';
$lang['app.step'] = 'Krok';
$lang['app.minimal_value'] = 'Minimalna wartość';
$lang['app.maximal_value'] = 'Wartość maksymalna';
$lang['app.count'] = 'Ilość';
$lang['app.no_count'] = 'Nie pokazuj ilości';
$lang['app.weight'] = 'Waga';
$lang['app.pieces'] = 'Count';
$lang['app.category'] = 'Kategoria';
$lang['app.name_p'] = 'Imię';
$lang['app.value'] = 'Wartość';
$lang['app.variation'] = 'Odmiana';
$lang['app.translation'] = 'Tłumaczenia';
$lang['app.gallery'] = 'Galeria';
$lang['app.variability'] = 'Zmienność';
$lang['app.no_variability'] = 'Brak zmienności';
$lang['app.unit_price'] = 'Cena jednostkowa';
$lang['app.price_profile'] = 'Ceny';
$lang['app.discounts'] = 'Odpowiednie rabaty';
$lang['app.no_facilities_defined'] = 'Nie masz zdefiniowanych żadnych właściwości';
$lang['app.product'] = 'Produkt';
$lang['app.use_variability'] = 'Użyj zmienności';
$lang['app.images'] = 'Obrazy';
$lang['app.no'] = 'Nie';
$lang['app.yes'] = 'Tak';

$lang['app.phone'] = 'Numer telefonu';
$lang['app.primary_color'] = 'Główny kolor';
$lang['app.secundary_color'] = 'Kolor dodatkowy';
$lang['app.default_language'] = 'Domyślny język';
$lang['app.available_languages'] = 'Dostępne języki';
$lang['app.orders'] = 'Podstawowy układ';
$lang['app.selling'] = 'Sprzedaż dozwolona';
$lang['app.price_definition'] = 'Definicja ceny';
$lang['app.price_definition_delimiter'] = 'Cena oddzielająca od centów';
$lang['app.allowed_currencies'] = 'Dozwolone waluty';
$lang['app.currency'] = 'Domyślna waluta';

$lang['app.price_up'] = 'Najniższa cena';
$lang['app.price_down'] = 'Cena od najwyższej';
$lang['app.letter_up'] = 'A do Z';
$lang['app.letter_down'] = 'Z do A';
$lang['app.rating_up'] = 'Od najmniej ulubionych';
$lang['app.rating_down'] = 'Od najpopularniejszych';

$lang['app.no_decimals'] = 'Brak miejsca po przecinku';
$lang['app.one_decimals'] = '1 miejsce po przecinku';
$lang['app.two_decimals'] = '2 miejsca po przecinku';

$lang['app.comma'] = 'Przecinek';
$lang['app.dot'] = 'Kropka';





$lang['langshort.sk'] = 'Slovenčina';
$lang['langshort.en'] = 'English';
$lang['langshort.pl'] = 'Polskie';
$lang['langshort.de'] = 'Deutsche';
$lang['langshort.hu'] = 'Magyar';
$lang['langshort.ru'] = 'Pусский';
$lang['langshort.cz'] = 'Čeština';
$lang['langshort.fr'] = 'Français';