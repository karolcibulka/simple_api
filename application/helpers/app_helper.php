<?php


function pre_r($expression, $return = FALSE)
{
    list($callee) = debug_backtrace();

    if ($return === true) {
        if (is_string($expression)) return '<pre>' . print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), TRUE) . '</pre>';
        return '<pre>' . print_r($expression, TRUE) . '</pre>';
    } elseif ($return == 'noDebug') {
        echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    } else {
        echo '<strong>' . $callee['file'] . '</strong>  <span style="color:tomato">@line ' . $callee['line'] . '</span>';
        echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    }
}

function appDevelopers()
{
    return array(
        '0' => 'kajo.cibulka@gmail.com',
        '1' => 'admin@admin.com',
    );
}

function getLangFromCode($lang_code)
{
    switch ($lang_code) {
        case 'sk':
            return 'slovak';
            break;
        case 'en':
            return 'english';
            break;
        case 'cz':
            return 'czech';
            break;
        case 'ru':
            return 'russian';
            break;
        case 'hu':
            return 'hungary';
            break;
        case 'de':
            return 'german';
            break;
        case 'pl':
            return 'polish';
            break;
        case 'fr':
            return 'french';
            break;
    }
}

function doSlug($string){
    $slug = url_title(iconv('UTF-8', 'ASCII//TRANSLIT', $string), 'dash', true);
    if(strlen($slug)>30){
        return substr($slug,0,30).'...';
    }

    return substr($slug,0,30);
}

function slugify($string){
    $ci = &get_instance();
    $ci->load->helper('text');

    return url_title(convert_accented_characters($string), 'dash', true);
}

function getDataTable(){

    $str = '';
    $str .= '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"/>';
    //$str .= '<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>';
    $str .= '<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>';


    $str .= '<script>$(document).find("#data-table").dataTable({});</script>';
    return $str;

}


function faviconSizes()
{
    return array(
        '32' => 32,
        '57' => 57,
        '76' => 76,
        '96' => 96,
        '120' => 120,
        '144' => 144,
        '195' => 195,
        '228' => 228,
        '128' => 128,
        '152' => 152,
        '167' => 167,
        '180' => 180,
        '192' => 192,
        '196' => 196
    );
}

function isDeveloper(){
    $ci = & get_instance();
    $developers = $ci->cache->file->get('developers');

    if($ci->session->has_userdata('user_id') && !empty($developers)) {
        if(in_array($ci->session->userdata('user_id'),$developers)){
            return true;
        }
    }

    return false;
}


function generateToken($length = 10)
{
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getPropertyDefaultLang(){
    $ci = & get_instance();

    if($property_settings = $ci->cache->file->get('property_ci_'.$ci->session->userdata('active_property'))){
        if(isset($property_settings['default_language']) && !empty($property_settings['default_language'])) {
            return $property_settings['default_language'];
        }
    }

    return 'sk';
}

function controller_url($path){
    $ci = & get_instance();
    return base_url($ci->uri->segment(1).'/'.$ci->uri->segment(2).'/'.$path);
}

function applicationLangs($return_all_languages = false)
{
    $ci = & get_instance();

    $languages = array(
        '0' => 'sk',
        '1' => 'en',
        '2' => 'pl',
        '3' => 'de',
        '4' => 'hu',
        '5' => 'ru',
        '6' => 'cz',
        '7' => 'fr'
    );

    if($return_all_languages){
        return $languages;
    }

    $response = array();

    if($property_settings = $ci->cache->file->get('property_ci_'.getActiveProperty())){
        if(isset($property_settings['public_languages']) && !empty($property_settings['public_languages'])){
            foreach($languages as $language){
                if(in_array($language,$property_settings['public_languages'])){
                    $response[$language] = $language;
                }
            }
        }
        if(isset($property_settings['default_lang']) && !empty($property_settings['default_lang']) && !isset($response[$property_settings['default_lang']])){
            $response[$property_settings['default_lang']] = $property_settings['default_lang'];
        }
    }

    if(empty($response)){
        $response[0] = 'sk';
    }


    return $response;
}

function getPaginationConfig()
{
    $config['full_tag_open']   = "<ul class='pagination'>";
    $config['full_tag_close']  = '</ul>';
    $config['num_tag_open']    = '<li>';
    $config['num_tag_close']   = '</li>';
    $config['cur_tag_open']    = '<li class="active"><a href="#">';
    $config['cur_tag_close']   = '</a></li>';
    $config['prev_tag_open']   = '<li>';
    $config['prev_tag_close']  = '</li>';
    $config['first_tag_open']  = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open']   = '<li>';
    $config['last_tag_close']  = '</li>';

    $config['prev_link']      = '<i class="fa fa-long-arrow-left"></i>Predchádzajúca stránka';
    $config['prev_tag_open']  = '<li>';
    $config['prev_tag_close'] = '</li>';

    $config['next_link']      = 'Nasledujúca stránka<i class="fa fa-long-arrow-right"></i>';
    $config['last_link']      = 'Posledná stránka';
    $config['first_link']     = 'Prvá stránka';
    $config['next_tag_open']  = '<li>';
    $config['next_tag_close'] = '</li>';

    return $config;
}

function savedToDBStatus()
{
    return 'savedToDB';
}

function savedToBGStatus()
{
    return 'savedToBG';
}

function errorItemsStatus()
{
    return 'errorItems';
}

function errorSaveToBGStatus()
{
    return 'errorSaveToBG';
}

function payedAndDontSavedToBGStatus(){
    return 'payedAndDontSavedToBG';
}

function savedToBGAndPayedStatus(){
    return 'savedToBGAndPayed';
}

function successPayment(){
    return 'successPayed';
}

function errorWhilePayment()
{
    return 'errorPayment';
}

function changedPriceStatus()
{
    return 'changedPrice';
}



function random_color_part() {
    return str_pad( rand( 0, 255 ), 3, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() .','. random_color_part() .','. random_color_part();
}

function dateToMicroSeconds($date)
{
    $explodedDate = explode('+', $date);
    return $explodedDate[0] . '.000+0000';
}

function generateOpeningHours($selected = '')
{
    $startTime = '00:15';
    $options   = '';
    for ($i = 1; $i < 96; $i++) {
        if ($selected && $selected == $startTime) {
            $options .= '<option selected value="' . $startTime . '">' . $startTime . '</option>';
        } else {
            $options .= '<option value="' . $startTime . '">' . $startTime . '</option>';
        }
        $startTime = date('H:i', strtotime('+ 15 minutes', strtotime($startTime)));
    }
    return $options;

}

function formatNumApi($num)
{
    return number_format(str_replace(',', '.', $num), '2', '.', '');
}

function getSummernote(){
    $str = '';
    $str .= '<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>';
    $str .= '<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">';

    return $str;
}

function getDateTimePicker(){
    $str = '<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous"></script>';
    $str .= '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" integrity="sha512-tjNtfoH+ezX5NhKsxuzHc01N4tSBoz15yiML61yoQN/kxWU0ChLIno79qIjqhiuTrQI0h+XPpylj0eZ9pKPQ9g==" crossorigin="anonymous" />';

    return $str;
}

function line_with_argument($lang,$variable){
    return str_replace('%s',$variable,$lang);
}

function line_with_arguments($lang,$variables = array()){
    $exploded = explode('%s',$lang);

    $str = '';

    foreach($exploded as $key => $explode){
        if(isset($variables[$key]) && !empty($variables[$key])){
            $str .= $explode.' '.$variables[$key].' ';
        }
        else{
            $str .= $explode.' ';
        }
    }

    return $str;
}

function set_message($key,$val){
    $ci = & get_instance();
    $ci->session->set_flashdata($key,$val);
}

function getSelect2(){
    $str = '';
    $str .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>';
    $str .= '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">';
    $str .= '<script>$(document).find(".select2").select2();</script>';

    $str .= '<style> .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #2097f3 !important;
        color: white !important;
        border: 1px solid #2097f3 !important;
        /* padding: 14px; */
        border-radius: 0 !important;
        padding: 2px 8px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        color:white !important;
    }</style>';

    return $str;
}

function getActiveProperty(){
    $ci = & get_instance();

    return $ci->session->userdata('active_property');
}

function imageSizes(){
    return array(
        'thumb' => 300,
        'medium' => 700,
        'large' => 1280
    );
}

function priceDefinitions(){
    return array(
        'two_decimals' => '',
        'one_decimals' => '',
        'no_decimals' => '',
    );
}

function priceSeparators(){
    return array(
        'dot' => '.',
        'comma' => ','
    );
}

function currencies(){

    $ci = & get_instance();
    if($currencies = $ci->cache->file->get('currencies')){
        return $currencies;
    }

    return $currencies;
}

function orderTypes(){
    return array(
        'letter_up' => 'letter_up',
        'letter_down' => 'letter_down',
        'price_up' => 'price_up',
        'price_down' => 'price_down',
        'rating_up' => 'rating_up',
        'rating_down' => 'rating_down'
    );
}

function getUploadPath($type,$custom,$only_one = true){
    $array = array();

    foreach(imageSizes() as $prefix => $size){
        $array[$prefix] = base_url('uploads/'.getActiveProperty().'/'.$type.'/'.implode('-'.$prefix.'.',explode('.',$custom)));
        if($only_one){
            return base_url('uploads/'.getActiveProperty().'/'.$type.'/'.implode('-'.$prefix.'.',explode('.',$custom)));
        }
    }

    return $array;
}

function getUploadPathFavicon($type,$custom){
    $array = array();

    foreach(faviconSizes() as $prefix => $size){
        $array[$prefix] = base_url('uploads/'.getActiveProperty().'/'.$type.'/'.implode('-'.$prefix.'.',explode('.',$custom)));
    }

    return $array;
}

function getUploadPathLogo($type,$custom){

    return  base_url('uploads/'.getActiveProperty().'/'.$type.'/'.$custom);
}

function getUploadPaths($type,$custom = null){
    $default_path = $upload_dir = './uploads/'.getActiveProperty().'/';

    switch($type){
        case 'products':
        case 'product':
            $upload_dir .= 'products/';
            break;
        case 'logo':
        case 'logos':
            $upload_dir .= 'logo/';
            break;
        case 'favicon':
        case 'favicons':
            $upload_dir .= 'favicon/';
            break;
        case 'variation':
        case 'variations':
            $upload_dir .= 'variations/';
            break;
    }

    if($upload_dir !== $default_path && $custom){

        $array = array();

        foreach(imageSizes() as $prefix => $size){
            $array[$prefix] = $upload_dir.implode('-'.$prefix.'.',explode('.',$custom));
        }

        return $array;
    }

    return $upload_dir;

}

function languagesSelect(){
   $str =  ' <select id="language-changer" class="form-control" style="min-width: 250px">';
   foreach (applicationLangs() as $language){
        $selected = $language === getPropertyDefaultLang() ? 'selected' : '';
        $str .= '<option '.$selected.' value="'.$language.'">'.lang('langshort.' . $language).'</option>';
   }
   $str .= '</select>';

   $str .= '<script>';
   $str .= '$(document).on("change","#language-changer",function(){$(document).find("language").hide(); $(document).find("language-"+$(this).val()).show();});';
   $str .= '</script>';

   $str .= '<style>.language{display:none} .language-'.getPropertyDefaultLang().'{display:block}</style>';

   return $str;
}

function _view($view,$data = array()){
    $ci = &get_instance();
    $ci->template->load('master',$ci->uri->segment(2).'/'.$view,$data);
}

function _partial($view,$data = array()){
    $ci = &get_instance();
    return $ci->load->view($ci->uri->segment(2).'/'.$view,$data,true);
}

function _return($view = null){
    $ci = &get_instance();

    if($view){
        redirect($ci->uri->segment(1).'/'.$ci->uri->segment(2).'/'.$view);
    }
    else{
        redirect($ci->uri->segment(1).'/'.$ci->uri->segment(2));
    }
}

function buildTree(array &$elements, $parentId = 0,$parent_key = 'parent_id') {
    $branch = array();

    foreach ($elements as $element) {
        if ($element[$parent_key] == $parentId) {
            $children = buildTree($elements, $element['id'], $parent_key );
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
}

function getNestable(){
   $str = '<link href="'.asset_url('css/MyApp.css').'" rel="stylesheet" type="text/css">';
   $str .= '<script src="'.asset_url('js/jquery.nestable2.js').'"></script>';

   return $str;
}

function getImageUpload($title,$name,$image = null){
    $str = '<div class="form-group">';
        $str .= '<label>'.$title.'</label>';
        $str .= '<input type="file" name="'.$name.'" class="form-control image-preview" hidden>';
        $str .= '<div class="row">';
            $hidden = $image ? '' : 'hidden';
            $str .= '<div class="col-md-12 mb-3 p-2">';
                $str .= '<div class="col-md-12 text-center" style="border:1px solid #dddddd;min-height:200px;">';
                    $str .= '<img src="'.$image.'" '.$hidden.' style="object-fit:contain;" height="200px;">';
                $str .= '</div>';
            $str .= '</div>';
            $str .= '<input type="hidden" value="0" name="delete_'.$name.'">';

                $hidden = !$image ? '' : 'hidden';
                $str .= '<div class="col-md-12">';
                    $str .= '<button type="button" '.$hidden.' data-name="'.$name.'" class="btn btn-upload w-100">'.__('app.upload').'</button>';
                $str .= '</div>';

                $hidden = $image ? '' : 'hidden';
                $str .= '<div class="col-md-6">';
                    $str .= '<button type="button" '.$hidden.' data-name="'.$name.'" class="btn w-100 btn-replace">'.__('app.replace').'</button>';
                $str .= '</div>';
                $str .= '<div class="col-md-6">';
                    $str .= '<button type="button" '.$hidden.' data-name="'.$name.'" class="btn w-100 btn-delete">'.__('app.delete').'</button>';
                $str .= '</div>';
        $str .= '</div>';
    $str .= '</div>';


    $str .= '<script>';
        $str .= '$(".btn-upload").off("click");';
        $str .= '$(".btn-delete").off("click");';
        $str .= '$(".btn-replace").off("click");';
        $str .= '$(".image-preview").off("input");';

        $str .= '$(document).on("click",".btn-replace",function(e){
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            
            var _this = $(this),
                _data = _this.data(),
                _form_group = $(this).closest(".form-group");

            console.log("click");

            _form_group.find(\'input[name="delete_\'+_data.name+\'"]\').val(0);

            _form_group.find(\'[name="\'+_data.name+\'"]\').click();
        });

        $(document).on("click",".btn-upload",function(e){
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            
            var _this = $(this),
                _data = _this.data(),
                _form_group = $(this).closest(".form-group");

            _form_group.find(\'[name="\'+_data.name+\'"]\').click();

            _form_group.find(\'input[name="delete_\'+_data.name+\'"]\').val(0);

        });

        $(document).on(\'input\',\'.image-preview\',function(e){
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            
            var _self = $(this),
                _element = $(this).closest(".form-group");

            readURL(_element.find(\'img\'));
        });

        $(document).on("click",".btn-delete",function(e){
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            
            var _this = $(this),
                _data = _this.data(),
                _form_group = $(this).closest(".form-group");

            _form_group.find(\'img\').attr(\'src\',\'\');
            _form_group.find(\'input[name="delete_\'+_data.name+\'"]\').val(1);
            _form_group.find(\'[name="\'+_data.name+\'"]\').val(\'\');

            _form_group.find(".btn-upload").attr(\'hidden\',false);
            _form_group.find(".btn-delete").attr(\'hidden\',true);
            _form_group.find(".btn-replace").attr(\'hidden\',true);

        });

        var readURL = function(element) {
            var wrapper = $(element).closest(".form-group"),
                input = wrapper.find(\'input[type="file"]\');


            wrapper.find(".btn-upload").attr(\'hidden\',false);
            wrapper.find(".btn-delete").attr(\'hidden\',true);
            wrapper.find(".btn-replace").attr(\'hidden\',true);
            //wrapper.find(".btn-delete").trigger(\'click\');

            //_form_group.find(\'input[name="delete_\'+_data.name+\'"]\').val(1);

            if(input.length > 0){
                if (input[0].files[0] && input[0].files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                    $(element)
                        .attr(\'src\', e.target.result);
                    };

                    reader.readAsDataURL(input[0].files[0]);

                    //wrapper.find(\'input[name="delete_\'+_data.name+\'"]\').val(0);

                    wrapper.find(".btn-upload").attr(\'hidden\',true);
                    wrapper.find(".btn-delete").attr(\'hidden\',false);
                    wrapper.find(".btn-replace").attr(\'hidden\',false);
                }
            }
        }';
    $str .= '</script>';

    return $str;
}

function nestableView($array){
    $ci = & get_instance();

    return $ci->load->view('dashboard/partials/nestable',array('array'=>$array),true);
}

function getImageUploadFile(){
    $str = '<script src="'.asset_url('dist/image-uploader.min.js').'"></script>';
    $str .= '<link href="'.asset_url('dist/image-uploader.min.css').'" rel="stylesheet" type="text/css">';

    return $str;
}

function getBreadcrumb($as = array(),$buttons = array(),$languages = false){
    $ci = & get_instance();

    return $ci->load->view('dashboard/partials/breadcrumb',array('as'=>$as,'buttons'=>$buttons,'languages'=>$languages),true);
}

function __($str){
    return lang($str) ? lang($str) : $str;
}