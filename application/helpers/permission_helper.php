<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function has_permission($method = false,$controller = false) {
    $ci = & get_instance();

    if(isDeveloper()){
        return true;
    }

    if(!$controller){
        $controller = $ci->uri->segment(2);
    }

    if($method){
        if($ci->session->has_userdata('permissions')) {
            $permissions = $ci->session->userdata('permissions');
            if ($controller) {
                if (isset($permissions[$controller][$method]) && !empty($permissions[$controller][$method])) {
                    return true;
                }
            }
        }
    }

    return false;
}
