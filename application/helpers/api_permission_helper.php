<?php


function has_api_permission($method,$controller = false){
    $ci = & get_instance();
    $api_key = $ci->input->get_request_header('Api-Key');

    if(!$controller){
        $controller = $ci->uri->segment(3);
    }

    if($permissions = $ci->cache->file->get('api_permissions')[$api_key]){
        if(isset($permissions[$controller][$method]) && !empty($permissions[$controller][$method])){
            return true;
        }
    }

    return false;
}