<?php

function successResult($data = FALSE)
{
	if ($data)
	{
		return array(
			'status' => TRUE,
			'data' => $data
		);
	}
	else
	{
		return array('status' => TRUE);
	}
}

function successResultWithWarning($message, $data = FALSE)
{
	if ($data)
	{
		return array(
			'status' => TRUE,
			'message' => $message,
			'data' => $data
		);
	}
	else
	{
		return array(
			'status' => TRUE,
			'message' => $message
		);
	}
}

function unsuccessResult($message, $data = FALSE, $code = FALSE)
{

	if (!empty($data))
	{
	    if($code != FALSE){
            return array(
                'status' => FALSE,
                'message' => $message,
                'data' => $data,
                'code' => $code,
            );
        }else{
            return array(
                'status' => FALSE,
                'message' => $message,
                'data' => $data
            );
        }

	}
	else
	{
        if($code != FALSE){
            return array(
                'status' => FALSE,
                'message' => $message,
                'code' => $code,
            );
        }else {
            return array(
                'status' => FALSE,
                'message' => $message
            );
        }
	}

}