<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Titan testovacia platobna metóda</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<!-- JS, Popper.js, and jQuery -->
	<?php
		$url = 'booking/payment/result/'.$token.'/'.$systemBookingID;
		if(isset($customUrl) && !empty($customUrl))
		{
			$url = $customUrl;
		}
	?>
</head>
<body style="background-color: aliceblue">
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center" style="position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);">
			<h1 class="text-center">Testovacia platobná brána</h1>
			<div class="row ml-auto">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<a href="<?=base_url($url.'?res=ok')?>" class="btn btn-success" style="width:100%">Simulovať zaplatenie</a>
						</div>
						<div class="col-md-6">
							<a href="<?=base_url($url.'?res=not')?>" class="btn btn-danger" style="width:100%">Simulovať nezaplatenie</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>
