<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
        if($this->uri->segment(2)){
            $title = lang('segment.'.$this->uri->segment(2));
        }
    ?>
    <title><?=(isset($title)) ? 'Warehouse | '.$title : 'Warehouse'?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">-->
<!--    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">-->
    <link href="<?=asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=asset_url('css/customStyle.css')?>" rel="stylesheet" type="text/css">

    <link href="<?=asset_url('css/jquery.mswitch.css')?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?=asset_url('global/js/main/jquery.min.js')?>"></script>
<!--    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>-->
    <script src="<?=asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?=asset_url('global/js/main/jquery-ui.min.js')?>"></script>
    <script src="<?=asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <script src="<?=asset_url('js/resizable-rotation.patch.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

    <script type="text/javascript" src="<?=asset_url('global/js/plugins/ui/moment/moment.min.js')?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">



    <script src="<?=asset_url('global/js/plugins/extensions/jquery.nestable.js')?>"></script>


    <!-- /core JS files -->

    <!-- Theme JS files -->


    <script src="<?=asset_url('js/app.js')?>"></script>
    <script src="<?=asset_url('js/jquery.mswitch.js')?>"></script>
    <script src="<?=asset_url('global/js/plugins/visualization/echarts/echarts.min.js')?>"></script>


    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>

</head>

<body class="<?=isset($full_size_menu) && !empty($full_size_menu) ? 'sidebar-xs' : ''?>">
<style>
    .navbar-brand {
        display: inline-block;
        padding-top: 11px;
        padding-bottom: 0px;
    }
    .language {
        display: none;
    }

    .language-<?= getPropertyDefaultLang() ?> {
        display: block;
    }


    .upload-preview{
        height:400px !important;
        background-position:center;
        background-size:cover;
        background-repeat: no-repeat;
    }

    .form-control,
    .btn,
    .select2-container--default .select2-selection--single,
    .select2-container--default .select2-selection--multiple,
    .badge,
    .card
    {
        border-radius:0 !important
    }

    .image-background{
        width:100%;
        height:200px;
        background-repeat: no-repeat;
        background-size:cover;
    }
</style>

<?php if(isset($active_dark_mode) && !empty($active_dark_mode)):?>
    <?=$this->load->view('layout/dark_mode',null,true)?>
<?php endif;?>

<?php
    $this->load->view('layout/main_nav');
?>


<!-- Page content -->
<div class="page-content">
    <?=$this->load->view('layout/main_sidebar',array('navs' => $navs),true)?>
    <?=$body?>

</div>
<!-- /page content -->

<script src="<?=asset_url('ckeditor/ckeditor.js')?>"></script>


<script>
    var summernotes = $(document).find('.summernote');

    if(summernotes.length > 0 ){
        summernotes.each(function(){
            CKEDITOR.replace($(this).attr('name'));
        });
    }

    $(document).on('click','.remove-image',function(){
        var _self = $(this),
            _default_parent = '.col-md-6';

        $(document).find('[name="'+_self.data('name')+'"]').val('1');

        _self.closest('.row').parent().siblings('.upload-preview').remove();
        _self.closest('.row').parent().remove();
    });

   // $(document).ready(function(){
        var body = $('body');
        if(getCookie('expanded')!==null){
            if(getCookie('expanded')==='1'){
                body.addClass('sidebar-xs');
            }
            else{
                if(body.hasClass('sidebar-xs')){
                    body.removeClass('sidebar-xs');
                }
                else {
                }
            }
        }
        else{
        }
   // });

    $(document).on('click','.sidebar-main-toggle',function(){
        var body = $('body');
        if(body.hasClass('sidebar-xs')){
            setCookie('expanded','1','365');
        }
        else{
            setCookie('expanded','0','365');
        }
    });

    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    function eraseCookie(name) {
        document.cookie = name+'=; Max-Age=-99999999;';
    }

    var internalNameCounter = function(_this,_maxLength){
        var _length = (+_maxLength)-(+_this.val().length);
        if(_this.siblings('small').length>0){
            _this.siblings('small').remove();
        }
        if(_length == 1){
            _this.after('<small class="loaderInternalName"> Ostáva: <strong style="color:#2096f3;">'+_length+'</strong> znak</small>');
        }
        else if(_length > 2 && _length < 5 ){
            _this.after('<small class="loaderInternalName"> Ostávajú: <strong style="color:#2096f3;">'+_length+'</strong> znaky</small>');
        }
        else{
            _this.after('<small class="loaderInternalName"> Ostáva: <strong style="color:#2096f3;">'+_length+'</strong> znakov</small>');
        }
    }

    $(document).ready(function(){
        var _counters = $(document).find('.internalNameCounter'),
            _maxLength = 30;
        _counters.each(function(){
            var _this = $(this);
            _this.attr('maxlength',_maxLength);
            internalNameCounter($(this),_maxLength);
        });

        $(document).on('input','.internalNameCounter',function(){
            internalNameCounter($(this),_maxLength);
        });
    });

    $(document).ready(function(){
       var _elements = $(document).find('[data-max_length]');
       _elements.each(function(){
          internalNameCounter($(this),$(this).data().max_length);
       });

       _elements.on('input',function(){
           internalNameCounter($(this),$(this).data().max_length);
       })
    });

    var swalSuccessMessageSession = function(text,icon='success'){
        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: icon,
            title: text
        });
    }

    <?php if($this->session->flashdata('successMessage')):?>
        swalSuccessMessageSession('<?=$this->session->flashdata('successMessage')?>');
    <?php endif;?>



    var swalAlertDelete = function(path,message1=null,message2=null,buttonText = null){
        Swal.fire({
            icon: 'question',
            title: message1 ? message1 : 'Zmazanie záznamu',
            text: message2 ? message2 : 'Naozaj chcete zmazať tento záznam?',
            showConfirmButton:true,
            showCancelButton:true,
            confirmButtonText: buttonText ? buttonText : 'Zmazať',
            cancelButtonText:'Zrušiť',
        }).then(function(result){
            if(result.value){
                window.location.href = path;
            }
        });
    }

    $('#language-changer').on('change', function() {
        $('.language').hide();
        $('.language-' + $(this).val()).show();
    });

    $('.delete').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        swalAlertDelete($(this).attr('href'));
    });

</script>



</body>
</html>
