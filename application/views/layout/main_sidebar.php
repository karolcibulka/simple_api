<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->

    <!-- Sidebar content -->
    <div class="sidebar-content" style="position:sticky;top:0;">
        <?php if(isset($properties_ci) && !empty($properties_ci) && count($properties_ci)>0):?>
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs"><?=lang('nav.property')?></div> <i class="icon-menu" title="<?=lang('nav.property')?>"></i></li>

                <li class="nav-item nav-item-submenu nav-item-expanded">
                    <?php if(isset($properties_ci[$property_id_ci]) && !empty($properties_ci[$property_id_ci])):?>
                        <a href="#" class="nav-link"><i class="icon-list"></i> <span><?=$properties_ci[$property_id_ci]['name']?></span></a>
                    <?php else:?>
                        <a href="#" class="nav-link"><i class="icon-list"></i> <span><?=__('app.no_property')?></span></a>
                    <?php endif;?>
                    <ul class="nav nav-group-sub" data-submenu-title="<?=lang('nav.properties')?>" style="display:none;max-height:210px;overflow-y: scroll;margin-bottom:5px;">
                        <?php if(count($properties_ci)>3):?>
                            <li class="nav-item" style="padding:10px;position:absolute;width:100%;height:50px;z-index:30;background-color:#253238;margin-bottom:3px;">
                                <input type="text" class="form-control propertySearcher" style="border-radius:0;" placeholder="Vyhľadať zariadenie">
                            </li>
                            <li style="margin-top:50px;"></li>
                        <?php endif;?>
                        <?php foreach($properties_ci as $property):?>
                        <li class="nav-item changePropertyId" data-property_id="<?=$property['id']?>">
                            <a href="#" class="nav-link <?=($property['id'] === $property_id_ci) ? 'active' : '' ?>">
                              <span><?=$property['name']?></span>
                            </a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </li>
            </ul>
        </div>
        <?php endif;?>

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs"><?=lang('nav.main')?></div> <i class="icon-menu" title="<?=lang('nav.main')?>"></i></li>

                <?php foreach($navs as $nav) :?>
                    <?php if(($nav['active'] == TRUE && $nav['deleted'] == FALSE && $nav['type'] == 'controller') || ($nav['active'] == TRUE && $nav['deleted'] == FALSE && $nav['type'] == 'link')):?>
                        <li class="nav-item">
                            <?php if(isset($nav['path']) && $nav['path'] != '#'):?>
                                <a href="<?=base_url('dashboard/'.$nav['path'])?>" class="nav-link <?=($nav['path'] == $this->uri->segment(2)) ? 'active' : ''?> ">
                            <?php elseif($nav['link'] == TRUE):?>
                                <a href="<?=$nav['link_path']?>" class="nav-link <?=($nav['path'] == $this->uri->segment(2)) ? 'active' : ''?> ">
                            <?php endif ?>

                                <?php if(isset($nav['icon']) && !empty($nav['icon'])):?>
                                <i class="icon-<?=$nav['icon']?>"></i>
                                <?php else :?>
                                    <i class="icon-pencil2"></i>
                                <?php endif;?>
                                <span><?=lang('segment.'.$nav['path'])?></span>
                            </a>
                        </li>
                    <?php endif;?>
                    <?php if ($nav['active']==TRUE && $nav['deleted'] == FALSE && $nav['type'] == 'placeholder' && isset($nav['children'])) :?>
                    <?php
                        $checkNav = array();
                        foreach($nav['children'] as $ch){
                            $checkNav[]=$ch['path'];
                        }
                        ?>
                        <li class="nav-item nav-item-submenu nav-item-expanded <?=(isset($checkNav) && in_array($this->uri->segment(2),$checkNav)) ? 'nav-item-open' : ''?>">
                            <a href="#" class="nav-link"><i class="icon-<?=$nav['icon']?>"></i> <span><?=lang('segment.'.$nav['name']) ? lang('segment.'.$nav['name']) : $nav['name']?></span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="<?=lang('segment.'.$nav['name']) ? lang('segment.'.$nav['name']) : $nav['name']?>" style="<?=(isset($checkNav) && in_array($this->uri->segment(2),$checkNav)) ? 'display:block' : 'display:none'?>;">
                                <?php foreach ($nav['children'] as $child):?>
                                <?php if (($child['active']==TRUE && $child['deleted'] == FALSE && $child['type'] == 'link') || $child['active']==TRUE && $child['deleted'] == FALSE && $child['type'] == 'controller') :?>
                                    <li class="nav-item ">
                                        <?php if(isset($child['path']) && $child['path'] != '#'):?>
                                                <a href="<?=base_url('dashboard/'.$child['path'])?>" class="nav-link <?=($child['path'] == $this->uri->segment(2)) ? 'active' : ''?>">
                                        <?php elseif($child['link'] == TRUE):?>
                                                <a href="<?=$child['link_path']?>" class="nav-link <?=($child['path'] == $this->uri->segment(2)) ? 'active' : ''?>">
                                        <?php endif;?>
                                            <?php if(isset($child['icon']) && !empty($child['icon'])) :?>
                                                <i class="icon-<?=$child['icon']?>"></i>
                                            <?php else:?>
                                                <i class="icon-pencil2"></i>
                                            <?php endif;?>
                                            <span><?=lang('segment.'.$child['path'])?></span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php endforeach;?>
                            </ul>
                        </li>
                    <?php endif;?>
                <?php endforeach?>


            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->
<script>
    $(".changePropertyId").on('click',function(e) {
        var id = $(this).data().property_id;
        var url = '<?=base_url('dashboard/properties/handleChangeProperty')?>/'+id;

        $.ajax({
            type: "POST",
            url: url,
            dataType:"json",
            data:id,
            success : function(data){
                if(data.status === '1'){
                    window.location.reload();
                }
            }
        });
    });

    $('.propertySearcher').on('input',function(){
       var _val = $(this).val(),
            _lis = $(this).closest('ul').find('.changePropertyId');

       if(_val!==""){
           _lis.each(function(){
               if($(this).find('span').html().toLowerCase().includes(_val.toLowerCase())){
                   $(this).show();
               }
               else{
                   $(this).hide();
               }
           });
       }
       else{
           _lis.show();
       }
    });
</script>