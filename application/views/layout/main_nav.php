<!-- Main navbar -->

<style>
    .activeLang{
        background-color:#10a69a;
        color:white;
    }
    .activeLang:hover{
        background-color:#10a69a;
        color:white;
    }
</style>
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="<?=base_url('dashboard/dashboard')?>" class="d-inline-block">
            <h5 style="margin:0;font-weight: bold;color:white;"><?=$projectName?></h5>
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span>
                        <img src="<?=asset_url('images/flags/'.$this->session->userdata('user_lang').'.svg')?>" style="<?=$this->session->userdata('user_lang') === 'pl' ? 'transform:rotate(180deg);' : ''?>" height="20px" alt="">
                        <?=lang('langshort.'.$this->session->userdata('user_lang'))?>
                    </span>
                </a>

                <div class="dropdown-menu dropdown-menu-left">
                    <?php foreach(applicationLangs(true) as $lang => $language):?>
                        <?php if($language !== $this->session->userdata('user_lang')):?>
                            <a href="<?=base_url('dashboard/user/changeLanguage/'.$language)?>" class="dropdown-item">
                                <img src="<?=asset_url('images/flags/'.$language.'.svg')?>" style="<?=$language === 'pl' ? 'transform:rotate(180deg);' : ''?>" height="20px" alt="">
                                <?=lang('langshort.'.$language)?>
                            </a>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span><?=$this->session->userdata('email');?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="<?=base_url('dashboard/dashboard/activeMode')?>" class="dropdown-item"><i class="icon-eye"></i><?=isset($active_dark_mode) && !empty($active_dark_mode) ? 'Light mode' : 'Dark Mode'?></a>
                    <a href="<?=base_url('dashboard/user/changePassword')?>" class="dropdown-item"><i class="icon-user-check"></i><?=__('app.change_password')?></a>
                    <a href="<?=base_url('dashboard/auth/logout')?>" class="dropdown-item"><i class="icon-switch2"></i><?=lang('menu.logout')?></a>
                </div>
            </li>
            
        </ul>
    </div>
</div>
<!-- /main navbar -->