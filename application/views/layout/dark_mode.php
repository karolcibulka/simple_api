<style>
    .content{
        background-color: #202a2d !important;
        color:#f1f1f1 !important;
    }

    .content-wrapper > .page-header.page-header-light >.breadcrumb-line.breadcrumb-line-light.header-elements-md-inline{
        background-color: #263235 !important;
        color: white !important;
    }

    .content-wrapper > .page-header.page-header-light >.page-header-content.header-elements-md-inline{
        background-color: #3e474a !important;
        color: white !important;
    }

    .card-body{
        background-color:#363c3e !important;
    }

    .nav-tabs .nav-item.show 
    .nav-link, 
    .nav-tabs .nav-link.active{
        background-color: #3e474a !important;
        color: white !important;
    }

    .form-control,
    .select2-container--default
    .select2-selection--single
    .select2-selection__rendered,
    .select2-container
    .select2-selection--multiple,
    .select2-container--default.select2-container--focus
    .select2-selection--multiple{
        background-color:#4c4c4c !important;
        color:white !important;
    }

</style>