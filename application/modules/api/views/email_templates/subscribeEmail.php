<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <title><?= lang('email.acceptingRes')?> <?= $reservation['id']?></title>
    <style type="text/css">#outlook a{padding:0;}body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}.ExternalClass p{line-height:inherit;}#body-layout{margin:0;padding:0;width:100%!important;line-height:100%!important;}img{display:block;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}a img{border:none;}table td{border-collapse:collapse;}table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}a{color:inherit;outline:none;text-decoration:none;}</style>
    <style type="text/css">@media only screen and (max-width: 599px){div[class="block"]{width:100%!important;max-width:none!important;}table[class="full-width"]{width:100%!important;}td[class="mobile-padding"]{padding:0 0 20px 0!important;}td[class="normalize-height"]{height:auto!important;}table[class="center"],td[class="center"]{text-align:center!important;float:none!important;}table[class="left"],td[class="left"]{text-align:left!important;float:left!important;}table[class="right"],td[class="right"]{text-align:right!important;float:right!important;}table[class="remove"],tr[class="remove"],span[class="remove"]{display:none;}}</style>
</head>
<body id="body-layout">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #ffffff;">
    <tr>
        <td align="center" valign="top">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                <tr>
                    <td align="center" valign="top">
                        <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                            <tr>
                                <td width="600" align="center" valign="top">
                                    <!-- Hotel LOGO BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:100%;">
                                                                            <tr>
                                                                                <td align="center" valign="middle" style="padding: 0" class="normalize-height">
                                                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                        <tr><td height="20" style="height:20px;"></td></tr>
                                                                                        <tr>
                                                                                            <td align="center" valign="top" style="line-height: 0px !important;">
                                                                                                <img src="<?=$property['logo']?>" alt="" width="200" style="max-width: 200px; height: auto;"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:30px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Hotel LOGO END -->
                                    <!-- HEADER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #f0f0f0;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #1E272D; font: 20px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 30px; font-weight: bold; text-align:center;">
                                                                        <?=lang('email.actSubscribeH')?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="18" style="height: 18px; line-height: 18px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- HEADER END -->

                                    <!-- KAPACITY BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                        <tr>
                                            <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">
                                                <?=lang('email.thxSubscribe')?>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- KAPACITY END -->


                                    <!-- PLATBY BEGIN -->

                                    <!-- PLATBY END -->


                                    <!-- 2 COLUMN BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                            <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="1" align="center"><tr><![endif]--><!--[if (gte mso 9)|(IE)]><td width="298" align="center" valign="top"><![endif]-->
                                                            <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 298px; padding:1px;" class="block">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin:  0; padding: 0;background: #fff;"><tr><td height="20" style="height:20px;"></td></tr></table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]><td width="298" align="center" valign="top"><![endif]-->
                                                            <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 298px; padding:1px;" class="block">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin:  0; padding: 0;background: #fff;"><tr><td height="20" style="height:20px;"></td></tr></table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- 2 COLUMN END -->

                                    <!-- INFO BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                    <?=lang('email.actSubscribe')?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">
                                                                                    <?=lang('email.actBellow')?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">

                                                                                    <a href="<?=$activatePath?>" style="background-color:black;color:white;width:100%;padding:10px"><?=lang('email.activate')?></a>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">
                                                                                    <small>
                                                                                        <?=lang('email.htmlRender')?>
                                                                                        <a href="<?=$activatePath?>"><?=$activatePath?></a>
                                                                                    </small>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                   <?=lang('email.deactSubscribe')?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">

                                                                                  <?=lang('email.deactBellow')?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">

                                                                                    <a href="<?=$deactivatePath?>" style="background-color:black;color:white;width:100%;padding:10px"> <?=lang('email.deactivate')?></a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td  align="center" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:center;">
                                                                                   <small>
                                                                                       <?=lang('email.htmlRender')?>
                                                                                       <a href="<?=$deactivatePath?>"><?=$deactivatePath?></a>
                                                                                   </small>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- INFO END -->


                                    <!-- FOOTER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: black;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 30px; line-height: 30px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #ffffff; font: 18px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 22px; font-weight: 700;">
                                                                        <?= $property['name'] ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td width="500" align="center" valign="top">
                                                                        <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"><tr><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-phone.png" height="24" width="24" alt="Phone" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp; <a href="tel:<?=$property['contact']['phone']?>" style="color:#fff;text-decoration:none;"><?= $property['contact']['phone']?></a></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-email.png" height="24" width="24" alt="E-mail" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp;
                                                                                        <a href="mailto:<?=$property['contact']['email']?>" style="color:#fff;text-decoration:none;">
                                                                                            <?php
                                                                                            echo $property['contact']['email'];
                                                                                            ?>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 40px; line-height: 40px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- FOOTER END -->

                                    <?php if(!empty($property['social']['facebook']) || !empty($property['social']['instagram'])):?>
                                        <!-- SOCIAL BEGIN -->
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: black;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                        <tr>
                                                            <td align="center" valign="top" style="padding: 0 20px;">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td height="30" style="height: 20px; line-height: 20px;"></td>
                                                                    </tr>
                                                                </table>
                                                                <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td width="600" align="center" valign="top">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal;">
                                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                                <tr>
                                                                                                    <td height="10" style="height: 10px; line-height: 10px;"></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td height="40" style="height: 30px; line-height: 30px;"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- SOCIAL END -->
                                    <?php endif;?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>