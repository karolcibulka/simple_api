<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <title><?= lang('email.acceptingRes')?> <?= $reservation['id']?></title>
    <style type="text/css">#outlook a{padding:0;}body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}.ExternalClass p{line-height:inherit;}#body-layout{margin:0;padding:0;width:100%!important;line-height:100%!important;}img{display:block;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}a img{border:none;}table td{border-collapse:collapse;}table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}a{color:inherit;outline:none;text-decoration:none;}</style>
    <style type="text/css">@media only screen and (max-width: 599px){div[class="block"]{width:100%!important;max-width:none!important;}table[class="full-width"]{width:100%!important;}td[class="mobile-padding"]{padding:0 0 20px 0!important;}td[class="normalize-height"]{height:auto!important;}table[class="center"],td[class="center"]{text-align:center!important;float:none!important;}table[class="left"],td[class="left"]{text-align:left!important;float:left!important;}table[class="right"],td[class="right"]{text-align:right!important;float:right!important;}table[class="remove"],tr[class="remove"],span[class="remove"]{display:none;}}</style>
</head>
<body id="body-layout">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #ffffff;">
    <tr>
        <td align="center" valign="top">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                <tr>
                    <td align="center" valign="top">
                        <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                            <tr>
                                <td width="600" align="center" valign="top">
                                    <!-- Hotel LOGO BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:30px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Hotel LOGO END -->
                                    <!-- HEADER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #f0f0f0;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #1E272D; font: 20px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 30px; font-weight: bold; text-align:center;">
                                                                        <?=lang('email.resetPassword2')?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="18" style="height: 18px; line-height: 18px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- HEADER END -->

                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>


                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                                <tr>
                                                                                    <td height="20" style="height:20px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                        <?=lang('email.resetPassword2')?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="background-color:<?='#ffffff'?>;color:#ffffff; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 30px 10px 0 10px; font-weight: 400; text-align:center;">
                                                                                        <a href="<?=$link?>" style="background-color:black;padding:10px;margin-top:10px;"><?=lang('email.resetPassword')?></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    <!-- FOOTER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: black;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 30px; line-height: 30px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #ffffff; font: 18px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 22px; font-weight: 700;">
                                                                        <?= $property['name'] ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td width="500" align="center" valign="top">
                                                                        <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"><tr><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-phone.png" height="24" width="24" alt="Phone" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp; <a href="tel:<?=isset($property['contact']['phone']) && !empty($property['contact']['phone']) ? $property['contact']['phone'] : ''?>" style="color:#fff;text-decoration:none;"><?= isset($property['contact']['phone']) && !empty($property['contact']['phone']) ? $property['contact']['phone'] : ''?></a></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-email.png" height="24" width="24" alt="E-mail" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp;
                                                                                        <a href="mailto:<?=isset($property['contact']['email']) && !empty($property['contact']['email']) ? $property['contact']['email'] : ''?>" style="color:#fff;text-decoration:none;">
                                                                                            <?=isset($property['contact']['email']) && !empty($property['contact']['email']) ? $property['contact']['email'] : '' ?>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 40px; line-height: 40px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- FOOTER END -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>