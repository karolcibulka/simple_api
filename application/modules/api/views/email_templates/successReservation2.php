<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <title><?= lang('email.acceptingRes')?> <?= $reservation['id']?></title>
    <style type="text/css">#outlook a{padding:0;}body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}.ExternalClass p{line-height:inherit;}#body-layout{margin:0;padding:0;width:100%!important;line-height:100%!important;}img{display:block;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}a img{border:none;}table td{border-collapse:collapse;}table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}a{color:inherit;outline:none;text-decoration:none;}</style>
    <style type="text/css">@media only screen and (max-width: 599px){div[class="block"]{width:100%!important;max-width:none!important;}table[class="full-width"]{width:100%!important;}td[class="mobile-padding"]{padding:0 0 20px 0!important;}td[class="normalize-height"]{height:auto!important;}table[class="center"],td[class="center"]{text-align:center!important;float:none!important;}table[class="left"],td[class="left"]{text-align:left!important;float:left!important;}table[class="right"],td[class="right"]{text-align:right!important;float:right!important;}table[class="remove"],tr[class="remove"],span[class="remove"]{display:none;}}</style>
</head>
<body id="body-layout">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #ffffff;">
    <tr>
        <td align="center" valign="top">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                <tr>
                    <td align="center" valign="top">
                        <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                            <tr>
                                <td width="600" align="center" valign="top">
                                    <!-- Hotel LOGO BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:100%;">
                                                                            <tr>
                                                                                <td align="center" valign="middle" style="padding: 0" class="normalize-height">
                                                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                        <tr><td height="20" style="height:20px;"></td></tr>
                                                                                        <tr>
                                                                                            <td align="center" valign="top" style="line-height: 0px !important;">
                                                                                                <img src="<?=$property['logo']?>" alt="" width="200" style="max-width: 200px; height: auto;"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:30px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Hotel LOGO END -->
                                    <!-- HEADER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #f0f0f0;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #1E272D; font: 20px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 30px; font-weight: bold; text-align:center;">
                                                                        <?php if(isset($reservation['id']) && !empty($reservation['id'])): ?>
                                                                            <?=lang('email.reservationNumber')?>
                                                                            <span style="color: black; font-size:30px;"><?= $reservation['id']?></span>
                                                                        <?php endif;?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="18" style="height: 18px; line-height: 18px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- HEADER END -->

                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <!-- KAPACITY BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color: black;">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                    <?= lang('email.orderedItems'); ?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <?php if(isset($reservation['items']) && !empty($reservation['items'])):?>
                                                                                <?php foreach ($reservation['items'] as $item) : ?>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                            <?php
                                                                                            echo '<b style="font-size: 1.1em;">'.$item['count'].'x '.$item['offer_data']['name'].'</b>';
                                                                                            echo '<br />';
                                                                                            if(isset($item['upsells']) && !empty($item['upsells'])){
                                                                                                $upsellRow = '';
                                                                                                $requiredRow = '';
                                                                                                foreach($item['upsells'] as $upsell){
                                                                                                    if(isset($upsell['upsell_data']['required_category']) && !empty($upsell['upsell_data']['required_category'])){
                                                                                                        if(isset($upsell['upsell_data']['name']) && !empty($upsell['upsell_data']['name'])){
                                                                                                            $requiredRow .= $upsell['upsell_data']['name'].'<br>';
                                                                                                        }
                                                                                                        else{
                                                                                                            $requiredRow .= $upsell['upsell_data']['internal_name'].'<br>';
                                                                                                        }
                                                                                                    }
                                                                                                    else{
                                                                                                        if(isset($upsell['upsell_data']['name']) && !empty($upsell['upsell_data']['name'])){
                                                                                                            $upsellRow .= $upsell['count'].'x '.$upsell['upsell_data']['name'].', ';
                                                                                                        }
                                                                                                        else{
                                                                                                            $upsellRow .= $upsell['count'].'x '.$upsell['upsell_data']['internal_name'].', ';
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                $upsellRow = rtrim($upsellRow,', ');

                                                                                                if( $requiredRow !== '' ){
                                                                                                    echo $requiredRow;
                                                                                                }

                                                                                                if($upsellRow !== ''){
                                                                                                    echo '+ '.$upsellRow.'</br>';
                                                                                                }
                                                                                            }
                                                                                            if(isset($item['note']) && !empty($item['note'])){
                                                                                                echo lang('email.note').$item['note'].'<br/>';
                                                                                            }
                                                                                            ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                            <?=number_format($item['ri_price_with_upsells'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php endforeach; ?>
                                                                            <?php endif;?>
                                                                            <?php if(isset($reservation['menu']) && !empty($reservation['menu'])):?>
                                                                                <?php foreach($reservation['menu'] as $menu_key => $menu_items):?>
                                                                                    <?php $otherFoods = '+ ';?>
                                                                                    <?php if(isset($menu_items['items']) && !empty($menu_items['items'])):?>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                                    <?php
                                                                                                    foreach($menu_items['items'] as $item_menu_type => $item_menu){

                                                                                                        if(isset($item_menu) && !empty($item_menu)){
                                                                                                            if($item_menu_type === 'main'){
                                                                                                                echo '<b style="font-size: 1.1em;">'.$menu_items['count'].'x '.$item_menu['daily_menu_data']['name'].'</b><br/>';
                                                                                                            }
                                                                                                            else{
                                                                                                                $otherFoods .=  $item_menu['daily_menu_data']['name'].', ';
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    if($otherFoods !=='+ '){
                                                                                                        $otherFoods = rtrim($otherFoods,', ');
                                                                                                        echo $otherFoods;
                                                                                                    }
                                                                                                    if(isset($menu_items['note']) && !empty($menu_items['note'])){
                                                                                                        echo lang('email.note').$menu_items['note'].'<br/>';
                                                                                                    }
                                                                                                    ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                                    <?php echo number_format($menu_items['price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                                </td>
                                                                                            </tr>
                                                                                    <?php endif;?>
                                                                                <?php endforeach;?>
                                                                            <?php endif;?>

                                                                            <tr>
                                                                                <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                    <?php
                                                                                    echo '<b style="font-size: 1.1em;">'.lang('email.packing').'</b>';
                                                                                    ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                    <?php echo number_format($reservation['packing_price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                    <?php
                                                                                    echo '<b style="font-size: 1.1em;">'.lang('email.delivery').'</b>';
                                                                                    ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                    <?php echo number_format($reservation['delivery_price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                </td>
                                                                            </tr>
                                                                            <?php if(isset($reservation['discount_id']) && !empty($reservation['discount_id'])):?>
                                                                                <tr>
                                                                                    <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                        <?php
                                                                                        echo '<b style="font-size: 1.1em;">'.lang('email.discount').'</b>';
                                                                                        ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                        <?php echo '-'.number_format($reservation['discount_unit_price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php endif;?>

                                                                            <tr>
                                                                                <td align="right" valign="top" style="color:black; font: 18px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 30px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right; border-top: solid 1px black;">
                                                                                    <?= lang('email.totalprice') ?>       <?= number_format($reservation['discount_price'], 2, ',', ' ')?> <?= $reservation['currency']?><br/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- KAPACITY END -->


                                    <!-- PLATBY BEGIN -->
                                    <?php if (isset($reservation['id']) && !empty($reservation['id'])): ?>
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                                <tr>
                                                                                    <td height="20" style="height:20px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                        <?= lang('email.payment') ?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                            <?php if ($reservation['payed'] == '1') :?>
                                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                    <tr>
                                                                                        <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                            <b><?= $reservation['payment_name'] ?></b> - <?= lang('email.payed') ?> -  <?= date('d.m.Y H:i:s', strtotime($reservation['payed_at'])) ?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                            <?= number_format($reservation['discount_price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            <?php else :?>
                                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                    <tr>
                                                                                        <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                            <?php if(isset($reservation['check_customer']) && !empty($reservation['check_customer'])):?>
                                                                                                <b><?= $reservation['payment_name']?>
                                                                                            <?php else:?>
                                                                                                <?php if($reservation['payment_type'] == 'onArrival'):?>
                                                                                                    <b><?= $reservation['payment_name'] ?></b> - <span style="color: darkred; font-weight: 600; font-style: italic;"><?=lang('email.payonstay')?> </span>
                                                                                                <?php else:?>
                                                                                                    <b><?= $reservation['payment_name'] ?></b> - <span style="color: darkred; font-weight: 600; font-style: italic;"><?=lang('email.notpayed')?> </span>
                                                                                                <?php endif;?>
                                                                                            <?php endif;?>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 0 10px 10px 10px;font-weight: bold; text-align:right;">
                                                                                            <?= number_format($reservation['total_price'], 2, ',', ' ')?> <?= $reservation['currency']?>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            <?php endif;?>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php endif; ?>
                                    <!-- PLATBY END -->

                                    <?php if(isset($notifications) && !empty($notifications)):?>
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                                <tr>
                                                                                    <td height="20" style="height:20px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                        <?=lang('email.notification')?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                            <?php foreach($notifications as $notification) :?>
                                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                    <tr>
                                                                                        <td align="left" valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                            <b><?=$notification['name'] ?></b>
                                                                                            <?=$notification['content']?>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            <?php endforeach;?>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php endif;?>

                                    <?php if(isset($property['version']) && !empty($property['version']) && $property['version'] == '2'):?>
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                                <tr>
                                                                                    <td height="20" style="height:20px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                        <?=lang('email.state')?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                    <tr>
                                                                                        <td align="center" valign="top" style="background-color:<?='#ffffff'?>;color:#ffffff; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 30px 10px 0 10px; font-weight: 400; text-align:center;">
                                                                                            <a href="<?=$state_link?>" style="background-color:black;padding:10px;margin-top:10px;"><?=lang('email.checkState')?></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>

                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php endif;?>


                                    <!-- 2 COLUMN BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="font-size: 0px !important;">
                                                            <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="1" align="center"><tr><![endif]--><!--[if (gte mso 9)|(IE)]><td width="298" align="center" valign="top"><![endif]-->
                                                            <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 298px; padding:1px;" class="block">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin:  0; padding: 0;background: #fff;"><tr><td height="20" style="height:20px;"></td></tr></table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="padding: 0;">
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                                            <tr>
                                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                                    <?= lang('email.customerInfo'); ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                            <tr>
                                                                                                <td height="10" style="height: 10px; line-height: 10px;"></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                            <tr>
                                                                                                <td align="left" valign="top" style="color:#1E272D; font: 12px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: 400; padding: 10px 20px;">
                                                                                                    <b><?= ucfirst($reservation['first_name']) .' '. ucfirst($reservation['last_name']) ?></b><br />
                                                                                                    <?php if(isset($reservation['street']) && !empty($reservation['street'])):?>
                                                                                                        <?= ucfirst($reservation['street']) .' '. $reservation['number'] .', '. mb_strtoupper($reservation['city']) ?><br />
                                                                                                    <?php endif;?>
                                                                                                    <?= $reservation['email'] ?><br />
                                                                                                    <?= $reservation['phone'] ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]><td width="298" align="center" valign="top"><![endif]-->
                                                            <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 298px; padding:1px;" class="block">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin:  0; padding: 0;background: #fff;"><tr><td height="20" style="height:20px;"></td></tr></table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: #fff;">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="padding: 0;">
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color: black;">
                                                                                            <tr>
                                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                                    <?= lang('email.note2')?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                            <tr>
                                                                                                <td height="10" style="height: 10px; line-height: 10px;"></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                            <tr>
                                                                                                <td align="left" valign="top" style="color:#1E272D; font: 12px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: 400; padding: 10px 20px;">
                                                                                                    <?= $reservation['note']?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- 2 COLUMN END -->

                                    <!-- INFO BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 0 0; padding: 0;background: #fff;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding:  0;">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0 20px; padding: 0; max-width:580px;">
                                                                            <tr>
                                                                                <td height="20" style="height:20px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0; background-color:black;">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="color: #fff; font: 14px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; font-weight: bold; padding: 8px 0;">
                                                                                   <?=lang('email.info')?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                    <b><?=lang('email.source')?></b> <?= (isset($data['reservation']['ha_IntName']) && !empty($data['reservation']['ha_IntName'])) ? $data['reservation']['ha_IntName'] : 'WEB' ?>
                                                                                </td>
                                                                            </tr>
                                                                            <?php if(isset($table) && !empty($table)):?>
                                                                                <td valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                    <b><?=lang('email.table')?></b> <?=$table['name'] ?>
                                                                                </td>
                                                                            <?php endif;?>
                                                                            <?php if(isset($reservation['room_nr']) && !empty($reservation['room_nr'])):?>
                                                                                <td valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                    <b><?=lang('email.room')?></b> <?=$reservation['room_nr'] ?>
                                                                                </td>
                                                                            <?php endif;?>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td valign="top" style="color:#1E272D; font: 13px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 20px; padding: 10px 10px 0 10px; font-weight: 400; text-align:left;">
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                            <tr>
                                                                                <td height="12" style="height: 12px; line-height: 12px;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- INFO END -->


                                    <!-- FOOTER BEGIN -->
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: black;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 20px;">
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 30px; line-height: 30px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td align="center" valign="top" style="color: #ffffff; font: 18px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 22px; font-weight: 700;">
                                                                        <?= $property['name'] ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="20" style="height: 20px; line-height: 20px;"></td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td width="500" align="center" valign="top">
                                                                        <!--[if (gte mso 9)|(IE)]><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"><tr><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                    <tr>
                                                                                        <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-phone.png" height="24" width="24" alt="Phone" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp; <a href="tel:<?=isset($property['contact']['phone']) && !empty($property['contact']['phone']) ? $property['contact']['phone'] : ''?>" style="color:#fff;text-decoration:none;"><?= isset($property['contact']['phone']) && !empty($property['contact']['phone']) ? $property['contact']['phone'] : ''?></a></td>
                                                                                    </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]><td width="240" align="center" valign="top"><![endif]-->
                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 240px;">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" style="color: #fff; font: 16px 'Trebuchet MS', Helvetica, sans-serif; mso-line-height-rule: exactly; line-height: 24px; font-weight: 400;"><img src="http://api.softsolutions.sk/assets/emailTemplates/default/contact-email.png" height="24" width="24" alt="E-mail" style="display:inline-block; margin: 5px 10px -5px 0;" />&nbsp;
                                                                                        <a href="mailto:<?=isset($property['contact']['email']) && !empty($property['contact']['email']) ? $property['contact']['email'] : ''?>" style="color:#fff;text-decoration:none;">
                                                                                            <?=isset($property['contact']['email']) && !empty($property['contact']['email']) ? $property['contact']['email'] : '' ?>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!--[if (gte mso 9)|(IE)]></td><![endif]--><!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                <tr>
                                                                    <td height="30" style="height: 40px; line-height: 40px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- FOOTER END -->

                                    <?php if(!empty($property['social']['facebook']) || !empty($property['social']['instagram'])):?>
                                        <!-- SOCIAL BEGIN -->
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;background: black;">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                        <tr>
                                                            <td align="center" valign="top" style="padding: 0 20px;">
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td height="30" style="height: 20px; line-height: 20px;"></td>
                                                                    </tr>
                                                                </table>
                                                                <table align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td width="600" align="center" valign="top">
                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                <tr>
                                                                                    <td align="center" valign="top" style="font-size: 0px !important;">
                                                                                        <div style="width: 100%; display: inline-block; vertical-align: top; font-size: normal;">
                                                                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                                                <tr>
                                                                                                    <td height="10" style="height: 10px; line-height: 10px;"></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin: 0; padding: 0;">
                                                                    <tr>
                                                                        <td height="40" style="height: 30px; line-height: 30px;"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- SOCIAL END -->
                                    <?php endif;?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>