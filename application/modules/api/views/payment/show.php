<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<style>
    body{
        background-color:aliceblue;
    }
</style>
<body>
    <div class="container">
        <div class="row" style="margin-top:200px;">
            <div class="col-md-12 text-center">
                <h5>Simulácia platby</h5>
            </div>
        </div>
        <div class="row" style="margin-top:100px;">
            <a class="btn btn-success" href="<?=base_url('api/payment/showReservation/'.$reservation['property_id'].'/'.$reservation['token'])?>"  target="_blank" style="width: 100%;">Zobraziť rezerváciu</a>
        </div>
        <div class="row" style="margin-top:100px;">
            <div class="col-md-6">
                <a href="<?=base_url('api/payment/savePayment/'.$reservation['property_id'].'/'.$reservation['token'].'/1')?>" class="btn btn-primary" style="width:100%;">Simulovať zaplatenie</a>
            </div>
            <div class="col-md-6">
                <a href="<?=base_url('api/payment/savePayment/'.$reservation['property_id'].'/'.$reservation['token'].'/0')?>" class="btn btn-danger" style="width:100%;">Simulovať nezaplatenie</a>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>