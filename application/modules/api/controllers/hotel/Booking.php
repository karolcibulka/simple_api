<?php


class Booking extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Requester',false,'requester');
        $this->load->library('Responser',false,'responser');
    }

    public function getBookings_get($propertyID = false,$lang = false, $userToken = false){
        if(has_api_permission('show')){

            $this->missingParams(array('propertyID','lang','userToken'));

            $bookings = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->getUserBookings();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::bookings($bookings)),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function getAccommodations_get($propertyID = false,$lang = false,$userToken = false){
        if(has_api_permission('show')){

            $this->missingParams(array('propertyID','lang','userToken'));

            $accommodations = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->getUserAccommodations();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::accommodations($accommodations)),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function getBill_get($propertyID = false,$lang = false,$userToken = false,$billID = false){
        if(has_permission('show')){

            $this->missingParams(array('propertyID','lang','userToken','billID'));

            $bill = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->setBillID($billID)
                ->getBill();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::bill($bill)),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function getBills_post($propertyID = false,$lang = false,$userToken = false){
        if(has_permission('show')) {

            $this->missingParams(array('propertyID', 'lang', 'userToken'));

            $bills = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->setData($this->input->post())
                ->getBills();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::bills($bills)),200);
            }
        }
        else{
            $this->wrongState();
        }

    }


}