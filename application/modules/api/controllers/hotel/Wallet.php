<?php


class Wallet extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Requester',false,'requester');
        $this->load->library('Responser',false,'responser');
    }

    public function storePoints_post($propertyID = false,$lang = false, $userToken = false){
        if(has_api_permission('edit')){

            $this->missingParams(array('propertyID','lang','userToken'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->setData($this->input->post())
                ->storePoints();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(array('message'=>'success')),200);
            }
            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function drawPoints_post($propertyID = false,$lang = false,$userToken = false){
        if(has_api_permission('edit')){

            $this->missingParams(array('propertyID','lang','userToken'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->setData($this->input->post())
                ->drawPoints();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(array('message'=>'success')),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function getWallet_get($propertyID = false,$lang = false, $userToken = false){
        if(has_api_permission('show')) {

            $this->missingParams(array('propertyID','lang','userToken'));

            $wallet = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->getWallet();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::wallet($wallet)),200);
            }

            $this->response(unsuccessResult($errors),200);

        }
        else {
            $this->wrongState();
        }
    }

    public function getActivities_get($propertyID = false,$lang = false,$userToken = false){
        if(has_api_permission('show')){

            $this->missingParams(array('propertyID','lang','userToken'));

            $activities = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setUserToken($userToken)
                ->getWalletActivities();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::walletActivities($activities)),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function storePointsByTitanID_post($propertyID,$titanID){
        if(has_api_permission('create')){

            $this->missingParams(array('propertyID','titanID'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang('sk')
                ->setTitanID($titanID)
                ->setData($this->input->post())
                ->pointOperationRaw('INCREASE');

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function drawPointsByTitanID_post($propertyID,$titanID){
        if(has_api_permission('create')){

            $this->missingParams(array('propertyID','titanID'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang('sk')
                ->setTitanID($titanID)
                ->setData($this->input->post())
                ->pointOperationRaw('DRAWING');

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

}