<?php


class Users extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Requester',false,'requester');
        $this->load->library('Responser',false,'responser');
    }

    public function createUser_post($propertyID = false,$lang = false){
        if(has_api_permission('create')){

            $this->missingParams(array('propertyID','lang'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setData($this->input->post())
                ->createUser();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Requester::getUserData()),200);
            }

            $this->response(unsuccessResult($errors),200);

        }
        else{
            $this->wrongState();
        }
    }

    public function login_post($propertyID = false,$lang = false){
        if(has_api_permission('show')){

            $this->missingParams(array('propertyID','lang'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setData($this->input->post())
                ->login();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Requester::getUserData()),200);
            }

            $this->response(unsuccessResult($errors),200);

        }
        else{
            $this->wrongState();
        }
    }

    public function getUserData_post($propertyID = false,$lang = false){
         if(has_api_permission('show')){

             $this->missingParams(array('propertyID','lang'));

             Requester::init()
                 ->setProperty($propertyID)
                 ->setLang($lang)
                 ->setData($this->input->post())
                 ->getUserWithValidation();

             if(!$errors = Requester::getErrors()){
                 $this->response(successResult(Requester::getUser()),200);
             }

             $this->response(unsuccessResult($errors),200);

         }
         else{
             $this->wrongState();
         }
    }

    public function getUserCategories_get($propertyID = false,$userID = false){
        if(has_api_permission('show')){

            $this->missingParams(array('propertyID','userID'));

            $categories = Requester::init()
                ->setProperty($propertyID)
                ->setLang('sk')
                ->setUserID($userID)
                ->getUserCategories();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Responser::marketingCategories($categories)),200);
            }

            $this->response(unsuccessResult($errors),200);

        }
        else{
            $this->wrongState();
        }
    }

    public function changeUserData_post($propertyID = false,$lang = false, $userToken = false){
        if(has_api_permission('edit')){

            $this->missingParams(array('propertyID','lang','userToken'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setData($this->input->post())
                ->setUserToken($userToken)
                ->updateUser();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Requester::getUserData()),200);
            }

            $this->response(unsuccessResult($errors),200);

        }
        else{
            $this->wrongState();
        }
    }

    public function forgotPassword_post($propertyID = false,$lang = false){
        if(has_api_permission('edit')){

            $this->missingParams(array('propertyID','lang'));

            $data = Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setData($this->input->post())
                ->getForgotPasswordUrl();

            if(!$errors = Requester::getErrors()){
                $this->load->library('notification',array(),'notification');
                $this->notification->sendForgotPasswordEmail($data,$lang,$propertyID);
                $this->response(successResult(array('message'=>'success')),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function resetPassword_post($propertyID = false,$lang = false,$userToken = false){
        if(has_api_permission('edit')){

            $this->missingParams(array('propertyID','lang','userToken'));

            Requester::init()
                ->setProperty($propertyID)
                ->setLang($lang)
                ->setData($this->input->post())
                ->setUserToken($userToken)
                ->resetPassword();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult(Requester::getUserData()),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

    public function getUserTitanID_post($propertyID = false){
        if(has_api_permission('show')) {

            $this->missingParams(array('propertyID'));

            $titan_id = Requester::init()
                ->setProperty($propertyID)
                ->setData($this->input->post())
                ->getUserTitanID();

            if(!$errors = Requester::getErrors()){
                $this->response(successResult($titan_id),200);
            }

            $this->response(unsuccessResult($errors),200);
        }
        else{
            $this->wrongState();
        }
    }

}