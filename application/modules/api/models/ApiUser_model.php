<?php


class ApiUser_model extends CI_Model
{

    public function storeUser($data){
        $this->db->insert('loyalty_users',$data);
        return $this->db->insert_id();
    }

    public function getUsersByEmail($email,$property_id){
        return $this->db->select('*')
            ->from('loyalty_users')
            ->where('property_id',$property_id)
            ->where('email',$email)
            ->get()
            ->row_array();
    }

    public function getUsersByEmailGroup($email,$group_ids){
        return $this->db->select('*')
            ->from('loyalty_users')
            ->where_in('property_id',$group_ids)
            ->where('email',$email)
            ->get()
            ->row_array();
    }

    public function getProperty($property_id){
        return $this->db->select('*')
            ->from('property')
            ->where('id',$property_id)
            ->get()
            ->row_array();
    }

    public function getUserRaw($property_id,$user_token){
        return $this->db->select('*')
            ->from('loyalty_users as fu')
            ->where('fu.property_id',$property_id)
            ->where('fu.token',$user_token)
            ->group_by('fu.token')
            ->get()
            ->row_array();
    }

    public function getUser($property_id,$user_token){
        return $this->db->select('
            first_name,
            last_name,
            email,
            phone,
            lang,
            zip,
            city,
            country,
            street_number,
            street,
            token,
            id,
            created_at
        ')
            ->from('loyalty_users as fu')
            ->where('fu.property_id',$property_id)
            ->where('fu.token',$user_token)
            ->group_by('fu.token')
            ->get()
            ->row_array();
    }

    public function getUserByGroup($group_ids,$user_token){
        $data = $this->db->select('
            first_name,
            last_name,
            email,
            phone,
            lang,
            zip,
            city,
            street_number,
            street,
            token,
            GROUP_CONCAT(t.id SEPARATOR "|") as tags
        ')
            ->from('loyalty_users as fu')
            ->where_in('fu.property_id',$group_ids)
            ->where('fu.token',$user_token)
            ->join('loyalty_users_tags as fut','fut.user_token = fu.token','left')
            ->join('tags as t','t.id = fut.tag_id AND t.active = 1 AND t.deleted = 0','left')
            ->group_by('fu.token')
            ->get()
            ->row_array();

        if(isset($data['tags']) && !empty($data['tags'])){
            $data['tags'] = explode('|',$data['tags']);
        }

        return $data;
    }

    public function getPropertyGroups($group_id){
        $data = $this->db->select('*')
            ->from('property')
            ->where('group_id',$group_id)
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d['id'];
            }
        }

        return $response;
    }

    public function getUserByEmail($email,$property_id){
        return $this->db->select('*')
            ->from('loyalty_users')
            ->where('email',$email)
            ->where('property_id',$property_id)
            ->get()
            ->row_array();
    }

    public function getUserByEmailByGroup($email,$group_ids){
        return $this->db->select('*')
            ->from('loyalty_users')
            ->where('email',$email)
            ->where_in('property_id',$group_ids)
            ->get()
            ->row_array();
    }

    public function updateUser($token,$data){
        $this->db->where('token',$token)->update('loyalty_users',$data);
    }

    public function getUserReservationsGroup($property_ids,$user_token){
        $data =  $this->db->select('
            r.id as reservation_id,
            r.note as r_note,
            r.property_id as r_property_id,
            r.token as r_token,
            p.name as p_name,
            p.version as p_version,
            r.total_price as r_total_price,
            r.menu_price as r_menu_price,
            r.accommodated_guest_id as r_accommodated_guest_id,
            r.created_at as r_created_at,
            r.room_nr as r_room_nr,
            r.order_id as r_order_id,
            r.status as r_status,
            r.currency as r_currency,
            r.lang as r_lang,
            r.transportation_id as r_transportation_id,
            tl.name as t_name,
            tl.lang as t_lang,
            tl.short_description as t_short_description,
            r.packing_price as r_packing_price,
            r.delivery_price as r_delivery_price,
            r.items_price as r_items_price,
            r.discount_price as r_discount_price,
            r.menu_price as r_menu_price,
            r.discount_unit_price as r_discount_unit_price,
            r.is_delivery as r_is_delivery,
            r.payment_id as r_payment_id,
            r.payed_at as r_payed_at,
            ri.count as ri_count,
            ri.id as ri_id,
            ri.reservation_id as ri_reservation_id,
            ri.unit_price as ri_unit_price,
            ri.unit_price_with_upsells as ri_unit_price_with_upsells,
            ri.price as ri_price,
            ri.price_with_upsells as ri_price_with_upsells,
            ri.offer_data as ri_offer_data,
            ri.type as ri_type,
            ri.menu_type as ri_menu_type,
            ri.is_traceable as ri_is_traceable,
            riu.upsell_id as riu_upsell_id,
            riu.id as riu_id,
            riu.count as riu_count,
            riu.unit_price as riu_unit_price,
            riu.price as riu_price,
            riu.upsell_data as riu_data,
            riu.item_id as riu_item_id
        ')
            ->from('reservations as r')
            ->join('property as p','p.id = r.property_id','left')
            ->join('transportation_lang as tl','tl.transportation_id = r.transportation_id','left')
            ->join('reservations_item as ri','ri.reservation_id = r.id','left')
            ->join('reservations_item_upsell as riu','riu.item_id = ri.id','left')
            ->where_in('r.property_id',$property_ids)
            ->where('r.user_token',$user_token)
            ->order_by('r.created_at','desc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['reservation_id']]['id'] = $d['reservation_id'];
                $response[$d['reservation_id']]['note'] = $d['r_note'];
                $response[$d['reservation_id']]['token'] = $d['r_token'];
                $response[$d['reservation_id']]['property_id'] = $d['r_property_id'];
                $response[$d['reservation_id']]['property_name'] = $d['p_name'];
                $response[$d['reservation_id']]['property_version'] = $d['p_version'];
                $response[$d['reservation_id']]['order_id'] = $d['r_order_id'];
                $response[$d['reservation_id']]['prices']['total_price'] = $d['r_total_price'];
                $response[$d['reservation_id']]['prices']['menu_price'] = $d['r_menu_price'];
                $response[$d['reservation_id']]['prices']['packing_price'] = $d['r_packing_price'];
                $response[$d['reservation_id']]['prices']['delivery_price'] = $d['r_delivery_price'];
                $response[$d['reservation_id']]['prices']['items_price'] = $d['r_items_price'];
                $response[$d['reservation_id']]['prices']['discount_price'] = $d['r_discount_price'];
                $response[$d['reservation_id']]['prices']['packing_price'] = $d['r_packing_price'];
                $response[$d['reservation_id']]['currency'] = $d['r_currency'];
                $response[$d['reservation_id']]['transportation']['id'] = $d['r_transportation_id'];
                $response[$d['reservation_id']]['transportation']['languages'][$d['t_lang']]['name'] = $d['t_name'];
                $response[$d['reservation_id']]['transportation']['languages'][$d['t_lang']]['short_description'] = $d['t_short_description'];
                $response[$d['reservation_id']]['status'] = $d['r_status'];
                $response[$d['reservation_id']]['created_at'] = $d['r_created_at'];
                $response[$d['reservation_id']]['accommodated_guest_id'] = $d['r_accommodated_guest_id'];
                if(isset($d['ri_id']) && !empty($d['ri_id'])){
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['id'] = $d['ri_id'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['count'] = $d['ri_count'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['unit_price'] = $d['ri_unit_price'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['unit_price_with_upsells'] = $d['ri_unit_price_with_upsells'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['price'] = $d['ri_price'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['price_with_usells'] = $d['ri_price_with_upsells'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['data'] = json_decode($d['ri_offer_data'],true);
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['type'] = $d['ri_type'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['menu_type'] = $d['ri_menu_type'];
                    if(isset($d['riu_id']) && !empty($d['riu_id'])){
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['id'] = $d['riu_id'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['count'] = $d['riu_count'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['prices']['unit_price'] = $d['riu_unit_price'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['prices']['price'] = $d['riu_price'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['data'] = json_decode($d['riu_data'],TRUE);
                    }
                }
            }
        }


        return $response;
    }

    public function getUserReservations($property_id,$user_token){
        $data =  $this->db->select('
            r.id as reservation_id,
            r.note as r_note,
            r.property_id as r_property_id,
            r.token as r_token,
            p.name as p_name,
            p.version as p_version,
            r.total_price as r_total_price,
            r.menu_price as r_menu_price,
            r.accommodated_guest_id as r_accommodated_guest_id,
            r.created_at as r_created_at,
            r.room_nr as r_room_nr,
            r.order_id as r_order_id,
            r.status as r_status,
            r.currency as r_currency,
            r.lang as r_lang,
            r.transportation_id as r_transportation_id,
            tl.name as t_name,
            tl.lang as t_lang,
            tl.short_description as t_short_description,
            r.packing_price as r_packing_price,
            r.delivery_price as r_delivery_price,
            r.items_price as r_items_price,
            r.discount_price as r_discount_price,
            r.menu_price as r_menu_price,
            r.discount_unit_price as r_discount_unit_price,
            r.is_delivery as r_is_delivery,
            r.payment_id as r_payment_id,
            r.payed_at as r_payed_at,
            ri.count as ri_count,
            ri.id as ri_id,
            ri.reservation_id as ri_reservation_id,
            ri.unit_price as ri_unit_price,
            ri.unit_price_with_upsells as ri_unit_price_with_upsells,
            ri.price as ri_price,
            ri.price_with_upsells as ri_price_with_upsells,
            ri.offer_data as ri_offer_data,
            ri.type as ri_type,
            ri.menu_type as ri_menu_type,
            ri.is_traceable as ri_is_traceable,
            riu.upsell_id as riu_upsell_id,
            riu.id as riu_id,
            riu.count as riu_count,
            riu.unit_price as riu_unit_price,
            riu.price as riu_price,
            riu.upsell_data as riu_data,
            riu.item_id as riu_item_id
        ')
            ->from('reservations as r')
            ->join('property as p','p.id = r.property_id','left')
            //->join('transportation as t','t.id = r.transportation_id','left')
            ->join('transportation_lang as tl','tl.transportation_id = r.transportation_id','left')
            ->join('reservations_item as ri','ri.reservation_id = r.id','left')
            ->join('reservations_item_upsell as riu','riu.item_id = ri.id','left')
            ->where('r.property_id',$property_id)
            ->where('r.user_token',$user_token)
            ->order_by('r.created_at','desc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['reservation_id']]['id'] = $d['reservation_id'];
                $response[$d['reservation_id']]['note'] = $d['r_note'];
                $response[$d['reservation_id']]['token'] = $d['r_token'];
                $response[$d['reservation_id']]['property_id'] = $d['r_property_id'];
                $response[$d['reservation_id']]['property_name'] = $d['p_name'];
                $response[$d['reservation_id']]['property_version'] = $d['p_version'];
                $response[$d['reservation_id']]['order_id'] = $d['r_order_id'];
                $response[$d['reservation_id']]['prices']['total_price'] = $d['r_total_price'];
                $response[$d['reservation_id']]['prices']['menu_price'] = $d['r_menu_price'];
                $response[$d['reservation_id']]['prices']['packing_price'] = $d['r_packing_price'];
                $response[$d['reservation_id']]['prices']['delivery_price'] = $d['r_delivery_price'];
                $response[$d['reservation_id']]['prices']['items_price'] = $d['r_items_price'];
                $response[$d['reservation_id']]['prices']['discount_price'] = $d['r_discount_price'];
                $response[$d['reservation_id']]['prices']['packing_price'] = $d['r_packing_price'];
                $response[$d['reservation_id']]['currency'] = $d['r_currency'];
                $response[$d['reservation_id']]['transportation']['id'] = $d['r_transportation_id'];
                $response[$d['reservation_id']]['transportation']['languages'][$d['t_lang']]['name'] = $d['t_name'];
                $response[$d['reservation_id']]['transportation']['languages'][$d['t_lang']]['short_description'] = $d['t_short_description'];
                $response[$d['reservation_id']]['status'] = $d['r_status'];
                $response[$d['reservation_id']]['created_at'] = $d['r_created_at'];
                $response[$d['reservation_id']]['accommodated_guest_id'] = $d['r_accommodated_guest_id'];
                if(isset($d['ri_id']) && !empty($d['ri_id'])){
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['id'] = $d['ri_id'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['count'] = $d['ri_count'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['unit_price'] = $d['ri_unit_price'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['unit_price_with_upsells'] = $d['ri_unit_price_with_upsells'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['price'] = $d['ri_price'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['prices']['price_with_usells'] = $d['ri_price_with_upsells'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['data'] = json_decode($d['ri_offer_data'],true);
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['type'] = $d['ri_type'];
                    $response[$d['reservation_id']]['items'][$d['ri_id']]['menu_type'] = $d['ri_menu_type'];
                    if(isset($d['riu_id']) && !empty($d['riu_id'])){
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['id'] = $d['riu_id'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['count'] = $d['riu_count'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['prices']['unit_price'] = $d['riu_unit_price'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['prices']['price'] = $d['riu_price'];
                        $response[$d['reservation_id']]['items'][$d['ri_id']]['upsells'][$d['riu_id']]['data'] = json_decode($d['riu_data'],TRUE);
                    }
                }
            }
        }


        return $response;

    }

    public function getUserLibrary($user_token,$property_id = false,$property_ids = false){
        $q =  $this->db->select('*')
            ->from('loyalty_users')
            ->where('token',$user_token);
        if($property_id){
            $q->where('property_id',$property_id);
        }
        else{
            $q->where_in('property_id',$property_ids);
        }
        return $q->get()
            ->row_array();
    }

    public function getPropertySettings($property_id){
        return $this->db->select('
            id,
            cgin,
            uach,
            port,
            server,
            point_ratio
        ')
            ->from('property')
            ->where('id',$property_id)
            ->where('active',1)
            ->where('deleted',0)
            ->get()
            ->row_array();
    }

    public function getUserByID($property_id,$user_id){
        return $this->db->select('*')
            ->from('loyalty_users')
            ->where('property_id',$property_id)
            ->where('id',$user_id)
            ->get()
            ->row_array();
    }

}