<?php

class CustomAuth_model extends CI_Model 
{
    public function getUsers( $user_id , $getUsers ,$getCustomUser = null ){
        $q = $this->db->select('p.id as "property_id",p.name as "property_name",g.order as "group_order" ,g.description as "group_name",g.id as "group_id"')
                    ->from('users_properties as up')
                    ->join('property as p','p.id = up.property_id','left')
                    ->join('users_groups as ug','ug.user_id = up.user_id','left')
                    ->join('groups as g','g.id = ug.group_id','left')
                    ->where('p.active','1')
                    ->where('p.deleted','0')
                    ->order_by('g.order','asc');

        if(!isDeveloper()){
            $q->where('up.user_id', $user_id );
        }

        $data = $q->get()->result_array();

        $response = array();

        if( !empty( $data ) ){
            $count = count($data)-1;
            $counter = 0;
            foreach($data as $d){
                $response['properties'][$d['property_id']]['id'] = $d['property_id'];
                $response['properties'][$d['property_id']]['name'] = $d['property_name'];
                if($counter === $count){
                    $response['roles'] = $this->roles($d['group_id']);
                    $response['max_role'] = $d['group_order'];
                }
                $counter++;
            }
            if( !empty($response['properties']) && $getUsers ){
                $response['users'] = $this->property_users($response['properties']);
            }

            if(!empty($response['properties']) && $getUsers && !is_null($getCustomUser)){
                $response['user'] = $this->getUser($response['properties'],$getCustomUser);
            }
        }

        return $response;
    
    }

    public function roles ( $max_order ){
        return $this->db->select('*')
            ->from('groups as g')
            ->where('g.order <= ', $max_order )
            ->order_by('g.order','asc')
            ->get()
            ->result_array();
    }

    public function getUser( $properties , $user_id ){
        $props = array();
        foreach($properties as $property){
            $props[] = $property['id'];
        }

        $user = $this->db->select('u.*,u.id as "user_id",p.name as "property_name",p.id as "property_id",g.id as "group_id",g.order as "group_order",g.description as "group_description"')
            ->from('users_properties as up')
            ->where_in('up.property_id',$props)
            ->where('u.id',$user_id)
            ->join('users as u','u.id = up.user_id','left')
            ->join('property as p','p.id = up.property_id','left')
            ->join('users_groups as ug','ug.user_id = u.id','left')
            ->join('groups as g','g.id = ug.group_id','left')
            ->order_by('g.order','asc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($user)){
            foreach($user as $u){
                $response['id'] = $u['user_id'];
                $response['first_name'] = $u['first_name'];
                $response['last_name'] = $u['last_name'];
                $response['email'] = $u['email'];
                $response['properties'][$u['property_id']] = $u['property_id'];
                $response['group_names'][$u['group_id']] = $u['group_description'];
                $response['group_ids'][$u['group_id']] = $u['group_id'];
                $response['group_order'][$u['group_id']] = $u['group_order'];
            }
        }

        return $response;

    }

    public function property_users ( $properties ){
        $props = array();
        foreach($properties as $property){
            $props[] = $property['id'];
        }

        $users = $this->db->select('u.*,u.id as "user_id",p.name as "property_name",p.id as "property_id",g.order as "group_order", g.id as "group_id",g.description as "group_description"')
            ->from('users_properties as up')
            ->where_in('up.property_id',$props)
            ->join('users as u','u.id = up.user_id','left')
            ->join('property as p','p.id = up.property_id','left')
            ->join('users_groups as ug','ug.user_id = u.id','left')
            ->join('groups as g','g.id = ug.group_id','left')
            ->order_by('g.order','asc')
            ->get()
            ->result_array();

        $response = array();

        if( !empty( $users ) ){
            foreach($users as $u){
                $response[$u['property_id']]['property_name'] = $u['property_name'];
                $response[$u['property_id']]['users'][$u['user_id']]['id'] = $u['user_id'];
                $response[$u['property_id']]['users'][$u['user_id']]['first_name'] = $u['first_name'];
                $response[$u['property_id']]['users'][$u['user_id']]['last_name'] = $u['last_name'];
                $response[$u['property_id']]['users'][$u['user_id']]['email'] = $u['email'];
                $response[$u['property_id']]['users'][$u['user_id']]['group_names'][] = $u['group_description'];
                $response[$u['property_id']]['users'][$u['user_id']]['group_ids'][] = $u['group_id'];
                $response[$u['property_id']]['users'][$u['user_id']]['group_order'][] = $u['group_order'];
            }
        }

        return $response;
    }

    public function validateEmail($email){
        $record = $this->db->select('*')
                ->from('users as u')
                ->where('u.email',$email)
                ->get()
                ->row_array();

        if(!empty($record)){
            return false;
        }
        return true;
    }

    public function createUser($data){
        $this->db->insert('users',$data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function createPivotUserGroup($data){
        $this->db->insert('users_groups',$data);
    }

    public function createPivotUserProperty($data){
        $this->db->insert('users_properties',$data);
    }

    public function getUserPropertiesByEmail($email){
        $data =  $this->db->select('up.*')
                ->from('users as u')
                ->where('u.email',$email)
                ->join('users_properties as up','up.user_id = u.id','left')
                ->get()
                ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[] = $d['property_id'];
            }
        }

        return $response;
    }

    public function updateUserByEmail($email,$data){
        $this->db->where('email',$email)->update('users',$data);

        $user = $this->db->select('u.id')
                ->where('u.email',$email)
                ->from('users as u')
                ->get()
                ->row_array();

        return $user['id'];
    }

    public function removePermissions($user_id){
        $this->db->where('user_id',$user_id)->delete('users_groups');
    }

    public function removeProperties($user_id){
        $this->db->where('user_id',$user_id)->delete('users_properties');
    }
}