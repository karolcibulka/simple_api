<?php


class FrontUsers_model extends CI_Model
{

    public function getPropertyIDs($property_id){
        $data = $this->db->select('*')
            ->from('property')
            ->where('id',$property_id)
            ->get()
            ->row_array();

        $response = array();

        if(isset($data['group_id']) && !empty($data['group_id'])){
            $properties = $this->db->select('*')
                ->from('property')
                ->where('group_id',$data['group_id'])
                ->get()
                ->result_array();

            if(!empty($properties)){
                foreach($properties as $property){
                    $response[$property['id']] = $property['id'];
                }
            }
        }

        if(empty($response)){
            $response[$property_id] = $property_id;
        }

        return $response;
    }

    public function getTags($propertyID){
        return $this->db->select('*')
            ->from('tags')
            ->where('property_id',$propertyID)
            ->where('active',1)
            ->where('deleted',0)
            ->order_by('order','asc')
            ->get()
            ->result_array();
    }

    public function deleteUserTags($property_id,$user_token){
        $this->db->where('property_id',$property_id)->where('user_token',$user_token)->delete('front_users_tags');
    }

    public function insertUserTagsBatch($data){
        $this->db->insert_batch('front_users_tags',$data);
    }

    public function getUsers($properties,$property_id){
        $data =  $this->db->select('
            id,
            email,
            phone,
            lang,
            token,
            first_name,
            last_name,
            zip,
            city,
            street,
            street_number,
            GROUP_CONCAT(fut.tag_id SEPARATOR "|") as tags
        ')
            ->from('front_users as fu')
            ->where_in('fu.property_id',$properties)
            ->join('front_users_tags as fut','fut.user_token = fu.token AND fut.property_id = '.$property_id,'left')
            ->group_by('fu.id')
            ->get()
            ->result_array();

        if(!empty($data)){
            foreach($data as $key => $d){
                if(isset($d['tags']) && !empty($d['tags'])){
                    $data[$key]['tags'] = explode('|',$d['tags']);
                }
            }
        }

        return $data;
    }
}