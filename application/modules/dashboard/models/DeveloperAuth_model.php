<?php


class DeveloperAuth_model extends CI_Model
{

    public function getUsers(){
        $data = $this->db->select('*,u.id as user_id,ug.group_id as g_id,up.property_id as p_id')
            ->from('users as u')
            ->join('users_groups as ug','ug.user_id = u.id','left')
            ->join('users_properties as up','up.user_id = u.id','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['user_id']]['id'] = $d['user_id'];
                $response[$d['user_id']]['email'] = $d['email'];
                $response[$d['user_id']]['is_developer'] = $d['is_developer'];
                if(isset($d['g_id']) && !empty($d['g_id'])){
                    $response[$d['user_id']]['groups'][$d['g_id']] = $d['g_id'];
                }
                if(isset($d['p_id']) && !empty($d['p_id'])){
                    $response[$d['user_id']]['properties'][$d['p_id']] = $d['p_id'];
                }
            }
        }

        return $response;
    }

    public function getProperties(){
        return $this->db->select('*')
            ->from('property')
            ->where('active',1)
            ->where('deleted',0)
            ->get()
            ->result_array();
    }

    public function getGroups(){
        return $this->db->select('*')
            ->from('groups')
            ->order_by('order')
            ->get()
            ->result_array();
    }

    public function updateUser($user_id,$type,$data){

        $settings = array(
            'table' => 'users_properties',
            'field' => 'property_id',
        );

        if($type !== 'properties'){
            $settings = array(
                'table' => 'users_groups',
                'field' => 'group_id'
            );
        }

        $this->db->where('user_id',$user_id)->delete($settings['table']);

        if(!empty($data)){
            $values = array();
            foreach($data as $d){
                $values[] = array(
                    'user_id'=>$user_id,
                    $settings['field'] => $d
                );
            }
            if(!empty($values)){
                $this->db->insert_batch($settings['table'],$values);
            }
        }
    }

    public function updateUserData($id,$data){
        $this->db->where('id',$id)->update('users',$data);
    }

    public function validateEmail($email){
        $data = $this->db->select('*')
            ->from('users')
            ->where('email',$email)
            ->get()
            ->row_array();

        if(!empty($data)){
            return false;
        }

        return true;
    }

    public function storeUser($data){
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }

    public function storeProperty($data){
        $this->db->insert('property',$data);
        return $this->db->insert_id();
    }

    public function storeDevelopersToProperty($data){
        $this->db->insert_batch('users_properties',$data);
    }
}