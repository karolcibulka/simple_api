<?php


class PropertySettings_model extends CI_Model
{

    public function getProperty($property_id,$pure = false){
        $data = $this->db->select('*')
            ->from('property_settings as ps')
            ->where('ps.property_id',$property_id)
            ->join('property_settings_lang as psl','psl.property_id = ps.property_id','left')
            ->get()
            ->result_array();

        $response = array();

        if(isset($data) && !empty($data)){
            foreach($data as $d){
                if(isset($d['lang']) && !empty($d['lang'])){
                    $response['languages'][$d['lang']]['description'] = $d['description'];
                }

                $d['currencies'] = json_decode($d['currencies'],true);
                $d['public_languages'] = json_decode($d['public_languages'],true);
                if(!$pure){
                    $d['favicons'] = isset($d['favicon']) && !empty($d['favicon']) ? getUploadPathFavicon('favicon',$d['favicon']) : '';
                    $d['logo'] = isset($d['logo']) && !empty($d['logo']) ? getUploadPathLogo('logo',$d['logo']) : '';
                }

                $response = array_merge($response,$d);
            }
        }

       return $response;

    }

    public function insertProperty($data){
        $this->db->insert('property_settings',$data);
        return $this->db->insert_id();
    }

    public function updateProperty($property_id,$data){
        $this->db->where('property_id',$property_id)->update('property_settings',$data);
    }
}