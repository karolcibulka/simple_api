<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-06-05
 * Time: 17:32
 */

class Dashboard_model extends CI_Model
{
    public function getDailyMenus($property_id, $type)
    {
        $this->db->select('*')
            ->from('daily_menu as dm')
            ->where('dm.property_id', $property_id)
            ->where('dm.deleted', '0');
        if ($type == 'today') {
            $this->db->where('DATE(dm.date)', date('Y-m-d'));
        } elseif ($type == 'week') {
            $this->db->where('DATE(dm.date)>=', date('Y-m-d', strtotime('monday this week')));
            $this->db->where('DATE(dm.date)<=', date('Y-m-d', strtotime('sunday this week')));
        } elseif ($type == 'month') {
            $this->db->where('YEAR(dm.date)', date('Y'));
            $this->db->where('MONTH(dm.date)', date('m'));
        } elseif ($type == 'year') {
            $this->db->where('YEAR(dm.date)', date('Y'));
        }

        $data = $this->db->get()->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]           = $d;
                $response[$d['id']]['selled'] = '0';
                $response[$d['id']]['color']  = random_color();
            }
        }

        return $response;
    }

    public function getDiscounts($property_id){
        $data = $this->db->select('*')
            ->from('discounts as d')
            ->where('d.property_id',$property_id)
            ->where('d.active','1')
            ->where('d.deleted','0')
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d;
                $response[$d['id']]['selled'] = 0;
                $response[$d['id']]['color'] = random_color();
            }
        }

        return $response;
    }

    public function getStats($type, $property_id)
    {
        $transportations = $this->getTransportations($property_id);
        $offers          = $this->getOffers($property_id);
        $dailyMenus      = $this->getDailyMenus($property_id, $type);
        $packings        = $this->getPacking($property_id);
        $discounts       = $this->getDiscounts($property_id);

        $accepted_statuses = array(savedToBGStatus(),savedToBGAndPayedStatus());

        $this->db->select('*,ri.count as "offerCount",r.discount_price as "resPrice",ri.type as "offerType"')
            ->from('reservations as r')
            ->join('reservations_item as ri', 'ri.reservation_id = r.id', 'left')
            ->where('r.property_id', $property_id)
            ->where_in('r.status', $accepted_statuses);
        if ($type == 'today') {
            $this->db->where('DATE(r.created_at)', date('Y-m-d'));
        } elseif ($type == 'week') {
            $this->db->where('DATE(r.created_at)>=', date('Y-m-d', strtotime('monday this week')));
            $this->db->where('DATE(r.created_at)<=', date('Y-m-d', strtotime('sunday this week')));
        } elseif ($type == 'month') {
            $this->db->where('YEAR(r.created_at)', date('Y'));
            $this->db->where('MONTH(r.created_at)', date('m'));
        } elseif ($type == 'year') {
            $this->db->where('YEAR(r.created_at)', date('Y'));
        }
        $data = $this->db->get()->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['reservation_id']]['created_at']                      = $d['created_at'];
                $response[$d['reservation_id']]['transportation_id']               = $d['transportation_id'];
                $response[$d['reservation_id']]['price']                           = $d['resPrice'];
                $response[$d['reservation_id']]['packingPrice']                    = $d['packing_price'];
                $response[$d['reservation_id']]['itemsPrice']                      = $d['items_price'];
                $response[$d['reservation_id']]['menuPrice']                       = $d['menu_price'];
                $response[$d['reservation_id']]['currency']                        = $d['currency'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['id']    = $d['offer_id'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['count'] = $d['offerCount'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['type']  = $d['offerType'];
            }
        }

        $statsHours          = array();
        $offersHours         = array();
        $dailyMenuHours      = array();
        $discountsHours      = array();
        $transportationPrice = 0;
        $packingPrice        = 0;
        $reservationsPrice   = 0;
        $dailyMenuPrice      = 0;
        $fullPrice           = 0;
        $currency            = '';
        $reservationsCount   = 0;
        for ($i = 0; $i <= 23; $i++) {
            $key                  = $i < 10 ? '0' . $i : $i;
            $statsHours[$key]     = 0;
            $offersHours[$key]    = 0;
            $dailyMenuHours[$key] = 0;
            $discountsHours[$key] = 0;
        }
        foreach ($response as $res) {
            $reservationsCount                                    += 1;
            $currency                                             = $res['currency'];
            $statsHours[date('H', strtotime($res['created_at']))] += 1;
            if (isset($transportations[$res['transportation_id']]) && !empty($transportations[$res['transportation_id']])) {
                $transportations[$res['transportation_id']]['selled'] += 1;
                $transportationPrice                                  += $transportations[$res['transportation_id']]['price'];
                $fullPrice                                            += $transportations[$res['transportation_id']]['price'];
            }
            $reservationsPrice += $res['itemsPrice'];
            $dailyMenuPrice    += $res['menuPrice'];
            $fullPrice         += $res['price'];
            $packingPrice      += $res['packingPrice'];
            if (!empty($res['offers'])) {
                foreach ($res['offers'] as $offerID => $offer) {
                    if(isset($offer['discount_id']) && !empty($offer['discount_id'])){
                        if(isset($discounts[$offer['discount_id']]) && !empty($discounts[$offer['discount_id']])){
                            $discounts[$offer['discount_id']]['selled'] += 1;
                            $discountsHours[date('H', strtotime($res['created_at']))] += 1;
                        }
                    }
                    if ($offer['type'] == 'products') {
                        if (isset($offers[$offerID]) && !empty($offers[$offerID])) {
                            $offersHours[date('H', strtotime($res['created_at']))] += 1;
                            $offers[$offerID]['selled'] += $offer['count'];
                        }
                    } else {
                        if (isset($dailyMenus[$offerID]) && !empty($dailyMenus[$offerID])) {
                            $dailyMenuHours[date('H', strtotime($res['created_at']))] += 1;
                            $dailyMenus[$offerID]['selled'] += $offer['count'];
                        }
                    }
                }
            }
        }

        $finalRes                         = array();
        $finalRes['hours']                = $statsHours;
        $finalRes['offerHours']           = $offersHours;
        $finalRes['dailyMenuHours']       = $dailyMenuHours;
        $finalRes['discountsHours']       = $discountsHours;
        $finalRes['currency']             = $currency;
        $finalRes['reservationsCount']    = $reservationsCount;
        $finalRes['transportationsPrice'] = formatNumApi($transportationPrice);
        $finalRes['reservationsPrice']    = formatNumApi($reservationsPrice);
        $finalRes['packingPrice']         = formatNumApi($packingPrice);
        $finalRes['fullPrice']            = formatNumApi($fullPrice);
        $finalRes['menuPrice']            = formatNumApi($dailyMenuPrice);
        $finalRes['transportations']      = $transportations;
        $finalRes['packings']             = $packings;
        $finalRes['offers']               = $offers;
        $finalRes['dailyMenus']           = $dailyMenus;
        $finalRes['discounts']           = $discounts;

        return $finalRes;
    }

    public function getTransportations($property_id)
    {
        $data = $this->db->select('internal_name,price,id')
            ->from('transportation as t')
            ->where('t.property_id', $property_id)
            ->where('t.active', '1')
            ->where('t.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['selled']        = 0;
                $response[$d['id']]['color']         = random_color();
            }
        }

        return $response;
    }

    public function getOffers($property_id)
    {
        $data = $this->db->select('o.internal_name,o.unit_price,o.id,o.packing_id')
            ->from('offers as o')
            ->where('o.property_id', $property_id)
            ->join('category_offers as co', 'co.id = o.category_id')
            ->where('o.active', '1')
            ->where('co.active', '1')
            ->where('o.deleted', '0')
            ->where('co.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['unit_price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['packing_id']    = $d['packing_id'];
                $response[$d['id']]['selled']        = 0;
                $response[$d['id']]['color']         = random_color();
            }
        }

        return $response;
    }

    public function getPacking($property_id)
    {
        $data = $this->db->select('internal_name,price,id')
            ->from('packing as p')
            ->where('p.property_id', $property_id)
            ->where('p.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['selled']        = 0;
            }
        }

        return $response;
    }

    public function getWeb($property_id)
    {
        $data = $this->db->select('web,default_lang')
            ->from('property_settings_web')
            ->where('property_id', $property_id)
            ->get()
            ->row_array();

        if (!empty($data['web'])) {
            return $data['web'] . '/' . $data['default_lang'];
        }
        return false;

    }

    public function updateProperty($propertyID, $data)
    {
        $this->db->where('id', $propertyID)->update('property', $data);
    }

    public function getDevReservationsData($dateFrom = false, $dateTo = false,$property_id = false){

        $q = $this->db->select('r.*,p.name as property_name')
            ->from('reservations as r')
            ->join('property as p','p.id = r.property_id')
            ->where('DATE(r.created_at) >= ',$dateFrom)
            ->where('DATE(r.created_at) <=',$dateTo)
            ->order_by('r.id','desc');

        if($property_id){
            $q->where('r.property_id',$property_id);
        }

        $data = $q->get()
            ->result_array();

        $response = array(
            'total' => 0,
            'total_success' => 0,
            'total_price' => 0,
            'menu_price' => 0,
            'packing_price' => 0,
            'delivery_price' => 0,
            'items_price' => 0,
            'discount_unit_price' => 0,
            'discount_price' => 0,
            'properties' => array(),
            'reservations' => array()
        );

        $default_fields = array('count','total_count','discount_price','delivery_price','total_price','packing_price','menu_price','discount_unit_price','packing_price','items_price');

        if(!empty($data)){
            foreach($data as $d){

                foreach($default_fields as $default_field){
                    if(!isset($response['properties'][$d['property_id']][$default_field]) || empty($response['properties'][$d['property_id']][$default_field] )){
                        $response['properties'][$d['property_id']][$default_field]  = 0;
                    }
                }

                $response['total'] += 1;
                $response['properties'][$d['property_id']]['total_count'] += 1;
                $response['properties'][$d['property_id']]['name'] = $d['property_name'];
                if($d['status'] === savedToBGStatus() || $d['status'] === savedToBGAndPayedStatus()){
                    $response['properties'][$d['property_id']]['count'] += 1;
                    $response['total_success'] += 1;
                    $response['total_price'] += $d['total_price'];
                    $response['properties'][$d['property_id']]['total_price'] += $d['total_price'];
                    $response['packing_price'] += $d['packing_price'];
                    $response['properties'][$d['property_id']]['packing_price'] += $d['packing_price'];
                    $response['delivery_price'] += $d['delivery_price'];
                    $response['properties'][$d['property_id']]['delivery_price'] += $d['delivery_price'];
                    $response['discount_price'] += $d['discount_price'];
                    $response['properties'][$d['property_id']]['discount_price'] += $d['discount_price'];
                    $response['items_price'] += $d['items_price'];
                    $response['properties'][$d['property_id']]['items_price'] += $d['items_price'];
                    $response['menu_price'] += $d['menu_price'];
                    $response['properties'][$d['property_id']]['menu_price'] += $d['menu_price'];
                    $response['discount_unit_price'] += $d['discount_unit_price'];
                    $response['properties'][$d['property_id']]['discount_unit_price'] += $d['discount_unit_price'];
                }

                $response['reservations'][$d['id']] = $d;
            }
        }

        return $response;
    }
}