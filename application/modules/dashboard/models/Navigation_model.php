<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-03-08
 * Time: 11:00
 */

class Navigation_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    private function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }


    public function getNavItems(){

        $data = $this->db->select('navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",controllers.name as "path",navigation.order as "order",icons.value as "icon",navigation_type.value as "type",navigation.parent as "parent",navigation.link,navigation.placeholder,navigation.link_path')
            ->from('navigation')
            ->where('navigation.active',1)
            ->where('navigation.deleted',0)
            ->join('navigation_type','navigation_type.id = navigation.type')
            ->join('controllers','controllers.id = navigation.controller','LEFT')
            ->join('icons','icons.id = navigation.icon','LEFT')
            ->order_by('order','asc')
            ->get()
            ->result_array();

        return $this->buildTree($data,0);

    }


    public function getAllChilds($id){
        $data = $this->db->select('navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",navigation.controller as "path",navigation.order as "order",navigation.icon as "icon",navigation.type as "type",navigation.parent as "parent",navigation.link,navigation.placeholder,navigation.link_path')
            ->from('navigation')
            ->where('parent',$id)
            ->order_by('order','asc')
            ->get()
            ->result_array();

        return $data;

    }

    public function getControllers(){
        $data = $this->db->select('controllers.id as "id", controllers.name as "name"')
            ->from('controllers')
            ->where('active',1)
            ->get()
            ->result_array();

        return $data;
    }

    public function getNavTypes(){
        return $this->db->select('navigation_type.value as "value", navigation_type.name as "name",navigation_type.id as "id"')
            ->from('navigation_type')
            ->get()
            ->result_array();
    }

    public function getIcons(){
        return $this->db->select('icons.value as "value", icons.name as "name",icons.id as "id"')
            ->from('icons')
            ->get()
            ->result_array();
    }

    public function getHighestOrderNum(){
        return $this->db->select('MAX("order") as "num"')
            ->from('navigation')
            ->get()
            ->row_array();
    }

    public function insertNewMenuOption($data){
        if(isset($data) && !empty($data)){
            $this->db->insert('navigation', $data);
        }
    }

    public function getCMSitemByID($id){
        return $this->db->select('navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",navigation.controller as "path",navigation.order as "order",navigation.icon as "icon",navigation.type as "type",navigation.parent as "parent",navigation.placeholder as "placeholder", navigation.link as "link", navigation.link_path as "link_path"')
            ->from('navigation')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function editMenuOption($data){
        if(isset($data) && !empty($data)){
            return $this->db->where('id', $data['id'])
            ->update('navigation', $data);
        }
    }

    public function deleteMenuOption($data,$id){
        if(isset($data) && !empty($data)){
            return $this->db->where('id', $id)
                ->update('navigation', $data);
        }
    }
    public function updateMenuOrder($data,$id){
        if(isset($data) && !empty($data)) {
            if(isset($id) && !empty($id)) {
                return $this->db->where('id', $id)
                    ->update('navigation', $data);
            }
        }
    }
}