<?php


class Products_model extends CI_Model
{
    public function getProducts($property_id){
        return  $this->db->select('
            p.*,
            group_concat(c.internal_name separator ", ") as categories_internal,
        ')
            ->from('products as p')
            ->where('p.property_id',$property_id)
            ->join('categories_product as cp','cp.product_id = p.id','left')
            ->join('categories as c','c.id = cp.category_id AND c.deleted = 0','left')
            ->where('p.deleted',0)
            ->group_by('p.id')
            ->get()
            ->result_array();
    }

    public function getCategories($property_id){

        return $this->db->select('*')
            ->from('categories as c')
            ->where('c.property_id',$property_id)
            ->where('c.deleted',0)
            ->order_by('c.order','asc')
            ->get()
            ->result_array();
    }

    public function getProduct($property_id,$id){
        $data =  $this->db->select('
            p.*,
            group_concat(cp.category_id separator "|") as categories
        ')
            ->from('products as p')
            ->where('p.id', $id)
            ->where('p.property_id',$property_id)
            ->join('categories_product as cp','cp.product_id = p.id','left')
            ->get()
            ->row_array();

        if(isset($data['categories']) && !empty($data['categories'])){
            $data['categories'] = explode('|',$data['categories']);
        }

        return $data;
    }

    public function insertCategories($data){
        $this->db->insert_batch('categories_product',$data);
    }

    public function removeCategories($id){
        $this->db->where('product_id',$id)->delete('categories_product');
    }

    public function insertProduct($data){
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }

    public function insertProductCategory($data){
        $this->db->insert('product_categories', $data);
        return $this->db->insert_id();
    }

    public function insertCategoryLanguages($data){
        $this->db->insert_batch('product_categories_lang',$data);
    }

    public function insertProductLanguages($data){
        $this->db->insert_batch('products_lang',$data);
    }

    public function removeCategoryLanguages($property_id,$id){
        $this->db->where('category_id',$id)
            ->where('property_id',$property_id)
            ->delete('product_categories_lang');
    }

    public function updateProductCategory($property_id,$id,$data){
        $this->db->where('id',$id)
            ->where('property_id',$property_id)
            ->update('product_categories',$data);
    }

    public function getCategory($property_id,$id){
        $data = $this->db->select('*')
            ->from('product_categories as pc')
            ->join('product_categories_lang as pcl','pcl.category_id = pc.id','left')
            ->where('pc.id',$id)
            ->where('pc.property_id',$property_id)
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response['internal_name'] = $d['internal_name'];
                $response['behavior_id'] = $d['behavior_id'];
                $response['parent'] = $d['parent'];
                if(isset($d['lang']) && !empty($d['lang'])){
                    $response['languages'][$d['lang']] = array(
                        'name' => $d['name'],
                        'description' => $d['description']
                    );
                }
            }
        }

        return $response;
    }

    public function updateProduct($property_id,$id,$data){
        $this->db->where('id',$id)
            ->where('property_id',$property_id)
            ->update('products',$data);
    }

    public function removeProductLanguages($property_id,$id){
        $this->db->where('product_id',$id)->where('property_id',$property_id)->delete('products_lang');
    }

}