<?php


class Reservations_model extends CI_Model
{
    public function getReservations($propertyID,$start,$per_page,$filter){
         $q = $this->db->select('r.*,p.internal_name as payment_internal_name,pt.name as payment_type_name')
            ->from('reservations as r')
            ->join('payments as p','r.payment_id = p.id','left')
            ->join('payment_types as pt','pt.id = p.payment_type_id','left')
            ->where('r.property_id',$propertyID)
            ->order_by('r.created_at','desc');

         if($start && $per_page){
            $q->limit($per_page, $start);
         }

        if(isset($filter) && !empty($filter)){
            foreach($filter as $field => $value){
                if($value == '') continue;
                if($field == 'created_at_from'){
                    $q->where('r.created_at>=',date('Y-m-d H:i:s',strtotime($value.' 00:00:01')));
                }
                elseif($field == 'created_at_to'){
                    $q->where('r.created_at<=',date('Y-m-d H:i:s',strtotime($value.' 23:59:59')));
                }
                else{
                    $q->where('r.'.$field,$value);
                }
            }
        }

        return $q->get()->result_array();

    }

    public function getReservationByEmail($propertyID,$email){
        return $this->db->select('*')
            ->from('reservations')
            ->where('property_id',$propertyID)
            ->where('email',$email)
            ->get()
            ->result_array();
    }

    public function get_count($property_id,$filter){
        $this->db->select('count(*)');
        $this->db->where('r.property_id',$property_id);
        if(isset($filter) && !empty($filter)){
            foreach($filter as $field => $value){
                if($value == '') continue;
                if($field == 'created_at_from'){
                    $this->db->where('r.created_at>=',date('Y-m-d H:i:s',strtotime($value.' 00:00:01')));
                }
                elseif($field == 'created_at_to'){
                    $this->db->where('r.created_at<=',date('Y-m-d H:i:s',strtotime($value.' 23:59:59')));
                }
                else{
                    $this->db->where('r.'.$field,$value);
                }
            }
        }
        $query = $this->db->get('reservations as r');
        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }

    public function getReservation($property_id,$reservation_id){
        $this->db->select('r.*,ri.*,r.total_price as "resPrice",ri.external_id as "oPluID",t.internal_name as "deliveryName",p.internal_name as "paymentName",rl.id as "log_id",rl.request as "log_request",rl.response as "log_response",rl.status as "log_status"')
            ->from('reservations as r')
            ->where('r.property_id',$property_id)
            ->where('r.id',$reservation_id)
            ->join('reservations_item as ri','ri.reservation_id = r.id','left')
            ->join('transportation as t','t.id = r.transportation_id','left')
            ->join('reservations_log as rl','r.id = rl.reservation_id','left')
            ->join('payments as p','p.id = r.payment_id','left')
            ->where('r.status!=',changedPriceStatus());

         $data =   $this->db->get()->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response['first_name'] = $d['first_name'];
                $response['last_name'] = $d['last_name'];
                $response['email'] = $d['email'];
                $response['phone'] = $d['phone'];
                $response['currency'] = $d['currency'];
                $response['note'] = $d['note'];
                $response['request'] = json_decode($d['request'],true);
                $response['total_price'] = $d['resPrice'];
                $response['packing_price'] = $d['packing_price'];
                $response['delivery_price'] = $d['delivery_price'];
                $response['items_price'] = $d['items_price'];
                $response['is_delivery'] = $d['is_delivery'];
                $response['delivery_name'] = $d['deliveryName'];
                $response['payment_name'] = $d['paymentName'];
                $response['lang'] = $d['lang'];
                $response['payed'] = $d['payed'];
                $response['city'] = $d['city'];
                $response['street'] = $d['street'];
                $response['number'] = $d['number'];
                $response['order_id'] = $d['order_id'];
                $response['status'] = $d['status'];
                $response['room_nr'] = $d['room_nr'];
                $response['table_token'] = $d['table_token'];
                $response['accommodated_guest_id'] = $d['accommodated_guest_id'];
                if($d['type'] == 'products'){
                    $response['items'][$d['offer_id']]['count'] = $d['count'];
                    $response['items'][$d['offer_id']]['offer_id'] = $d['offer_id'];
                    $response['items'][$d['offer_id']]['external_id'] = $d['oPluID'];
                    $response['items'][$d['offer_id']]['unit_price'] = $d['unit_price_with_upsells'];
                    $response['items'][$d['offer_id']]['price'] = $d['price_with_upsells'];
                    $response['items'][$d['offer_id']]['offer_data'] = json_decode($d['offer_data'],true);
                    $response['items'][$d['offer_id']]['external_plu_data'] = json_decode($d['external_plu_data'],true);
                }
                else{
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['id'] = $d['daily_menu_internal_id'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['count'] = $d['count'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['external_id'] = $d['oPluID'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['offer_id'] = $d['offer_id'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['unit_price'] = $d['unit_price'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['is_traceable'] = $d['is_traceable'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['menu_type'] = $d['menu_type'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['price'] = $d['price'];
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['offer_data'] = json_decode($d['offer_data'],true);
                    $response['menu'][$d['daily_menu_internal_id']][$d['menu_type']]['external_plu_data'] = json_decode($d['external_plu_data'],true);
                }
                if(isset($d['log_id'])){
                    $response['logs'][$d['log_id']]['id'] = $d['log_id'];
                    $response['logs'][$d['log_id']]['request'] = $d['log_request'];
                    $response['logs'][$d['log_id']]['response'] = $d['log_response'];
                    $response['logs'][$d['log_id']]['status'] = $d['log_status'];
                }
            }
        }

       return $response;
    }
}