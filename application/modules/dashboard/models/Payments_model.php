<?php


class Payments_model extends CI_Model
{
    public function getExternalPayments($property_id){
        return $this->db->select('*')
            ->from('external_payment_type_list')
            ->where('enabled','1')
            ->where('active','1')
            ->where('property_id',$property_id)
            ->get()
            ->result_array();
    }

    public function getPaymentTypes(){
        return $this->db->select('*')
            ->from('payment_types')
            ->where('deleted','0')
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function insert($data){
        $this->db->insert('payments',$data);
        return $this->db->insert_id();
    }

    public function insertLanguages($languages){
        $this->db->insert_batch('payments_lang',$languages);
    }


    public function insertLanguage($data){
        $this->db->insert('payments_lang',$data);
    }

    public function removeLanguages($property_id,$payment_id){
        $this->db->where('property_id',$property_id)->where('payment_id',$payment_id)->delete('payments_lang');
    }

    public function update($property_id,$payment_id,$data){
        $this->db->where('property_id',$property_id)->where('id',$payment_id)->update('payments',$data);
    }

    public function getPayments($property_id){
       return  $this->db->select('*')
           ->from('payments')
           ->where('property_id',$property_id)
           ->order_by('order','asc')
           ->where('deleted','0')
           ->get()
           ->result_array();
    }

    public function getPayment($id,$property_id){
        $data = $this->db->select('*')
            ->from('payments as p')
            ->where('p.id',$id)
            ->where('p.property_id',$property_id)
            ->join('payments_lang as pl','pl.payment_id = p.id','left')
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response['id'] = $d['id'];
                $response['internal_name'] = $d['internal_name'];
                $response['table_check_customer'] = $d['table_check_customer'];
                $response['check_customer'] = $d['check_customer'];
                $response['for_table'] = $d['for_table'];
                $response['external_id'] = $d['external_id'];
                $response['payment_type_id'] = $d['payment_type_id'];
                $response['image'] = upload_url($property_id.'/payments/'.$d['image']);
                $response['image_path'] = $d['image'];
                $response['langs'][$d['lang']]['name'] = $d['name'];
                $response['langs'][$d['lang']]['short_description'] = $d['short_description'];
            }
        }

        return $response;
    }

}