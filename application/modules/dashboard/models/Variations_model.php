<?php


class Variations_model extends CI_Model
{

    public function insertVariation($data){
        $this->db->insert('variations',$data);
        return $this->db->insert_id();
    }

    public function insertImages($data){
        $this->db->insert_batch('variations_images',$data);
    }

    public function getVariations($property_id){
        return $this->db->select('
            v.*,
            p.internal_name as product_internal_name,
            group_concat(c.internal_name separator ", ") as categories
        ')
            ->from('variations as v')
            ->where('v.property_id',$property_id)
            ->join('products as p','p.id = v.product_id AND p.deleted = 0','left')
            ->where('v.deleted',0)
            ->join('categories_product as cp','cp.product_id = p.id','left')
            ->join('categories as c','c.id = cp.category_id AND c.deleted = 0','left')
            ->group_by('v.id')
            ->get()
            ->result_array();
    }

    public function updateVariation($property_id,$id,$data){
        $this->db->where('property_id',$property_id)
            ->where('id',$id)
            ->update('variations',$data);
    }

    public function getVariation($property_id,$id){
        $data = $this->db->select('
            vi.*,
            v.*,
            vl.*,
            vd.*,
            vf.*,

            vv.variability_id as vv_variability_id,
            vv.variability_item_id as vv_variability_item_id,
            vv.price_profile as vv_price_profile,
            vv.unit_price as vv_unit_price,
            vvd.discount_id as vvd_discount_id
        ')
            ->from('variations as v')
            ->where('v.property_id',$property_id)
            ->where('v.id',$id)
            ->join('variations_images as vi','vi.variation_id = v.id','left')
            ->join('variations_lang as vl','vl.variation_id = v.id','left')
            ->join('variations_discount as vd','vd.variation_id = v.id','left')
            ->join('variations_facilities as vf','vf.variation_id = v.id','left')
            ->join('variations_variabilities as vv','vv.variation_id = v.id AND vv.active = 1','left')
            ->join('variations_variabilities_discounts as vvd','vvd.variation_id = v.id','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                if(empty($response)){
                    $response = $d;
                }

                if(isset($d['lang']) && !empty($d['lang'])){
                    $response['languages'][$d['lang']] = array(
                        'name' => $d['name'],
                        'perex' => $d['perex'],
                        'description' => $d['description']
                    );
                }

                if(isset($d['vv_variability_id']) && !empty($d['vv_variability_id'])){
                    $response['variabilities'][$d['vv_variability_id']][$d['vv_variability_item_id']]['price_profile'] = $d['vv_price_profile'];
                    $response['variabilities'][$d['vv_variability_id']][$d['vv_variability_item_id']]['unit_price'] = $d['vv_unit_price'];
                }

                if(isset($d['vvd_discount_id']) && !empty($d['vvd_discount_id'])){
                    $response['variabilities'][$d['vv_variability_id']][$d['vv_variability_item_id']]['discounts'][$d['vvd_discount_id']] = $d['vvd_discount_id'];
                }

                if(isset($d['facility_id']) && !empty($d['facility_id'])){
                    $response['facilities'][$d['facility_id']][$d['facility_item_id']] = 1;
                }

                if(isset($d['discount']) && !empty($d['discount'])){
                    $response['discounts'][$d['discount']] = $d['discount'];
                }

                if(isset($d['image']) && !empty($d['image'])){
                    $response['images'][$d['image']] = $d['image'];
                }
            }
        }

        return $response;
    }

    public function getOldImage($id,$images){
        return $this->db->select('image')
            ->from('variations_images')
            ->where_not_in('image',$images)
            ->where('variation_id',$id)
            ->get()
            ->result_array();
    }

    public function removeOldImage($id,$image){
        return $this->db->where('image',$image)
            ->where('variation_id',$id)
            ->delete('variations_images');
    }

    public function getAllOldImage($id){
        return $this->db->select('image')
            ->from('variations_images')
            ->where('variation_id',$id)
            ->get()
            ->result_array();
    }

    public function getProducts($property_id){
        return $this->db->select('*')
            ->from('products')
            ->where('property_id',$property_id)
            ->where('deleted',0)
            ->get()
            ->result_array();
    }

    public function getFacilities($property_id){
        $data =  $this->db->select('
            f.internal_name,
            f.id,
            fi.id as fi_id,
            fi.value as fi_value,
        ')
            ->from('facilities as f')
            ->where('f.active',1)
            ->where('f.deleted',0)
            ->where('f.property_id',$property_id)
            ->order_by('f.order,fi.order','asc')
            ->join('facilities_items as fi','fi.facility_id = f.id AND fi.active = 1','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                if(isset($d['fi_id']) && !empty($d['fi_id'])){
                    $response[$d['id']]['id'] = $d['id'];
                    $response[$d['id']]['internal_name'] = $d['internal_name'];
                    $response[$d['id']]['items'][$d['fi_id']]['id'] = $d['fi_id'];
                    $response[$d['id']]['items'][$d['fi_id']]['value'] = $d['fi_value'];
                }
            }
        }

       return $response;
    }

    public function getVariabilities($property_id){
        $data =  $this->db->select('
            v.internal_name,
            v.id,
            vi.id as vi_id,
            vi.value as vi_value,
        ')
            ->from('variabilities as v')
            ->where('v.active',1)
            ->where('v.deleted',0)
            ->where('v.property_id',$property_id)
            ->order_by('v.order,vi.order','asc')
            ->join('variabilities_items as vi','vi.variability_id = v.id AND vi.active = 1','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                if(isset($d['vi_id']) && !empty($d['vi_id'])){
                    $response[$d['id']]['id'] = $d['id'];
                    $response[$d['id']]['internal_name'] = $d['internal_name'];
                    $response[$d['id']]['items'][$d['vi_id']]['id'] = $d['vi_id'];
                    $response[$d['id']]['items'][$d['vi_id']]['value'] = $d['vi_value'];
                }
            }
        }

        return $response;
    }

    public function insertVariability($data){
        $this->db->insert('variations_variabilities',$data);
        return $this->db->insert_id();
    }

    public function insertVariationVariabilitiesLanguages($data){
        $this->db->insert_batch('variations_variabilities_lang',$data);
    }

    public function removeVariabilityLanguages($id){
        $this->db->where('variation_variability_id',$id)->delete('variations_variabilities_lang');
    }

    public function updateVariationVariability($id,$data){
        $this->db->where('id',$id)->update('variations_variabilities',$data);
    }

    public function deactivateVariationVariabilities($id){
        $this->db->where('variation_id',$id)->update('variations_variabilities',array('active'=>0));
    }

    public function insertLanguages($data){
        $this->db->insert_batch('variations_lang',$data);
    }

    public function insertDiscounts($data){
        $this->db->insert_batch('variations_discount',$data);
    }

    public function insertFacilities($data){
        $this->db->insert_batch('variations_facilities',$data);
    }

    public function deleteFacilities($id){
        $this->db->where('variation_id',$id)->delete('variations_facilities');
    }

    public function deleteDiscounts($id){
        $this->db->where('variation_id',$id)->delete('variations_discount');
    }

    public function deleteLanguages($id){
        $this->db->where('variation_id',$id)->delete('variations_lang');
    }

    public function disableVariationVariabilities($id){
        $this->db->where('variation_id',$id)->update('variations_variabilities',array( 'active' => 0 ));
    }

    public function replaceVariationVariability($data){
        $this->db->replace('variations_variabilities',$data);
    }

    public function insertVariationVariabilitiesDiscounts($data){
        $this->db->insert_batch('variations_variabilities_discounts',$data);
    }

    public function removeVariationVariabilitiesDiscounts($id){
        $this->db->where('variation_id',$id)->delete('variations_variabilities_discounts');
    }
}