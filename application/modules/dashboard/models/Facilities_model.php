<?php


class Facilities_model extends CI_Model
{

    public function getFacilities($property_id){
        return $this->db->select('*')
            ->from('facilities')
            ->where('property_id',$property_id)
            ->order_by('order','asc')
            ->where('deleted',0)
            ->get()
            ->result_array();
    }

    public function getFacility($property_id,$id){
        $data = $this->db->select('
            f.internal_name,
            f.id,
            fl.lang as fl_lang,
            fl.name as fl_name,
            fi.id as fi_id,
            fi.value as fi_value,
            fil.name as fil_name,
            fil.lang as fil_lang
        ')->from('facilities as f')
            ->where('f.property_id',$property_id)
            ->where('f.id',$id)
            ->join('facilities_lang as fl','fl.facility_id = f.id','left')
            ->join('facilities_items as fi','fi.facility_id = f.id AND fi.active = 1','left')
            ->join('facilities_items_lang as fil','fil.facilities_item_id = fi.id','left')
            ->order_by('fi.order','asc')
            ->get()
            ->result_array();


        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response['id'] = $d['id'];
                $response['internal_name'] = $d['internal_name'];
                if(isset($d['fl_lang']) && !empty($d['fl_lang'])){
                    $response['languages'][$d['fl_lang']] = $d['fl_name'];
                }
                if(isset($d['fi_id']) && !empty($d['fi_id'])){
                    $response['items'][$d['fi_id']]['id'] = $d['fi_id'];
                    $response['items'][$d['fi_id']]['value'] = $d['fi_value'];
                    if(isset($d['fil_lang']) && !empty($d['fil_lang'])){
                        $response['items'][$d['fi_id']]['languages'][$d['fil_lang']] = $d['fil_name'];
                    }
                }
            }
        }

        return $response;
    }

    public function insertData($data){
        $this->db->insert('facilities',$data);
        return $this->db->insert_id();
    }

    public function updateFacility($property_id,$id,$data){
        return $this->db->where('property_id',$property_id)
            ->where('id',$id)
            ->update('facilities',$data);
    }

    public function deactivateItems($property_id,$id){
        return $this->db->where('property_id',$property_id)
            ->where('facility_id',$id)
            ->update('facilities_items',array('active'=>0));
    }

    public function updateItem($property_id,$id,$data){
        return $this->db->where('property_id',$property_id)
            ->where('id',$id)
            ->update('facilities_items',$data);
    }

    public function insertLanguages($data){
        $this->db->insert_batch('facilities_lang',$data);
    }

    public function removeLanguages($id){
        $this->db->where('facility_id',$id)->delete('facilities_lang');
    }

    public function insertItem($data){
        $this->db->insert('facilities_items',$data);
        return $this->db->insert_id();
    }

    public function insertItemLanguages($data){
        $this->db->insert_batch('facilities_items_lang',$data);
    }

    public function deleteItemLanguages($id){
        $this->db->where('facilities_item_id',$id)->delete('facilities_items_lang');
    }
}