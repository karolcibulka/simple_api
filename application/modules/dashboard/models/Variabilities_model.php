<?php


class Variabilities_model extends CI_Model
{

    public function getVariabilities($property_id){
        return $this->db->select('*')
            ->from('variabilities')
            ->where('property_id',$property_id)
            ->order_by('order','asc')
            ->where('deleted',0)
            ->get()
            ->result_array();
    }

    public function getVariability($property_id,$id){
        $data = $this->db->select('
            v.internal_name,
            v.id,
            vl.lang as vl_lang,
            vl.name as vl_name,
            vi.id as vi_id,
            vi.value as vi_value,
            vil.name as vil_name,
            vil.lang as vil_lang
        ')->from('variabilities as v')
            ->where('v.property_id',$property_id)
            ->where('v.id',$id)
            ->join('variabilities_lang as vl','vl.variability_id = v.id','left')
            ->join('variabilities_items as vi','vi.variability_id = v.id AND vi.active = 1','left')
            ->join('variabilities_items_lang as vil','vil.variabilities_item_id = vi.id','left')
            ->order_by('vi.order','asc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response['id'] = $d['id'];
                $response['internal_name'] = $d['internal_name'];
                if(isset($d['vl_lang']) && !empty($d['vl_lang'])){
                    $response['languages'][$d['vl_lang']] = $d['vl_name'];
                }
                if(isset($d['vi_id']) && !empty($d['vi_id'])){
                    $response['items'][$d['vi_id']]['id'] = $d['vi_id'];
                    $response['items'][$d['vi_id']]['value'] = $d['vi_value'];
                    if(isset($d['vil_lang']) && !empty($d['vil_lang'])){
                        $response['items'][$d['vi_id']]['languages'][$d['vil_lang']] = $d['vil_name'];
                    }
                }
            }
        }

        return $response;
    }

    public function insertData($data){
        $this->db->insert('variabilities',$data);
        return $this->db->insert_id();
    }

    public function updateVariability($property_id,$id,$data){
        return $this->db->where('property_id',$property_id)
            ->where('id',$id)
            ->update('variabilities',$data);
    }

    public function deactivateItems($property_id,$id){
        return $this->db->where('property_id',$property_id)
            ->where('variability_id',$id)
            ->update('variabilities_items',array('active'=>0));
    }

    public function updateItem($property_id,$id,$data){
        return $this->db->where('property_id',$property_id)
            ->where('id',$id)
            ->update('variabilities_items',$data);
    }

    public function insertLanguages($data){
        $this->db->insert_batch('variabilities_lang',$data);
    }

    public function removeLanguages($id){
        $this->db->where('variability_id',$id)->delete('variabilities_lang');
    }

    public function insertItem($data){
        $this->db->insert('variabilities_items',$data);
        return $this->db->insert_id();
    }

    public function insertItemLanguages($data){
        $this->db->insert_batch('variabilities_items_lang',$data);
    }

    public function deleteItemLanguages($id){
        $this->db->where('variabilities_item_id',$id)->delete('variabilities_items_lang');
    }
}