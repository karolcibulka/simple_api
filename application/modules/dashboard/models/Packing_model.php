<?php


class Packing_model extends CI_Model
{
    public function getPlus($propertyID){
        return $this->db->select('*')
            ->from('external_plu_list')
            ->where('property_id',$propertyID)
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function storePacking($data){
        $this->db->insert('packing',$data);
        return $this->db->insert_id();
    }

    public function getPackings($propertyID){
        return $this->db->select('*')
            ->from('packing')
            ->where('deleted','0')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();
    }

    public function getPacking($propertyID,$packingID){
        return $this->db->select('*')
            ->from('packing')
            ->where('property_id',$propertyID)
            ->where('id',$packingID)
            ->get()
            ->row_array();
    }

    public function updatePacking($data,$propertyID,$packingID){
        $this->db->where('property_id',$propertyID)->where('id',$packingID)->update('packing',$data);
    }
}