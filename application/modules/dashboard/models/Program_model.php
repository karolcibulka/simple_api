<?php


class Program_model extends CI_Model
{
    public function getPrograms(){
        return $this->db->select('*')
            ->from('program')
            ->where('deleted','0')
            ->get()
            ->result_array();
    }

    public function getProgram($id){
        return $this->db->select('*')
            ->from('program')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function updateProgram($data,$id){
        $this->db->where('id',$id)->update('program',$data);
    }

    public function getControllers(){
        return $this->db->select('*')
            ->from('controllers')
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function insertProgram($data){
        $this->db->insert('program',$data);
        return $this->db->insert_id();
    }
}