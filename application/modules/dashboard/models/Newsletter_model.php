<?php


class Newsletter_model extends CI_Model
{

    public function getSubscribersLimit($property_id,$start = false,$limit = false){

            $q =  $this->db->select('id,property_id,email,created_at,activated_at,active')
                ->from('subscribers as s')
                ->where('s.active','1')
                ->where('s.property_id',$property_id);

            if($start && $limit){
                $q->limit($limit,$start);
            }

          return  $q->get()
                ->result_array();
    }
    public function getSubscribers($property_id){

        return $this->db->select('id,property_id,email,created_at,activated_at,active')
            ->from('subscribers as s')
            ->where('s.active','1')
            ->where('s.property_id',$property_id)
            ->get()
            ->result_array();
    }

    public function get_count($property_id){
        $this->db->select('count(*)');
        $this->db->where('property_id',$property_id);
        $query = $this->db->get('subscribers');
        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }
}