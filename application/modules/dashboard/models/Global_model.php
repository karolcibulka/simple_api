<?php
class Global_model extends CI_Model
{

    public function get_languages()
    {
        return $this->db->select('*')
            ->from('languages')
            ->get()
            ->result_array();
    }

    public function getWeb($propertyID){
        $data = $this->db->select('web')
            ->from('property_settings_web')
            ->where('property_id',$propertyID)
            ->get()
            ->row_array();

        return $data['web'];
    }


    public function getProperty($propertyID){
        $data = $this->db->select('p.*,ps.public_languages,ps.default_language')
            ->from('property as p')
            ->join('property_settings as ps','ps.property_id = p.id')
            ->where('p.id',$propertyID)
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response['program_id'] = $d['program_id'];
                $response['default_language'] = $d['default_language'];
                $response['public_languages'] = isset($d['public_languages']) && !empty($d['public_languages']) ? json_decode($d['public_languages'],true) : array();
            }
        }

        return $response;
    }

    public function getCurrencies(){
        $data = $this->db->select('*')
            ->from('currencies')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d;
            }
        }

        return $response;
    }

    public function getTags($propertyID){
        return $this->db->select('*')
            ->from('tags')
            ->where('property_id',$propertyID)
            ->where('active',1)
            ->where('deleted',0)
            ->get()
            ->result_array();
    }

    public function getDevelopers(){
        $data = $this->db->select('id')
            ->from('users')
            ->where('is_developer',1)
            ->get()
            ->result_array();


        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d['id'];
            }
        }

        return $response;
    }

    public function getUserLang($user_id){
        $data = $this->db->select('lang')
            ->from('users')
            ->where('id',$user_id)
            ->get()
            ->row_array();

        if(isset($data['lang']) && !empty($data['lang'])){
            return $data['lang'];
        }

        return null;
    }

    public function updateUser($id,$data){
        return $this->db->where('id',$id)->update('users',$data);
    }
}