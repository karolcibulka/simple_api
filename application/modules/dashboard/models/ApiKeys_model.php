<?php


class ApiKeys_model extends CI_Model
{

    public function getApiKeys(){
        $data =  $this->db->select('ak.*,GROUP_CONCAT(akg.api_group_id SEPARATOR "|") as groups')
            ->from('api_keys as ak')
            ->join('api_key_groups as akg','akg.api_key_id = ak.id','left')
            ->group_by('ak.id')
            ->get()
            ->result_array();

        if(!empty($data)){
            foreach($data as $key => $d){
                if(isset($d['groups']) && !empty($d['groups'])){
                    $data[$key]['groups'] = explode('|',$d['groups']);
                }
            }
        }

        return $data;
    }

    public function getApiKey($id){
        return $this->db->select('*')
            ->from('api_keys')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function storeApiKey($data){
        $this->db->insert('api_keys',$data);
    }

    public function deleteApiKeyGroups($id){
        $this->db->where('api_key_id',$id)->delete('api_key_groups');
    }

    public function insertApiKeyGroupsBatch($data){
        $this->db->insert_batch('api_key_groups',$data);
    }

    public function getGroups(){
        return $this->db->select('*')
            ->from('api_groups as ag')
            ->where('ag.deleted',0)
            ->order_by('ag.order','asc')
            ->get()
            ->result_array();
    }

    public function updateApiKey($id,$data){
        $this->db->where('id',$id)->update('api_keys',$data);
    }
}