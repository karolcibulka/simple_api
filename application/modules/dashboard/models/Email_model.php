<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-09-23
 * Time: 10:44
 */

class Email_model extends CI_Model
{

    public function getEmailTemplates($property_id){
        return $this->db->select('*')
            ->where('deleted','0')
            ->group_start()
            ->where('property_id',$property_id)
            ->or_where('id','10')
            ->group_end()
            ->from('email_templates')
            ->get()
            ->result_array();
    }

    public function getDefaultTemplate(){
        $data = $this->db->select('mjml')
            ->where('id','10')
            ->from('email_templates')
            ->get()
            ->row_array();

        return $data['mjml'];
    }

    public function insertNewTemplate($data){
        $this->db->insert('email_templates',$data);
        return $this->db->insert_id();
    }

    public function checkID($id,$property_id){
        $data = $this->db->select('*')
            ->where('id',$id)
            ->group_start()
            ->where('property_id',$property_id)
            ->or_where('id','10')
            ->group_end()
            ->from('email_templates')
            ->get()
            ->row_array();

        return $data;
    }

    public function updateEmailTemplate($data,$id){
        $this->db->where('id',$id)
            ->update('email_templates',$data);
    }
}