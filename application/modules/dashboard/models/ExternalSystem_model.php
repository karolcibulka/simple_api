<?php


class ExternalSystem_model extends CI_Model
{
    public function getProperty($property_id){
        return $this->db->select('server,port,cash_desk_id,system_id,id')
            ->where('id',$property_id)
            ->from('property')
            ->get()
            ->row_array();
    }

    public function get_count($property_id,$table,$getData = array()){
        $this->db->select('count(*)');
        $this->db->where('property_id',$property_id);
        if(isset($getData) && !empty($getData)){
            foreach($getData as $name => $value){
                if($value !== ''){
                    if($name == 'name'){
                        $this->db->like($name,$value);
                    }
                    else{
                        $this->db->where($name,$value);
                    }
                }
            }
        }
        $query = $this->db->get($table);
        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }

    public function insertData($data){
        $this->db->insert('external_property',$data);
        return $this->db->insert_id();
    }

    public function getRow($property_id,$id){
        return $this->db->select('*')
            ->where('property_id',$property_id)
            ->where('id',$id)
            ->from('external_property')
            ->get()
            ->row_array();
    }

    public function updateData($property_id,$id,$data){
        $this->db->where('property_id',$property_id)->where('id',$id)->update('external_property',$data);
    }

    public function getTable($propertyID,$token){
        return $this->db->select('*')
            ->from('tables')
            ->where('property_id',$propertyID)
            ->where('token',$token)
            ->get()
            ->row_array();
    }

    public function getAllRecords($property_id,$limit,$start,$table,$getData = array()){
        $query =  $this->db->select('*')
            ->from($table)
            ->where('property_id',$property_id)
            ->where('active','1');
        if($limit && $start) {
            $query->limit($limit, $start);
        }
            if(isset($getData) && !empty($getData)){
                foreach($getData as $name => $value){
                    if($value !== ''){
                        if($name == 'name'){
                            $query->like($name,$value);
                        }
                        else{
                            $query->where($name,$value);
                        }
                    }
                }
            }
        $data = $query->get()
            ->result_array();

        return $data;
    }

    public function getAllProperties($property_id,$limit,$start,$table,$getData = array()){
        $query =  $this->db->select('*')
            ->from($table)
            ->where('property_id',$property_id)
            ->where('deleted',0)
            ->limit($limit, $start);
        if(isset($getData) && !empty($getData)){
            foreach($getData as $name => $value){
                if($value !== ''){
                    if($name == 'name'){
                        $query->like($name,$value);
                    }
                    else{
                        $query->where($name,$value);
                    }
                }
            }
        }
        $data = $query->get()
            ->result_array();

        return $data;
    }

    public function getAreas($propertyID){
        $data = $this->db->select('area_id,area_name')
            ->from('tables')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[$d['area_id']] = $d['area_name'];
            }
        }

        return $response;
    }

    public function getCategories($propertyID,$table){
        $data = $this->db->select('category_number,category_name')
            ->from($table)
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[$d['category_number']] = $d['category_name'];
            }
        }

        return $response;
    }

    public function deactivateByHotelID($table,$property_id,$external_property_id){
        $this->db->where('property_id',$property_id)->where('external_property_id',$external_property_id)->update($table,array('active'=>'0'));
    }

    public function deactivate($property_id,$table){
        $this->db->where('property_id',$property_id)->update($table,array('active'=>'0'));
    }

    public function updateRoom($propertyID,$id,$data){
        $this->db->where('property_id',$propertyID)->where('external_id',$id)->update('rooms',$data);
    }

    public function getExternalProperties($table,$property_id,$key = 'external_hotel_id',$isNull = true){
        $query =  $this->db->select('*')
            ->from($table)
            ->where('property_id',$property_id)
            ->where('deleted',0);

        if($isNull){
            $query->where('external_hotel_id IS NOT NULL',NULL,false);
        }

       $data = $query->get()
            ->result_array();

        $result = array();

        if(!empty($data)){
            foreach($data as $d){
                $result[$d[$key]] = $d;
            }
        }

        return $result;
    }

    public function getRoom($propertyID,$id){
        return $this->db->select('*')
            ->where('property_id',$propertyID)
            ->where('external_id',$id)
            ->from('rooms')
            ->get()
            ->row_array();
    }

    public function store($table,$array){

        $arrayToString = '';
        foreach($array as $key => $ar){
            if($key == 'token') continue;
            if(is_int($ar)){
                $arrayToString .= $key.' = '.$ar.', ';
            }
            else{
                $arrayToString .= $key.' = \''.$ar.'\', ';
            }
        }

        $sql = $this->db->insert_string($table, $array) . ' ON DUPLICATE KEY UPDATE '.$arrayToString;
        $sql = rtrim($sql,', ');
        $this->db->query($sql);
    }

    public function getPropertyWeb($propertyID){
        return $this->db->select('*')
            ->from('property_settings_web')
            ->where('property_id',$propertyID)
            ->get()
            ->row_array();
    }

    public function getStatus($property_id,$id){
        $data =  $this->db->select('*')
            ->from('external_statuses_lang')
            ->where('external_id',$id)
            ->where('property_id',$property_id)
            ->get()
            ->result_array();


        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['lang']] = array(
                    'name' => $d['name'],
                    'description' => $d['description']
                );
            }
        }

        return $response;

    }

    public function deleteStatusLanguages($property_id,$id){
        $this->db->where('property_id',$property_id)->where('external_id',$id)->delete('external_statuses_lang');
    }

    public function storeStatusLanguages($data){
        $this->db->insert_batch('external_statuses_lang',$data);
    }
}