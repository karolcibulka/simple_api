<?php


class Transportation_model extends CI_Model
{

    public $table = 'transportation';

    public function getTransportations($propertyID){
        return $this->db->select('*')
            ->from($this->table)
            ->where('property_id',$propertyID)
            ->where('deleted','0')
            ->order_by('order','asc')
            ->get()
            ->result_array();
    }

    public function getPlus($property_id){
        return $this->db->select('*')
            ->from('external_plu_list')
            ->where('property_id',$property_id)
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function getExternalOrderTypes($property_id){
        return $this->db->select('*')
            ->from('external_order_type_list')
            ->where('property_id',$property_id)
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function insertTransportation($data){
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

    public function insertLang($data){
        $this->db->insert($this->table.'_lang',$data);
    }

    public function getTransportation($transportation_id,$property_id){
        $data = $this->db->select('*')
            ->from($this->table.' as t')
            ->join($this->table.'_lang as tl','t.id = tl.transportation_id','left')
            ->where('t.id',$transportation_id)
            ->where('t.property_id',$property_id)
            ->get()
            ->result_array();

        $result = array();
        if(!empty($data)){
            foreach($data as $d){
                $result['internal_name'] = $d['internal_name'];
                $result['id'] = $d['id'];
                $result['price'] = $d['price'];
                $result['external_id'] = $d['external_id'];
                $result['visible_from'] = $d['visible_from'];
                $result['external_plu_id'] = $d['external_plu_id'];
                $result['is_delivery'] = $d['is_delivery'];
                $result['estimated_time'] = $d['estimated_time'];
                $result['estimated_date'] = $d['estimated_date'];
                $result['min_days'] = $d['min_days'];
                $result['max_days'] = $d['max_days'];
                $result['on_room'] = $d['on_room'];
                $result['region'] = $d['region'];
                $result['cities'] = $d['cities'];
                $result['langs'][$d['lang']]['name'] = $d['name'];
                $result['langs'][$d['lang']]['short_description'] = $d['short_description'];
            }
        }

        return $result;
    }

    public function removeLangs($property_id,$transportation_id){
        $this->db->where('property_id',$property_id)->where('transportation_id',$transportation_id)->delete($this->table.'_lang');
    }

    public function update($property_id,$transportation_id,$data){
        $this->db->where('property_id',$property_id)->where('id',$transportation_id)->update($this->table,$data);
    }

}