<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-05-30
 * Time: 15:04
 */

class Cron_model extends CI_Model
{
    public function getReservations()
    {
        $current_time = date('Y-m-d H:i:s');
        //pre_r($current_time);
        $data = $this->db->select('r.*,c.*,p.name as "property_name",r.id as "reservation_id"')
            ->from('reservations as r')
            ->where('r.timeFrom>', $current_time)
            ->where('DATE(r.timeFrom)', date('Y-m-d'))
            ->where('r.deleted', '0')
            ->where('r.notificated', '0')
            ->join('customers as c', 'r.customer_id = c.id')
            ->join('property as p', 'r.property_id = p.id')
            ->get()
            ->result_array();

        foreach ($data as $key => $d) {
            if (strtotime(date('H:i', strtotime($d['timeFrom'] . '-30 minutes'))) == strtotime(date('H:i'))) {
                continue;
            } else {
                unset($data[$key]);
            }
        }
        return $data;
    }


    public function getDoneReservations()
    {
        $current_time = date('Y-m-d H:i:s');
        $data         = $this->db->select('r.*,c.*,p.name as "property_name",r.id as "reservation_id",c.email as "customer_email",p.id as "property_id", r.id as "reservation_id",r.token as "token",c.first_name as "firstName",c.last_name as "lastName"')
            ->from('reservations as r')
            ->where('DATE(r.timeTo)', date('Y-m-d'))
            ->where('r.deleted', '0')
            ->where('r.notificatedbyemail', '0')
            ->join('customers as c', 'r.customer_id = c.id')
            ->join('property as p', 'r.property_id = p.id')
            ->get()
            ->result_array();

        foreach ($data as $key => $d) {
            if (strtotime(date('H:i', strtotime($d['timeTo'] . '+ 1 hour'))) == strtotime(date('H:i'))) {
                continue;
            } else {
                unset($data[$key]);
            }
        }

        return $data;
    }

    public function notificated($data, $id)
    {
        $this->db->where('id', $id)
            ->update('reservations', $data);
    }

    public function getReviewReservations()
    {
        $data = $this->db->select('*,r.token as reservation_token,p.email as property_email,r.email as email,p.phone as property_phone,r.phone as reservation_phone')
            ->from('reservations as r')
            ->where('r.status', savedToBGStatus())
            ->where('DATE(r.created_at)', date('Y-m-d'))
            ->where('r.send_review_email', 0)
            ->where('r.reviewed', 0)
            ->join('property_settings_web as p', 'p.property_id = r.property_id AND p.reviews = 1')
            ->get()
            ->result_array();

        $response = array();

        if (!empty($data)) {
            foreach ($data as $d) {
                $minutes          = ($d['reviews_interval'] * 60);
                $reservation_time = strtotime('+ ' . $minutes . ' minutes', strtotime($d['created_at']));
                $current_time     = strtotime(date('Y-m-d H:i:s'));

                if ($reservation_time > $current_time) { //TODO:: Otočiť podmienku !!!!

                    $response[$d['property_id']]['logo']                = upload_url($d['property_id'] . '/logo/' . $d['logo']);
                    $response[$d['property_id']]['name']                = $d['name'];
                    $response[$d['property_id']]['web']                 = $d['web'];
                    $response[$d['property_id']]['contact']['email']    = $d['property_email'];
                    $response[$d['property_id']]['contact']['phone']    = $d['property_phone'];
                    $response[$d['property_id']]['interval']            = $d['reviews_interval'];
                    $response[$d['property_id']]['social']['facebook']  = $d['facebook'];
                    $response[$d['property_id']]['social']['instagram'] = $d['instagram'];
                    $response[$d['property_id']]['social']['twitter']   = $d['twitter'];


                    $response[$d['property_id']]['reservations'][$d['id']] = array(
                        'first_name' => $d['first_name'],
                        'last_name' => $d['last_name'],
                        'email' => $d['email'],
                        'lang' => $d['lang'],
                        'token' => $d['reservation_token'],
                        'phone' => $d['reservation_phone'],
                        'created_at' => $d['created_at'],
                    );
                }
            }
        }

        return $response;
    }

    public function updateReservation($reservationID,$data){
        $this->db->where('id',$reservationID)->update('reservations',$data);
    }
}