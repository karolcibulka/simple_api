<?php


class Categories_model extends CI_Model
{

    public function getCategories($property_id,$id = null){
        $q =  $this->db->select('*')
            ->from('categories')
            ->where('property_id',$property_id)
            ->where('deleted',0)
            ->order_by('order','asc');

        if($id){
            $q->where('id !=',$id);
        }

        return $q->get()
            ->result_array();
    }

    public function insertCategory($data){
        $this->db->insert('categories',$data);
        return $this->db->insert_id();
    }

    public function insertLanguages($data){
        $this->db->insert_batch('categories_lang',$data);
    }

    public function updateCategory($property_id,$id,$data){
        $this->db->where('property_id',$property_id)->where('id',$id)->update('categories',$data);
    }

    public function getCategory($property_id,$id){
        $data =  $this->db->select('c.*,cl.*')
            ->from('categories as c')
            ->where('c.property_id',$property_id)
            ->join('categories_lang as cl','cl.category_id = c.id AND c.property_id = '.$property_id,'left')
            ->where('c.id',$id)
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                if(empty($response)){
                    $response = $d;
                }

                if(isset($d['lang']) && !empty($d['lang'])){
                    $response['languages'][$d['lang']] = array(
                        'name' => $d['name'],
                        'description' => $d['description']
                    );
                }
            }
        }

        return $response;
    }

    public function removeLanguages($property_id,$id){
        $this->db->where('property_id',$property_id)->where('category_id',$id)->delete('categories_lang');
    }
}