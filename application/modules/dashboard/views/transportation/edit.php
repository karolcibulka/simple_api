
<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=base_url('dashboard/transportation/editProcess/'.$transportation['id'])?>" id="form" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" class="form-control internalNameCounter" value="<?=$transportation['internal_name']?>" name="internal_name" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-4 languageChanger language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <input type="text" class="form-control" value="<?=isset($transportation['langs'][$lang]['name']) && !empty($transportation['langs'][$lang]['name']) ? $transportation['langs'][$lang]['name'] : ''?>" name="name[<?=$lang?>]" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.unit_price')?></label>
                                <input type="number" min="0" step="0.01" class="form-control" value="<?=formatNumApi($transportation['price'])?>" name="price" placeholder="<?=__('app.unit_price')?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.show_from_price')?></label>
                                <input type="number" name="visible_from" placeholder="<?=__('app.show_from_price')?>" value="<?=$transportation['visible_from']?>" min="0" step="1" class="form-control">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-6 languageChanger language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.perex_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <textarea type="text" class="form-control" name="short_description[<?=$lang?>]"><?=isset($transportation['langs'][$lang]['short_description']) && !empty($transportation['langs'][$lang]['short_description']) ? $transportation['langs'][$lang]['short_description'] : ''?></textarea>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" name="is_delivery" value="0">
                            <label
                            <span class="switchText"> <?=__('app.is_delivery')?></span>
                            <label class="switch">
                                <input type="checkbox" name="is_delivery" <?=($transportation['is_delivery'] == '1') ? 'checked' : ''?> value="1">
                                <span class="slider round"></span>
                            </label>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <input type="hidden" name="estimated_time" value="0">
                            <label
                            <span class="switchText"> <?=__('app.estimated_time')?></span>
                            <label class="switch">
                                <input type="checkbox" <?=($transportation['estimated_time'] == '1') ? 'checked' : ''?> name="estimated_time" value="1">
                                <span class="slider round"></span>
                            </label>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <input type="hidden" name="estimated_date" value="0">
                            <label
                            <span class="switchText"> <?=__('app.estimated_date')?></span>
                            <label class="switch">
                                <input type="checkbox" name="estimated_date" <?=isset($transportation['estimated_date']) && !empty($transportation['estimated_date']) ? 'checked' : ''?> value="1">
                                <span class="slider round"></span>
                            </label>
                            </label>
                        </div>
                    </div>
                    <div class="row estimated_date" style="<?=isset($transportation['estimated_date']) && !empty($transportation['estimated_date']) ? '' : 'display:none;'?>">
                        <legend></legend>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.min_days')?></label>
                                <input type="number" step="1" min="0" class="form-control" max="180" value="<?=isset($transportation['min_days']) && !empty($transportation['min_days']) ? $transportation['min_days'] : '1'?>" name="min_days">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.max_days')?></label>
                                <input type="number" step="1" min="0" class="form-control" max="180" value="<?=isset($transportation['max_days']) && !empty($transportation['max_days']) ? $transportation['max_days'] : '1'?>" name="max_days">
                            </div>
                        </div>
                    </div>
                    <div class="row delivery_wrapper" style="<?=$transportation['is_delivery']=='1' ? '' : 'display:none;'?>">
                        <legend></legend>
                        <div class="col-md-12 cities" style="<?=$transportation['is_delivery']=='1' ? '' : 'display:none;'?>">
                            <div class="form-group">
                                <label><?=__('app.cities')?></label>
                                <select name="cities[]" class="form-control form-control-select2" multiple="multiple">
                                    <?php if(isset($cities) && !empty($cities)):?>
                                        <?php foreach($cities as $city):?>
                                            <option <?=isset($transportation['cities']) && !empty($transportation['cities']) && in_array($city['objectId'],$transportation['cities']) ? 'selected' : ''?> value="<?=$city['objectId']?>"><?=$city['municipalityName']?> <?=isset($city['regionName']) && !empty($city['regionName']) ? ' ('.$city['regionName'].' kraj ) ' : ''?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    $('[name="estimated_date"]').on('change',function(){
        var _self = $(this),
            _wrapper = $('.estimated_date');

        if(_self.is(':checked')){
            _wrapper.show();
        }
        else{
            _wrapper.hide();
            _wrapper.find('input').val(1);
        }
    });

    $('[name="is_delivery"]').on('change',function(){
        var _on_room = $('[name="on_room"]');
        if($(this).is(':checked')){
            if(_on_room.is(':checked')){
                _on_room.click();
            }
        }
    });

    $('[name="on_room"]').on('change',function(){
        var is_delivery = $('[name="is_delivery"]');
        if($(this).is(':checked')){
            if(is_delivery.is(':checked')){
                is_delivery.click();
            }
        }
    });

    $('.form-control-select2').select2();

    $('[name="external_plu_id"]').on('change',function(){
        $('[name="price"]').val($(this).find('option:selected').data().price);
    });

    $('[name="cities[]"]').select2();
    $('[name="region"]').on('change',function(){
        var _this = $(this);
        var val = _this.val();
        var _cities = $('.cities');
        var _options = '';

        if(val!==""){
            _cities.hide();

            $.ajax({
                type:'get',
                url:'<?=base_url('dashboard/transportation/getCitiesForRegion/')?>'+val,
                dataType:'json',
                success:function(data){
                    if(data.status === '1'){
                        //_cities.find('.form-control-select2').select2('remove');
                        _cities.show();
                        _options = getOptions(data.cities);
                        _cities.find('select').empty().append(_options);
                        _cities.find('.form-control-select2').select2();
                    }
                }
            });
        }
        else{
            _cities.hide();
            $('[name="cities"]').val('');
        }
    });

    var getOptions = function(cities){
        var options = '';
        $.each(cities,function(key,city){
            options += '<option value="'+city.objectId+'">'+city.municipalityName+'</option>';
        });
        return options;
    }


    $('[name="is_delivery"]').on('change',function(){
        if($(this).is(':checked')){
            $('.delivery_wrapper').show();
        }
        else{
            $('.delivery_wrapper').hide();
            $('[name="region"]').val('');
            $('[name="cities[]"]').empty();
        }
    });

    $('#languageLanger').on('change',function(){
        $('.languageChanger').hide();
        $('.language-'+$(this).val()).show();
    })
</script>