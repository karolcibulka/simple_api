<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=base_url('dashboard/transportation/createProcess')?>" id="form" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" class="form-control internalNameCounter" value="" name="internal_name" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-4 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <input type="text" class="form-control" value="" name="name[<?=$lang?>]" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.unit_price')?></label>
                                <input type="number" min="0" step="0.01" class="form-control" value="" name="price" placeholder="<?=__('app.unit_price')?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.show_from_price')?></label>
                                <input type="number" name="visible_from"  value="0" min="0" step="1" placeholder="<?=__('app.show_from_price')?>" class="form-control">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-6 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.perex_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <textarea type="text" class="form-control" name="short_description[<?=$lang?>]"></textarea>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" name="is_delivery" value="0">
                            <label
                            <span class="switchText"> <?=__('app.is_delivery')?></span>
                            <label class="switch">
                                <input type="checkbox" name="is_delivery" value="1">
                                <span class="slider round"></span>
                            </label>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <input type="hidden" name="estimated_time" value="0">
                            <label
                                <span class="switchText"> <?=__('app.show_estimated_time')?></span>
                                <label class="switch">
                                    <input type="checkbox" name="estimated_time" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>

                        <div class="col-md-4">
                            <input type="hidden" name="estimated_date" value="0">
                            <label
                                <span class="switchText"> <?=__('app.show_estimated_date')?></span>
                                <label class="switch">
                                    <input type="checkbox" name="estimated_date" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>
                    </div>
                    <div class="row estimated_date" style="display:none;">
                        <legend></legend>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.min_days')?></label>
                                <input type="number" step="1" min="0" class="form-control" max="180" value="1" name="min_days">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.max_days')?></label>
                                <input type="number" step="1" min="0" class="form-control" max="180" value="1" name="max_days">
                            </div>
                        </div>
                    </div>
                    <div class="row delivery_wrapper" style="display:none;">
                        <legend></legend>
                        <div class="col-md-12 cities">
                            <div class="form-group">
                                <label><?=__('app.cities')?></label>
                                <select name="cities[]" class="form-control form-control-select2" multiple="multiple">
                                    <?php if(isset($cities) && !empty($cities)):?>
                                        <?php foreach($cities as $city):?>
                                            <option <?=isset($transportation['cities']) && !empty($transportation['cities']) && in_array($city['objectId'],$transportation['cities']) ? 'selected' : ''?> value="<?=$city['objectId']?>"><?=$city['municipalityName']?> <?=isset($city['regionName']) && !empty($city['regionName']) ? ' ('.$city['regionName'].' kraj ) ' : ''?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?=getSelect2()?>
<script>

    $('[name="estimated_date"]').on('change',function(){
       var _self = $(this),
           _wrapper = $('.estimated_date');

       if(_self.is(':checked')){
           _wrapper.show();
       }
       else{
           _wrapper.hide();
           _wrapper.find('input').val(1);
       }
    });

    $('[name="is_delivery"]').on('change',function(){
        var _checkCustomer = $('[name="on_room"]');
        if($(this).is(':checked')){
            if(_checkCustomer.is(':checked')){
                _checkCustomer.click();
            }
        }
    });

    $('[name="on_room"]').on('change',function(){
        var _forTable = $('[name="is_delivery"]');
        if($(this).is(':checked')){
            if(_forTable.is(':checked')){
                _forTable.click();
            }
        }
    });

    $('.form-control-select2').select2();

    $('[name="external_plu_id"]').on('change',function(){
        $('[name="price"]').val($(this).find('option:selected').data().price);
    });


    $('[name="is_delivery"]').on('change',function(){
        if($(this).is(':checked')){
            $('.delivery_wrapper').show();
        }
        else{
            $('.delivery_wrapper').hide();
            $('[name="cities[]"]').empty();
        }
    });
</script>