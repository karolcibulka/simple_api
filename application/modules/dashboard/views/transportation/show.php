<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')))?>
    <div class="content">
        <div class="card">

            <div class="card-body">
                <?php if(isset($transportations) && !empty($transportations)):?>
                    <?=nestableView($transportations)?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script src="<?=asset_url('js/jquery.nestable2.js')?>"></script>
<script>

    $('.dd').nestable({
        maxDepth:1,
        allowDecrease   : false,
        allowIncrease   : false
    });

    $('.dd').on('change',function(){
        var serialized = $('.dd').nestable('serialize');

        $.ajax({
            type:'POST',
            url:'<?=base_url('dashboard/transportation/order')?>',
            data:{serialized:serialized},
            dataType:'json',
            success:function(data){

            }
        })
    });
</script>