<?php if (!isset($has_no_property)): ?>

    <div class="content-wrapper">
        <?=getBreadcrumb()?>
        <div class="content">
            <?php if($this->session->flashdata()): ?>
                <?php if($this->session->flashdata('status') == 'warning'):?>
                <div class="alert alert-danger border-0 alert-dismissible">
                <?php elseif($this->session->flashdata('status') == 'success') :?>
                <div class="alert alert-success border-0 alert-dismissible">
                <?php elseif($this->session->flashdata('status') == 'warning') :?>
                <div class="alert alert-warning border-0 alert-dismissible">
                <?php else:?>
                <div class="alert alert-primary border-0 alert-dismissible">
                <?php endif;?>
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    <div style="text-align:center;">
                    <?=$this->session->flashdata('message')?>
                    </div>
                </div>
                <?php endif;?>
        </div>
    </div>
<?php else:?>
    <div class="content-wrapper">
        <div class="content">
            <div class="col-md-12 alert alert-danger text-center">
                Momentálne ku vašému účtu <strong>nie je priradené žiadne zariadenie</strong>, kontaktujte developera na email
                <strong><a href="mailto:kajo.cibulka@gmail.com" style="color:#7f231d">kajo.cibulka@gmail.com</a></strong>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            V prípade, že máte vytvorené zariadenie, pridajte si ho ku svojmu účtu
                            <legend></legend>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Token zariadenia">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary w-100">Priradiť</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="card card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5>Vytvoriť zariadenie</h5>
                        <legend></legend>
                        <div class="row">
                            <?//php foreach($programs_ci as $program):?>
                                <div class="card col m-1">
                                    <div class="card-body">
                                        <h5><?//=$program['internal_name']?></h5>
                                    </div>
                                </div>
                            <?//php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
    
<?php endif;?>
