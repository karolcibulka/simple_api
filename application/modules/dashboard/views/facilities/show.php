<?=getNestable()?>
<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')),array(),false)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="box-content nopadding dd mt-0" style="padding: 0;">
                    <ol class="servicesView dd-list">
                        <?php if(isset($facilities) && !empty($facilities)):?>
                            <?=nestableView($facilities)?>
                        <?php endif;?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.dd').nestable({
        maxDepth:1,
        allowIncrease:false,
        allowDecrease:false
    }).on('change',function(){
        $.ajax({
            type:'POST',
            url:'<?=controller_url('order')?>',
            data:{serialized:$(this).nestable('serialize')},
            dataType:'json'
        })
    });
</script>