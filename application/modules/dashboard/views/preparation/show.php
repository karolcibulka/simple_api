<div class="content-wrapper">
    <?=$this->load->view('partials/breadcrumb',array('noButton'=>true),true)?>
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-9">
                        <h5>Dĺžka príparav jedál</h5>
                    </div>
                    <div class="col-md-3">
                        <?php if(has_permission('edit') && has_permission('create')):?>
                            <button class="btn btn-primary w-100" form="form">
                                Uložiť
                            </button>
                        <?php endif;?>
                    </div>
                </div>

                <legend></legend>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/preparation/createProcess')?>" method="post" id="form">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Počet objednávok od</th>
                                        <th>Počet objednávok do</th>
                                        <th>Dĺžka prípravy objednávky <small>(minúty)</small></th>
                                        <th>Príprava + rozvoz <small>(minúty)</small></th>
                                        <?php if(has_permission('edit') && has_permission('create')):?>
                                            <th>Akcia</th>
                                        <?php endif;?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($preparations) && !empty($preparations)):?>
                                        <?php foreach($preparations as $key => $preparation):?>
                                            <tr class="<?=$key === 0 ? 'template' : ''?>">
                                                <?php if(has_permission('edit') && has_permission('create')):?>
                                                    <td><input name="from[]" type="number" min="0" step="1" class="form-control" value="<?=$preparation['from']?>" placeholder="Počet jedál od"></td>
                                                    <td><input name="to[]" type="number" min="0" step="1" class="form-control" value="<?=$preparation['to']?>" placeholder="Počet jedál do"></td>
                                                    <td><input name="prepare_time[]" type="number" min="0" step="1" class="form-control" value="<?=$preparation['prepare_time']?>" placeholder="Dĺžka prípravy jedla"></td>
                                                    <td><input name="delivery_time[]" type="number" min="0" step="1" class="form-control" value="<?=$preparation['delivery_time']?>" placeholder="Príprava + rozvoz"></td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="button" class="btn btn-success w-100 clone">+</button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <button type="button" class="btn btn-danger w-100 delete">-</button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                <?php else:?>
                                                    <td><?=$preparation['from']?></td>
                                                    <td><?=$preparation['to']?></td>
                                                    <td><?=$preparation['prepare_time']?></td>
                                                    <td><?=$preparation['delivery_time']?></td>
                                                <?php endif;?>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <?php if(has_permission('edit') && has_permission('create')):?>
                                            <tr class="template">
                                                <td><input name="from[]" type="number" min="0" step="1" class="form-control" value="0" placeholder="Počet jedál od"></td>
                                                <td><input name="to[]" type="number" min="0" step="1" class="form-control" placeholder="Počet jedál do"></td>
                                                <td><input name="prepare_time[]" type="number" min="0" step="1" class="form-control" placeholder="Dĺžka prípravy jedla"></td>
                                                <td><input name="delivery_time[]" type="number" min="0" step="1" class="form-control" placeholder="Príprava + rozvoz"></td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-success w-100 clone">+</button>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-danger w-100 delete">-</button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif;?>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click','.clone',function(){
        var _template = $('.template').clone(),
            _tbody = $(document).find('tbody');

        _template.removeClass('template');
        _template.find('input').val('');

        _tbody.append(_template);

    });

    $(document).on('click','.delete',function(){
        if($(document).find('.delete').length > 1){
            $(this).closest('tr').remove();
        }
        else{
            $(this).closest('tr').find('input').val('');
        }

    });

</script>