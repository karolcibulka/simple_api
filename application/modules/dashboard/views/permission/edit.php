<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>
                    Úprava skupiny
                </h5>
            </div>
            <div class="card-body">
                <form action="<?=controller_url('editProcess/'.$group['id'])?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label></label>
                                <input type="text" name="name" value="<?=$group['name']?>" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label></label>
                                <input type="text" name="description" value="<?=$group['description']?>" required class="form-control">
                            </div>
                        </div>
                        <legend></legend>
                        <button class="btn btn-primary w-100">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>