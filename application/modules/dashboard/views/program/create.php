<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Vytvorenie programu</h5>
                    </div>
                </div>
                <legend></legend>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/program/createProcess')?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Interný názov</label>
                                <input type="text" name="internal_name" value="" class="form-control" placeholder="Interný názov">
                            </div>
                        </div>
                        <legend></legend>
                        <?php if(isset($controllers) && !empty($controllers)):?>
                            <div class="col-md-4">
                            <ul class="list-group">
                            <?php foreach($controllers as $key => $controller):?>
                                <?=($key % 10 === 0 && $key!==0) ? '</ul></div><div class="col-md-4"><ul class="list-group">' : ''?>
                                <li class="list-group-item">
                                    <label>
                                        <input type="checkbox" name="controllers[]" value="<?=$controller['id']?>">
                                        &nbsp <?=$controller['description']?>
                                    </label>
                                </li>
                            <?php endforeach;?>
                            </ul>
                            </div>
                        <?php endif;?>
                    </div>
                    <legend></legend>
                    <button class="btn btn-primary w-100">
                        Uložiť
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>