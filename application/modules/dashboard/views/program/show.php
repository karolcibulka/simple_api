<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-9">
                        <h5>Programy</h5>
                    </div>
                    <div class="col-md-3">
                        <a href="<?=base_url('dashboard/program/create')?>" class="btn btn-primary w-100">Vytvoriť program</a>
                    </div>
                </div>
                <legend></legend>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Interný názov</th>
                                <th>Akcia</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($programs) && !empty($programs)):?>
                            <?php foreach($programs as $program):?>
                                <tr>
                                    <td><?=$program['id']?></td>
                                    <td><?=$program['internal_name']?></td>
                                    <td>
                                        <?php if(has_permission('edit')):?>
                                            <?php if($program['active'] === '1'):?>
                                                <a href="<?=base_url('dashboard/program/active/'.$program['id'].'/0')?>"><span class="badge badge-success">Aktívne</span></a>
                                            <?php else:?>
                                                <a href="<?=base_url('dashboard/program/active/'.$program['id'].'/1')?>"><span class="badge badge-warning">Neaktívne</span></a>
                                            <?php endif;?>
                                            <a href="<?=base_url('dashboard/program/edit/'.$program['id'])?>"><span class="badge badge-primary">Upraviť</span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('delete')):?>
                                            <a href="<?=base_url('dashboard/program/delete/'.$program['id'])?>"><span class="badge badge-danger">Zmazať</span></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>