<?=getDataTable()?>
<?=getSelect2()?>

<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-9">
                        <h5>Api kľúče</h5>
                    </div>
                    <div class="col-md-3">
                        <a href="<?=controller_url('create')?>" class="btn btn-primary w-100">Vytvoriť api kľúč</a>
                    </div>
                    <legend></legend>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="data-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Interný názov</th>
                                <th>Api kľúč</th>
                                <th>Právomoci</th>
                                <th>Akcia</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($api_keys) && !empty($api_keys)):?>
                                <?php foreach($api_keys as $api_key):?>
                                    <tr>
                                        <td><?=$api_key['id']?></td>
                                        <td><?=$api_key['internal_name']?></td>
                                        <td><?=$api_key['api_key']?></td>
                                        <td>
                                            <select data-id="<?=$api_key['id']?>" class="form-control change-permission select2" multiple>
                                                <?php if(isset($groups) && !empty($groups)):?>
                                                    <?php foreach($groups as $group):?>
                                                        <option <?=isset($api_key['groups']) && !empty($api_key['groups']) && in_array($group['id'],$api_key['groups']) ? 'selected' : ''?> value="<?=$group['id']?>"><?=$group['description']?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php if(isset($api_key['active']) && !empty($api_key['active'])):?>
                                                <a href="<?=controller_url('setActivity/'.$api_key['id'].'/'.$api_key['active'])?>" class="badge badge-success">Aktívne</a>
                                            <?php else:?>
                                                <a href="<?=controller_url('setActivity/'.$api_key['id'].'/'.$api_key['active'])?>" class="badge badge-warning">Neaktívne</a>
                                            <?php endif;?>
                                            <a href="<?=controller_url('edit/'.$api_key['id'])?>" class="badge badge-primary">Upraviť</a>
                                            <a href="<?=controller_url('delete/'.$api_key['id'])?>" class="badge badge-danger">Zmazať</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.select2').select2();

    $('#data-table').dataTable();

    $('.change-permission').on('change',function(){
        var _self = $(this),
            _val = _self.val(),
            _id = _self.data().id;

        $.ajax({
            type:'post',
            url:'<?=controller_url('setPermission')?>'+'/'+_id,
            dataType:'json',
            data:{val:_val},
            success:function(data){
                if(data.status === 1){
                    swalSuccessMessageSession('Záznam bol úspešne aktualizovaný!');
                }
            }
        })
    });
</script>