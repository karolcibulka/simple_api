<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Vytvorenie api kľúču</h5>
            </div>
            <div class="card-body">
                <form action="<?=isset($id) && !empty($id) ? controller_url('editProcess/'.$id) : controller_url('createProcess')?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Interný názov</label>
                                <input type="text" data-max_length="30" class="form-control" name="internal_name" value="<?=isset($api_key['internal_name']) && !empty($api_key['internal_name']) ? $api_key['internal_name'] : ''?>" placeholder="interný názov">
                            </div>
                        </div>
                        <legend></legend>
                        <button class="btn btn-primary w-100">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>