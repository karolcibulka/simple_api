<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')))?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?=__('app.internal_name')?></label>
                            <input type="text" name="internal_name" data-max_length="50" placeholder="<?=__('app.internal_name')?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-white">asd</label>
                            <button class="btn btn-primary w-100">Pridať typ cenotvorby</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="price-profiles" class="w-100">
        </div>
        
        <?=_partial('partials/template')?>
        
    </div>
</div>

<?=getSelect2()?>