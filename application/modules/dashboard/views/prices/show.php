<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')),array(),false)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th><?=__('app.name_p')?></th>
                                <th><?=__('app.category')?></th>
                                <th><?=__('app.action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($prices) && !empty($prices)):?>
                                <?php foreach($prices as $price):?>
                                    <tr>
                                        <td><?=$price['id']?></td>
                                        <td><?=$price['internal_name']?></td>
                                        <td><?=$price['categories_internal']?></td>
                                        <td>
                                            <?php if(has_permission('edit')):?>
                                                <a href="<?=controller_url('active/'.$price['id'].'/'.$price['active'])?>" class="badge badge-<?=!empty($price['active']) ? 'success' : 'warning'?>"><?=!empty($price['active']) ? __('app.active') : __('app.no_active')?></a>
                                                <a href="<?=controller_url('edit/'.$price['id'])?>" class="badge badge-primary"><?=__('app.edit')?></a>
                                            <?php endif;?>

                                            <?php if(has_permission('delete')):?>
                                                <a href="<?=controller_url('delete/'.$price['id'])?>" class="badge badge-danger delete"><?=__('app.delete')?></a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?=getDataTable()?>

<script>
    $('#data-table').dataTable();
</script>
