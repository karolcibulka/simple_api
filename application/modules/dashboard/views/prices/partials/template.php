<div class="card template">
    <div class="card-body">
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label>Typ cenotvorby</label>
                    <select class="form-control" data-template="type">
                        <option value="">Zvoľ typ cenotvorby</option>
                        <option data-key="fake" value="fake_discount">Falošná zľava</option>
                        <option data-key="percentage" value="percentage_discount">Percentuálna zľava</option>
                        <option data-key="const" value="const_discount">Konštantná zľava</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="text-white">dummy_label</label>
                    <button class="btn btn-danger w-100">Odstrániť typ</button>
                </div>
            </div>
            <div class="col-md-12 discount-item" data-type="fake-discount">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Platné pre krajiny</label>
                            <select class="form-control" data-template="fake-countries">
                                <option value="all">Všetky</option>
                                <option value="custom">Vybrané</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Typ</label>
                            <select class="form-control" data-template="fake-type">
                                <option value="percentual">Percentuálne</option>
                                <option value="constant">Konštatné</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Hodnota</label>
                            <input type="number" min="0" max="100000" step="0.01" class="form-control" value="0" data-template="fake-value">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Vybrané krajiny</label>
                            <select class="form-control select2" multiple data-template="fake-custom_countries">

                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>