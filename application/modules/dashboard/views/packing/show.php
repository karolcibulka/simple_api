<?=getDataTable()?>
<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')))?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <?php if(isset($packings) && !empty($packings)):?>
                    <?=nestableView($packings)?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>