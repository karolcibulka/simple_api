<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.create_new')))?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=controller_url('createProcess')?>" id="form" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" name="internal_name" class="form-control internalNameCounter" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.unit_price')?></label>
                                <input type="number" min="0" step="0.01" name="price" class="form-control" placeholder="<?=__('app.unit_price')?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>