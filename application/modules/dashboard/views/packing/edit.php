<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.create_new')))?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=controller_url('editProcess/'.$packing['id'])?>" id="form" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" name="internal_name" class="form-control internalNameCounter" value="<?=$packing['internal_name']?>" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.unit_price')?></label>
                                <input type="number" min="0" step="0.01" name="price" class="form-control" value="<?=$packing['price']?>" placeholder="<?=__('app.unit_price')?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>