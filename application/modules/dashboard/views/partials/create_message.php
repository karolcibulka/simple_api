<div class="alert alert-warning">
    <ul>
        <li><strong>Interný názov</strong> je povinný</li>
        <?php if(isset($type) && !empty($type)):?>
            <?php $exploded = explode(',',$type)?>
            <?php foreach($exploded as $item):?>
                <li><strong><?=$item?></strong>
                    <?php if(isset($customMessage) && !empty($customMessage)):?>
                        <?=$customMessage?>
                    <?php else:?>
                        je povinný parameter pre zobrazenie
                    <?php endif;?>
                </li>
            <?php endforeach;?>
        <?php endif;?>
    </ul>
</div>

<script>
</script>