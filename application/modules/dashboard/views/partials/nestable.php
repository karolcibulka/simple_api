<?php foreach($array as $array_item):?>
    <li class="dd-item col-md-12 " style="border-left:3px solid deepskyblue" data-id="<?=$array_item['id']?>">
        <div class="name dd-handle col-md-8">
            <?=$array_item['internal_name']?>
            <small>
                (
                ID = <?=$array_item['id']?> <?=isset($array_item['parent']) && !empty($array_item['parent']) ? '| Parent:'.  $array_item['parent'] : ''?>
                )
            </small>
        </div>
        <div class="actions col-md-4">
            <?php if(has_permission('edit')):?>
                <a class="active" rel="tooltip" title="Aktivita" href="<?=controller_url('active/'.$array_item['id'].'/'.$array_item['active'])?>">
                    <?php if($array_item['active'] == '1'):?>
                        <span class="badge badge-success"><?=__('app.active')?></span>
                    <?php else:?>
                        <span class="badge badge-warning"><?=__('app.no_active')?></span>
                    <?php endif;?>
                </a>&nbsp;
                <a rel="tooltip" title="Edit" class="badge badge-primary" href="<?=controller_url('edit/'.$array_item['id'])?>">
                    <?=__('app.edit')?>
                </a>&nbsp;
            <?php endif;?>

            <?php if(has_permission('delete')):?>
                <a class="badge badge-danger delete" rel="tooltip" title="<?=lang('admin.remove')?>" href="<?=controller_url('delete/'.$array_item['id'])?>">
                    <?=__('app.delete')?>
                </a>
            <?php endif;?>
        </div>

        <?php if(isset($array_item['children']) && !empty($array_item['children'])):?>
            <ol class="dd-list">
                <?=nestableView($array_item['children'])?>
            </ol>
        <?php endif;?>
    </li>
<?php endforeach;?>