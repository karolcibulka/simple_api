<div class="page-header page-header-light" style="position:sticky;top:0;">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <?php if($this->uri->segment(2) && $this->uri->segment(2) !== 'dashboard'): ?>
                    <span class="font-weight-semibold">
                        <?=lang('segment.'.$this->uri->segment(2))?>
                    </span>
                    -
                    <?=lang('segment.'.$this->uri->segment(2).'.'.$this->uri->segment(2)) ?>
                <?php else:?>
                    <span class="font-weight-semibold">
                        <?=lang('segment.dashboard')?>
                    </span>
                <?php endif;?>
            </h4>
           
        </div>

        <div class="header-elements d-none">
            <?php if($languages):?>
                <div class="col w-100">
                    <?=languagesSelect()?>
                </div>
            <?php endif;?>
            <?php if(isset($as) && !empty($as)):?>
                <?php foreach($as as $c => $a):?>
                    <a href="<?=controller_url($c)?>" class="btn btn-primary mr-1" style="min-width: 200px"><?=$a?></a>
                <?php endforeach;?>
            <?php endif;?>
            <?php if(isset($buttons) && !empty($buttons)):?>
                <?php foreach($buttons as $f => $button):?>
                    <div class="col">
                        <button type="submit" form="<?=$f?>" class="btn btn-primary d-inline-block mr-1 w-100" style="min-width: 200px"><?=$button?></button>
                    </div>
                <?php endforeach;?>
            <?php endif;?>


        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?=base_url('dashboard')?>" class="breadcrumb-item">
                    <i class="icon-home2 mr-2"></i>
                    <?=ucfirst(lang('segment.dashboard'))?>
                </a>
                <?php if($this->uri->segment(2)):?>
                    <?php if($this->uri->segment(3)):?>
                        <a href="<?=controller_url('')?>" class="breadcrumb-item">
                            <?=ucfirst(lang('segment.'.$this->uri->segment(2)))?>
                        </a>
                        <span class="breadcrumb-item active">
                            <?=lang('segment.'.$this->uri->segment(2).'.'.$this->uri->segment(3))?>
                        </span>
                    <?php else:?>
                        <span class="breadcrumb-item active">
                            <?=lang('segment.'.$this->uri->segment(2))?>
                        </span>
                    <?php endif;?>
                <?php else:?>
                    <span class="breadcrumb-item active">
                        <?=lang('segment.dashboard')?>
                    </span>
                <?php endif;?>
            </div>

        </div>
    </div>
</div>