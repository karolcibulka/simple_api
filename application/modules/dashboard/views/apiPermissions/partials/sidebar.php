<div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">
    <div id="controllerProccesWrapper">
        <div id="dynamicCreateController">
            <?php  if(has_permission('create')):?>
                <div class="sidebar-content">
                    <form action="<?=controller_url('createNewController')?>" method="post">
                        <div class="card">
                            <div class="card-header bg-transparent header-elements-inline">
                                <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('menu.addController')?></span>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label><?=lang('menu.controllerName')?>:</label>
                                    <input type="text" name="name" class="form-control" placeholder="navigation" required>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label><?=lang('menu.controllerDescription')?>:</label>
                                        <input type="text" name="description" class="form-control" placeholder="Navigácia" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary btn-block" data-popup="tooltip" title="" data-original-title="<?=lang('menu.submit')?>"><?=lang('menu.submit')?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="<?=controller_url('createNewGroup')?>" method="post">
                        <div class="card">
                            <div class="card-header bg-transparent header-elements-inline">
                                <span class="text-uppercase font-size-sm font-weight-semibold">Pridanie skupiny</span>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Interný názov:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Interný názov" required>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Názov:</label>
                                        <input type="text" name="description" class="form-control" placeholder="Názov" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary btn-block" data-popup="tooltip" title="" data-original-title="<?=lang('menu.submit')?>"><?=lang('menu.submit')?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>


