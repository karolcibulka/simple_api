<style>
    .switch{
        transform: scale(.5);
    }
</style>
<link href="<?=asset_url('css/MyApp.css')?>" rel="stylesheet" type="text/css">

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><?=lang('menu.groups')?></h5>
        </div>

        <div class="card-body">
            <div id="accordion-child1">
                <?php if(isset($groups) && !empty($groups)):?>
                    <?php foreach($groups as $name => $g):?>
                        <div class="card mb-1">
                            <div class="card-header bg-dark">
                                <h6 class="card-title">
                                    <a data-toggle="collapse" class="text-white collapsed" href="#group_<?=$name?>" aria-expanded="false"><?=$g['description']?></a>
                                </h6>
                            </div>

                            <div id="group_<?=$name?>" class="collapse" data-parent="#accordion-child1" style="">
                                <div class="card-body">
                                    <table class="table" style="margin-bottom:30px;">
                                        <thead>
                                        <tr>
                                            <th>Controller</th>
                                            <th><?=lang('menu.showing') ?></th>
                                            <th><?=lang('menu.creating') ?></th>
                                            <th><?=lang('menu.editing') ?></th>
                                            <th><?=lang('menu.deleting') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($g['controllers'] as $controller => $c):?>
                                            <tr>
                                                <td>
                                                    <?=$c['description']?>
                                                    <a href="<?=controller_url('edit/'.$c['id'].'/controller')?>" class="badge badge-primary">Upraviť</a>
                                                    <a href="<?=controller_url('delete/'.$c['id'].'/controller')?>" class="delete badge badge-danger">Zmazať</a>
                                                </td>
                                                <td>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" class="aj"  data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="s" name="s" <?=(isset($c['permissions']['show']) && ($c['permissions']['show']==TRUE)) ? 'checked' : '' ?> value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" class="aj"  data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="c" name="c" <?=(isset($c['permissions']['create']) && ($c['permissions']['create']==TRUE)) ? 'checked' : '' ?> value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" class="aj"  data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="e" name="e" <?=(isset($c['permissions']['edit']) && ($c['permissions']['edit']==TRUE)) ? 'checked' : '' ?> value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" class="aj"  data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="d" name="d" <?=(isset($c['permissions']['delete']) && ($c['permissions']['delete']==TRUE)) ? 'checked' : '' ?> value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h5>Usporiadanie právomocí</h5>
                </div>
            </div>

        </div>
        <div class="card-body">
            <?php if(isset($groups) && !empty($groups)):?>
                <div class="box-content nopadding" style="padding: 0;">
                    <div class="servicesView" style="background-color:aliceblue;border:1px solid #dddddd">
                        <div class="category col-md-12">
                            <div class="name dd-handle col-md-10" style="padding: 14px 10px;font-size: 14px;">
                                Skupiny
                            </div>
                            <div class="dd" style="padding-right: 0;padding-left: 10px;">
                                <ol class="dd-list">
                                    <?php foreach($groups as $g):?>
                                        <li class="service dd-item col-md-12"  data-id="<?=$g['id']?>" data-type="group" style="border-left: 5px solid #55b4ff">
                                            <div class="name dd-handle col-md-8"><i class="fa fa-angle-right"></i><?=$g['description']?> <small>( ID Skupiny = <?=$g['id']?>)</small></div>
                                            <div class="actions col-md-4">
                                                <?php if(has_permission('edit')):?>
                                                    <a rel="tooltip" title="Upraviť" href="<?=controller_url('edit/'.$g['id'])?>">
                                                        <span class="badge badge-primary">Upraviť</span>
                                                    </a>&nbsp;
                                                <?php endif;?>
                                                <?php if(has_permission('delete')):?>
                                                    <a class="delete" rel="tooltip" title="Zmazať" href="<?=controller_url('delete/'.$g['id'])?>">
                                                        <span class="badge badge-danger">Zmazať</span>
                                                    </a>
                                                <?php endif;?>
                                            </div>
                                        </li>
                                    <?php endforeach;?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>

<script>

    $('.dd').nestable({
        maxDepth:1,
    });

    $('.dd').on('change',function (e) {

        var _json = $('.dd').nestable('serialize');

        $.ajax({
            type: 'POST',
            url: '<?=controller_url('handleChangeGroupOrder')?>',
            data: { json:_json },
            dataType:'json',
            success:function(data){
                if( data.status === '1' ){
                    console.log('success');
                }
            }
        })
    });
    $('.aj').on('change',function(e) {

        var data = $(this).data();
        var checkbox = $(this).is(":checked");

        var url = '<?=controller_url('handleEditPermission')?>';

        $.ajax({
            type: "POST",
            url: url,
            data: {data,checkbox},
            dataType:'json', // serializes the form's elements. serialize() || serializeArray() ->do pola
            success : function(data){
                if(data.status == 1){
                    //$('#dynamicNav').html(obj.view);
                }
            }
        });

        e.preventDefault();
    });

    $('.delete').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        swalAlertDelete($(this).attr('href'));
    })
</script>

