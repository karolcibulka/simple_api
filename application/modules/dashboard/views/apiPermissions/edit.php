<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Úprava skupiny</h5>
            </div>
            <div class="card-body">
                <form action="<?=controller_url('editProcess/'.$id.'/'.$type)?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Názov</label>
                                <input type="text" data-max_length="50" class="form-control" value="<?=$data['name']?>" name="name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Popis</label>
                                <input type="text" data-max_length="50" class="form-control" value="<?=$data['description']?>" name="description">
                            </div>
                        </div>
                        <legend></legend>
                        <button class="btn btn-primary w-100">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>