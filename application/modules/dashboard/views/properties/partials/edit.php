<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Vytvorenie zariadenia</h5>
                <legend></legend>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/properties/editProcess/'.$property['id'])?>" method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Názov zariadenia</label>
                                <input type="text" class="form-control" name="name" value="<?=$property['name']?>" placeholder="Názov zariadenia">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Server</label>
                                <input type="text" class="form-control" name="server" value="<?=$property['server']?>" placeholder="Server">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Port</label>
                                <input type="text" class="form-control" name="port" value="<?=$property['port']?>" placeholder="Port">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>UACH</label>
                                <input type="text" class="form-control" name="uach" value="<?=$property['uach']?>" placeholder="UACH">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CGIN</label>
                                <input type="text" class="form-control" name="cgin" value="<?=$property['cgin']?>" placeholder="CGIN">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control">
                                    <?php if(isset($programs) && !empty($programs)):?>
                                        <?php foreach($programs as $program):?>
                                            <option <?=$program['id'] == $property['program_id'] ? 'selected' : ''?> value="<?=$program['id']?>"><?=$program['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Point ratio za 1 prvok ceny</label>
                                <input type="number" name="point_ratio" value="<?=$property['point_ratio']?>" step="0.01" min="0" max="100" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Povoliť zápis bodov</label>
                                <select name="increase_points" class="form-control">
                                    <option <?=isset($property) && empty($property['increase_points']) ? 'selected' : ''?> value="0">Nie</option>
                                    <option <?=isset($property) && !empty($property['increase_points']) ? 'selected' : ''?> value="1">Áno</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Povoliť čerpanie bodov</label>
                                <select name="draw_points" class="form-control">
                                    <option <?=isset($property) && empty($property['draw_points']) ? 'selected' : ''?> value="0">Nie</option>
                                    <option <?=isset($property) && !empty($property['draw_points']) ? 'selected' : ''?> value="1">Áno</option>
                                </select>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <h5>Užívatelia pre zariadenie</h5>
                        </div>
                        <?php if(isset($users) && !empty($users)):?>
                            <?php foreach($users as $user):?>
                                <div class="col-md-12">
                                    <label>
                                        <input type="checkbox" <?=isset($property['users']) && !empty($property['users']) && in_array($user['id'],$property['users']) ? 'checked' : ''?> name="user[<?=$user['id']?>]" value="<?=$user['id']?>"> <?=$user['first_name'].' '.$user['last_name'].' <small>('.$user['email'].')</small>' ?>
                                    </label>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <legend></legend>
                        <button class="w-100 btn btn-primary">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>