<style>
    .disabled{
        cursor:not-allowed;
        opacity:.5;
    }
</style>

<div class="content-wrapper">
    <?=getBreadcrumb()?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card" >
                    <div class="card-header">
                        <a href="<?=base_url('dashboard/properties/createGroup')?>" class="btn btn-primary w-100">Pridať skupinu</a>
                        <legend></legend>
                        <a href="<?=base_url('dashboard/properties/create')?>" class="btn btn-primary w-100">Pridať zariadenie</a>
                        <legend></legend>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" id="propertiesTableWrap">
                            <table class="table table-bordered table-striped" id="propertiesTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?=lang('properties.propertyName')?></th>
                                    <th>Server</th>
                                    <th>UACH</th>
                                    <th>CGIN</th>
                                    <th>Program</th>
                                    <th>Skupina</th>
                                    <th>Token</th>
                                    <th><?=lang('properties.actions')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($properties) && !empty($properties)):?>
                                <?php foreach ($properties as $p):?>
                                <tr>
                                    <td><?=$p['id']?></td>
                                    <td><?=$p['name']?></td>
                                    <td><?=$p['server'].':'.$p['port']?></td>
                                    <td><?=$p['uach']?></td>
                                    <td><?=$p['cgin']?></td>
                                    <td>
                                        <select data-id="<?=$p['id']?>" class="form-control changeProgram" style="height:30px;">
                                            <?php if(isset($programs) && !empty($programs)):?>
                                                <?php foreach($programs as $program):?>
                                                    <option <?=$program['id']==$p['program_id'] ? 'selected' : ''?> value="<?=$program['id']?>"><?=$program['internal_name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control changeGroup" data-id="<?=$p['id']?>">
                                            <option value="">---</option>
                                            <?php if(isset($groups) && !empty($groups)):?>
                                                <?php foreach($groups as $group):?>
                                                    <option <?=$group['id']==$p['group_id'] ? 'selected' : ''?> value="<?=$group['id']?>"><?=$group['internal_name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </td>
                                    <td>
                                        <span data-token="<?=$p['token']?>" data-popup="tooltip" data-placement="top" title="Kopírovať token" class="badge badge-primary copyToClipBoard cursor-pointer">
                                            <i class="icon-copy "></i>
                                        </span>
                                        <a class="generateToken" href="<?=base_url('dashboard/properties/createNewToken/'.$p['id'])?>">
                                            <span data-token="<?=$p['token']?>" data-popup="tooltip" data-placement="top" title="Vygenerovať nový token" class="badge badge-info cursor-pointer">
                                                <i class="icon-popout"></i>
                                            </span>
                                        </a>
                                    </td>
                                    <td>
                                        <?php if(has_permission('edit')):?>
                                            <a href="<?=base_url('dashboard/properties/setProperty/'.$p['id'])?>"><span data-popup="tooltip" title="Nastaviť zariadenie ako aktívne" class="badge badge-primary" style="background-color:black"><i class="icon-new-tab2"></i></span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('edit')):?>
                                            <a href="<?=base_url('dashboard/properties/sync/'.$p['id'])?>"><span class="badge sync badge-info" data-popup="tooltip" title="Synchronizácia" style="background-color:mediumpurple"><i class="icon-loop"></i></span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('edit')):?>
                                            <a href="<?=base_url('dashboard/properties/checkConnection/'.$p['id'])?>"><span data-popup="tooltip" title="Skontrolovať pripojenie" class="badge checkConnection badge-info"><i class="icon-connection"></i></span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('edit')):?>
                                            <?php if($p['active'] == '1'):?>
                                                <a href="<?=base_url('dashboard/properties/activity/'.$p['id'].'/0')?>"><span data-popup="tooltip" title="Aktívne" class="badge badge-success"><i class="icon-checkmark"></i></span></a>
                                            <?php else:?>
                                                <a href="<?=base_url('dashboard/properties/activity/'.$p['id'].'/1')?>"><span data-popup="tooltip" title="Neaktívne" class="badge badge-warning"><i class="icon-cross"></i></span></a>
                                            <?php endif;?>
                                        <?php endif;?>
                                        <?php if(has_permission('edit')):?>
                                            <a href="<?=base_url('dashboard/properties/edit/'.$p['id'])?>"><span data-popup="tooltip" title="Upraviť" class="badge badge-primary"><i class="icon-pencil3"></i></span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('delete')):?>
                                            <a class="deleteProperty" href="<?=base_url('dashboard/properties/delete/'.$p['id'])?>"><span data-popup="tooltip" title="Zmazať" class="badge badge-danger"><i class="icon-cancel-circle2"></i></span></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.generateToken').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _this = $(this),
            _path = _this.attr('href');
        swalAlertDelete(_path,'Generovanie tokenu','Naozaj chcete vygenerovať nový token?','Generovať');
    });

    $('.changeVersion').on('change',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _id = $(this).data().id;
        var _version = $(this).val();

        $.ajax({
            type:'post',
            url:'<?=base_url('dashboard/properties/changeVersion')?>',
            data:{id:_id,version:_version},
            dataType:'json',
            success:function(data){
                if(data.status === '1'){
                    swalSuccessMessageSession('Úspešne zmenená verzia')
                }
                else{
                    swalSuccessMessageSession('Pri zmenene verzie sa vyskytla chyba','error')
                }
            }
        });
    });

    $('.copyToClipBoard').on('click',function(){
        var $temp = $("<input>"),
            _this = $(this);
        $("body").append($temp);
        $temp.val(_this.data().token).select();
        document.execCommand("copy");
        $temp.remove();
        swalSuccessMessageSession('Úspešne skopírované do clipboardu');
    });

    $('.changeGroup').on('change',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _id = $(this).data().id;
        var _group_id = $(this).val();

        $.ajax({
            type:'post',
            url:'<?=base_url('dashboard/properties/changeGroup')?>',
            data:{id:_id,group_id:_group_id},
            dataType:'json',
            success:function(data){
                if(data.status === '1'){
                    swalSuccessMessageSession('Úspešne zmenený program')
                }
                else{
                    swalSuccessMessageSession('Pri zmenene programu sa vyskytla chyba','error')
                }
            }
        });
    });

    $('.changeProgram').on('change',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _id = $(this).data().id;
        var _program_id = $(this).val();

        $.ajax({
            type:'post',
            url:'<?=base_url('dashboard/properties/changeProgram')?>',
            data:{id:_id,program_id:_program_id},
            dataType:'json',
            success:function(data){
                if(data.status === '1'){
                    swalSuccessMessageSession('Úspešne zmenený program')
                }
                else{
                    swalSuccessMessageSession('Pri zmenene programu sa vyskytla chyba','error')
                }
            }
        });
    });

    $('.deleteProperty').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _this = $(this);
        swalAlertDelete(_this.attr('href'));
    });

    $(document).on('click','.checkConnection',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _this = $(this),
            _a = $(this).closest('a'),
            _href = _a.attr('href');

        if(!_this.hasClass('disabled')){
            $.ajax({
                type:'get',
                url:_href,
                dataType:'json',
                beforeSend:function(){
                    _a.html(getLoader(_this.attr('class')));
                },
                success:function(data){
                    if(data.status === '1'){
                        swalSuccessMessageSession('Spojenie úspešné');
                        _a.html(_this);
                    }
                    else{
                        swalSuccessMessageSession('Nepodarilo sa pripojiť ku serveru','error');
                        _a.html(_this);
                    }
                }
            });
        }
    });

    $(document).on('click','.sync',function(e){
        e.preventDefault();
        e.stopPropagation();
        var _this = $(this),
            _a = _this.closest('a'),
            _href = _a.attr('href');

        if(!_this.hasClass('disabled')){
            $.ajax({
                type:'get',
                url:_href,
                dataType:'json',
                beforeSend:function(){
                    _a.html(getLoader(_this.attr('class'),_this.attr('style')));
                },
                success:function(data){
                    if(data.status === '1'){
                        swalSuccessMessageSession('Synchronizácia úspešná');
                        _a.html(_this);
                    }
                    else{
                        swalSuccessMessageSession('Nepodarilo sa pripojiť ku serveru','error');
                        _a.html(_this);
                    }
                }
            });
        }
    });

    var getLoader = function(classes, styles = ''){
        var loader = '<span class="'+classes+' disabled" style="'+styles+'"><i class="spinner icon-spinner2"></i></span>';
        return loader;
    }


</script>


