<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Vytvorenie skupiny</h5>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/properties/createGroupProcess')?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Interný názov</label>
                                <input type="text" class="form-control" name="internal_name" placeholder="Interný názov" value="<?=isset($group['internal_name']) && !empty($group['internal_name']) ? $group['internal_name'] : ''?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kód</label>
                                <input type="text" class="form-control" placeholder="Kód" name="code" value="<?=isset($group['code']) && !empty($group['code']) ? $group['code'] : ''?>">
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <button class="btn btn-primary w-100">Uložiť</button>
                </form>
            </div>
        </div>
    </div>
</div>