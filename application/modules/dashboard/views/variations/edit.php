<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <ul class="nav nav-tabs nav-tabs-highlight outer-tabs mb-0 border-bottom-0">
            <li class="nav-item">
                <a href="#variation" class="nav-link active" data-toggle="tab"><?=__('app.variation')?></a>
            </li>
            <li class="nav-item">
                <a href="#translations" class="nav-link" data-toggle="tab"><?=__('app.translation')?></a>
            </li>
            <li class="nav-item">
                <a href="#gallery" class="nav-link" data-toggle="tab"><?=__('app.gallery')?></a>
            </li>
            <li class="nav-item">
                <a href="#facilities" class="nav-link" data-toggle="tab"><?=__('app.facilities')?></a>
            </li>
            <li class="nav-item variability" style="<?=isset($variation['use_variability']) && !empty($variation['use_variability']) ? '' : 'display:none;'?>">
                <a href="#variability" class="nav-link" data-toggle="tab"><?=__('app.variability')?></a>
            </li>
        </ul>
        <div class="card">
            <div class="card-body">
                <form action="<?=controller_url('editProcess/'.$variation['id'])?>" method="post" enctype="multipart/form-data" id="form">
                    <div class="tab-content" >
                        <div class="tab-pane" id="translations">
                            <div class="row">
                                <?php foreach(applicationLangs() as $language):?>
                                    <div class="col-md-12 language language-<?=$language?>">
                                        <div class="form-group">
                                            <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?></label>
                                            <input type="text" value="<?=isset($variation['languages'][$language]['name']) ? $variation['languages'][$language]['name'] : ''?>" name="name[<?=$language?>]" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?>" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-12 language language-<?=$language?>">
                                        <div class="form-group">
                                            <label><?=__('app.perex_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?></label>
                                            <textarea class="form-control" name="perex[<?=$language?>]"><?=isset($variation['languages'][$language]['perex']) ? $variation['languages'][$language]['perex'] : ''?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12 language language-<?=$language?>">
                                        <div class="form-group">
                                            <label><?=__('app.description_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?></label>
                                            <textarea class="summernote form-control" name="description[<?=$language?>]"><?=isset($variation['languages'][$language]['description']) ? $variation['languages'][$language]['description'] : ''?></textarea>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="tab-pane" id="gallery">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?=__('app.images')?></label>
                                        <div class="input-images"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <legend></legend>
                                    <div class="form-group">
                                        <label><?=__('app.images')?></label>
                                    </div>
                                    <div class="row">
                                        <?php if(isset($images) && !empty($images)):?>
                                            <?php foreach($images as $key => $image):?>
                                                <div class="col-sm-3 mb-2">
                                                    <div class="image-background" style="background-image: url(<?=$image['src']?>);">
                                                        <div class="card-img-actions m-1">
                                                            <a class="btn btn-outline bg-white btn-icon rounded-round ml-2 cropFile dd-nodrag"  data-image="<?=$image['id']?>" data-id="<?=$image['id']?>" style="float: right">
                                                                <i class="icon-trash"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="variability">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?=__('app.variability')?></label>
                                        <select class="form-control" name="variability">
                                            <option value=""><?=__('app.no_variability')?></option>
                                            <?php if(isset($variabilities) && !empty($variabilities)):?>
                                                <?php foreach($variabilities as $variability):?>
                                                    <option <?=isset($variation['variability']) && $variation['variability'] === $variability['id'] ? 'selected' : ''?> value="<?=$variability['id']?>"><?=$variability['internal_name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>
                                <?php if(isset($variabilities) && !empty($variabilities)):?>
                                    <?php foreach($variabilities as $variability_id => $variability):?>
                                        <div class="col-md-12 variability-item variability-item-<?=$variability['id']?>" style="border:1px solid #dddddd;padding:20px;<?=isset($variation['variability']) && $variation['variability'] === $variability['id'] ? '' : 'display:none;'?>">
                                            <strong><?=$variability['internal_name']?></strong>
                                            <legend></legend>
                                            <div class="row mb-1">
                                                <div class="col-sm-3 text-center"><?=__('app.value')?></div>
                                                <div class="col-sm-3 text-center"><?=__('app.unit_price')?></div>
                                                <div class="col-sm-3 text-center"><?=__('app.price_profile')?></div>
                                                <div class="col-sm-3 text-center"><?=__('app.discounts')?></div>
                                            </div>
                                            <?php if(isset($variability['items']) && !empty($variability['items'])):?>
                                                <?php foreach($variability['items'] as $item_id => $item):?>
                                                <div class="row mb-1">
                                                    <div class="col-sm-3">
                                                        <input type="text" disabled class="form-control" value="<?=$item['value']?>">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="number" placeholder="<?=__('app.unit_price')?>" name="variability_unit_price[<?=$variability_id?>][<?=$item_id?>]" value="<?=isset($variation['variabilities'][$variability_id][$item_id]['unit_price']) ? $variation['variabilities'][$variability_id][$item_id]['unit_price'] : ''?>" class="form-control" step="0.01" min="0" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select name="variability_price_profile[<?=$variability_id?>][<?=$item_id?>]" class="form-control">
                                                            <option value=""><?=__('app.price_profile')?></option>
                                                            <?php if(isset($price_profiles) && !empty($price_profiles)):?>
                                                                <?php foreach($price_profiles as $price_profile):?>
                                                                    <option <?=isset($variation['variabilities'][$variability_id][$item_id]['price_profile']) && $variation['variabilities'][$variability_id][$item_id]['price_profile'] == $price_profile['id'] ? 'selected' : ''?> value="<?=$price_profile['id']?>"><?=$price_profile['internal_name']?></option>
                                                                <?php endforeach;?>
                                                            <?php endif;?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select name="variability_discount[<?=$variability_id?>][<?=$item_id?>][]" class="form-control select2" multiple>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="tab-pane" id="facilities">
                            <div class="row">
                                <?php if(isset($facilities) && !empty($facilities)):?>
                                    <?php foreach($facilities as $facility_id => $facility):?>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><?=$facility['internal_name']?></label>
                                                <select name="facility[<?=$facility_id?>][]" multiple class="select2 form-control">
                                                    <?php if(isset($facility['items']) && !empty($facility['items'])):?>
                                                        <?php foreach($facility['items'] as $item_id => $item):?>
                                                            <option <?=isset($variation['facilities'][$facility_id][$item_id]) && !empty($variation['facilities'][$facility_id][$item_id]) ? 'selected' : ''?> value="<?=$item['id']?>"><?=$item['value']?></option>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <div class="col-md-12 text-center">
                                        <?=__('app.no_facilities_defined')?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="tab-pane show active" id="variation">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?=__('app.internal_name')?></label>
                                        <input type="text" name="internal_name" value="<?=isset($variation['internal_name']) ? $variation['internal_name'] : ''?>" placeholder="<?=__('app.internal_name')?>" data-max_length="30" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?=__('app.product')?></label>
                                        <select name="product_id" class="select2 form-control">
                                            <?php if(isset($products) && !empty($products)):?>
                                                <?php foreach($products as $product):?>
                                                    <option <?=isset($variation['product_id']) && $variation['product_id'] == $product['id'] ? 'selected' : ''?> value="<?=$product['id']?>"><?=$product['internal_name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?=__('app.use_variability')?></label>
                                        <select name="use_variability" class="form-control">
                                            <option <?=isset($variation['use_variability']) && empty($variation['use_variability']) ? 'selected' : ''?> value="0"><?=__('app.no')?></option>
                                            <option <?=isset($variation['use_variability']) && !empty($variation['use_variability']) ? 'selected' : ''?> value="1"><?=__('app.yes')?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 dont-use-variability" style="<?=isset($variation['use_variability']) && !empty($variation['use_variability']) ? 'display:none;' : ''?>" >
                                    <div class="form-group">
                                        <label><?=__('app.unit_price')?></label>
                                        <input type="number" name="unit_price" value="<?=$variation['unit_price']?>" class="form-control" step="0.01" placeholder="Jednotková cena">
                                    </div>
                                </div>

                                <div class="col-md-4 dont-use-variability" style="<?=isset($variation['use_variability']) && !empty($variation['use_variability']) ? 'display:none;' : ''?>" >
                                    <div class="form-group">
                                        <label><?=__('app.price_profile')?></label>
                                        <select name="price_profile" class="form-control">
                                            <option value=""><?=__('app.price_profile')?></option>
                                            <?php if(isset($price_profiles) && !empty($price_profiles)):?>
                                                <?php foreach($price_profiles as $price_profile):?>
                                                    <option <?=isset($variation['price_profile']) && $variation['price_profile'] == $price_profile['id'] ? 'selected' : ''?> value="<?=$price_profile['id']?>"><?=$price_profile['internal_name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 dont-use-variability" style="<?=isset($variation['use_variability']) && !empty($variation['use_variability']) ? 'display:none;' : ''?>" >
                                    <div class="form-group">
                                        <label><?=__('app.discounts')?></label>
                                        <select name="discount[]" class="form-control select2" multiple>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?=getImageUploadFile()?>
<?=getSelect2()?>

<script>

    $('.select2').select2();

    $('.input-images').imageUploader({
        imagesInputName: 'images',
        preloadedInputName: 'old',
        maxSize: 2 * 1024 * 1024,
        maxFiles: 10
    });

    $('[name="use_variability"]').on('change',function(){
        var _self = $(this),
            _wrapper = $('.variability'),
            _dontUserVariability = $('.dont-use-variability');

        if(_self.val() === '1'){
            _wrapper.show();
            _dontUserVariability.hide();
        }
        else{
            _dontUserVariability.show();
            _wrapper.hide();
        }
    });

    $('[name="variability"]').on('change',function(){
        var _self = $(this),
            _val = _self.val();

        $('.variability-item').hide();

        if( _val !== '' ){
            $('.variability-item-'+_val).show();
        }
    });

</script>





