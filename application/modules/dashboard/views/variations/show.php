<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')),array(),false)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?=__('app.internal_name')?></th>
                                <th><?=__('app.product')?></th>
                                <th><?=__('app.category')?></th>
                                <th><?=__('app.action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($variations) && !empty($variations)):?>
                            <?php foreach($variations as $variation):?>
                                <tr>
                                    <td><?=$variation['id']?></td>
                                    <td><?=$variation['internal_name']?></td>
                                    <td>
                                        <?php if(isset($variation['product_internal_name']) && !empty($variation['product_internal_name'])):?>
                                            <a href="<?=base_url('dashboard/products/edit/'.$variation['product_id'])?>"><?=$variation['product_internal_name']?></a>
                                        <?php endif;?>
                                    </td>
                                    <td><?=$variation['categories']?></td>
                                    <td>
                                        <?php if(has_permission('edit')):?>
                                            <?php if(!empty($variation['active'])):?>
                                                <a href="<?=controller_url('active/'.$variation['id'].'/0')?>" class="badge badge-success"><?=__('app.active')?></a>
                                            <?php else:?>
                                                <a href="<?=controller_url('active/'.$variation['id'].'/1')?>" class="badge badge-danger"><?=__('app.no_active')?></a>
                                            <?php endif;?>
                                            <a href="<?=controller_url('edit/'.$variation['id'])?>" class="badge badge-primary"><?=__('app.edit')?></a>
                                        <?php endif;?>

                                        <?php if(has_permission('delete')):?>
                                            <a href="<?=controller_url('delete/'.$variation['id'])?>" class="badge badge-danger delete"><?=__('app.delete')?></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<?=getDataTable()?>






