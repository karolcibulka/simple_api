<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login to system</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo asset_url('global/js/main/jquery.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo asset_url('js/app.js')?>"></script>
    <!-- /theme JS files -->

</head>

<style>
    .form-group-feedback-left .form-control {
        padding-left: 10px;
    }
</style>
<body>

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">
            <?php echo form_open('dashboard/auth/reset_password/' . $code,'class="login-form" style="width:500px"');?>

            <h1><?=lang('reset_password_heading');?></h1>

            <div id="infoMessage"><?php echo $message;?></div>

            	<p>
            		<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
            		<?php echo form_input($new_password,'','class="form-control" placeholder="'.lang('reset_password_new_password_label').'"');?>
            	</p>

            	<p>
            		<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
            		<?php echo form_input($new_password_confirm,'','class="form-control" placeholder="'.lang('reset_password_new_password_confirm_label').'"');?>
            	</p>

            	<?php echo form_input($user_id);?>
            	<?php echo form_hidden($csrf); ?>

            	<p><?php echo form_submit('submit', lang('reset_password_submit_btn'),'class="btn btn-primary btn-block"');?></p>

            <?php echo form_close();?>
        </div>
        <!-- /content area -->


    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>
