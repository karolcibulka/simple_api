<div class="content-wrapper">
    <div class="content">


    <div class="content d-flex justify-content-center align-items-center">
    <?php echo form_open("dashboard/auth/create_group",'class="login-form" style="width:500px;"');?>

    <h1><?php echo lang('create_group_heading');?></h1>
    <p><?php echo lang('create_group_subheading');?></p>

    <div id="infoMessage"><?php echo $message;?></div>

          <p>
                <?php echo lang('create_group_name_label', 'group_name');?> <br />
                <?php echo form_input($group_name,'','class="form-control" placeholder="'.lang('create_group_name_label').'"');?>
          </p>

          <p>
                <?php echo lang('create_group_desc_label', 'description');?> <br />
                <?php echo form_input($description,'','class="form-control" placeholder="'.lang('create_group_desc_label').'"');?>
          </p>

          <p><?php echo form_submit('submit', lang('create_group_submit_btn'),'class="btn btn-primary btn-block"');?></p>

    <?php echo form_close();?>
</div>

    </div>