
<div class="content-wrapper">
    <div class="content">



    <div class="content d-flex justify-content-center align-items-center">

    <?php echo form_open("dashboard/auth/create_user",'class="login-form" style="width:500px"');?>

        <h1><?php echo lang('create_user_heading');?></h1>
        <p><?php echo lang('create_user_subheading');?></p>
        <div id="infoMessage"><?php echo $message;?></div>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name,'','class="form-control" placeholder="'.lang('create_user_fname_label').'"');?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name,'','class="form-control" placeholder="'.lang('create_user_lname_label').'"');?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company,'','class="form-control" placeholder="'.lang('create_user_company_label').'"');?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email,'','class="form-control" placeholder="'.lang('create_user_email_label').'"');?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone,'','class="form-control" placeholder="'.lang('create_user_phone_label').'"');?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password,'','class="form-control" placeholder="'.lang('create_user_password_label').'"');?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm,'','class="form-control" placeholder="'.lang('create_user_password_confirm_label').'"');?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-primary btn-block"');?></p>

<?php echo form_close();?>
</div>


</div>