
<div class="content-wrapper">
    <div class="content">

<h1><?php echo lang('index_heading');?></h1>
<p><?php echo lang('index_subheading');?></p>



    <div class="card ">
    <div style="text-align:right;margin:10px 40px 0 0;">
        <?php if(has_permission('create')):?>
            <p><?php echo anchor('dashboard/auth/create_user', lang('index_create_user_link'),'data-popup="tooltip" title="" data-original-title="'.lang('index_create_user_link').'"')?> | <?php echo anchor('dashboard/auth/create_group', lang('index_create_group_link'),'data-popup="tooltip" title="" data-original-title="'.lang('index_create_group_link').'"')?></p>
        <?php endif;?>
    </div>
<div id="infoMessage"><?php echo $message;?></div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th><?php echo lang('index_fname_th');?></th>
                <th><?php echo lang('index_lname_th');?></th>
                <th><?php echo lang('index_email_th');?></th>
                <?php if(has_permission('edit')):?>
                    <th><?php echo lang('index_groups_th');?></th>
                    <th><?php echo lang('index_status_th');?></th>
                    <th><?php echo lang('index_action_th');?></th>
                <?php endif;?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user):?>
                <tr>
                    <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                    <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                    <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                    <?php if(has_permission('edit')):?>
                        <td>
                            <?php foreach ($user->groups as $group):?>
                                <?php echo anchor("dashboard/auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'),'data-popup="tooltip" title="" data-original-title="'.lang('group.singular').'"') ;?><br />
                            <?php endforeach?>
                        </td>
                        <td><?php echo ($user->active) ? anchor("dashboard/auth/deactivate/".$user->id, lang('index_active_link'),'data-popup="tooltip" title="" data-original-title="'.lang('index_status_th').'"') : anchor("dashboard/auth/activate/". $user->id, lang('index_inactive_link','','data-popup="tooltip" title="" data-original-title="'.lang('index_status_th').'"'));?></td>
                        <td><?php echo anchor("dashboard/auth/edit_user/".$user->id, 'Edit','data-popup="tooltip" title="" data-original-title="'.lang('index_action_th').'"') ;?></td>
                    <?php endif;?>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

    </div>
