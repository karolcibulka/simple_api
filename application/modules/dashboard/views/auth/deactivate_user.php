<div class="content-wrapper">
    <div class="content">


    <div class="content d-flex justify-content-center align-items-center">
<?php echo form_open("dashboard/auth/deactivate/".$user->id,'style="width:500px;"');?>

    <h1><?php echo lang('deactivate_heading');?></h1>
    <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>


    <p>
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p>

  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>

  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),'class="btn btn-primary btn-block"');?></p>

<?php echo form_close();?>
</div>

    </div>