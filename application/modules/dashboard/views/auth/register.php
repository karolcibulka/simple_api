
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register to system</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/default_font.css')?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo asset_url('global/js/main/jquery.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo asset_url('js/app.js')?>"></script>
    <!-- /theme JS files -->

</head>

<style>
    .form-group-feedback-left .form-control fw-300 br-0 {
        padding-left: 10px;
    }

    body{
        background-color:#2196f3;
        font-family: Roboto;
    }

    .fw-300{
        font-weight:300;
    }

    .br-0{
        border-radius:0 !important;
    }
</style>
<body>

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">


            <form action="<?=base_url('dashboard/auth/register')?>" method="post">
                <h1 class="fw-300 text-white text-center text-uppercase">Warehouse</h1>
                <div class="card mb-0 br-0" style="width: 600px;">
                    <div class="card-body">
                        <?php if($this->session->flashdata('message')):?>
                            <div class="alert alert-primary text-center">
                                <?=$this->session->flashdata('message')?>
                            </div>
                        <?php endif;?>
                        <?php if(isset($errors) && !empty($errors)):?>
                            <div class="alert alert-danger text-center">
                                <ul>
                                    <?php foreach($errors as $error):?>
                                        <li><?=$error?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                        <?php endif;?>
                        <div class="text-center mb-3">
                            <h4 class="fw-300"><?=__('app.register')?></h4>
                            <h5 class="mb-0"></h5>
                            <span class="d-block text-muted"></span>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" readonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.name')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="surname" readonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.surname')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="email" readonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.email')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="phone" readonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.phone')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" eadonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.password')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" readonly onfocus="this.removeAttribute('readonly');" required placeholder="<?=__('app.password_confirmation')?>" class="form-control fw-300 br-0">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select name="language" required class="form-control fw-300 br-0">
                                        <option value=""><?=__('app.language')?></option>
                                        <?php foreach(applicationLangs(true) as $language):?>
                                            <option <?=isset($lang) && $lang === $language ? 'selected' : ''?> value="<?=$language?>"><?=lang('langshort.'.$language)?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" readonly onfocus="this.removeAttribute('readonly');" placeholder="<?=__('app.property_token')?>" class="form-control fw-300 br-0">
                                    <small><?=__('app.token_explain')?></small>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-block br-0 fw-300"><?=__('app.register')?></button>
                        </div>
                        <legend></legend>
                        <div class="col-md-12 text-center">
                            <a href="<?=base_url('dashboard/auth/login')?>" class="fw-300"><?=__('app.login')?></a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /login form -->

        </div>
        <!-- /content area -->


    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script>
    $('form').attr('autocomplete', 'off');
    $('input').attr('autocomplete', 'off');
</script>
</body>
</html>
