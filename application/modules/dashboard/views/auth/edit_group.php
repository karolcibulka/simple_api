<div class="content-wrapper">
    <div class="content">



    <div class="content d-flex justify-content-center align-items-center">
<?php echo form_open(current_url(),'class="login-form" style="width:500px"');?>

    <h1><?php echo lang('edit_group_heading');?></h1>
    <p><?php echo lang('edit_group_subheading');?></p>

    <div id="infoMessage"><?php echo $message;?></div>

      <p>
            <?php echo lang('edit_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name,'','class="form-control" placeholder="Group name"');?>
      </p>

      <p>
            <?php echo lang('edit_group_desc_label', 'description');?> <br />
            <?php echo form_input($group_description,'','class="form-control" placeholder="Group description"');?>
      </p>

      <p><?php echo form_submit('submit', lang('edit_group_submit_btn'),'class="btn btn-primary btn-block"');?></p>

<?php echo form_close();?>
</div>

    </div>