<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo asset_url('global/js/main/jquery.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo asset_url('js/app.js')?>"></script>
    <!-- /theme JS files -->

</head>
<style>
    .error{
        color: red;
        font-weight: bold;
    }
</style>

<body>

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Registration form -->
            <form action="<?=base_url()?>dashboard/auth/property_preregistration" class="flex-fill" method="post">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card mb-0">
                            <div class="card-body">
                                <div class="text-center mb-3">
                                    <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                                    <h5 class="mb-0"><?=lang('preregistration.header')?></h5>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="name" placeholder="<?=lang('preregistration.name')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="propertyName" placeholder="<?=lang('preregistration.propertyName')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-home2 text-muted"></i>
                                            </div>
                                            <?php echo form_error('propertyName', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="surname" placeholder="<?=lang('preregistration.surname')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="telephone" placeholder="<?=lang('preregistration.telephone')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-phone2 text-muted"></i>
                                            </div>
                                            <?php echo form_error('telephone', '<div class="error">', '</div>'); ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="email" class="form-control" name="email" placeholder="<?=lang('preregistration.email')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-mention text-muted"></i>
                                            </div>
                                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="street" placeholder="<?=lang('preregistration.street')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-road text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="password" class="form-control" name="password" placeholder="<?=lang('preregistration.password')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-lock text-muted"></i>
                                            </div>
                                            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="city" placeholder="<?=lang('preregistration.city')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-city text-muted"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="postalcode" placeholder="<?=lang('preregistration.postcode')?>">
                                            <div class="form-control-feedback">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="url" placeholder="<?=lang('preregistration.web')?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-link text-muted"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" name="capacity" placeholder="<?=lang('preregistration.capacity')?>">
                                            <div class="form-control-feedback">
                                                <i class="text-muted"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <select name="lang" id="" class="form-control">
                                                <?php foreach ($languages as $language):?>
                                                    <option value="<?=$language['code']?>"><?=$language['language']?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> <?=lang('preregistration.create')?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /registration form -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>
