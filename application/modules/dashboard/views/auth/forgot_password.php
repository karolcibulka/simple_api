

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login to system</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo asset_url('global/js/main/jquery.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo asset_url('js/app.js')?>"></script>
    <!-- /theme JS files -->

</head>

<style>
    .form-group-feedback-left .form-control {
        padding-left: 10px;
    }
</style>
<body>

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <div class="content d-flex justify-content-center align-items-center">
            <?php echo form_open("dashboard/auth/forgot_password",'class="login-form" style="width:500px;"');?>
                <h1><?php echo lang('forgot_password_heading');?></h1>
                <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                <div id="infoMessage"><?php echo $message;?></div>
              <p>
              	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
              	<?php echo form_input($identity,'','class="form-control" placeholder="Identity"');?>
              </p>

              <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn btn-primary btn-block"');?></p>

            <?php echo form_close();?>
        </div>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>
