
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login to system</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('global/css/icons/icomoon/styles.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/bootstrap_limitless.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/layout.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/components.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/colors.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset_url('css/default_font.css')?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo asset_url('global/js/main/jquery.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/main/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo asset_url('global/js/plugins/loaders/blockui.min.js')?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo asset_url('js/app.js')?>"></script>
    <!-- /theme JS files -->

</head>

<style>
    .form-group-feedback-left .form-control {
        padding-left: 10px;
    }

    body{
        background-color:#2196f3;
        font-family: Roboto;
    }

    .fw-300{
        font-weight:300;
    }

    .br-0{
        border-radius:0 !important;
    }
</style>
<body>

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">


            <?php echo form_open("dashboard/auth/login",'class="login-form"');?>
                <h1 class="fw-300 text-white text-center text-uppercase">Warehouse</h1>
                <div class="card mb-0 br-0" style="width: 300px;">
                    <div class="card-body">
                        <?php if($this->session->flashdata('message')):?>
                            <div class="alert alert-primary text-center">
                                <?=$this->session->flashdata('message')?>
                            </div>
                        <?php endif;?>
                        <div class="text-center mb-3">
                            <h4 class="fw-300"><?=__('app.login')?></h4>
                            <h5 class="mb-0"></h5>
                            <span class="d-block text-muted"></span>
                        </div>
                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <select class="form-control change-language br-0 fw-300">
                                <?php foreach(applicationLangs(true) as $language):?>
                                    <option <?=isset($lang) && $lang === $language ? 'selected' : ''?> value="<?=$language?>"><?=lang('langshort.'.$language)?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                       
                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="text" name="identity" class="form-control br-0 fw-300" placeholder="<?=__('app.email')?>">
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" readonly onfocus="this.removeAttribute('readonly');" name="password" class="form-control br-0 fw-300" placeholder="<?=__('app.password')?>">
                        </div>

                        <div class="form-group">
                            <?php echo form_submit('submit', __('app.login') ,'class="btn btn-primary btn-block br-0 fw-300"');?>
                        </div>
                        <legend></legend>
                        <div class="col-md-12 text-center">
                        <a href="<?=base_url('dashboard/auth/register')?>" class="fw-300"><?=__('app.register')?></a>
                        </div>
                        

                    </div>
                </div>
            <?php echo form_close();?>
            <!-- /login form -->

        </div>
        <!-- /content area -->


    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
<script>
    $('.change-language').on('change',function(){
        window.location.href = '<?=base_url('dashboard/auth/changeLanguage')?>/'+$(this).val();
    });
</script>
</body>
</html>
