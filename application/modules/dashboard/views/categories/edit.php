<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app_save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?= controller_url('editProcess/'.$id) ?>" method="post" id="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input value="<?=isset($category['internal_name']) && !empty($category['internal_name']) ? $category['internal_name'] : ''?>" class="form-control mb-3" placeholder="<?=__('app.internal_name')?>" data-max_length="30" name="internal_name">
                            </div>
                        </div>

                        <?php foreach (applicationLangs() as $language) : ?>
                            <div class="col-md-6 language language-<?= $language ?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?></label>
                                    <input value="<?=isset($category['languages'][$language]['name']) && !empty($category['languages'][$language]['name']) ? $category['languages'][$language]['name'] : ''?>" placeholder="<?=__('app.name_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?>" class="form-control mb-3" type="text" name="name[<?= $language ?>]">
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.facilities')?></label>
                                <select name="behavior_id" class="form-control">
                                    <option value=""><?=__('app.no_facilities')?></option>
                                    <?php if(isset($behaviors) && !empty($behaviors)):?>
                                        <?php foreach($behaviors as $behavior):?>
                                            <option <?=isset($category['behavior_id']) && $category['behavior_id'] === $behavior['id'] ? 'selected' : ''?> value="<?=$behavior['id']?>"><?=$behavior['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.parent')?></label>
                                <select name="parent" class="form-control">
                                    <option value="0"><?=__('app.no_parent')?></option>
                                    <?php if(isset($categories) && !empty($categories)):?>
                                        <?php foreach($categories as $item):?>
                                            <option <?=isset($category['parent']) && $category['parent'] === $item['id'] ? 'selected' : ''?> value="<?=$item['id']?>"><?=$item['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <?php foreach (applicationLangs() as $language) : ?>
                            <div class="col-md-12 language language-<?=$language?>">
                                <div class="form-group">
                                    <label><?=__('app.description_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?></label>
                                    <textarea name="description[<?= $language ?>]" class="summernote" cols="30" rows="10"><?=isset($category['languages'][$language]['description']) && !empty($category['languages'][$language]['description']) ? $category['languages'][$language]['description'] : ''?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
