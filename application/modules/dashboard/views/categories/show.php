<?=getNestable()?>
<div class="content-wrapper">
    <?=getBreadcrumb(array('create'=>__('app.create_new')))?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-content nopadding dd" style="padding: 0;">
                            <ol class="servicesView dd-list">
                                <?php if(isset($categories) && !empty($categories)):?>
                                    <?=nestableView($categories)?>
                                <?php endif;?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.dd').nestable({
        maxDepth:10,
        allowDecrease   : true,
        allowIncrease   : true,
    });


    $('.dd').on('change',function(){
        var serialized = $('.dd').nestable('serialize');

        $.ajax({
            type:'POST',
            url:'<?=controller_url('order')?>',
            data:{serialized:serialized},
            dataType:'json'
        })
    });
</script>