<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?= controller_url('createProcess') ?>" method="post" id="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input class="form-control mb-3" placeholder="<?=__('app.internal_name')?>" data-max_length="30" name="internal_name">
                            </div>
                        </div>

                        <?php foreach (applicationLangs() as $language) : ?>
                            <div class="col-md-6 language language-<?= $language ?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?></label>
                                    <input placeholder="<?=__('app.name_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?>" class="form-control mb-3" type="text" name="name[<?= $language ?>]">
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.facilities')?></label>
                                <select name="behavior_id" class="form-control">
                                    <option value=""><?=__('app.no_facilities')?></option>
                                    <?php if(isset($behaviours) && !empty($behaviours)):?>
                                        <?php foreach($behaviours as $behaviour):?>
                                            <option value="<?=$behaviour['id']?>"><?=$behaviour['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.parent')?></label>
                                <select name="parent" class="form-control">
                                    <option value="0"><?=__('app.no_parent')?></option>
                                    <?php if(isset($categories) && !empty($categories)):?>
                                        <?php foreach($categories as $item):?>
                                            <option value="<?=$item['id']?>"><?=$item['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <?php foreach (applicationLangs() as $language) : ?>
                            <div class="col-md-12 language language-<?=$language?>">
                                <div class="form-group">
                                    <label><?=__('app.description_in_language')?> <?= mb_strtolower(lang('langshort.' . $language)) ?></label>
                                    <textarea name="description[<?= $language ?>]" class="summernote" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
