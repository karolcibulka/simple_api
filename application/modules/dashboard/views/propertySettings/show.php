<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=isset($property['property_id']) ? controller_url('editProcess/'.$property['property_id']) : controller_url('createProcess')?>" method="post" enctype="multipart/form-data" id="form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?=__('app.name_p')?></label>
                                <input class="form-control" value="<?=isset($property['name']) ? $property['name'] : ''?>" placeholder="<?=__('app.name_p')?>" type="text" name="name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.email')?></label>
                                <input class="form-control" value="<?=isset($property['email']) ? $property['email'] : ''?>" placeholder="<?=__('app.email')?>" type="email" name="email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.phone')?></label>
                                <input class="form-control" value="<?=isset($property['phone']) ? $property['phone'] : ''?>" placeholder="<?=__('app.phone')?>" type="text" name="phone">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.primary_color')?></label>
                                <input class="form-control" value="<?=isset($property['web_color_1']) ? $property['web_color_1'] : ''?>" placeholder="<?=__('app.primary_color')?>" type="color" name="web_color_1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.secundary_color')?></label>
                                <input class="form-control" value="<?=isset($property['web_color_2']) ? $property['web_color_2'] : ''?>" placeholder="<?=__('app.secundary_color')?>" type="color" name="web_color_2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.default_language')?></label>
                                <select name="default_language" class="form-control">
                                    <?php foreach(applicationLangs(true) as $language):?>
                                        <option <?=isset($property['default_language']) && $property['default_language'] == $language ? 'selected' : '' ?> value="<?=$language?>"><?=lang('langshort.'.$language)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.available_languages')?></label>
                                <select name="public_languages[]" class="form-control select2" multiple>
                                    <?php foreach(applicationLangs(true) as $language):?>
                                        <option <?=isset($property['public_languages']) && in_array($language,$property['public_languages']) ? 'selected' : '' ?> value="<?=$language?>"><?=lang('langshort.'.$language)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.currency')?></label>
                                <select name="currency" class="form-control">
                                    <?php foreach(currencies() as $currency_id => $currency):?>
                                        <option <?=isset($property['default_currency']) && $property['default_currency'] == $currency_id ? 'selected' : '' ?> value="<?=$currency_id?>"><?=$currency['name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.allowed_currencies')?></label>
                                <select name="currencies[]" class="form-control select2" multiple>
                                    <?php foreach(currencies() as $currency_id => $currency):?>
                                        <option <?=isset($property['currencies']) && in_array($currency_id,$property['currencies']) ? 'selected' : '' ?> value="<?=$currency_id?>"><?=$currency['name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.orders')?></label>
                                <select name="default_order" class="form-control">
                                    <?php foreach(orderTypes() as $order_type => $order_type_item):?>
                                        <option <?=isset($property['default_order']) && $property['default_order'] == $order_type ? 'selected' : '' ?> value="<?=$order_type?>"><?=__('app.'.$order_type)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.selling')?></label>
                                <select name="active_selling" class="form-control">
                                    <option <?=isset($property['active_selling']) && $property['active_selling'] === '0' ? 'selected' : '' ?> value="0"><?=__('app.no')?></option>
                                    <option <?=isset($property['active_selling']) && $property['active_selling'] === '1' ? 'selected' : '' ?> value="1"><?=__('app.yes')?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.price_definition')?></label>
                                <select name="price_definition" class="form-control">
                                    <?php foreach(priceDefinitions() as $price_definition_name => $price_definition):?>
                                        <option <?=isset($property['price_definition']) && $property['price_definition'] == $price_definition_name ? 'selected' : '' ?> value="<?=$price_definition_name?>"><?=__('app.'.$price_definition_name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.price_definition_delimiter')?></label>
                                <select name="price_separator" class="form-control">
                                    <?php foreach(priceSeparators() as $price_separator_name => $priceSeparator):?>
                                        <option <?=isset($property['price_separator']) && $property['price_separator'] == $price_separator_name ? 'selected' : '' ?> value="<?=$price_separator_name?>"><?=__('app.'.$price_separator_name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-6">
                            <?=getImageUpload('Logo','logo',isset($property['logo']) && !empty($property['logo']) ? $property['logo'] : '')?>
                        </div>
                        <div class="col-md-6">
                            <?=getImageUpload('Favicon','favicon',isset($property['favicons']) && !empty($property['favicons']) ? $property['favicons']['228'] : '')?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?=getSelect2()?>
<script>
    $('.select2').select2();
</script>