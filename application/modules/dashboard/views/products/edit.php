<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?= controller_url('editProcess/'.$id) ?>" method="post" enctype="multipart/form-data" id="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input class="form-control " data-max_length="30" value="<?=isset($product['internal_name']) ? $product['internal_name'] : ''?>" placeholder="<?=__('app.internal_name')?>" name="internal_name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.count')?></label>
                                <select name="quantity" class="form-control">
                                    <option value=""><?=__('app.no_count')?></option>
                                    <option <?=isset($product['quantity']) && $product['quantity'] == 'weight' ? 'selected' : ''?> value="weight"><?=__('app.weight')?></option>
                                    <option <?=isset($product['quantity']) && $product['quantity'] == 'pieces' ? 'selected' : ''?> value="pieces"><?=__('app.pieces')?></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?=__('app.minimal_value')?></label>
                            <input type="number" name="min" class="form-control" min="0" step="1" value="<?=isset($product['min']) ? $product['min'] : ''?>" placeholder="<?=__('app.minimal_value')?>">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?=__('app.maximal_value')?></label>
                            <input type="number" name="max" class="form-control" min="0" step="1" value="<?=isset($product['max']) ? $product['max'] : ''?>" placeholder="<?=__('app.maximal_value')?>">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                            <label><?=__('app.step')?></label>
                            <input type="number" name="step" class="form-control" min="0" step="0.01" value="<?=isset($product['step']) ? $product['step'] : ''?>" placeholder="<?=__('app.step')?>">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?=__('app.category')?></label>

                                <select class="form-control select2" multiple name="category[]" >
                                    <?php if (isset($categories) && !empty($categories)) : ?>
                                        <?php foreach ($categories as $category) : ?>
                                            <option <?=isset($product['categories']) && in_array($category['id'],$product['categories']) ? 'selected' : ''?> value="<?= $category['id'] ?>"><?= $category['internal_name'] ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?=getSelect2()?>