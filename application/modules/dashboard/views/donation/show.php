<style>
    .item-donate.active{
        background-color:#2097f3;
        color:white;
    }

    .item-donate{
        font-weight:300;
    }
</style>
<div class="content-wrapper">
    <?=getBreadcrumb()?>
    <div class="content">
        <div class="card">
            <div class="card-header">
                <small>
                    lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim lorem upsim 
                </small>
                <legend></legend>
            </div>
            <div class="card-body">
                <div class="row">
                    <?php for($i = 10 ;$i <= 50;$i++):?>
                        <?php if($i%10 === 0 ):?>
                            <div class="col card <?=$i === 10 ? 'active' : ''?> item-donate text-center m-2 cursor-pointer p-2" data-value="<?=$i?>">
                                <h5 class="my-auto"><?=$i?> €</h5>
                            </div>
                        <?php endif;?>
                    <?php endfor;?>
                    <div class="col item-donate card text-center m-2 cursor-pointer p-2" data-value="own">
                        <h5 class="my-auto">Vlastná suma</h5>
                    </div>
                </div>

                <div class="donate-val-wrapper" style="display:none;">
                    <legend></legend>
                    <input type="number" class="form-control donate-val" step="1" min="1" max="100000" value="100">
                </div>
                <legend></legend>
                <input type="hidden" name="donate_value" value="100">
                <a href="" class="btn btn-primary w-100 donate">Prispieť sumou - <span class="price">10 €</span></a>
            </div>
        </div>
    </div>
  
</div>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>
 
<script type="text/javascript">

$('.item-donate').on('click',function(){
    $(document).find('.item-donate').removeClass('active');

    var _self = $(this);

    _self.addClass('active');

    if(_self.data('value') === 'own'){
        $('.donate-val-wrapper').show();
        $('.donate').find('.price').html($('.donate-val').val() + ' €');
    }
    else{
        $('.donate-val-wrapper').hide();
        $('[name="donate_value"]').val(_self.data('value'));
        $('.donate').find('.price').html(_self.data('value') + ' €');
    }
})

$('.donate-val').on('input',function(){

    $('[name="donate_value"]').val($(this).val());
    $('.donate').find('.price').html($(this).val() + ' €');
});
 
 $('.donate').on('click',function(e){
    e.preventDefault();
    e.stopPropagation();

    var _value = $('[name="donate_value"]').val(),
        _self = $(this);

    pay(_value);
 });

 function pay(amount) {
   var handler = StripeCheckout.configure({
     key: '<?=$stripe['pk']?>',
     locale: 'auto',
     currency:'eur',
     name:'Podpora',
     description:'Podpora pre chod projektu',
     token: function (token) {
       // You can access the token ID with `token.id`.
       // Get the token ID to your server-side code for use.
       $.ajax({
         url:"<?=controller_url('payment'); ?>",
         method: 'post',
         data: { tokenId: token.id, amount: amount*100 },
         dataType: "json",
         success: function( response ) {
           if(response.status === 1){
               window.location.reload();
           }
         }
       })
     }
   });

   handler.open({
     amount: amount * 100
   });
 }
</script>
    </div>
</div>