<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">

<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Logy</h5>
                <legend></legend>
            </div>
            <div class="card-body">
                <form id="filterForm" action="<?=base_url('dashboard/log/index/'.$start)?>" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <select name="property_id" class="form-control form-control-select2">
                            <option value="">Žiadne zariadenie</option>
                            <?php if(isset($properties) && !empty($properties)):?>
                                <?php foreach($properties as $property):?>
                                    <option <?=isset($get['property_id']) && !empty($get['property_id']) && $get['property_id'] == $property['id'] ? 'selected' : ''?> value="<?=$property['id']?>"><?=$property['name']?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="user_id" class="form-control form-control-select2">
                            <option value="">Žiadny užívateľ</option>
                            <?php if(isset($users) && !empty($users)):?>
                                <?php foreach($users as $user):?>
                                    <option <?=isset($get['user_id']) && !empty($get['user_id']) && $get['user_id'] == $user['id'] ? 'selected' : ''?> value="<?=$user['id']?>"><?=$user['email']?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                            <input type="text" autocomplete="off" value="<?=isset($get['created_at']) && !empty($get['created_at']) ? $get['created_at'] : ''?>" placeholder="Vytvorené" name="created_at" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#datetimepicker1"/>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-dark w-100 removeDate" type="button"><i class="icon-cross2"></i></button>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary w-100">Hľadať</button>
                    </div>
                </div>
                <legend></legend>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Zariadenie</th>
                            <th>Užívateľ</th>
                            <th>Controller</th>
                            <th>Metóda</th>
                            <th>Item #</th>
                            <th>Vykonané</th>
                            <th>Request</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($logs) && !empty($logs)):?>
                                <?php foreach($logs as $log):?>
                                    <tr>
                                        <td><?=$log['id']?></td>
                                        <td><?=$log['property']?></td>
                                        <td><?=$log['email']?></td>
                                        <td><?=$log['controller']?></td>
                                        <td><?=$log['method']?></td>
                                        <td><?=$log['item_id']?></td>
                                        <td><?=date('d.m.Y H:i:s',strtotime($log['created_at']))?></td>
                                        <td>
                                            <?php if(isset($log['request']) && !empty($log['request'])):?>
                                                <span class="badge badge-primary show-request cursor-pointer" data-request='<?=$log['request']?>'>Zobraziť</span>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <?=$links?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.show-request').on('click',function(){
        var _self = $(this),
            _modal = $('#exampleModal'),
            _body = _modal.find('.modal-body');

        _body.empty();
        _body.html('<pre>'+JSON.stringify(_self.data().request,null,2)+'</pre>');

        _modal.modal();
    });

    $('.removeDate').on('click',function(){
        $('[name="created_at"]').val('');
    });

    $('.form-control-select2').select2();

    $('#datetimepicker1').datetimepicker({
        format:'DD.MM.YYYY'
    });
</script>