<div class="content-wrapper">
<?php
$this->load->view('partials/breadcrumb',array('noButton'=>'1'));
?>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->session->has_userdata('changePasswordError')):?>
                            <div class="alert alert-danger">
                                <?=$this->session->userdata('changePasswordError')?>
                            </div>
                        <?php endif;?>
                        <form action="<?=base_url('dashboard/user/changePasswordPost')?>" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nové heslo</label>
                                        <input type="password" name="new_password" placeholder="Nové heslo" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nové heslo znovu</label>
                                        <input type="password" name="new_password_confirm" placeholder="Nové heslo znovu" class="form-control">
                                    </div>
                                </div>
                                <legend></legend>
                                <div class="col-md-12">
                                    <button class="btn btn-primary">
                                        Uložiť
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
