

<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=base_url('dashboard/payments/editProcess/'.$payment['id'])?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" class="form-control internalNameCounter" value="<?=$payment['internal_name']?>" name="internal_name" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-4 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <input type="text" class="form-control" value="<?=isset($payment['langs'][$lang]['name']) && !empty($payment['langs'][$lang]['name']) ? $payment['langs'][$lang]['name'] : ''?>" name="name[<?=$lang?>]" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.payment')?></label>
                                <select name="payment_type_id"  class="form-control">
                                    <?php if(isset($payment_types) && !empty($payment_types)):?>
                                        <?php foreach($payment_types as $payment_type):?>
                                            <option <?=$payment_type['id'] == $payment['payment_type_id'] ? 'selected' : ''?> value="<?=$payment_type['id']?>"><?=$payment_type['name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <legend></legend>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-6 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.perex_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <textarea type="text" class="form-control" name="short_description[<?=$lang?>]"><?=isset($payment['langs'][$lang]['short_description']) && !empty($payment['langs'][$lang]['short_description']) ? $payment['langs'][$lang]['short_description'] : ''?></textarea>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.image')?></label>
                                <input type="file" name="image" class="form-control">
                            </div>
                            <legend></legend>
                            <?php if(isset($payment['image']) && !empty($payment['image'])):?>
                            <button class="btn btn-danger w-100 deleteImg"><?=__('app.delete')?></button>
                            <div class="imgPreview" style="width: 100%;height:300px;background:url('<?=$payment['image']?>');background-position: center;background-repeat: no-repeat;background-size: contain;"></div>
                            <input type="hidden" name="hasImg" value="1">
                            <input type="hidden" name="imgPath" value="<?=$payment['image_path']?>">
                            <?php endif;?>
                            <input type="hidden" name="deleteImg" value="0">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
   
    $('.deleteImg').on('click',function(){
        $(this).remove();
        $('.imgPreview').remove();
        $('[name="deleteImg"]').val('1');
    });
</script>