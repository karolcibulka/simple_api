<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <div class="card">
            <div class="card-body">
                <form action="<?=base_url('dashboard/payments/createProcess')?>" id="form" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" class="form-control internalNameCounter" value="" name="internal_name" placeholder="<?=__('app.internal_name')?>">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-4 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <input type="text" class="form-control" value="" name="name[<?=$lang?>]" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?=__('app.payment')?></label>
                                <select name="payment_type_id"  class="form-control">
                                    <?php if(isset($payment_types) && !empty($payment_types)):?>
                                        <?php foreach($payment_types as $payment_type):?>
                                            <option value="<?=$payment_type['id']?>"><?=$payment_type['name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>

                        <legend></legend>
                        <?php foreach(applicationLangs() as $lang):?>
                            <div class="col-md-6 language language-<?=$lang?>">
                                <div class="form-group">
                                    <label><?=__('app.perex_in_language')?> <?=mb_strtolower(lang('langshort.'.$lang))?></label>
                                    <textarea type="text" class="form-control" name="short_description[<?=$lang?>]"></textarea>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?=__('app.image')?></label>
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>