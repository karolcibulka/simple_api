<style>
    .switch{
        transform: scale(.5);
    }

    .sliderText{
        display: inline-block;
        float: left;
        margin-left: 73px;
        margin-top: -28px;
        font-size: 18px;
    }
</style>
<div class="content-wrapper">
    <?php $this->load->view('partials/breadcrumb',array('noButton'=>'1'))?>
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5><?=__('app.create_user_property')?></h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?php if(has_permission('create')):?>
                    <form action="<?=base_url('dashboard/customAuth/createProccess')?>" method="post">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?=__('app.email')?></label>
                                    <input type="text" class="form-control" name="email" placeholder="<?=__('app.email')?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?=__('app.name')?></label>
                                    <input type="text" class="form-control" name="first_name" placeholder="<?=__('app.name')?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?=__('app.surname')?></label>
                                    <input type="text" class="form-control" name="last_name" placeholder="<?=__('app.surname')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=__('app.password')?></label>
                                    <input type="password" class="form-control" name="password" placeholder="<?=__('app.password')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?=__('app.password_confirmation')?></label>
                                    <input type="password" class="form-control" name="password_confirm" placeholder="<?=__('app.password_confirmation')?>">
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <strong><?=__('app.role')?></strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if(isset($roles) && !empty($roles)):?>
                                        <?php foreach($roles as $role):?>
                                            <div class="col-md-2">
                                                <label>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" value="<?=$role['id']?>" checked name="role[]">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <?=$role['description']?>
                                                </label>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <strong><?=__('app.property')?></strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if(isset($properties) && !empty($properties)):?>
                                        <?php foreach($properties as $property):?>
                                            <div class="col-md-2">
                                                <label>
                                                    <label class="switch float-left">
                                                        <input type="checkbox" value="<?=$property['id']?>" checked name="property[]">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <?=$property['name']?>
                                                </label>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <legend></legend>
                            <button class="btn btn-success" style="width:100%;"><?=__('app.save_new_user')?></button>
                        </div>
                    </form>
                <?php endif;?>
                <legend></legend>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5><?=__('app.users')?></h5>
                    </div>
                </div>
                <legend></legend>
                <select class="form-control mb-3" id="change-property">
                    <?php if(isset($properties) && !empty($properties)):?>
                        <?php foreach($properties as $property):?>
                            <option value="<?=$property['id']?>" <?=$property['id'] === getActiveProperty() ? 'selected' : ''?> ><?=$property['name']?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>

                <div class="tab-content">
                    <?php if(isset($properties) && !empty($properties)):?>
                        <?php foreach($properties as $property):?>
                            <div class="col-md-12 solid-justified-tab" id="solid-justified-tab-<?=$property['id']?>" style="<?=$property['id'] === getActiveProperty() ? '' : 'display:none;' ?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th><?=__('app.email')?></th>
                                            <th><?=__('app.name')?></th>
                                            <th><?=__('app.surname')?></th>
                                            <th><?=__('app.role')?></th>
                                            <th><?=__('app.action')?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($users[$property['id']]['users']) && !empty($users[$property['id']]['users'])):?>
                                            <?php foreach($users[$property['id']]['users'] as $customer_id => $user):?>
                                                <?php if(isset($user['id']) && !empty($user['id'])):?>
                                                    <?php $showEdit = true;?>
                                                    <?php if(isset($user['group_order']) && !empty($user['group_order'])):?>
                                                        <?php $j=0;$count = count($user['group_order'])-1;foreach($user['group_order'] as $group_id):?>
                                                            <?php
                                                            if($j == $count){
                                                                if($group_id <= $max_role){
                                                                    $showEdit = true;
                                                                }
                                                                else{
                                                                    $showEdit = false;
                                                                }
                                                            }
                                                            ?>
                                                            <?php $j++;endforeach;?>
                                                    <?php else:?>
                                                        <?php $showEdit = true;?>
                                                    <?php endif;?>
                                                    <?php if($showEdit):?>
                                                        <tr>
                                                            <td><?=$user['email']?></td>
                                                            <td><?=$user['first_name']?></td>
                                                            <td><?=$user['last_name']?></td>
                                                            <td><?=implode(', ',$user['group_names'])?></td>
                                                            <td>
                                                                <?php if($showEdit && has_permission('edit')):?>
                                                                    <a href="<?=base_url('dashboard/customAuth/edit/'.$user['id'])?>"><span class="badge badge-success"><?=__('app.edit')?></span></a>
                                                                <?php else:?>
                                                                    <small><?=__('app.no_permission_edit_user')?></small>
                                                                <?php endif;?>
                                                            </td>
                                                        </tr>
                                                    <?php endif;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $('#change-property').on('change',function(){
        $('.solid-justified-tab').hide();
        $('#solid-justified-tab-'+$(this).val()).show();
    });
</script>
