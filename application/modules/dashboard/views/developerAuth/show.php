<?=getDataTable()?>
<?=getSelect2()?>

<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <h5>Administrácia</h5>
                    </div>
                    <div class="col-md-3">
                        <a href="<?=controller_url('create')?>" class="btn btn-primary w-100">Vytvoriť užívateľa</a>
                    </div>
                    <legend></legend>
                </div>

            </div>
            <div class="card-header">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="data-table">
                        <thead>
                            <tr>
                                <th style="min-width: 50px;">#</th>
                                <th>Email</th>
                                <th class="no-filter">Právomoci</th>
                                <th class="no-filter">Zariadenia</th>
                                <th class="no-filter">Vývojár</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($users) && !empty($users)):?>
                                <?php foreach($users as $user):?>
                                    <tr>
                                        <td><?=$user['id']?></td>
                                        <td><?=$user['email']?></td>
                                        <td>
                                            <select data-user_id="<?=$user['id']?>" data-type="groups" class="select2 form-control" multiple>
                                                <?php if(isset($groups) && !empty($groups)):?>
                                                    <?php foreach($groups as $group):?>
                                                        <option value="<?=$group['id']?>" <?=isset($user['groups']) && !empty($user['groups']) && in_array($group['id'],$user['groups']) ? 'selected' : ''?> ><?=$group['description']?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </td>
                                        <td>
                                            <select data-user_id="<?=$user['id']?>" data-type="properties" class="select2 form-control" multiple>
                                                <?php if(isset($properties) && !empty($properties)):?>
                                                    <?php foreach($properties as $property):?>
                                                        <option value="<?=$property['id']?>" <?=isset($user['properties']) && !empty($user['properties']) && in_array($property['id'],$user['properties']) ? 'selected' : ''?> ><?=$property['name']?></option>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php if(isset($user['is_developer']) && !empty($user['is_developer'])):?>
                                                <a href="<?=controller_url('developer/'.$user['id'].'/0')?>" class="badge badge-success">Áno</a>
                                            <?php else:?>
                                                <a href="<?=controller_url('developer/'.$user['id'].'/1')?>" class="badge badge-danger">Nie</a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var _datatable = $('#data-table'),
        _select2 = $('.select2');

    _select2.select2();

    _select2.on('change',function(){
        var _self = $(this),
            _val = _self.val(),
            _type = _self.data().type,
            _user_id = _self.data().user_id

        $.ajax({
            url:'<?=controller_url('changeProps')?>',
            type:'post',
            data:{val:_val,type:_type,user_id:_user_id},
            dataType:'json',
            success:function(data){
                if(data.status === 1){
                    swalSuccessMessageSession('Úspešne zmenené atribúty');
                }
            }
        })
    })

    $(document).ready(function(){
        $('#data-table thead tr').clone(true).appendTo( '#data-table thead' );
        $('#data-table thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if($(this).hasClass('no-filter')){
                $(this).html('');
            }
            else{
                $(this).html( '<input type="text" class="form-control form-control-sm" placeholder="'+title+'" />' );
            }

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        var table = $('#data-table').DataTable({
            dom: 'Bfltip',
            pageLength: 100,
            buttons: [
                { extend: 'excel', className: 'btn' },
                { extend: 'pdf', className: 'btn' },
                { extend: 'print', className: 'btn' },
            ],
            lengthMenu: [[10, 25, 50,100, -1], [10, 25, 50,100, "Všetky"]],
            order: [[ 0, 'asc' ]],
            bSortCellsTop:true,
        });

    });
</script>