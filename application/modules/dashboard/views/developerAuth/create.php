<?=getSelect2()?>
<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Vytvorenie užívateľa</h5>
                <legend></legend>
            </div>
            <div class="card-body">
                <?php if(isset($errors) && !empty($errors)):?>
                    <div class="col-md-12 alert alert-danger">
                        <ul>
                            <?php foreach($errors as $error):?>
                                <li><?=$error?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                <?php endif;?>
                <form action="<?=controller_url('create')?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Meno</label>
                                <input type="text" placeholder="Meno" value="<?=isset($post['first_name']) ? $post['first_name'] : ''?>" name="first_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Priezvisko</label>
                                <input type="text" placeholder="Priezvisko" value="<?=isset($post['last_name']) ? $post['last_name'] : ''?>" name="last_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" placeholder="Email" value="<?=isset($post['email']) ? $post['email'] : ''?>" name="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Heslo</label>
                                <input type="password" placeholder="Heslo" name="password" class="form-control">
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Zariadenia</label>
                                <select name="properties[]" class="form-control select2" multiple>
                                    <?php if(isset($properties) && !empty($properties)):?>
                                        <?php foreach($properties as $property):?>
                                            <option <?=isset($post['properties']) && in_array($property['id'],$post['properties']) ? 'selected' : ''?> value="<?=$property['id']?>"><?=$property['name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Skupiny</label>
                                <select name="groups[]" class="form-control select2" multiple>
                                    <?php if(isset($groups) && !empty($groups)):?>
                                        <?php foreach($groups as $group):?>
                                            <option <?=isset($post['groups']) && in_array($group['id'],$post['groups']) ? 'selected' : ''?> value="<?=$group['id']?>"><?=$group['description']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>
                            <input type="hidden" value="0" name="create_property">
                            <input type="checkbox" <?=isset($post['create_property']) && !empty($post['create_property']) ? 'checked' : ''?> value="1" name="create_property">
                                Vytvoriť zariadenie?<br>
                                <small>Po vytvorení budeš presmerovaný na vyplnenie údajov o zariadení</small>
                            </label>
                        </div>
                        <legend></legend>
                        <button class="btn btn-primary w-100">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var _select2 = $('.select2');
    _select2.select2();
</script>