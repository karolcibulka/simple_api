<style>
    .mw-100{
        min-width:100%;
    }

    .mh-38{
        min-height:38px !important;
    }
</style>

<div class="content-wrapper">
    <?=getBreadcrumb(array(),array('form'=>__('app.save')),true)?>
    <div class="content">
        <form action="<?=controller_url('editProcess/'.$id)?>" id="form" method="post">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?=__('app.internal_name')?></label>
                                <input type="text" class="form-control" value="<?=$variability['internal_name']?>" placeholder="<?=__('app.internal_name')?>" name="internal_name" data-max_length="30">
                            </div>
                        </div>
                        <?php foreach(applicationLangs() as $language):?>
                            <div class="col-md-12 language language-<?=$language?>">
                                <div class="form-group">
                                    <label><?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?></label>
                                    <input type="text" name="name[<?=$language?>]" value="<?=isset($variability['languages'][$language]) ? $variability['languages'][$language] : ''?>" class="form-control" placeholder="<?=__('app.name_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body ">
                    <div class="dd m-0 p-0">
                        <ol class="dd-list items m-0">
                            <?php if(isset($variability['items']) && !empty($variability['items'])):?>
                                <?php foreach($variability['items'] as $item_id => $item):?>
                                    <li class="dd-item item mh-38 mb-2">
                                        <div class="dd-handle row mw-100">
                                            <div class="col-sm-1 text-center mt-1">
                                                <i class="icon-menu3"></i>
                                            </div>
                                            <div class="col-sm-11 dd-nodrag">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input type="text" name="value[<?=$item_id?>]" value="<?=isset($item['value']) ? $item['value'] : ''?>" placeholder="<?=__('app.value')?>" class="form-control clone-value">
                                                    </div>
                                                    <input type="hidden" name="added[<?=$item_id?>]" value="<?=$item_id?>" class="clone-added">
                                                    <?php foreach(applicationLangs() as $language):?>
                                                        <div class="col-sm-4 language language-<?=$language?>">
                                                            <input type="text" name="language[<?=$item_id?>][<?=$language?>]" value="<?=isset($item['languages'][$language]) ? $item['languages'][$language] : ''?>" placeholder="<?=__('app.value_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?> " class="form-control clone-name" data-language="<?=$language?>">
                                                        </div>
                                                    <?php endforeach;?>
                                                    <div class="col-sm-2">
                                                        <button type="button" class="btn btn-success clone-item w-100"><?=__('app.new')?></button>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <button type="button" class="btn btn-danger delete-item w-100"><?=__('app.delete')?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            <?php else:?>
                                <li class="dd-item item mh-38 mb-2">
                                    <div class="dd-handle row mw-100">
                                        <div class="col-sm-1 mt-1 text-center">
                                            <i class="icon-menu3"></i>
                                        </div>
                                        <div class="col-sm-11 dd-nodrag">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <input type="text" name="value[0]" placeholder="<?=__('app.value')?>" class="form-control clone-value">
                                                </div>
                                                <?php foreach(applicationLangs() as $language):?>
                                                    <div class="col-sm-4 language language-<?=$language?>">
                                                        <input type="text" name="language[0][<?=$language?>]" placeholder="<?=__('app.value_in_language')?> <?=mb_strtolower(lang('langshort.'.$language))?> " class="form-control clone-name" data-language="<?=$language?>">
                                                    </div>
                                                <?php endforeach;?>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-success clone-item w-100"><?=__('app.new')?></button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-danger delete-item w-100"><?=__('app.delete')?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?=getNestable()?>

<script>

    $('.dd').nestable({
        maxDepth:1,
        allowIncrease:false,
        allowDecrease:false
    });

    $(document).on('click','.clone-item',function(){
        var _self = $(this),
            _clone = _self.closest('.item').clone(),
            _index = getIndex(1),
            _names = _clone.find('.clone-name');

        _clone.find('input').val('');
        _clone.find('.clone-added').remove();
        _clone.find('.clone-value').removeAttr('name');
        _clone.find('.clone-value').attr('name','value['+_index+']');

        _names.each(function(){
            $(this).removeAttr('name');
            $(this).attr('name','language['+_index+']['+$(this).data('language')+']');
        });

        $(document).find('.items').append(_clone);
    });

    $(document).on('click','.delete-item',function(){
        var _self = $(this),
            _items = $(document).find('.item');

        if(_items.length > 1){
            _self.closest('.item').remove();
        }
        else{
            _self.closest('.item').find('input').val('');
        }
    });

    var getIndex = function(_index){
        if( $(document).find('[name="value['+_index+']"]').length > 0 ){
            return getIndex((_index+1));
        }
        else{
            return _index;
        }
    }
</script>