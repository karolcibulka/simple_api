<div class="content">
    <div class="card">
        <div class="card-body">
            <?php if(isset($navs) && !empty($navs)):?>
                <div class="box-content nopadding" style="padding: 0;">
                    <div class="servicesView" style="background-color:aliceblue;border:1px solid #dddddd">
                        <div class="category col-md-12">
                            <div class="name dd-handle col-md-10" style="padding: 14px 10px;font-size: 14px;">
                                Navigácia
                            </div>
                            <div class="dd" style="padding-right: 0;padding-left: 10px;">
                                <ol class="dd-list">
                                    <?php foreach($navs as $nav):?>
                                        <?php if($nav['deleted'] == 0):?>
                                            <?php if(!isset($nav['children'])):?>
                                                <li class="service dd-item col-md-12"  data-id="<?=$nav['id']?>" data-parent="<?=$nav['parent']?>" data-type="navigation" style="border-left: 5px solid #55b4ff">
                                                    <div class="name dd-handle col-md-8"><i class="icon-<?=$nav['icon']?>"></i><?=$nav['name']?> <small>( ID Navigácie = <?=$nav['id']?>)</small></div>
                                                    <div class="actions col-md-4">
                                                        <a href="<?=controller_url('editCMSOption/'.$nav['id'])?>" class="badge badge-primary">Upraviť</a>
                                                        <a href="<?=controller_url('deleteCMSOption/'.$nav['id'])?>" class="delete badge badge-danger">Zmazať</a>
                                                    </div>
                                                </li>
                                            <?php elseif(isset($nav['children']) && !empty($nav['children'])) :?>
                                                <li class="service dd-item col-md-12" data-parent="<?=$nav['parent']?>" data-id="<?=$nav['id']?>" style="border-left: 5px solid #55b4ff">
                                                    <div class="name dd-handle col-md-8"><i class="icon-<?=$nav['icon']?>"></i><?=$nav['name']?> <small>( ID Navigácie = <?=$nav['id']?>)</small></div>
                                                    <div class="actions col-md-4">
                                                        <a href="<?=controller_url('editCMSOption/'.$nav['id'])?>" class="badge badge-primary">Upraviť</a>
                                                        <a href="<?=controller_url('deleteCMSOption/'.$nav['id'])?>" class="delete badge badge-danger">Zmazať</a>
                                                    </div>
                                                    <ol class="dd-list">
                                                        <?php foreach ($nav['children'] as $child) :?>
                                                            <?php if ($child['deleted'] == 0) :?>
                                                                <li class="service dd-item col-md-12"  data-id="<?=$child['id']?>" data-parent="<?=$nav['id']?>" data-type="navigation" style="border-left: 5px solid #55b4ff">
                                                                    <div class="name dd-handle col-md-8"><i class="icon-<?=$child['icon']?>"></i><?=$child['name']?> <small>( ID Navigácie = <?=$child['id']?>)</small></div>
                                                                    <div class="actions col-md-4">
                                                                        <a href="<?=controller_url('editCMSOption/'.$child['id'])?>" class="badge badge-primary">Upraviť</a>
                                                                        <a href="<?=controller_url('deleteCMSOption/'.$child['id'])?>" class="delete badge badge-danger">Zmazať</a>
                                                                    </div>
                                                                </li>
                                                            <?php endif;?>
                                                        <?php endforeach;?>
                                                    </ol>
                                                </li>

                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>


<script>
    $('.dd').nestable({
        maxDepth:2,
    });

    $('.dd').on('change',function (e) {
        var json = $('.dd').nestable('serialize');

        $.ajax({
            type: 'POST',
            url: '<?=base_url('dashboard/navigation/handleChangeMenu')?>',
            data: { json:json }
        })
    });

    $('.delete').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        swalAlertDelete($(this).attr('href'));
    })
</script>

