<div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-secondary-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold"></span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Form sample -->
        <form id="handleAddForm" method="post">
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('menu.addMenuItem')?></span>
                </div>

                <div class="card-body">

                    <div class="form-group">
                        <label><?=lang('menu.navigationName')?>:</label>
                        <input type="text" name="name" class="form-control" placeholder="<?=lang('menu.navigationName')?>" required>
                    </div>



                    <div class="form-group">
                        <label><?=lang('menu.type')?>:</label>
                        <select name="type" class="form-control" id="itemType" required>
                            <option value=""><?=lang('menu.chooseType')?></option>
                            <?php if(isset($itemTypes) && !empty($itemTypes)):?>
                                <?php foreach ($itemTypes as $itemType):?>
                                    <option value="<?=$itemType['id']?>"><?=$itemType['name']?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>

                    <div class="form-group" id="controllerT" style="display:none">
                        <label>Controllers:</label>
                        <select name="controller" class="form-control" id="itemController" required>
                            <option value="0">Zvoľ controller</option>
                            <?php if(isset($controllers) && !empty($controllers)):?>
                                <?php foreach ($controllers as $controller):?>
                                    <option value="<?=$controller['id']?>"><?=$controller['name']?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>

                    <div class="form-group" id="pathLink" style="display:none">
                        <label><?=lang('menu.path')?>:</label>
                        <input id="pathInput" type="text" name="path" class="form-control" placeholder="<?=lang('menu.chooseType')?>">
                    </div>

                    <div class="form-group" id="chooseParent" style="display:none">
                        <label><?=lang('menu.parent')?>:</label>
                        <select name="parent" class="form-control" id="choosedParent">
                            <option value="0"><?=lang('menu.iAmParent')?></option>
                            <?php if(isset($parents) && !empty($parents)):?>
                                <?php foreach ($parents as $parent):?>
                                    <?php if($parent['type'] == 'placeholder' && $parent['deleted'] == 0):?>
                                        <option value="<?=$parent['id']?>"><?=$parent['name']?></option>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('menu.icon')?></span>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 iconsWrap" >
                                <?php if(isset($icons) && !empty($icons)):?>
                                    <?php foreach ($icons as $key => $icon):?>
                                        <label class="form-check-label">
                                            <div class="col-md-4">
                                                <div class="uniform-choice">
                                                        <span class="checked">
                                                            <input type="radio" value="<?=$icon['id']?>" class="form-input-styled" name="icon">
                                                        </span>
                                                </div>
                                                <i style="margin:-40px 0 0 25px;" class="icon-<?=$icon['name']?>"></i>
                                            </div>
                                        </label>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn bg-teal btn-block" data-popup="tooltip" title="" data-original-title="<?=lang('menu.submit')?>"><?=lang('menu.submit')?></button>
                    </div>
                </div>
            </div>
            <!-- /form sample -->
        </form>

    </div>
    <!-- /sidebar content -->

</div>

<script>
    $('#chooseParent').hide(250);
    $("#pathInput").prop('disabled', true);


    $("#controllerT").hide(250);
    $("#controllerT").closest('select').val('0');

    $('select#itemType').on('change', function() {
        if (this.value === '3'){
            $('#chooseParent').show(250);
            $('select#choosedParent').closest('select').val('0');
            $("#controllerT").show(250);
            $('#controllerT').find('select').val('0');

            $("#pathLink").hide(250);
            //$("#pathInput").prop('disabled', false);
            //$("#pathInput").attr("placeholder", "<?=lang('menu.pathPlaceholder')?>");

        }
        else if(this.value === '2'){
            $('select#choosedParent').closest('select').val('0');
            $('#chooseParent').hide(250);

            $("#controllerT").hide(250);
            $("#controllerT").closest('select').val('0');

            $("#pathLink").hide(250);

            $('#pathInput').closest('select').val('#');
            $("#pathInput").prop('disabled', true);
            $("#pathInput").attr("placeholder", "#");
        }
        else if (this.value === '1'){
            $("#controllerT").hide(250);
            $("#controllerT").find('select').val('0');

            $("#pathLink").show(250);
            //$("#controllerT").find('select').val('0');
            $('select#choosedParent').closest('select').val('0');
            $('#chooseParent').show(250);
            $("#pathInput").prop('disabled', false);
            $("#pathInput").attr("placeholder", "<?=lang('menu.pathLinkPlaceholder')?>");
        }
        else{
            $("#controllerT").hide(250);
            $("#controllerT").closest('select').val('0');

            $("#pathLink").hide(250);

            $('select#choosedParent').closest('select').val('0');
            $('#chooseParent').hide(250);
            $("#pathInput").prop('disabled', true);
            $("#pathInput").attr("placeholder", "<?=lang('menu.chooseType')?>");
        }
    });
</script>