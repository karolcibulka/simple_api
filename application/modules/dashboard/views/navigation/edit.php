<div class="content-wrapper">
    <div class="content">



    <div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="header-elements-inline">
                    <h5 class="card-title"></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="card-body">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <form action="<?=base_url('dashboard/navigation/editMenuOption')?>" method="post">
                    <div class="form-group">
                        <label><?=lang('menu.navigationName')?>:</label>
                        <input type="text" name="name" value="<?=$choosedItem['name'] ?>" class="form-control" placeholder="<?=lang('menu.navigationName')?>" required>
                    </div>

                    <div class="form-group">
                        <label><?=lang('menu.path')?>:</label>
                        <input type="text" name="path" value="<?=(isset($choosedItem['path'])) ? $choosedItem['path'] : '#' ?>" <?=($choosedItem['placeholder'] == TRUE) ? 'disabled' : '' ?> class="form-control" placeholder="<?=lang('menu.pathPlaceholder')?>">
                    </div>

                    <div class="form-group">
                        <label>Type:</label>
                        <select name="type" class="form-control" id="itemType" required>
                            <option value=""></option>
                            <?php if(isset($itemTypes) && !empty($itemTypes)):?>
                                <?php foreach ($itemTypes as $itemType):?>
                                    <option <?=($choosedItem['type'] == $itemType['id']) ? 'selected' : ''?> value="<?=$itemType['id']?>"><?=$itemType['name']?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label><?=lang('menu.activeHeader')?>:</label>
                        <select name="active" class="form-control"  required>
                            <option <?=($choosedItem['active'] == 1) ? 'selected' : ''?> value="1"><?=lang('menu.active')?></option>
                            <option <?=($choosedItem['active'] == 0) ? 'selected' : ''?> value="0"><?=lang('menu.nonactive')?></option>
                        </select>
                    </div>

                    <div class="form-group" id="chooseParent" style="display:none">
                        <label><?=lang('menu.parent')?>:</label>
                        <select name="parent" class="form-control" id="choosedParent">
                            <option value="0">I'm parent!</option>
                            <?php if(isset($parents) && !empty($parents)):?>
                                <?php foreach ($parents as $parent):?>
                                    <?php if($parent['type'] == 'placeholder'):?>
                                        <option value="<?=$parent['id']?>"><?=$parent['name']?></option>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label class="d-block"><?=lang('menu.icon')?>:</label>
                        <div class="form-check form-check-inline">
                                <div class="row col-md-12 col-sm-12 col-xs-12 col-lg-12" style="height:200px;overflow-y: scroll;">
                                    <?php if(isset($icons) && !empty($icons)):?>
                                        <?php foreach ($icons as $key => $icon):?>
                                            <label class="form-check-label col-md-1 col-sm-2 col-xs-3 col-lg-1">
                                               <div class="uniform-choice">
                                                  <span class="checked">
                                                      <input type="radio" <?=($choosedItem['icon'] == $icon['id']) ? 'checked' : '' ?> value="<?=$icon['id']?>" class="form-input-styled" name="icon">
                                                  </span>
                                               </div>
                                               <i style="margin: 0 35px 0 12px;" class="icon-<?=$icon['name']?>"></i>
                                            </label>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?=$choosedItem['id']?>">
                    <div class="text-right">
                        <button type="submit" class="btn bg-teal btn-block"><?=lang('menu.submit')?><i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    </div>

<script>
    $('#chooseParent').hide();

    $('select#itemType').on('change', function() {
        if (this.value === 'link'){
            $('#chooseParent').show();
        }
        else{
            $('select#choosedParent').closest('select').val('0');
            $('#chooseParent').hide();
        }
    });
</script>