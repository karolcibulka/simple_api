<link href="<?=asset_url('css/MyApp.css')?>" rel="stylesheet" type="text/css">

<style>
    .iconsWrap{
        height: 200px;
        overflow-y: scroll;
    }
    .dd{
        min-width:100% !important;
    }
</style>

<?php
    if(has_permission('create')){
        $this->load->view('navigation/partials/sidebar');
    }
?>




<div class="content-wrapper">

    <div id="dynamicNav">
        <?php
            $this->load->view('navigation/partials/menu',array('navs' => $navs));
        ?>
    </div>
</div>

<script>
    $('.dd').nestable({
        maxDepth:2,
    });

    $(".dd").on('change',function (e) {
        var json = $('.dd').nestable('serialize');
        var ids = $(json).map(function () {
            return this.id;
        }).get();

        $.ajax({
            type: 'POST',
            url: '<?=base_url('dashboard/navigation/handleChangeMenu')?>',
            data: { json:json }
        })
        //.done(function (result) {
        //    alert(result.success);
        //});
    });
</script>

<script>
    $("#handleAddForm").submit(function(e) {


        var form = $(this);
        var url = '<?=base_url('dashboard/navigation/handleAddNewMenuOption')?>';

        $.ajax({
            type: "POST",
            url: url,
            dataType : 'json',
            data: form.serializeArray(), // serializes the form's elements. serialize() || serializeArray() ->do pola
            success : function(data){
                    //var obj = jQuery.parseJSON(data);
                   if(data.status === '1'){
                        $('#dynamicNav').html(data.view);
                   }
            }
        });



        e.preventDefault();
    });
</script>
