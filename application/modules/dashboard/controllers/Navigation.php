<?php

class Navigation extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index(){
        if(has_permission('show')){
            $data['title'] = 'WMS - CMS Menu';
            $data['navs'] = $this->navigation_model->getNavItems();
            $data['itemTypes'] = $this->navigation_model->getNavTypes();
            $data['parents'] = $this->navigation_model->getNavItems();

            $data['icons'] = $this->navigation_model->getIcons();

            $data['controllers'] = $this->navigation_model->getControllers();
            $this->template->load('master','navigation/show',$data);
        }
        else{
            $this->wrongState();
        }
    }



    public function handleAddNewMenuOption(){
        if(has_permission('create')){
            $post = $this->input->post();

            if(isset($post['type']) && !empty($post['type'])){
                if($post['type'] == '1'){         //LINK
                    $data=array(
                        'name' => $post['name'],
                        'controller' => '',
                        'icon' => $post['icon'],
                        'type' => $post['type'],
                        'parent' => $post['parent'],
                        'active' => '1',
                        'deleted' => '0',
                        'link' => '1',
                        'link_path'=> $post['path'],
                        'order' => 99999,
                    );
                    $this->navigation_model->insertNewMenuOption($data);
                }
                else if($post['type'] == '2'){       //Placeholder
                    $data=array(
                        'name' => $post['name'],
                        'controller' => '',
                        'icon' => $post['icon'],
                        'type' => $post['type'],
                        'parent' => $post['parent'],
                        'active' => '1',
                        'deleted' => '0',
                        'placeholder' => '1',
                        'order' => 9999,
                    );
                    $this->navigation_model->insertNewMenuOption($data);
                }
                else if($post['type'] == '3'){       //Controller
                    $data=array(
                        'name' => $post['name'],
                        'controller' => $post['controller'],
                        'icon' => $post['icon'],
                        'type' => $post['type'],
                        'parent' => $post['parent'],
                        'active' => '1',
                        'deleted' => '0',
                        'order' => 99999,
                    );
                    $this->navigation_model->insertNewMenuOption($data);
                }
            }



            $data['navs'] = $this->navigation_model->getNavItems();

            $this->deleteCacheItem('navigation');

            if ($data) {
                $pageView =  $this->load->view('dashboard/navigation/partials/menu',$data,true);

                $response = array(
                    'status' => '1',
                    'view' => $pageView,
                );

            } else {
                $response = array(
                    'status' => 0,
                );
            }
            echo json_encode($response);
        }
        else{
            $this->wrongState();
        }
    }

    public function editCMSOption($id){
        if(has_permission('edit')) {

            $data['choosedItem'] = $this->navigation_model->getCMSitemByID($id);

            $data['title'] = 'WMS - CMS ADD';
            $data['itemTypes'] = $this->navigation_model->getNavTypes();
            $data['parents'] = $this->navigation_model->getNavItems();

            $data['icons'] = $this->navigation_model->getIcons();
            $data['controllers'] = $this->navigation_model->getControllers();
            $this->template->load('master', 'navigation/edit', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editMenuOption(){
        $post = $this->input->post();

        $data=array(
            'id' =>$post['id'],
            'name' => $post['name'],
            'controller' => (isset($post['path'])) ? $post['path'] : '',
            'icon' => $post['icon'],
            'type' => $post['type'],
            'parent' => $post['parent'],
            'active' => $post['active'],
        );


        $this->navigation_model->editMenuOption($data);
        $this->deleteCacheItem('navigation');
        redirect('dashboard/navigation');
    }

    public function deleteCMSOption($id){
        if(has_permission('delete')){
            $navs = $this->navigation_model->getAllChilds($id);

            if(isset($navs) && !empty($navs)){
                foreach($navs as $child){
                    $data=array(
                        'parent' => '0',
                    );
                    $this->navigation_model->deleteMenuOption($data,$child['id']);
                }

                $data=array(
                    'deleted' => '1',
                );

                $this->navigation_model->deleteMenuOption($data,$id);

                $this->deleteCacheItem('navigation');
                redirect('dashboard/navigation');
            }
            else{
                $data=array(
                    'deleted' => '1',
                );

                $this->navigation_model->deleteMenuOption($data,$id);

                $this->deleteCacheItem('navigation');
                redirect('dashboard/navigation');
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function changeCMSOrder(){
        if(has_permission('edit')){
            $data['title'] = 'WMS - CMS Menu';
            $data['navs'] = $this->navigation_model->getNavItems();
            $this->template->load('master','navigation/changeCMSOrder',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function handleChangeMenu(){

        $post = $this->input->post();
        if(has_permission('edit')){
            if(isset($post['json']) && !empty($post['json'])){
                $response = $post['json'];
            }

            $i = 1;
            foreach ($response as $key => $item){
                $id = $item['id'];
                $data= array(
                    'order' =>$i,
                    'parent' => '0',
                );
                $this->navigation_model->updateMenuOrder($data,$id);

                if(isset($item['children']) && !empty($item['children'])){
                    foreach($item['children'] as $child){

                        $child_id = $child['id'];
                        $child_data= array(
                            'order' => $i,
                            'parent' => $item['id'],
                        );
                        $this->navigation_model->updateMenuOrder($child_data,$child_id);
                        $i++;
                    }
                }

                $i++;
            }
            $this->deleteCacheItem('navigation');
        }
        else{
            $this->wrongState();
        }
    }
}
