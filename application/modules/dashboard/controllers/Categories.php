<?php


class Categories extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categories_model','categories_model');
    }

    public function index(){
        if(has_permission('show')){
            $categories = $this->categories_model->getCategories($this->property_id);
            $data['categories'] = array();

            if(!empty($categories)){
                $data['categories'] = buildTree($categories,0,'parent');
            }

            _view('show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){

            $data['categories'] = $this->categories_model->getCategories($this->property_id);

            _view('create',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            if($post = $this->input->post()){

                $insert_data = array(
                    'property_id' => $this->property_id,
                    'internal_name' => $post['internal_name'],
                    'parent' => $post['parent'],
                    'behavior_id' => isset($post['behavior_id']) && !empty($post['behavior_id']) ? $post['behavior_id'] : null
                );

                if($id = $this->categories_model->insertCategory($insert_data)){

                    $languages = array();

                    if(isset($post['name']) && !empty($post['name'])){
                        foreach($post['name'] as $lang => $name){
                            if(!empty($name)){
                                $languages[] = array(
                                    'property_id' => $this->property_id,
                                    'lang' => $lang,
                                    'category_id' => $id,
                                    'name' => $name,
                                    'slug' => slugify($name),
                                    'description' => $post['description'][$lang]
                                );
                            }
                        }
                    }

                    if(!empty($languages)){
                        $this->categories_model->insertLanguages($languages);
                    }

                    $this->categories_model->updateCategory($this->property_id,$id,array('order'=>$id));
                }

                set_message('successMessage','Záznam bol úspešne vytvorený!');
                _return();
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){
            $data['id'] = $id;
            $data['categories'] = $this->categories_model->getCategories($this->property_id,$id);

            if(!$data['category'] = $this->categories_model->getCategory($this->property_id,$id)){
                $this->wrongState();
            }

            _view('edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        if(has_permission('edit')){
            if($post = $this->input->post()){

                $update_data = array(
                    'internal_name' => $post['internal_name'],
                    'parent' => $post['parent'],
                    'behavior_id' => isset($post['behavior_id']) && !empty($post['behavior_id']) ? $post['behavior_id'] : null
                );

                $this->categories_model->updateCategory($this->property_id,$id,$update_data);
                $this->categories_model->removeLanguages($this->property_id,$id);

                $languages = array();

                if(isset($post['name']) && !empty($post['name'])){
                    foreach($post['name'] as $lang => $name){
                        if(!empty($name)){
                            $languages[] = array(
                                'property_id' => $this->property_id,
                                'lang' => $lang,
                                'category_id' => $id,
                                'name' => $name,
                                'slug' => slugify($name),
                                'description' => $post['description'][$lang]
                            );
                        }
                    }
                }

                if(!empty($languages)){
                    $this->categories_model->insertLanguages($languages);
                }

                set_message('successMessage','Záznam bol úspešne upravený!');
                _return();
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function order(){
        if(has_permission('edit')){
            if($post = $this->input->post('serialized')){
                $this->orderProcess($post);
            }
        }
    }

    private function orderProcess($items,$parent = 0){
        foreach($items as $key => $item) {
            $this->categories_model->updateCategory($this->property_id, $item['id'], array('order' => $key,'parent'=> $parent ));
            if (isset($item['children']) && !empty($item['children'])) {
                $this->orderProcess($item['children'],$item['id']);
            }
        }
    }

    public function delete($id){
        if(has_permission('delete')){
            $this->categories_model->updateCategory($this->property_id, $id, array('deleted' => 1 ));

            _return();
        }
    }

    public function active($id,$state){
        if(has_permission('edit')){
            $state = !empty($state) ? 0 : 1;
            $this->categories_model->updateCategory($this->property_id, $id, array('active' => $state ));

            _return();
        }
    }
}