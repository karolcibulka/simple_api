<?php


class Packing extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Packing_model','packing_model');
    }

    public function index(){
        if(has_permission('show')){
            $data['packings'] = $this->packing_model->getPackings($this->property_id);
            _view('show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){
            _view('create',array());
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            if($postData = $this->input->post()){
                if(!empty($postData['internal_name'])){
                    $postData['property_id'] = $this->property_id;
                    if(empty($postData['price'])){
                        $postData['price'] = '0.0';
                    }
                    if($id = $this->packing_model->storePacking($postData)){
                        set_message('successMessage','Záznam bol úspešne vytvorený!');
                    }
                }
            }
            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){

            if(!$data['packing'] = $this->packing_model->getPacking($this->property_id,$id)){
                redirect('dashboard/packing');
            }

            _view('edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        $postData = $this->input->post();

        if(!empty($postData['internal_name'])){
            if(empty($postData['price'])){
                $postData['price'] = '0.0';
            }
            
            if($this->packing_model->updatePacking($postData,$this->property_id,$id)){
                set_message('successMessage','Záznam bol úspešne upravený!');
            }
            
            _return();
        }
        else{
            redirect('dashboard/packing/edit/'.$id);
        }
    }

    public function delete($id){
        if(has_permission('delete')){

            if($this->packing_model->updatePacking(array('deleted'=>'1'),$this->property_id,$id)){
                set_message('successMessage','Záznam bol úspešne zmazaný!');
            }

           _return();
        }
        else{
            $this->wrongState();
        }
    }
}