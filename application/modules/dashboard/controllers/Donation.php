<?php

class Donation extends DASH_Controller{

    protected $production = 'test';
    protected $stripe = array();
    protected $stripe_config;

    //pk_test_51I2MF1Eyay9k96sy5pMnQXu1jXW2WVbVwJNcBMa44ntetDj4H2zqQJ2llSBxvYDzv27oZzLCVZgFUjTFt6MGVNoD002vSAwFSL
    public function __construct()
    {
        parent::__construct();
        
        $this->config->load('stripe');
        $this->stripe_config = $this->config->item('stripe');
        

        $this->stripe = array(
            'pk' => $this->stripe_config['pk_'.$this->stripe_config['production']],
            'sk' => $this->stripe_config['sk_'.$this->stripe_config['production']],
        );

        $this->load->model('Donation_model','donation_model');

    }

    public function index(){
        _view('show',array('stripe'=>$this->stripe));
    }

    public function payment(){
   
        \Stripe\Stripe::setApiKey($this->stripe['sk']);
        
          $stripe = \Stripe\Charge::create ([
                  'amount' => $this->input->post('amount'),
                  'currency' => 'eur',
                  'source' => $this->input->post('tokenId'),
                  'description' => 'Podpora mywarehouse.sk'
          ]);

          if(isset($stripe) && !empty($stripe)){
              $insert_log = array(
                  'property_id' => $this->property_id,
                  'user_id' => $this->user_id,
                  'amount' => $this->input->post('amount'),
                  'token' => $this->input->post('tokenId'),
                  'response' => json_encode($stripe),
                  'type' => $this->production
              );

              $this->donation_model->insertLog($insert_log);
          }
                
          $data = array('status' => 1);
   
          echo json_encode($data);
    }
}