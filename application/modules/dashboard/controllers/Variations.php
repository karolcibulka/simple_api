<?php


class Variations extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Variations_model','variations_model');
    }

    public function index(){
        if(has_permission('show')){

            $data['variations'] = $this->variations_model->getVariations($this->property_id);
            _view('show',$data);

        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){
            $data['products'] = $this->variations_model->getProducts($this->property_id);
            $data['facilities'] = $this->variations_model->getFacilities($this->property_id);
            $data['variabilities'] = $this->variations_model->getVariabilities($this->property_id);
            _view('create',$data);

        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            if($post = $this->input->post()){

                $insert_data = array(
                    'internal_name' => $post['internal_name'],
                    'property_id' => $this->property_id,
                    'price_profile' => isset($post['price_profile']) && !empty($post['price_profile']) ? $post['price_profile'] : null,
                    'unit_price' => $post['unit_price'],
                    'product_id' => $post['product_id'],
                    'use_variability' => $post['use_variability']
                );

                if($id = $this->variations_model->insertVariation($insert_data)){

                    $uploaded_images = array();

                    if($images = $this->uploader->uploadImages('variations','images')){
                        foreach($images as $key => $image){
                            $uploaded_images[] = array(
                                'image' => $image,
                                'variation_id' => $id
                            );
                        }
                    }

                    $languages = array();

                    if(isset($post['name']) && !empty($post['name'])){
                        foreach($post['name'] as $language => $name){
                            if(!empty($name)){
                                $languages[] = array(
                                    'variation_id' => $id,
                                    'lang' => $language,
                                    'name' => $name,
                                    'slug' => slugify($name),
                                    'perex' => $post['perex'][$language],
                                    'description' => $post['description'][$language]
                                );
                            }
                        }
                    }

                    $facilities = array();

                    if(isset($post['facility']) && !empty($post['facility'])){
                        foreach($post['facility'] as $facility_id => $facility){
                            if(!empty($facility)){
                                foreach($facility as $key => $facility_item_id){
                                    $facilities[] = array(
                                        'variation_id' => $id,
                                        'facility_id' => $facility_id,
                                        'facility_item_id' => $facility_item_id
                                    );
                                }
                            }
                        }
                    }

                    $discounts = array();

                    if(isset($post['discount']) && !empty($post['discount'])){
                        foreach($post['discount'] as $key => $discount){
                            $discounts[] = array(
                                'variation_id' => $id,
                                'discount_id' => $discount
                            );
                        }
                    }

                    if(isset($post['variability']) && !empty($post['variability'])){
                        if(isset($post['variability_unit_price'][$post['variability']]) && !empty($post['variability_unit_price'][$post['variability']])){
                            foreach($post['variability_unit_price'][$post['variability']] as $item_id => $unit_price){
                                if(isset($unit_price) && !empty($unit_price)){
                                    $variations_variability = array(
                                        'variation_id' => $id,
                                        'variability_id' => $post['variability'],
                                        'variability_item_id' => $item_id,
                                        'unit_price' => $unit_price,
                                        'price_profile' => isset($post['variability_price_profile'][$post['variability']][$item_id]) && !empty($post['variability_price_profile'][$post['variability']][$item_id]) ? $post['variability_price_profile'][$post['variability']][$item_id] : null,
                                        'active' => 1,
                                    );

                                    $this->variations_model->replaceVariationVariability($variations_variability);
                                    
                                }
                            
                            }
                        }
                    }

                    $variations_variabilities_discounts = array();

                    if(isset($post['variability']) && !empty($post['variability'])){
                        if(isset($post['variability_discount'][$post['variability']]) && !empty($post['variability_discount'][$post['variability']])){
                            foreach($post['variability_discount'][$post['variability']] as $item_id => $item){
                                if(isset($item) && !empty($item)){
                                    $variations_variabilities_discounts[] = array(
                                        'variation_id' => $id,
                                        'variability_id' => $post['variability'],
                                        'variability_item_id' => $item_id,
                                        'discount_id' => $item
                                    );
                                }
                            }
                        }
                    }

                    if(!empty($variations_variabilities_discounts)){
                        $this->variations_model->insertVariationVariabilitiesDiscounts($variations_variabilities_discounts);
                    }

                    if(!empty($facilities)){
                        $this->variations_model->insertFacilities($facilities);
                    }

                    if(!empty($discounts)){
                        $this->variations_model->insertDiscounts($discounts);
                    }

                    if(!empty($languages)){
                        $this->variations_model->insertLanguages($languages);
                    }

                    if(!empty($uploaded_images)){
                        $this->variations_model->insertImages($uploaded_images);
                    }
                }

                set_message('successMessage','Záznam bol úspešne vytvorený!');
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){
            if(!$data['variation'] = $this->variations_model->getVariation($this->property_id,$id)){
                $this->wrongState();
            }

            if(isset($data['variation']['images']) && !empty($data['variation']['images'])){
                $data['images'] = $this->uploader->getImages('variations',$data['variation']['images']);
            }

            $data['products'] = $this->variations_model->getProducts($this->property_id);
            $data['facilities'] = $this->variations_model->getFacilities($this->property_id);
            $data['variabilities'] = $this->variations_model->getVariabilities($this->property_id);

            _view('edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        if(has_permission('edit')){
            if($post = $this->input->post()){

                $update_data = array(
                    'internal_name' => $post['internal_name'],
                    'price_profile' => isset($post['price_profile']) && !empty($post['price_profile']) ? $post['price_profile'] : null,
                    'unit_price' => $post['unit_price'],
                    'product_id' => $post['product_id'],
                    'use_variability' => $post['use_variability'],
                    'variability' => $post['variability']
                );


                $this->variations_model->updateVariation($this->property_id,$id,$update_data);

                if(isset($post['old']) && !empty($post['old'])){
                    $old_images = $this->variations_model->getOldImage($id,$post['old']);
                }
                else{
                   $old_images = $this->variations_model->getAllOldImage($id);
                }

                if(isset($old_images) && !empty($old_images)){
                    foreach($old_images as $key => $image){
                        $this->uploader->removeImages('variations',$image['image']);
                        $this->variations_model->removeOldImage($id,$image['image']);
                    }
                }

                $uploaded_images = array();

                if($images = $this->uploader->uploadImages('variations','images')){
                    foreach($images as $key => $image){
                        $uploaded_images[] = array(
                            'image' => $image,
                            'variation_id' => $id
                        );
                    }
                }

                $this->variations_model->deleteFacilities($id);
                $facilities = array();

                if(isset($post['facility']) && !empty($post['facility'])){
                    foreach($post['facility'] as $facility_id => $facility){
                        if(!empty($facility)){
                            foreach($facility as $key => $facility_item_id){
                                $facilities[] = array(
                                    'variation_id' => $id,
                                    'facility_id' => $facility_id,
                                    'facility_item_id' => $facility_item_id
                                );
                            }
                        }
                    }
                }

                $this->variations_model->deleteLanguages($id);
                $languages = array();

                if(isset($post['name']) && !empty($post['name'])){
                    foreach($post['name'] as $language => $name){
                        if(!empty($name)){
                            $languages[] = array(
                                'variation_id' => $id,
                                'lang' => $language,
                                'name' => $name,
                                'slug' => slugify($name),
                                'perex' => $post['perex'][$language],
                                'description' => $post['description'][$language]
                            );
                        }
                    }
                }

                $this->variations_model->deleteDiscounts($id);
                $discounts = array();

                if(isset($post['discount']) && !empty($post['discount'])){
                    foreach($post['discount'] as $key => $discount){
                        $discounts[] = array(
                            'variation_id' => $id,
                            'discount_id' => $discount
                        );
                    }
                }

                //$variations_variabilities = array();

                $this->variations_model->disableVariationVariabilities($id);

                if(isset($post['variability']) && !empty($post['variability'])){
                    if(isset($post['variability_unit_price'][$post['variability']]) && !empty($post['variability_unit_price'][$post['variability']])){
                        foreach($post['variability_unit_price'][$post['variability']] as $item_id => $unit_price){
                            if(isset($unit_price) && !empty($unit_price)){
                                $variations_variability = array(
                                    'variation_id' => $id,
                                    'variability_id' => $post['variability'],
                                    'variability_item_id' => $item_id,
                                    'unit_price' => $unit_price,
                                    'price_profile' => isset($post['variability_price_profile'][$post['variability']][$item_id]) && !empty($post['variability_price_profile'][$post['variability']][$item_id]) ? $post['variability_price_profile'][$post['variability']][$item_id] : null,
                                    'active' => 1,
                                );

                                $this->variations_model->replaceVariationVariability($variations_variability);
                                
                            }
                        }
                    }
                }

                $this->variations_model->removeVariationVariabilitiesDiscounts($id);
                $variations_variabilities_discounts = array();

                if(isset($post['variability']) && !empty($post['variability'])){
                    if(isset($post['variability_discount'][$post['variability']]) && !empty($post['variability_discount'][$post['variability']])){
                        foreach($post['variability_discount'][$post['variability']] as $item_id => $item){
                            if(isset($item) && !empty($item)){
                                $variations_variabilities_discounts[] = array(
                                    'variation_id' => $id,
                                    'variability_id' => $post['variability'],
                                    'variability_item_id' => $item_id,
                                    'discount_id' => $item
                                );
                            }
                        }
                    }
                }

                if(!empty($variations_variabilities_discounts)){
                    $this->variations_model->insertVariationVariabilitiesDiscounts($variations_variabilities_discounts);
                }

                if(!empty($facilities)){
                    $this->variations_model->insertFacilities($facilities);
                }

                if(!empty($discounts)){
                    $this->variations_model->insertDiscounts($discounts);
                }

                if(!empty($languages)){
                    $this->variations_model->insertLanguages($languages);
                }

                if(!empty($uploaded_images)){
                    $this->variations_model->insertImages($uploaded_images);
                }

                set_message('successMessage','Záznam bol úspešne upravený!');
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function active($id,$state){
        if(has_permission('edit')){

            set_message('successMessage','Záznam bol úspešne upravený!');
            $this->variations_model->updateVariation($this->property_id,$id,array('active'=>$state));
            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function delete($id){
        if(has_permission('delete')){

            set_message('successMessage','Záznam bol úspešne zmazaný!');
            $this->variations_model->updateVariation($this->property_id,$id,array('deleted'=>1));
            _return();
        }
        else{
            $this->wrongState();
        }
    }

}