<?php


class Newsletter extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Newsletter_model','newsletter_model');
    }

    public function index($start = 0){
        if(has_permission('show')){
            $data['title']     = 'Odoberatelia';

            $data['subscribers'] = $this->newsletter_model->getSubscribersLimit($this->property_id,null,null);

            $this->template->load('master', 'newsletter/show', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function exportCsv(){
        $filename = date('d-m-Y').'_subscribers.csv';
        $fp = fopen('php://output', 'w');

        $header = array(
            'id','email','created_at','activated_at','active'
        );

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);

        $subscribers = $this->newsletter_model->getSubscribers($this->property_id);
        if(!empty($subscribers)){
            foreach($subscribers as $subscriber){
                fputcsv($fp, $subscriber, $delimiter = ',', $enclosure = '"');
            }
        }
        exit;
    }

    public function exportExcel(){
        $filename = date('d-m-Y').'_subscribers.xlsx';

        $this->load->library('excel');
        $subscribers = $this->newsletter_model->getSubscribers($this->property_id);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'EMAIL');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'VYTVORENÉ');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'AKTIVOVANÉ');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'AKTÍVNE');
        // set Row
        $rowCount = count($subscribers);
        if(isset($subscribers) && !empty($subscribers) ){
            foreach ($subscribers as $val)
            {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $val['id']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $val['email']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $val['created_at']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $val['activated_at']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $val['active']);
                $rowCount++;
            }
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $objWriter->save('php://output');

    }
}