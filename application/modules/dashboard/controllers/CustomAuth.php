<?php 

class CustomAuth extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customAuth_model','custom_auth_model');
    }

    public function index(){
        if(has_permission('show')){
            $data = $this->custom_auth_model->getUsers( $this->user_id , true );
            $this->template->load('master','customAuth/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProccess(){
        $postData = $this->input->post();
        if(has_permission('create')){
            if(!empty($postData['email']) && !empty($postData['password']) && !empty($postData['password_confirm']) && !empty($postData['role']) && !empty($postData['property']) && !empty($postData['first_name']) && !empty($postData['last_name'])){
                $createUser = array(
                    'first_name' => $postData['first_name'],
                    'last_name' => $postData['last_name'],
                    'email' => $postData['email'],
                    'username' => $postData['email'],
                    'created_on' => strtotime(date('Y-m-d H:i:s')),
                    'lang' => 'sk',
                    'active' => '1', 
                );

                if($postData['password'] == $postData['password_confirm']){
                    $createUser['password'] = $this->bcrypt->hash($postData['password']);
                }
                if($this->custom_auth_model->validateEmail($postData['email'])){
                    
                    $userID = $this->custom_auth_model->createUser($createUser);

                    foreach($postData['role'] as $role){
                        $insertData = array(
                            'user_id' => $userID,
                            'group_id' => $role
                        );

                        $this->custom_auth_model->createPivotUserGroup($insertData);
                    }

                    foreach($postData['property'] as $property){
                        $insertData = array(
                            'user_id' => $userID,
                            'property_id' => $property
                        );

                        $this->custom_auth_model->createPivotUserProperty($insertData);
                    }

                }
            }

            _return();
        }
        else{
            $this->wrongState();
        }

    }

    public function edit($id){
        if(has_permission('edit')){

            $session_data = $this->session->userdata();

            $data = $this->custom_auth_model->getUsers( $session_data['user_id'] , true , $id );

            if(isset($data['user']) && !empty($data['user']) && isset($data['max_role']) && !empty($data['max_role'])){
                $canContinue = false;
                if(isset($data['user']['group_ids']) && !empty($data['user']['group_ids'])){
                    foreach($data['user']['group_order'] as $group_id){
                        if($group_id <= $data['max_role']){
                            $canContinue = true;
                        }
                        else{
                            $canContinue = false;
                        }
                    }
                }
                if($canContinue){

                    $this->template->load('master','customAuth/edit',$data);
                    exit;
        
                }
            }
            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function editProccess(){

        $postData = $this->input->post();

        if(has_permission('edit')){
            if(!empty($postData['email']) && !empty($postData['role']) && !empty($postData['property']) && !empty($postData['first_name']) && !empty($postData['last_name'])){
                $userProperties = $this->custom_auth_model->getUserPropertiesByEmail($postData['email']);
                
                $unauthorizedProperties = array();

                $hasPermissionForProperties = $postData['allProperties'];

                if(!empty($userProperties)){
                    foreach($userProperties as $userProperty){
                        if(!empty($hasPermissionForProperties)){
                            if(!in_array($userProperty,$hasPermissionForProperties)){
                                $unauthorizedProperties[] = $userProperty;
                            }
                        }
                    }
                }
                
                $properties = $postData['property'];

                if(!empty($unauthorizedProperties)){
                    $properties = $unauthorizedProperties + $properties;
                }
                
                $updateData = array(
                    'first_name' => $postData['first_name'],
                    'last_name' => $postData['last_name']
                );

                $userID = $this->custom_auth_model->updateUserByEmail($postData['email'],$updateData);

                $this->custom_auth_model->removePermissions($userID);
                $this->custom_auth_model->removeProperties($userID);

                foreach($postData['role'] as $role){
                    $this->custom_auth_model->createPivotUserGroup(array('user_id'=>$userID,'group_id'=>$role));
                }
                foreach($properties as $property){
                    $this->custom_auth_model->createPivotUserProperty(array('user_id'=>$userID,'property_id'=>$property));
                }
            }
            
            _return();
        }
        else{
            $this->wrongState();
        }
    }
}
