<?php


class Program extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Program_model','program_model');

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index(){
        if(has_permission('show')){
            $data['title'] = 'Vytvorenie programu';
            $data['programs'] = $this->program_model->getPrograms();
            $this->template->load('master', 'program/show', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){
            $data['title'] = 'Vytvorenie programu';
            $data['controllers'] = $this->program_model->getControllers();
            $this->template->load('master', 'program/create', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        $post = $this->input->post();
        if(isset($post['internal_name']) && !empty($post['internal_name']) && isset($post['controllers']) && !empty($post['controllers'])){
            $insertProgram = array(
                'internal_name' => $post['internal_name'],
                'controllers' => isset($post['controllers']) && !empty($post['controllers']) ? json_encode($post['controllers']) : ''
            );

            $id = $this->program_model->insertProgram($insertProgram);
            $this->deleteCacheItem('property_program_ci');
            set_message('successMessage','Záznam bol úspešne vytvorený!');
            redirect('dashboard/program');
        }
        else{
            redirect('dashboard/program/create');
        }

    }

    public function edit($id){
        if(has_permission('edit')){
            $data['title'] = 'Úprava programu';
            $data['controllers'] = $this->program_model->getControllers();
            if(!$data['program'] = $this->program_model->getProgram($id)){
               redirect('dashboard/program');
            }
            $data['program']['controllers'] = json_decode($data['program']['controllers'],true);

            $this->template->load('master', 'program/edit', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        $post = $this->input->post();
        if(isset($post['internal_name']) && !empty($post['internal_name']) && isset($post['controllers']) && !empty($post['controllers'])){
            $updateProgram = array(
                'internal_name' => $post['internal_name'],
                'controllers' => isset($post['controllers']) && !empty($post['controllers']) ? json_encode($post['controllers']) : ''
            );

            $this->program_model->updateProgram($updateProgram,$id);
            set_message('successMessage','Záznam bol úspešne upravený!');
            $this->deleteCacheItem('property_program_ci');
            redirect('dashboard/program');
        }
        else{
            redirect('dashboard/program/edit/'.$id);
        }
    }

    public function active($id,$state){
        if(has_permission('edit')){
            $this->program_model->updateProgram(array('active'=>$state),$id);
            $this->deleteCacheItem('property_program_ci');
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/program');
        }
    }

    public function delete($id){
        $this->program_model->updateProgram(array('deleted'=>'1'),$id);
        $this->deleteCacheItem('property_program_ci');
        set_message('successMessage','Záznam bol úspešne zmazaný!');
        redirect('dashboard/program');
    }
}