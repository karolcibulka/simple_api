<?php


class Facilities extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Facilities_model','facilities_model');
    }

    public function index(){
        if(has_permission('show')){

            $data['facilities'] = $this->facilities_model->getFacilities($this->property_id);

            _view('show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){

            $data = array();
            _view('create',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            if($post = $this->input->post()){

                $insert_data = array(
                    'property_id' => $this->property_id,
                    'internal_name' => $post['internal_name']
                );

                if($id = $this->facilities_model->insertData($insert_data)){

                    $this->facilities_model->updateFacility($this->property_id,$id,array('order'=>$id));

                    $languages = array();

                    if(isset($post['name']) && !empty($post['name'])){
                        foreach($post['name'] as $language => $name){
                            if(!empty($name)){
                                $languages[] = array(
                                    'facility_id' => $id,
                                    'name' => $name,
                                    'lang' => $language
                                );
                            }
                        }
                    }

                    if(!empty($languages)){
                        $this->facilities_model->insertLanguages($languages);
                    }

                    $insert_items = array();
                    $counter = 0;

                    if(isset($post['value']) && !empty($post['value'])){
                        foreach($post['value'] as $key => $value){
                            if(!empty($value)){
                                $insert_items[$key] = array(
                                    'facility_id' => $id,
                                    'value' => slugify($value),
                                    'property_id' => $this->property_id,
                                    'active' => 1,
                                    'order' => $counter
                                );

                                $counter++;
                            }
                        }
                    }

                    $insert_items_languages = array();

                    if(!empty($insert_items)){
                        foreach($insert_items as $key => $insert_item){
                            if(isset($post['language'][$key]) && !empty($post['language'][$key])){

                                if($item_id = $this->facilities_model->insertItem($insert_item)){

                                    foreach($post['language'][$key] as $language => $item_language){
                                        if(!empty($item_language)){
                                            $insert_items_languages[] = array(
                                                'lang' => $language,
                                                'facilities_item_id' => $item_id,
                                                'name' => $item_language
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if(!empty($insert_items_languages)){
                        $this->facilities_model->insertItemLanguages($insert_items_languages);
                    }

                    set_message('successMessage','Záznam bol úspešne pridaný!');
                }
            }
            else{
                $this->wrongState();
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){

            $data['id'] = $id;
            if(!$data['facility'] = $this->facilities_model->getFacility($this->property_id,$id)){
                $this->wrongState();
            }

            _view('edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        if(has_permission('edit')){

            if($post = $this->input->post()){
                $update_data = array(
                    'internal_name' => $post['internal_name']
                );

                if($this->facilities_model->updateFacility($this->property_id,$id,$update_data)){

                    $this->facilities_model->removeLanguages($id);
                    $this->facilities_model->deactivateItems($this->property_id,$id);

                    $languages = array();

                    if(isset($post['name']) && !empty($post['name'])){
                        foreach($post['name'] as $language => $name){
                            if(!empty($name)){
                                $languages[] = array(
                                    'facility_id' => $id,
                                    'name' => $name,
                                    'lang' => $language
                                );
                            }
                        }
                    }

                    if(!empty($languages)){
                        $this->facilities_model->insertLanguages($languages);
                    }

                    $insert_items = array();
                    $counter = 0;

                    if(isset($post['value']) && !empty($post['value'])){
                        foreach($post['value'] as $key => $value){
                            if(!empty($value)){
                                $insert_items[$key] = array(
                                    'facility_id' => $id,
                                    'value' => slugify($value),
                                    'property_id' => $this->property_id,
                                    'active' => 1,
                                    'order' => $counter
                                );

                                $counter++;
                            }
                        }
                    }

                    $insert_items_languages = array();

                    if(!empty($insert_items)){
                        foreach($insert_items as $key => $insert_item){
                            if(isset($post['language'][$key]) && !empty($post['language'][$key])){

                                if(isset($post['added']) && in_array($key,$post['added'])) {
                                    $this->facilities_model->deleteItemLanguages($key);

                                    if($this->facilities_model->updateItem($this->property_id,$key,$insert_item)) {
                                        foreach ($post['language'][$key] as $language => $item_language) {
                                            if(!empty($item_language)){
                                                $insert_items_languages[] = array(
                                                    'lang' => $language,
                                                    'facilities_item_id' => $key,
                                                    'name' => $item_language
                                                );
                                            }
                                        }
                                    }
                                }
                                else {
                                    if ($item_id = $this->facilities_model->insertItem($insert_item)) {

                                        foreach ($post['language'][$key] as $language => $item_language) {
                                            if(!empty($item_language)) {
                                                $insert_items_languages[] = array(
                                                    'lang' => $language,
                                                    'facilities_item_id' => $item_id,
                                                    'name' => $item_language
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($insert_items_languages)) {
                        $this->facilities_model->insertItemLanguages($insert_items_languages);
                    }
                }
            }
            else{
                $this->wrongState();
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function active($id,$active){
        if(has_permission('edit')){

            $active = $active == 1 ? 0 : 1;

            if($this->facilities_model->updateFacility($this->property_id,$id,array('active'=>$active))){
                set_message('successMessage','Záznam bol úspešne upravený!');
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function delete($id){
        if(has_permission('delete')){
            if($this->facilities_model->updateFacility($this->property_id,$id,array('deleted'=>1))){
                set_message('successMessage','Záznam bol úspešne zmazaný!');
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function order(){
        if(has_permission('edit')){
            if($post = $this->input->post('serialized')){
                foreach($post as $key => $item){
                    $this->facilities_model->updateFacility($this->property_id,$item['id'],array('order'=>$key));
                }
            }
        }
    }
}