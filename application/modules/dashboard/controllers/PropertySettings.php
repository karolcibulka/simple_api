<?php


class PropertySettings extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('PropertySettings_model','psm_model');

    }

    public function index(){
        if(has_permission('show')){
            $data['property'] = $this->psm_model->getProperty($this->property_id);
            $this->template->load('master','propertySettings/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            if($post = $this->input->post()){
                $insert_data = array(
                    'property_id' => $this->property_id,
                    'name' => $post['name'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'default_language' => $post['default_language'],
                    'default_currency' => $post['currency'],
                    'active_selling' => $post['active_selling'],
                    'price_separator' => $post['price_separator'],
                    'price_definition' => $post['price_definition'],
                    'web_color_1' => $post['web_color_1'],
                    'web_color_2' => $post['web_color_2'],
                    'public_languages' => isset($post['public_languages']) ? json_encode($post['public_languages']) : json_encode(array()),
                    'currencies' => isset($post['currencies']) ? json_encode($post['currencies']) : json_encode(array()),
                    'favicon' => $this->uploader->uploadImage('favicon','favicon',true),
                    'logo' => $this->uploader->uploadImage('logo','logo',false,true),
                    'default_order' => $post['default_order']
                );

                if($id = $this->psm_model->insertProperty($insert_data)){

                   $this->deleteCacheItem('property_ci_'.$this->property_id);

                }
            }

            redirect(controller_url('index'));
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        if(has_permission('edit')){
            if($post = $this->input->post()){
                $update_data = array(
                    'name' => $post['name'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'default_language' => $post['default_language'],
                    'default_currency' => $post['currency'],
                    'active_selling' => $post['active_selling'],
                    'price_separator' => $post['price_separator'],
                    'price_definition' => $post['price_definition'],
                    'web_color_1' => $post['web_color_1'],
                    'web_color_2' => $post['web_color_2'],
                    'public_languages' => isset($post['public_languages']) ? json_encode($post['public_languages']) : json_encode(array()),
                    'currencies' => isset($post['currencies']) ? json_encode($post['currencies']) : json_encode(array()),
                    'default_order' => $post['default_order']
                );

                if($property = $this->psm_model->getProperty($this->property_id,true)){

                    if($logo = $this->uploader->uploadImage('logo','logo',false,true)){
                        $update_data['logo'] = $logo;
                    }


                    if(isset($property['logo']) && !empty($property['logo']) && ((isset($post['delete_logo']) && !empty($post['delete_logo'])) || $logo)){
                        $this->uploader->removeImages('logo',$property['logo'],false,true);
                        if(!$logo){
                            $update_data['logo'] = null;
                        }
                    }

                    if($favicon = $this->uploader->uploadImage('favicon','favicon',true)){
                        $update_data['favicon'] = $favicon;
                    }

                    if(isset($property['favicon']) && !empty($property['favicon']) && ((isset($post['delete_favicon']) && !empty($post['delete_favicon'])) || $favicon)){
                        $this->uploader->removeImages('logo',$property['favicon'],true);
                        if(!$favicon){
                            $update_data['favicon'] = null;
                        }
                    }

                    $this->deleteCacheItem('property_ci_'.$this->property_id);
                }


                $this->psm_model->updateProperty($this->property_id,$update_data);
            }

            redirect(controller_url('index'));
        }
        else{
            $this->wrongState();
        }
    }
}