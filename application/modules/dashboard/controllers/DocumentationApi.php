<?php


class DocumentationApi extends DASH_Controller
{
    public $property_token;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Documentation_model','documentation_model');
        $this->property_token = $this->documentation_model->getToken($this->property_id);
    }

    public function index(){
        $data['title'] = 'Api dokumentácia';
        $data['docs'] = $this->getApiDocs();
        $data['property_token'] = $this->property_token;
        $this->template->load('master', 'documentation/api/show', $data);
    }

    private function getApiDocs(){
        $array = array(
            'GET' => array(
                'getDailyMenu' => array(
                    'params' => array(
                        'property_token' => array(
                            'required' => true,
                            'example' => 'sjdsSAFDSdsdaaJHASDBZý263arfe78729asd1342aasd14',
                        ),
                        'date' => array(
                            'required' => false,
                            'example' => date('Y-m-d'),
                        ),
                    ),
                    'url' => '/property_token/date',
                    'response' => '
                    {
                        "status": true,
                        "data": {
                            "2020-05-04": {
                                "main": [
                                    {
                                        "internal_name": "Palacinky s čokoládou ",
                                        "type": "main",
                                        "date": "2020-05-04",
                                        "day": "monday",
                                        "unit_price": "4.00",
                                        "weight": "200g",
                                        "traceable": false,
                                        "languages": {
                                            "sk": {
                                                "name": "Palacinky s čokoládou ",
                                                "short_description": "Francúzke palacinky s čokoládou a domácou marmeládou"
                                            }
                                        },
                                        "allergens": "2",
                                        "packing": {
                                            "internal_name": "balne_denne_menu_test",
                                            "price": "5.00"
                                        }
                                    },
                                    {
                                        "internal_name": "kuracie stripsy kaša ",
                                        "type": "main",
                                        "date": "2020-05-04",
                                        "day": "monday",
                                        "unit_price": "0.00",
                                        "weight": "",
                                        "traceable": false,
                                        "languages": {
                                            "sk": {
                                                "name": "kuracie stripsy so zemiakovou kašou",
                                                "short_description": "Vyprážané kuracie stripsy so zemiakovou kašou"
                                            }
                                        },
                                        "allergens": "1|2"
                                    }
                                ],
                                "soup": [
                                    {
                                        "internal_name": "Kuracia polievka ",
                                        "type": "soup",
                                        "date": "2020-05-04",
                                        "day": "monday",
                                        "unit_price": "0.00",
                                        "weight": "250ml",
                                        "traceable": true,
                                        "languages": {
                                            "sk": {
                                                "name": "Kuracia polievka ",
                                                "short_description": ""
                                            }
                                        }
                                    }
                                ],
                                "desert": [
                                    {
                                        "internal_name": "Čokoládová torta ",
                                        "type": "desert",
                                        "date": "2020-05-04",
                                        "day": "monday",
                                        "unit_price": "0.00",
                                        "weight": "50g",
                                        "traceable": true,
                                        "languages": {
                                            "sk": {
                                                "name": "Čokoládová torta ",
                                                "short_description": ""
                                            }
                                        },
                                        "allergens": "2|3"
                                    }
                                ]
                            }
                        }
                    }'
                ),
                'getAllergens' => array(
                    'params' => array(
                        'property_token' => array(
                            'required' => true,
                            'example' => 'sjdsSAFDSdsdaaJHASDBZý263arfe78729asd1342aasd14',
                        )
                    ),
                    'url' => '/property_token',
                    'response' => '
                    {
                    "status": true,
                    "data": [
                        {
                            "code": "1",
                            "internal_name": "Múka",
                            "languages": {
                                "en": {
                                    "name": "Múka_en",
                                    "short_description": "Múčka_en"
                                },
                                "sk": {
                                    "name": "Múka",
                                    "short_description": "Múka popis"
                                }
                            }
                        },
                        {
                            "code": "2",
                            "internal_name": "Mlieko",
                            "languages": {
                                "sk": {
                                    "name": "Mlieko",
                                    "short_description": "Mliečko popis"
                                }
                            }
                        },
                        {
                            "code": "3",
                            "internal_name": "Orechy",
                            "languages": {
                                "sk": {
                                    "name": "Orechy",
                                    "short_description": "Orechyy"
                                }
                            }
                        }
                    ]
                }',
                )
            )
        );

        return $array;
    }
}