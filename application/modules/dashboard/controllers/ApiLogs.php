<?php


class ApiLogs extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Log_model','log_model');

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index($start = 0){
        if(has_permission('show')){
            $getData = $this->input->get();

            $this->load->library('pagination');
            $config                = array();
            $config                = array_merge($config, getPaginationConfig());
            $config['base_url']    = base_url('dashboard/log/index/');
            $config['total_rows']  = $this->log_model->getApiLogsCount($getData);
            $config['per_page']    = 100;
            $config['uri_segment'] = 4;
            if (count($getData) > 0) $config['suffix'] = '?' . http_build_query($getData);
            if (count($getData) > 0) $config['first_url'] = $config['base_url'].'?'.http_build_query($getData);

            $this->pagination->initialize($config);

            $data['get'] = $getData;
            $data['title'] = 'Logy';
            $data['logs'] = $this->log_model->getApiLogs($start,$config['per_page'],$getData);
            $data['properties'] = $this->log_model->getProperties();
            $data['start'] = $start;
            $data['links'] = $this->pagination->create_links();
            $this->template->load('master','apiLogs/show',$data);
        }
        else{
            $this->wrongState();
        }
    }
}

