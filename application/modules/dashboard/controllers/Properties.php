<?php

class Properties extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Properties_model', 'propertiesModel');
        $this->load->model('ExternalSystem_model', 'es_model');

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function handleChangeProperty($id)
    {
        $this->session->set_userdata('active_property', $id);
        echo json_encode(array(
            'status' => '1'
        ));
    }

    public function index()
    {
        if (has_permission('show')) {
            $data['title']      = 'Zariadenia';
            $data['properties'] = $this->propertiesModel->getProperties();
            $data['programs']   = $this->propertiesModel->getPrograms();
            $data['groups']   = $this->propertiesModel->getPropertyGroups();
            $data['versions']   = $this->propertiesModel->getVersions();
            $this->template->load('master', 'properties/show', $data);
        } else {
            $this->wrongState();
        }
    }

    public function create()
    {
        if (has_permission('edit')) {
            $data['title']    = 'Vytvorenie zariadenia';
            $data['users']    = $this->propertiesModel->getUsers();
            $data['roles']    = $this->propertiesModel->getRoles();
            $data['groups']   = $this->propertiesModel->getPropertyGroups();
            $data['versions'] = $this->propertiesModel->getVersions();
            $data['programs'] = $this->propertiesModel->getPrograms();
            $this->template->load('master', 'properties/partials/create', $data);
        } else {
            $this->wrongState();
        }
    }

    public function createProcess()
    {
        $postData = $this->input->post();


        if (isset($postData['name']) && !empty($postData['name'])) {
            $propertyID = $this->propertiesModel->insertProperty(
                array(
                    'program_id' => $postData['program_id'],
                    'name' => $postData['name'],
                    'uach' => $postData['uach'],
                    'server' => $postData['server'],
                    'port' => $postData['port'],
                    'cgin' => $postData['cgin'],
                    'active' => '1',
                    'deleted' => '0',
                    'token' => generateToken(50)
                )
            );

            if (isset($postData['user']) && !empty($postData['user'])) {
                foreach ($postData['user'] as $userID => $user) {
                    $true = $this->propertiesModel->insertUserProperties($user, $propertyID);
                }
            }

            if (isset($postData['first_user']) && !empty($postData['first_user']) && $postData['createUser'] == '1') {
                if ($this->propertiesModel->emailExist($postData['first_user']['email'])) {
                    $insertUser = array(
                        'email' => $postData['first_user']['email'],
                        'first_name' => $postData['first_user']['first_name'],
                        'last_name' => $postData['first_user']['last_name'],
                        'lang' => 'sk',
                        'active' => '1',
                        'password' => $this->bcrypt->hash($postData['first_user']['password']),
                    );

                    if ($insertID = $this->propertiesModel->insertUser($insertUser)) {
                        $this->propertiesModel->insertConnection(array('user_id' => $insertID, 'property_id' => $propertyID));

                        if (isset($postData['first_user']['roles']) && !empty($postData['first_user']['roles'])) {
                            foreach ($postData['first_user']['roles'] as $role) {
                                $this->propertiesModel->insertConnectionPermission(array('user_id' => $insertID, 'group_id' => $role));
                            }
                        }
                    }
                }
            }

            $this->session->set_userdata('active_property', $propertyID);
            set_message('successMessage','Záznam bol úspešne vytvorený!');
            redirect(base_url('dashboard/properties'));
        } else {
            redirect(base_url('dashboard/properties/create'));
        }

    }

    public function editProcess($id)
    {
        $postData = $this->input->post();

        if (isset($postData['name']) && !empty($postData['name'])) {
            $this->propertiesModel->updateProperty(
                array(
                    'version'=>$postData['version'],
                    'name' => $postData['name'],
                    'program_id' => $postData['program_id'],
                    'uach' => $postData['uach'],
                    'server' => $postData['server'],
                    'port' => $postData['port'],
                    'cgin' => $postData['cgin']
                ), $id);
            if (isset($postData['user']) && !empty($postData['user'])) {

                $this->propertiesModel->deleteAllUsersFromProperty($id);

                foreach ($postData['user'] as $userID => $user) {
                    $true = $this->propertiesModel->insertUserProperties($user, $id);
                }
            }

            $this->loglib->storeLog('properties', 'edit', $id);
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect(base_url('dashboard/properties'));
        } else {
            redirect(base_url('dashboard/properties/edit/' . $id));
        }
    }

    public function changeProgram()
    {
        $postData = $this->input->post();
        $this->propertiesModel->updateProperty(array('program_id' => $postData['program_id']), $postData['id']);
        $this->loglib->storeLog('properties', 'changeProgram', $postData['id']);
        echo json_encode(array(
            'status' => '1'
        ));
    }

    public function changeGroup()
    {
        $postData = $this->input->post();
        $this->propertiesModel->updateProperty(array('group_id' => $postData['group_id']), $postData['id']);
        $this->loglib->storeLog('properties', 'changeGroup', $postData['id']);
        echo json_encode(array(
            'status' => '1'
        ));
    }

    public function changeVersion()
    {
        $postData = $this->input->post();
        $this->propertiesModel->updateProperty(array('version' => $postData['version']), $postData['id']);
        $this->loglib->storeLog('properties', 'changeVersion', $postData['id']);
        echo json_encode(array(
            'status' => '1'
        ));
    }


    public function edit($id)
    {
        if (has_permission('edit')) {
            $data['title']          = 'Vytvorenie zariadenia';
            $data['property']       = $this->propertiesModel->getPropertyDev($id);
            $data['property']['id'] = $id;
            $data['versions']       = $this->propertiesModel->getVersions();
            $data['groups']   = $this->propertiesModel->getPropertyGroups();
            $data['programs']       = $this->propertiesModel->getPrograms();
            if (isset($data['property']['users']) && !empty($data['property']['users'])) {
                $data['property']['users'] = explode('|', $data['property']['users']);
            }
            $data['users'] = $this->propertiesModel->getUsers();

            $this->template->load('master', 'properties/partials/edit', $data);
        } else {
            $this->wrongState();
        }
    }

    public function activity($id, $state)
    {
        if (has_permission('edit')) {
            $this->loglib->storeLog('properties', 'active', $id);
            $this->propertiesModel->updateProperty(array('active' => $state), $id);
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/properties');
        } else {
            $this->wrongState();
        }
    }

    public function checkConnection($id)
    {
        $settings = $this->propertiesModel->getPropertyDev($id);
        $this->loglib->storeLog('properties', 'checkConnection', $id);
        if (isset($settings['id']) && !empty($settings['id']) && isset($settings['server']) && !empty($settings['server']) && isset($settings['port']) && !empty($settings['port'])) {
            $this->load->library('ExternalSystems/BlueGastro', $settings, 'blueGastro');

            if ($this->blueGastro->checkConnection()) {
                $res = array(
                    'status' => '1'
                );
            } else {
                $res = array(
                    'status' => '0',
                );
            }
        } else {
            $res = array(
                'status' => '0',
            );
        }

        echo json_encode($res);
    }

    public function delete($id)
    {
        if (has_permission('delete')) {

            $this->propertiesModel->updateProperty(array('deleted' => '1'), $id);
            $this->loglib->storeLog('properties', 'delete', $id);
            set_message('successMessage','Záznam bol úspešne zmazaný!');
            redirect('dashboard/properties');
        } else {
            $this->wrongState();
        }
    }

    public function validateUser()
    {
        $postData = $this->input->post();
        $status   = '0';

        if ($this->propertiesModel->emailExist($postData['email'])) {
            $status = '1';
        }

        echo json_encode(array(
            'status' => $status
        ));
    }

    public function setProperty($id)
    {
        if (has_permission('edit')) {
            $this->session->set_userdata('active_property', $id);
            redirect('dashboard/dashboard');
        }
    }

    public function createNewToken($id)
    {
        if (has_permission('edit')) {
            $updateData = array(
                'token' => generateToken(50)
            );
            $this->propertiesModel->updateProperty($updateData, $id);
            redirect('dashboard/properties');
        } else {
            redirect('dashboard/dashboard');
        }
    }

    public function createGroup(){
        if(has_permission('create') && isDeveloper()){
            $data = array();
            $this->template->load('master', 'properties/createGroup', $data);
        }
    }

    public function createGroupProcess(){
        if(has_permission('create') && isDeveloper()){
            $post = $this->input->post();

            $this->propertiesModel->storeGroup($post);

            redirect('dashboard/properties');
        }
    }
}