<?php


class Payments extends DASH_Controller
{
    public $upload_dir;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Payments_model','payments_model');
    }

    public function index(){
        if(has_permission('show')) {

            $data['payments'] = $this->payments_model->getPayments($this->property_id);
            _view('show',$data);

        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')) {

            $data['payment_types'] = $this->payments_model->getPaymentTypes();
            _view('create',$data);

        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            $postData = $this->input->post();

            if ($postData = $this->input->post()) {

                $paymentTypeInsertData = array(
                    'internal_name' => $postData['internal_name'],
                    'order' => '999999',
                    'active' => '1',
                    'payment_type_id' => $postData['payment_type_id'],
                    'deleted' => '0',
                    'created_at' => date('Y-m-d H:i:s'),
                    'property_id' => $this->property_id
                );

                if($image = $this->uploader->uploadImage('payments','image')){
                    $paymentTypeInsertData['image'] = $image;
                }


                if($id = $this->payments_model->insert($paymentTypeInsertData)){

                    $this->payments_model->update($this->property_id,$id,array('order'=>$id));

                    $languages = array();

                    if (isset($postData['name']) && !empty($postData['name'])) {
                        foreach ($postData['name'] as $language => $name) {
                            if (!empty($name)) {
                                $languages[] = array(
                                    'lang' => $language,
                                    'payment_id' => $id,
                                    'property_id' => $this->property_id,
                                    'name' => $name,
                                    'short_description' => $postData['short_description'][$language],
                                );

                            }
                        }
                    }

                    if(!empty($languages)){
                        $this->payments_model->insertLanguages($languages);
                    }

                    set_message('successMessage','Záznam bol úspešne vytvorený!');
                }
            } 

            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')) {
            if($data['payment'] = $this->payments_model->getPayment($id,$this->property_id)){
                $data['external_payments'] = $this->payments_model->getExternalPayments($this->property_id);
                $data['payment_types'] = $this->payments_model->getPaymentTypes();
                _view('edit',$data);
            }
            else{
                _return();
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){

        if(has_permission('edit')){

            if ($postData = $this->input->post()) {

                $paymentUpdateData = array(
                    'internal_name' => $postData['internal_name'],
                    'payment_type_id' => $postData['payment_type_id'],
                );

                if($postData['deleteImg']=='1' && !empty($postData['imgPath'])){
                    $this->uploader->removeImages('payments',$postData['imgPath']);
                    $paymentUpdateData['image'] = '';
                }

                if($image = $this->uploader->uploadImage('payments','image')){
                    $paymentUpdateData['image'] = $image;
                }

                if($this->payments_model->update($this->property_id,$id,$paymentUpdateData)){

                    $this->payments_model->removeLanguages($this->property_id,$id);

                    $languages = array();

                    if (isset($postData['name']) && !empty($postData['name'])) {
                        foreach ($postData['name'] as $langCode => $name) {
                            if (!empty($postData['name'][$langCode])) {
                                $languages[] = array(
                                    'lang' => $langCode,
                                    'payment_id' => $id,
                                    'property_id' => $this->property_id,
                                    'name' => $postData['name'][$langCode],
                                    'short_description' => $postData['short_description'][$langCode],
                                );

                            }
                        }
                    }

                    if(!empty($languages)){
                        $this->payments_model->insertLanguages($languages);
                    }

                    set_message('successMessage','Záznam bol úspešne upravený!');
                }

                _return();
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function order(){
        if(has_permission('edit')){
            $postData= $this->input->post();

            if(!empty($postData['serialized'])){
                foreach($postData['serialized'] as $order => $item){
                    $this->payments_model->update($this->property_id,$item['id'],array('order'=>$order));
                }
            }
        }
    }

    public function active($id,$value){
        if(has_permission('edit')){
            if($this->payments_model->update($this->property_id,$id,array('active'=>$value))){
                $this->frontCache->deleteCache();
                set_message('successMessage','Záznam bol úspešne upravený!');
            }
            
            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function delete($id){
        if(has_permission('delete')){
            if($this->payments_model->update($this->property_id,$id,array('deleted'=>'1'))){
                $this->frontCache->deleteCache();
                set_message('successMessage','Záznam bol úspešne zmazaný!');
            }

            _return();
        }
        else{
            $this->wrongState();
        }
    }
}