<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-09-22
 * Time: 20:00
 */

class Email extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Email_model','email_model');
    }

    public function index(){
        $data['title'] = lang('segment.email');
        $property_id = $this->session->userdata('active_property');
        $data['templates'] = $this->email_model->getEmailTemplates($property_id);
        $this->template->load('master','email/show',$data);
    }

    public function create(){
        $postData = $this->input->post();

        $title = isset($postData['name']) && !empty($postData['name']) ? 'Emailová šablóna: '.$postData['name'] : 'Emailová šablóna';
        $data['title'] = $title;
        $data['name'] = $postData['name'];
        if(isset($postData['defaultTemplate']) && !empty($postData['defaultTemplate']) && $postData['defaultTemplate']=='1'){
            $data['defaultTemplate'] = $this->email_model->getDefaultTemplate();
        }
        $this->template->load('master','email/create',$data);
    }

     public function edit($id){
        $property_id = $this->session->userdata('active_property');
        $template = $this->email_model->checkID($id,$property_id);
        if(isset($template) && !empty($template) && $template['property_id']==$property_id){
            $data['template'] = $template;
            $data['id'] = $id;
            $data['title'] = 'Editovanie emailovej šablóny: '.$template['name'];
            $this->template->load('master','email/edit',$data);
        }
        else{
            redirect('dashboard/email');
        }
    }

    public function handleCreateEmailTemplate(){
        $postData = $this->input->post();
        $insertData = array(
            'name' => $postData['name'],
            'template'=> $postData['template'],
            'mjml' => $postData['mjml'],
            'property_id'=> $this->session->userdata('active_property'),
        );
        $id = $this->email_model->insertNewTemplate($insertData);
        if(isset($id) && !empty($id)){
            $response = array(
                'status' => '1',
            );
        }
        else{
            $response = array(
                'status' => '0',
            );
        }

        echo json_encode($response);
    }

    public function handleChangeName(){
        $postData = $this->input->post();
        $updateData = array(
          'name' => $postData['name'],
        );
        $this->email_model->updateEmailTemplate($updateData,$postData['id']);
        $response = array(
            'status' => '1',
        );
        echo json_encode($response);
    }

    public function handleUpdateTemplate(){
        $postData = $this->input->post();
        $updateData = array(
            'template' => $postData['template'],
            'mjml' => $postData['mjml'],
        );
        if($postData['id']!='10') {
            $this->email_model->updateEmailTemplate($updateData, $postData['id']);
        }
        $response = array(
            'status' => '1',
        );
        echo json_encode($response);
    }

    public function handleSoftDelete(){
        $postData = $this->input->post();
        $updateData = array(
          'deleted'=> '1',
        );
        if($postData['id']!='10'){
            $this->email_model->updateEmailTemplate($updateData,$postData['id']);
        }
        $response = array(
            'status' => '1',
        );
        echo json_encode($response);
    }

    public function checkTemplate($template_id){
        $template = $this->email_model->checkID($template_id,$this->property_id);
        if(isset($template) && !empty($template)){
            echo $template['template'];
        }
        else{
            redirect('dashboard/email');
        }
    }

}