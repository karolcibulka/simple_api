<?php


class FrontUsers extends DASH_Controller
{

    protected $properties;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('FrontUsers_model','fum');
        $this->properties = $this->fum->getPropertyIDs($this->property_id);
    }

    public function index(){
        if(has_permission('show')){
            $data['front_users'] = $this->fum->getUsers($this->properties,$this->property_id);
            $data['tags'] = $this->fum->getTags($this->property_id);
            $this->template->load('master','frontUsers/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function show($token){
        if(has_permission('show')){

        }
        else{
            $this->wrongState();
        }
    }

    public function changeTags($user_token){
        if(has_permission('edit')){
            $post = $this->input->post();

            $insertData = array();

            if(isset($post['data']) && !empty($post['data'])){
                $tags = json_decode($post['data'],true);
                if(!empty($tags)){
                    foreach($tags as $tag){
                        $insertData[] = array(
                            'user_token' => $user_token,
                            'property_id' => $this->property_id,
                            'tag_id' => $tag
                        );
                    }
                }
            }

            $this->fum->deleteUserTags($this->property_id,$user_token);
            if(!empty($insertData)){
                $this->fum->insertUserTagsBatch($insertData);
            }

            echo json_encode(array('status' => 1));
        }
    }


}