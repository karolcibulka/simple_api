<?php


class ApiKeys extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ApiKeys_model','akm');

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index(){
        if(has_permission('show')){
            $data['api_keys'] = $this->akm->getApiKeys();
            $data['groups'] = $this->akm->getGroups();
            $this->template->load('master','apiKeys/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){
            $this->template->load('master','apiKeys/create');
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){
            $post = $this->input->post();
            $post['api_key'] = generateToken(50);
            $this->akm->storeApiKey($post);
            $this->deleteCacheItem('api_keys');
            set_message('successMessage','Záznam bol úspešne uložený!');
            redirect('dashboard/apiKeys');
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){
            $data['id'] = $id;
            $data['api_key'] = $this->akm->getApiKey($id);
            $this->template->load('master','apiKeys/create',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id){
        if(has_permission('edit')){
            $post = $this->input->post();
            $this->akm->updateApiKey($id,$post);
            $this->deleteCacheItem('api_keys');
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/apiKeys');
        }
        else{
            $this->wrongState();
        }
    }

    public function delete($id){
        if(has_permission('delete')){
            $this->akm->updateApiKey($id,array('deleted'=>1));
            $this->deleteCacheItem('api_keys');
            set_message('successMessage','Záznam bol úspešne zmazaný!');
            redirect('dashboard/apiKeys');
        }
        else{
            $this->wrongState();
        }
    }

    public function setActivity($id,$state){
        if(has_permission('edit')){
            $negation = $state == '1' ? 0 : 1;
            $this->akm->updateApiKey($id,array('active'=>$negation));
            $this->deleteCacheItem('api_keys');
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/apiKeys');

        }
        else{
            $this->wrongState();
        }
    }

    public function setPermission($id){
        if(has_permission('edit')){
            $post = $this->input->post();

            $this->akm->deleteApiKeyGroups($id);
            if(isset($post['val']) && !empty($post['val'])){
                $insert_data = array();

                foreach($post['val'] as $val){
                    $insert_data[] = array(
                        'api_key_id' => $id,
                        'api_group_id' => $val
                    );
                }

                if(!empty($insert_data)){
                    $this->akm->insertApiKeyGroupsBatch($insert_data);
                }
            }

            $this->deleteCacheItem('api_keys');
            echo json_encode(array('status' => 1));
        }
    }

}