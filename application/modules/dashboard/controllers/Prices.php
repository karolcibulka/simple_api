<?php

class Prices extends DASH_Controller{

    public function __construct()
    {
        parent::__construct();
    }


    public function index(){
        if(has_permission('show')){
            _view('show',array());
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){
            _view('create',array());
        }
        else{
            $this->wrongState();
        }
    }
}