<?php 

class Products extends DASH_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Products_model', 'products_model');
    }

    public function index(){
        if(has_permission('show')){
            $data['products'] = $this->products_model->getProducts($this->property_id);

            _view('show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){

            $data['categories'] = $this->products_model->getCategories($this->property_id);

            _view('create',$data);
        }
        else{
            $this->wrongState();
        }
    }


    public function createProcess(){
        if(has_permission('create')){
            if($post = $this->input->post()){

                $insert_data = array(
                    'property_id' => $this->property_id,
                    'internal_name' => $post['internal_name'],
                    'quantity' => isset($post['quantity']) && !empty($post['quantity']) ? $post['quantity'] : null,
                    'min' => $post['min'],
                    'max' => $post['max'],
                    'step' => $post['step'],
                );

                if($id = $this->products_model->insertProduct($insert_data)){
                   $categories = array();

                   if(isset($post['category']) && !empty($post['category'])){
                       foreach($post['category'] as $category_id) {
                           $categories[] = array(
                               'product_id' => $id,
                               'category_id' => $category_id
                           );
                       }
                   }

                   if(!empty($categories)){
                       $this->products_model->insertCategories($categories);
                   }
                }

                set_message('successMessage','Záznam bol úspešne vytvorený!');
                _return();

            }
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id){
        if(has_permission('edit')){

            if(!$data['product'] = $this->products_model->getProduct($this->property_id,$id)){
                $this->wrongState();
            }

            $data['id'] = $id;
            $data['categories'] = $this->products_model->getCategories($this->property_id);

            _view('edit',$data);
        }
        else{
            $this->wrongState();
        }

    }

    public function editProcess($id){
        if(has_permission('edit')){
            if($post = $this->input->post()){

                if(!$product = $this->products_model->getProduct($this->property_id,$id)){
                    $this->wrongState();
                }

                $update_data = array(
                    'internal_name' => $post['internal_name'],
                    'quantity' => isset($post['quantity']) && !empty($post['quantity']) ? $post['quantity'] : null,
                    'min' => $post['min'],
                    'max' => $post['max'],
                    'step' => $post['step'],
                );

                $categories = array();

                $this->products_model->removeCategories($id);
                if(isset($post['category']) && !empty($post['category'])){
                    foreach($post['category'] as $category_id) {
                        $categories[] = array(
                            'product_id' => $id,
                            'category_id' => $category_id
                        );
                    }
                }

                if(!empty($categories)){
                    $this->products_model->insertCategories($categories);
                }

                $this->products_model->updateProduct($this->property_id,$id,$update_data);

                set_message('successMessage','Záznam bol úspešne upravený!');
                _return();
            }
        }
        else{
            $this->wrongState();
        }
    }

    public function active($id,$active){
        if(has_permission('edit')){
            $active = !empty($active) ? 0 : 1;

            $this->products_model->updateProduct($this->property_id,$id,array( 'active'=> $active ));

            set_message('successMessage','Záznam bol úspešne upravený!');
            _return();
        }
        else{
            $this->wrongState();
        }
    }

    public function delete($id){
        if(has_permission('delete')){

            $this->products_model->updateProduct($this->property_id,$id,array('deleted'=>1));

            set_message('successMessage','Záznam bol úspešne zmazaný!');
            _return();
        }
        else{
            $this->wrongState();
        }
    }


}