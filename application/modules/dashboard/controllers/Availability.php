<?php

class Availability extends DASH_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        if(has_permission('show')){
            $data = array();
            _view('show',$data);
        }
        else{
            $this->wrongState();
        }
    }
}