<?php

class User extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user_model');

    }

    public function changePassword()
    {
        $this->template->load('master','user/show');
    }

    public function changePasswordPost(){
        $postData = $this->input->post();

        if(isset($postData['new_password']) && !empty($postData['new_password']) && isset($postData['new_password_confirm']) && $postData['new_password_confirm']){
            if($postData['new_password'] == $postData['new_password_confirm']){
                $updateData = array(
                    'password' => $this->bcrypt->hash($postData['new_password']),
                );
                $this->user_model->updatePassword($this->session->userdata('user_id'),$updateData);
                redirect(base_url('/dashboard'));
            }
            else{
                set_message('changePasswordError','Vaše heslá sa nezhodujú');
                redirect(base_url('/dashboard/user/changePassword'));
            }
        }

        set_message('changePasswordError','Musíte vyplniť údaje!');
        redirect(base_url('/dashboard/user/changePassword'));
    }

    public function changeLanguage($language){

        if($this->global_model->updateUser($this->user_id,array('lang'=>$language))){
            $this->session->set_userdata('user_lang',$language);
            set_message('successMessage','Jazyk bol úspešne zmenený!');
        }
       
        redirect($_SERVER['HTTP_REFERER'],'redirect',301);
    }
}