<?php


class ApiPermissions extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('api_model','api_model');
        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index(){
        if(has_permission('show')){
            $data['groups'] = $this->api_model->getApiPermissions();
            $this->template->load('master','apiPermissions/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function edit($id,$type = 'group'){
        if(has_permission('edit')){
            $data['id'] = $id;
            $data['type'] = $type;
            if($type === 'group'){
                $data['data'] = $this->api_model->getApiGroup($id);
            }
            else{
                $data['data'] = $this->api_model->getController($id);
            }
            $this->template->load('master','apiPermissions/edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($id,$type = 'group'){
        if(has_permission('edit')){
            $post = $this->input->post();

            if($type === 'group') {
                $this->api_model->updateGroup($id, $post);
            }
            else{
                $this->api_model->updateController($id,$post);
            }

            $this->deleteCacheItem('api_permissions');
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/apiPermissions');
        }
        else{
            $this->wrongState();
        }
    }


    public function createNewController(){
        if(has_permission('create')){
            $post = $this->input->post();
            $this->api_model->storeController($post);
            $this->deleteCacheItem('api_permissions');
            set_message('successMessage','Záznam bol úspešne uložený!');
            redirect('dashboard/apiPermissions');
        }
        else{
            $this->wrongState();
        }
    }

    public function createNewGroup(){
        if(has_permission('create')){
            $post = $this->input->post();
            if($id = $this->api_model->storeGroup($post)) {
                $this->api_model->updateGroup($id,array('order'=>$id));
            }
            $this->deleteCacheItem('api_permissions');
            set_message('successMessage','Záznam bol úspešne uložený!');
            redirect('dashboard/apiPermissions');
        }
        else{
            $this->wrongState();
        }
    }

    public function handleEditPermission(){
        if(has_permission('edit')){
            $post = $this->input->post();

            //pre_r($post);exit;
            $data = array(
                'api_group_id' => $post['data']['gid'],
                'api_controller_id' => $post['data']['cid'],
                $post['data']['method'] => $post['checkbox'] == 'true' ? 1 : 0
            );

           $this->deleteCacheItem('api_permissions');
           $this->api_model->insertOrUpdatePermission($data,$post['data']['method']);
        }
    }

    public function delete($id,$type = 'group'){
        if(has_permission('delete')){
            if($type === 'group'){
                $this->api_model->updateGroup($id,array('deleted'=>1));
            }
            else{
                $this->api_model->updateController($id,array('deleted'=>1));
            }

            $this->deleteCacheItem('api_permissions');
            set_message('successMessage','Záznam bol úspešne zmazaný!');
            redirect('dashboard/apiPermissions');
        }
        else{
            $this->wrongState();
        }
    }

    public function handleChangeGroupOrder(){
        if(has_permission('edit')){
            $post = $this->input->post();

            if(isset($post['json']) && !empty($post['json'])){
                foreach($post['json'] as $order => $item){
                    $this->api_model->updateGroup($item['id'],array('order'=>$order));
                }
            }
        }
    }


}