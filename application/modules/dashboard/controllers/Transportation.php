<?php


class Transportation extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Transportation_model', 'transportation_model');
        $this->load->model('Geography_model', 'geo_model');
        $this->load->library('FrontCache',array('property_id'=>$this->property_id,'type'=>'transportation'),'frontCache');
    }

    public function index()
    {
        if (has_permission('show')) {
            $data['transportations'] = $this->transportation_model->getTransportations($this->property_id);
            _view('show', $data);
        } 
        else {
            $this->wrongState();
        }
    }

    public function create()
    {
        if (has_permission('create')) {
            $data = array();
            _view('create',$data);
        } 
        else {
            $this->wrongState();
        }
    }

    public function createProcess()
    {
        $postData = $this->input->post();

        if (isset($postData['internal_name']) && !empty($postData['internal_name'])) {
            $insertData = array(
                'property_id' => $this->property_id,
                'internal_name' => $postData['internal_name'],
                'external_id' => $postData['external_id'],
                'external_plu_id' => $postData['external_plu_id'],
                'price' => isset($postData['price']) && !empty($postData['price']) ? $postData['price'] : '0,0',
                'is_delivery' => $postData['is_delivery'],
                'on_room' => $postData['on_room'],
                'estimated_time' => $postData['estimated_time'],
                'estimated_date' => $postData['estimated_date'],
                'min_days' => $postData['min_days'],
                'max_days' => $postData['max_days'],
                'visible_from' => $postData['visible_from'],
                'order' => '99999',
                'cities' => isset($postData['cities']) && !empty($postData['cities']) ? json_encode($postData['cities']) : null,
                'active' => '1',
                'deleted' => '0',
            );

            if ($transportationID = $this->transportation_model->insertTransportation($insertData)) {
                if (isset($postData['name']) && !empty($postData['name'])) {
                    foreach ($postData['name'] as $lang => $name) {
                        if (empty($postData['name'][$lang])) continue;
                        $inserLangData = array(
                            'transportation_id' => $transportationID,
                            'property_id' => $this->property_id,
                            'lang' => $lang,
                            'name' => $postData['name'][$lang],
                            'short_description' => $postData['short_description'][$lang],
                        );

                        $this->transportation_model->insertLang($inserLangData);
                    }

                    $this->frontCache->deleteCache();
                    set_message('successMessage','Záznam bol úspešne vytvorený!');
                    redirect('dashboard/transportation');
                } else {
                    redirect('dashboard/transportation/create');
                }
            } else {
                redirect('dashboard/transportation/create');
            }
        } else {
            redirect('dashboard/transportation/create');
        }
    }

    public function edit($id)
    {
        if (has_permission('create')) {
            $data['title']              = 'Úprava typu dopravy';
            $data['externalOrderTypes'] = $this->transportation_model->getExternalOrderTypes($this->property_id);
            $data['cities']             = $this->geo_model->getCities();
            $data['plus']               = $this->transportation_model->getPlus($this->property_id);
            if (!$data['transportation'] = $this->transportation_model->getTransportation($id, $this->property_id)) {
                redirect('dashboard/transportation/edit/' . $id);
            }
            if (isset($data['transportation']['cities']) && !empty($data['transportation']['cities'])) {
                $data['transportation']['cities'] = json_decode($data['transportation']['cities'], true);
            }

            $this->template->load('master', 'transportation/edit', $data);
        } else {
            $this->wrongState();
        }
    }

    public function editProcess($id)
    {
        $postData = $this->input->post();

        if (isset($postData['internal_name']) && !empty($postData['internal_name'])) {
            $updateData = array(
                'internal_name' => $postData['internal_name'],
                'external_id' => $postData['external_id'],
                'external_plu_id' => $postData['external_plu_id'],
                'on_room' => $postData['on_room'],
                'estimated_time' => $postData['estimated_time'],
                'estimated_date' => $postData['estimated_date'],
                'min_days' => $postData['min_days'],
                'max_days' => $postData['max_days'],
                'visible_from' => $postData['visible_from'],
                'price' => isset($postData['price']) && !empty($postData['price']) ? $postData['price'] : '0,0',
                'is_delivery' => $postData['is_delivery'],
                'cities' => isset($postData['cities']) && !empty($postData['cities']) ? json_encode($postData['cities']) : null,
            );

            $this->transportation_model->update($this->property_id, $id, $updateData);

            $this->transportation_model->removeLangs($this->property_id, $id);
            if (isset($postData['name']) && !empty($postData['name'])) {
                foreach ($postData['name'] as $lang => $name) {
                    if (empty($postData['name'][$lang])) continue;
                    $insertLangData = array(
                        'transportation_id' => $id,
                        'property_id' => $this->property_id,
                        'lang' => $lang,
                        'name' => $postData['name'][$lang],
                        'short_description' => $postData['short_description'][$lang],
                    );

                    $this->transportation_model->insertLang($insertLangData);
                }

                $this->frontCache->deleteCache();
                set_message('successMessage','Záznam bol úspešne upravený!');
                redirect('dashboard/transportation');
            } else {
                redirect('dashboard/transportation/create');
            }
        } else {
            redirect('dashboard/transportation/create');
        }
    }

    public function delete($id)
    {
        $this->transportation_model->update($this->property_id, $id, array('deleted' => '1'));
        $this->frontCache->deleteCache();
        set_message('successMessage','Záznam bol úspešne zmazaný!');
        redirect('dashboard/transportation');
    }

    public function active($id, $val)
    {
        $this->transportation_model->update($this->property_id, $id, array('active' => $val));
        $this->frontCache->deleteCache();
        set_message('successMessage','Záznam bol úspešne upravený!');
        redirect('dashboard/transportation');
    }

    public function getCitiesForRegion($regionCode)
    {
        $cities = $this->geo_model->getCitiesByCode($regionCode);
        echo json_encode(array('status' => '1', 'cities' => $cities));
    }

    public function order()
    {
        $postData = $this->input->post();
        if (!empty($postData['serialized'])) {
            foreach ($postData['serialized'] as $order => $item) {
                $this->transportation_model->update($this->property_id, $item['id'], array('order' => $order));
            }
        }
        $this->frontCache->deleteCache();
    }

}