<?php


class DeveloperAuth extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('DeveloperAuth_model','dam');

        if(!isDeveloper()){
            $this->wrongState();
        }
    }

    public function index(){
        if(has_permission('show')){
            $data['users'] = $this->dam->getUsers();
            $data['groups'] = $this->dam->getGroups();
            $data['properties'] = $this->dam->getProperties();
            $this->template->load('master','developerAuth/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function create(){
        if(has_permission('create')){

            $data['errors'] = array();

            if($post = $this->input->post()) {
                if(isset($post['email']) && !empty($post['email'])){
                    if($this->dam->validateEmail($post['email'])){
                        $user_data = array(
                            'first_name' => $post['first_name'],
                            'last_name' => $post['last_name'],
                            'email' => $post['email'],
                            'password' => $this->bcrypt->hash($post['password']),
                            'username' => $post['email'],
                            'active' => 1,
                            'lang' => 'sk',
                            'created_on' => strtotime(date('Y-m-d H:i:s')),
                        );

                        if($user_id = $this->dam->storeUser($user_data)){
                            if(isset($post['groups'])){
                                $this->dam->updateUser($user_id,'groups',$post['groups']);
                            }

                            $properties = isset($post['properties']) && !empty($post['properties']) ? $post['properties'] : array();

                            $created_property_id = null;

                            if(isset($post['create_property']) && !empty($post['create_property'])){
                                $propertyData = array(
                                    'version' => '1',
                                    'program_id' => '1',
                                    'name' => 'AG:'.$post['email'],
                                    'active' => '1',
                                    'deleted' => '0',
                                    'selling' => '0'
                                );

                                if($created_property_id = $this->dam->storeProperty($propertyData)){
                                    $properties[] = $created_property_id;
                                }
                            }

                            if(!empty($properties)){
                                $this->dam->updateUser($user_id,'properties',$properties);
                            }

                            if($created_property_id){
                                $this->session->userdata('active_property',$created_property_id);
                                redirect('dashboard/properties/edit/'.$created_property_id);
                            }
                        }
                    }
                    else{
                        $data['errors'][] = 'Užívateľ s daným emailom existuje';
                    }
                }
            }

            $data['post'] = $post;
            $data['groups'] = $this->dam->getGroups();
            $data['properties'] = $this->dam->getProperties();

            $this->template->load('master','developerAuth/create',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function createProcess(){
        if(has_permission('create')){

        }
        else{
            $this->wrongState();
        }
    }

    public function developer($user_id,$state){
        if(has_permission('edit')){
            $this->dam->updateUserData($user_id,array('is_developer'=>$state));
            $this->deleteCacheItem('developers');
            set_message('successMessage','Záznam bol úspešne upravený!');
            redirect('dashboard/developerAuth');
        }
        else{
            $this->wrongState();
        }
    }

    public function changeProps(){
        if(has_permission('edit')){
            $post = $this->input->post();

            $this->dam->updateUser($post['user_id'],$post['type'],$post['val']);

            echo json_encode(array('status'=>1));
        }
        else{
            $this->wrongState();
        }
    }

}