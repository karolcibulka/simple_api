<?php

require APPPATH . '/libraries/REST_Controller.php';
/**
 *
 *                         * ************** for Controllers *****************
 *============ Codeigniter Core System ================
 * @property CI_Benchmark $benchmark              Benchmarks
 * @property CI_Config $config                    This class contains functions that enable config files to be managed
 * @property CI_Controller $controller            This class object is the super class that every library in.
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property CI_Hooks $hooks                      Provides a mechanism to extend the base system without hacking.
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Loader $load                      Loads views and files
 * @property CI_Log $log                          Logging Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Profiler $profiler                Display benchmark results, queries you have run, etc
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_URI $uri                          Retrieve information from URI strings
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 *
 *
 * @property CI_Model $model                      Codeigniter Model Class
 *
 * @property CI_Driver $driver                    Codeigniter Drivers
 *
 *
 *============ Codeigniter Libraries ================
 *
 * @property CI_Cache $cache                      Caching
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encryption $encryption            The Encryption Library provides two-way data encryption.
 * @property CI_Upload $upload                    File Uploading class
 * @property CI_Form_validation $form_validation  Form Validation class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Migration $migration              Tracks & saves updates to database structure
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Template parser
 * @property CI_Security $security                Processing input data for security.
 * @property CI_Session $session                  Session Class
 * @property CI_Table $table                      HTML table generation
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 *
 *
 *                          *============ Database Libraries ================
 *
 *
 * @property CI_DB_query_builder $db   Database
 * @property CI_DB_forge $dbforge     Database
 * @property CI_DB_result $result                 Database
 *
 *
 *
 *
 *                            *============ Codeigniter Depracated  Libraries ================
 *
 * @property CI_Javascript $javascript            Javascript (not supported
 * @property CI_Jquery $jquery                    Jquery (not supported)
 * @property CI_Encrypt $encrypt                  Its included but move over to new Encryption Library
 *
 *            ======= MY PROPERTIES =======
 * @property Template $template                  Its included but move over to new Encryption Library
 * @property Permission_model $permission_model                  Its included but move over to new Encryption Library
 * @property loglib $loglib                  Its included but move over to new Encryption Library
 * @property Global_model $global_model                  Its included but move over to new Encryption Library
 * @property Ion_auth_model $ion_auth_model                  Its included but move over to new Encryption Library
 * @property Navigation_model $navigation_model                  Its included but move over to new Encryption Library
 * @property PropertySettingsWeb_model $psw_model                  Its included but move over to new Encryption Library
 * @property Dashboard_model $dashboard_model                  Its included but move over to new Encryption Library
 * @property Ion_auth $ion_auth                  Its included but move over to new Encryption Library
 * @property User_model $user_model                  Its included but move over to new Encryption Library
 * @property Banners_model $banners_model                  Its included but move over to new Encryption Library
 * @property FrontCache $frontCache                  Its included but move over to new Encryption Library
 * @property Allergens_model $allergens_model                  Its included but move over to new Encryption Library
 * @property Transportation_model $transportation_model                  Its included but move over to new Encryption Library
 * @property Geography_model $geo_model                  Its included but move over to new Encryption Library
 * @property Bcrypt $bcrypt                  Its included but move over to new Encryption Library
 * @property Upsells_model $upsells_model                  Its included but move over to new Encryption Library
 * @property BlueGastro $blueGastro                  Its included but move over to new Encryption Library
 * @property ExternalSystem_model $es_model                  Its included but move over to new Encryption Library
 * @property Reservations_model $reservations_model                  Its included but move over to new Encryption Library
 * @property PropertySettingsWeb_model $psw_model                  Its included but move over to new Encryption Library
 * @property Curl $curl                  Its included but move over to new Encryption Library
 * @property Properties_model $propertiesModel                  Its included but move over to new Encryption Library
 * @property Program_model $program_model                  Its included but move over to new Encryption Library
 * @property Payments_model $payments_model                  Its included but move over to new Encryption Library
 * @property Packing_model $packing_model                  Its included but move over to new Encryption Library
 * @property Offers_model $offers_model                  Its included but move over to new Encryption Library
 * @property Notifications_model $notifications_model                  Its included but move over to new Encryption Library
 * @property Newsletter_model $newsletter_model                  Its included but move over to new Encryption Library
 * @property Navigationfront_model $navigationfront_model                  Its included but move over to new Encryption Library
 * @property Log_model $log_model                  Its included but move over to new Encryption Library
 * @property DailyMenu_model $dailyMenu_model                  Its included but move over to new Encryption Library
 * @property Ciqrcode $ciqrcode                  Its included but move over to new Encryption Library
 * @property Tags_model $tm                  Its included but move over to new Encryption Library
 * @property FrontUsers_model $fum                  Its included but move over to new Encryption Library
 * @property Feeds_model $fm                  Its included but move over to new Encryption Library
 * @property Tutorials_model $tum                  Its included but move over to new Encryption Library
 * @property Preparation_model $preparation_model                  Its included but move over to new Encryption Library
 * @property Reviews_model $reviews_model                  Its included but move over to new Encryption Library
 * @property Rates_model $rates_model                  Its included but move over to new Encryption Library
 * @property customAuth_model $custom_auth_model                  Its included but move over to new Encryption Library
 * @property DeveloperAuth_model $dam                  Its included but move over to new Encryption Library
 * @property ApiKeys_model $akm                  Its included but move over to new Encryption Library
 * @property Api_model $api_model                  Its included but move over to new Encryption Library
 * @property Products_model $products_model                  Its included but move over to new Encryption Library
 * @property Uploader $uploader                  Its included but move over to new Encryption Library
 * @property PropertySettings_model $psm_model                  Its included but move over to new Encryption Library
 * @property Categories_model $categories_model                  Its included but move over to new Encryption Library
 * @property Variations_model $variations_model                  Its included but move over to new Encryption Library
 * @property Facilities_model $facilities_model                  Its included but move over to new Encryption Library
 * @property Variabilities_model $variabilities_model                  Its included but move over to new Encryption Library
 *
 *
 */

class MY_Controller extends CI_Controller
{

    private $defaultLanguage = NULL;


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'language','form','typography','app', 'permission'));

        $this->lang->load(array('global'),$this->config->item('language'));

    
        $this->load->driver('cache',
            array(
                'driver'=>'file',
                'backup'=>'file'
            )
        );

        $this->load->database();


    }
}

class DASH_Controller extends MY_Controller
{

    protected $property_id = null;
    protected $language;
    protected $languages;
    protected $user_id;
    protected $property_data;
    protected $cache_store_length;
    protected $user_permissions;
    protected $project_name;
    protected $developers;

    public function __construct()
    {
        parent::__construct();

        $this->project_name = 'Warehouse';

        $this->load->library(array('ion_auth', 'form_validation','curl','loglib'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->model('Permission_model','permission_model');
        $this->load->model('Ion_auth_model','ion_auth_model');
        $this->load->model('Global_model','global_model');
        $this->load->model('Navigation_model','navigation_model');
        //$this->lang->load(array('global', 'auth'),$this->config->item('language'));

        $this->cache_store_length = 3600*24*31;

        if(isset($_GET['deleteCache'])){
            $this->cache->file->clean();
            set_message('successMessage','Vyrovnávacia pamäť úspešne premazaná!');
        }

        if(!$this->ion_auth->logged_in()){
            redirect('dashboard/auth/login');
        }

        $this->loglib->storeLogMyController();

        if(!$this->developers = $this->cache->file->get('developers')){
            $this->developers = $this->global_model->getDevelopers();
            $this->cache->file->save('developers',$this->developers,$this->cache_store_length);
        }

        if($this->session->has_userdata('user_id')){
            $this->user_id = $this->session->userdata('user_id');

            if($userProperties = $this->ion_auth_model->getUserProperties($this->user_id)) {
                $this->template->set('properties_ci', $userProperties);
                $properties = array_values($userProperties);
                if((!$this->session->has_userdata('active_property')) || (!isset($userProperties[$this->session->userdata('active_property')]) || empty($userProperties[$this->session->userdata('active_property')]))) {
                    $this->session->set_userdata('active_property', $properties[0]['id']);
                }
            }
        }


        if(!$this->session->has_userdata('user_lang')){
            $this->session->set_userdata('user_lang',$this->global_model->getUserLang($this->user_id));
        }

        $this->lang->load(array('global'),getLangFromCode($this->session->userdata('user_lang')));

        if(empty($userProperties)){
            $this->template->set('has_no_property',true);
        }

        if($this->session->has_userdata('active_property')){
            $this->property_id = $this->session->userdata('active_property');
        }

        $this->load->library('Uploader',$this->property_id,'uploader');


        if(!$this->property_data = $this->cache->file->get('property_ci_'.$this->property_id)){
            $this->property_data = $this->global_model->getProperty($this->property_id);
            $this->cache->file->save('property_ci_'.$this->property_id,$this->property_data,$this->cache_store_length);
        }

        if(!$navigation = $this->cache->file->get('navigation')){
            $navigation = $this->navigation_model->getNavItems();
            $this->cache->file->save('navigation',$navigation,$this->cache_store_length);
        }

        if(!$permissions = $this->cache->file->get('permissions')){
            $permissions = $this->permission_model->getPermissions();
            $this->cache->file->save('permissions',$permissions,$this->cache_store_length);
        }

        if(!$currencies = $this->cache->file->get('currencies')){
            $currencies = $this->global_model->getCurrencies();
            $this->cache->file->save('currencies',$currencies,$this->cache_store_length);
        }


        if(!$property_program = $this->cache->file->get('property_program_ci')){
            $property_program = $this->permission_model->getPrograms();
            $this->cache->file->save('property_program_ci',$property_program,$this->cache_store_length);
        }

        if(isset($this->property_data['program_id']) && !empty($this->property_data['program_id'])){
            if(isset($property_program[$this->property_data['program_id']]) && !empty($property_program[$this->property_data['program_id']])){
                $property_program = $property_program[$this->property_data['program_id']];
            }
        }

        if(!$this->programs = $this->cache->file->get('programs')){
            $this->programs = $this->permission_model->getFullPrograms();
            $this->cache->file->save('programs',$this->programs,$this->cache_store_length);
        }


        $user_groups = $this->permission_model->getUserGroup($this->user_id);

        $this->setUserPermissions($user_groups,$property_program,$permissions);
        $this->load->helper('cookie');

        $this->template->set('property_data_ci',$this->property_data);
        $this->template->set('programs_ci',$this->programs);
        $this->template->set('property_id_ci',$this->property_id);
        $this->template->set('projectName',$this->project_name);
        $this->template->set('navs',$this->getNavigation($navigation));
        $this->template->set('property_languages',isset($this->property_data['public_languages']) && !empty($this->property_data['public_languages']) ? $this->property_data['public_languages'] : array());
        $this->template->set('full_size_menu',get_cookie('expanded'));
        $this->template->set('active_dark_mode',get_cookie('active_dark_mode'));

        

    }

    public function loadView($view,$data = array()){
        $this->template->load('master',$this->uri->segment(2).'/'.$view,$data);
    }

    public function deleteCacheItem($item){
        $this->cache->file->delete($item);
    }

    public function getNavigation($navigation){
        if(!empty($navigation)){
            foreach($navigation as $key => $nav_item){
                if(isset($nav_item['children']) && !empty($nav_item['children'])){
                    foreach($nav_item['children'] as $key_child => $children){
                        if($children['type'] === 'controller' && !has_permission('show',$children['path'])){
                            unset($navigation[$key]['children'][$key_child]);
                        }
                    }
                }
                else{
                    if($nav_item['type'] === 'controller' && !has_permission('show',$nav_item['path'])){
                        unset($navigation[$key]);
                    }
                }
            }
        }

        if(!empty($navigation)){
            foreach($navigation as $key => $nav_item){
                if(isset($nav_item['children']) && empty($nav_item['children']) && $nav_item['type'] === 'placeholder'){
                    unset($navigation[$key]);
                }
            }
        }

        return $navigation;
    }

    public function setUserPermissions($user_groups,$property_program,$permissions){
        $user_permissions = array();

        if(!empty($permissions)){
            foreach($permissions as $user_group_id => $permission){
                if(in_array($user_group_id,$user_groups)){
                    foreach($permission as $controller => $p){
                        $user_permissions[$controller]['show'] = isset($user_permissions[$controller]['show']) && !empty($user_permissions[$controller]['show']) ? '1' : $p['show'];
                        $user_permissions[$controller]['edit'] = isset($user_permissions[$controller]['edit']) && !empty($user_permissions[$controller]['edit']) ? '1' : $p['edit'];
                        $user_permissions[$controller]['create'] = isset($user_permissions[$controller]['create']) && !empty($user_permissions[$controller]['create']) ? '1' : $p['create'];
                        $user_permissions[$controller]['delete'] = isset($user_permissions[$controller]['delete']) && !empty($user_permissions[$controller]['show']) ? '1' : $p['delete'];
                        $user_permissions[$controller]['controller_id'] = $p['controller_id'];
                    }
                }
            }
        }

        if(!empty($property_program) && !empty($user_permissions)){
            foreach($user_permissions as $controller => $user_permission){
                if(!in_array($user_permission['controller_id'],$property_program)){
                    $user_permissions[$controller]['show'] = 0;
                    $user_permissions[$controller]['edit'] = 0;
                    $user_permissions[$controller]['create'] = 0;
                    $user_permissions[$controller]['delete'] = 0;
                }
            }
        }

        $this->session->set_userdata('permissions',$user_permissions);
    }

    public function wrongState(){
        set_message('warning',lang('message.warning'));
        redirect(base_url());
    }
}

/**
 *
 *                         * ************** for Controllers *****************
 *============ Codeigniter Core System ================
 * @property CI_Benchmark $benchmark              Benchmarks
 * @property CI_Config $config                    This class contains functions that enable config files to be managed
 * @property CI_Controller $controller            This class object is the super class that every library in.
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property CI_Hooks $hooks                      Provides a mechanism to extend the base system without hacking.
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Loader $load                      Loads views and files
 * @property CI_Log $log                          Logging Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Profiler $profiler                Display benchmark results, queries you have run, etc
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_URI $uri                          Retrieve information from URI strings
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 *
 *
 * @property CI_Model $model                      Codeigniter Model Class
 *
 * @property CI_Driver $driver                    Codeigniter Drivers
 *
 *
 *============ Codeigniter Libraries ================
 *
 * @property CI_Cache $cache                      Caching
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encryption $encryption            The Encryption Library provides two-way data encryption.
 * @property CI_Upload $upload                    File Uploading class
 * @property CI_Form_validation $form_validation  Form Validation class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Migration $migration              Tracks & saves updates to database structure
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Template parser
 * @property CI_Security $security                Processing input data for security.
 * @property CI_Session $session                  Session Class
 * @property CI_Table $table                      HTML table generation
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 *
 *
 *                          *============ Database Libraries ================
 *
 *
 * @property CI_DB_query_builder $db   Database
 * @property CI_DB_forge $dbforge     Database
 * @property CI_DB_result $result                 Database
 *
 *
 *
 *
 *                            *============ Codeigniter Depracated  Libraries ================
 *
 * @property CI_Javascript $javascript            Javascript (not supported
 * @property CI_Jquery $jquery                    Jquery (not supported)
 * @property CI_Encrypt $encrypt                  Its included but move over to new Encryption Library
 *
 *            ======= MY PROPERTIES =======
 * @property Template $template                  Its included but move over to new Encryption Library
 * @property Permission_model $permission_model                  Its included but move over to new Encryption Library
 * @property loglib $loglib                  Its included but move over to new Encryption Library
 * @property Api_model $api_model                  Its included but move over to new Encryption Library
 * @property Property_model $property_model                  Its included but move over to new Encryption Library
 * @property Requester $requester                Its included but move over to new Encryption Library
 * @property ApiUser_model $user_model                  Its included but move over to new Encryption Library
 * @property Notification $notification                  Its included but move over to new Encryption Library
 * @property Responser $responser                  Its included but move over to new Encryption Library
 *
 *
 */
class Api_Controller extends REST_Controller
{
    protected $permissions;
    protected $api_keys;
    protected $api_key;
    protected $api_cache_store_length;

    public function __construct()
    {
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: API-KEY, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Cache-Control: no-cache, must-revalidate");

        if($_SERVER['REQUEST_METHOD'] === "OPTIONS") {
            die();
        }

        $this->load->driver('cache',
            array(
                'driver'=>'file',
                'backup'=>'file'
            )
        );

        $this->api_cache_store_length = 3600*24*31;

        $this->load->helper(array('url', 'language','form','typography','app', 'api_permission', 'rest'));
        $this->load->library('loglib');
        $this->load->model('Api_model','api_model');

        if(!$this->api_keys = $this->cache->file->get('api_keys')){
            $this->api_keys = $this->api_model->getApiKeys();
            $this->cache->file->save('api_keys',$this->api_keys,$this->api_cache_store_length);
        }

        if (!$this->validateApiKey($this->input->get_request_header('Api-Key'))) {
            $this->wrongState();
        }

        if(!$this->permissions = $this->cache->file->get('api_permissions')[$this->api_key]){
            $this->permissions = $this->set_permission();
        }

        $this->load->database();

    }

    public function missingParam($missing_param){
        $this->response(unsuccessResult('Missing param : '.$missing_param), 200);
    }

    public function missingParams($values = array()){
        if(!empty($values)){
            $segments = $this->uri->segment_array();
            $start = 5;
            $end = $start + count($values);

            $counter = 0;
            for($i = $start; $i < $end;$i++){
                if(!isset($segments[$i]) || empty($segments[$i])){
                    $this->missingParam($values[$counter]);
                }
                $counter++;
            }
        }
    }

    public function wrongState(){
        $this->response(array('error' => 403, 'Message' => 'Not authorized'), 403);
    }

    private function set_permission(){
        $api_key_permissions = array();

        if($permissions = $this->api_model->getPermissionsImportant($this->api_keys[$this->api_key])){
            foreach($permissions as $group_id => $permission){
                if(isset($permission) && !empty($permission)){
                    foreach($permission as $controller => $methods){
                        $api_key_permissions[$controller] = array(
                            'show' => isset($api_key_permissions[$controller]['show']) && !empty($api_key_permissions[$controller]['show']) ? '1' : $methods['show'],
                            'edit' => isset($api_key_permissions[$controller]['edit']) && !empty($api_key_permissions[$controller]['edit']) ? '1' : $methods['edit'],
                            'create' => isset($api_key_permissions[$controller]['create']) && !empty($api_key_permissions[$controller]['create']) ? '1' : $methods['create'],
                            'delete' => isset($api_key_permissions[$controller]['delete']) && !empty($api_key_permissions[$controller]['delete']) ? '1' : $methods['delete'],
                        );
                    }
                }
            }
        }

        $all_permissions = $this->cache->file->get('api_permissions');
        $all_permissions[$this->api_key] = $api_key_permissions;
        $this->cache->file->save('api_permissions',$all_permissions,$this->api_cache_store_length);

        return $api_key_permissions;
    }

    private function validateApiKey($api_key){
        if(!$api_key){
            return false;
        }

        $this->api_key = $api_key;
        return isset($this->api_keys[$this->api_key]) && !empty($this->api_keys[$this->api_key]);
    }
}