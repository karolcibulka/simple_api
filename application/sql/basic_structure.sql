# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: basic_structure
# Generation Time: 2020-03-25 09:38:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table controllers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `controllers`;

CREATE TABLE `controllers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `menu` varchar(30) DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `controllers` WRITE;
/*!40000 ALTER TABLE `controllers` DISABLE KEYS */;

INSERT INTO `controllers` (`id`, `name`, `menu`, `description`, `active`)
VALUES
	(1,'dashboard','controller','Dashboard',1),
	(2,'navigation','controller','Navigácia - dev ',1),
	(3,'auth','controller','Authorizácia - dev',1),
	(4,'permission','controller','Právomoci - dev',1),
	(8,'propertySettings','controller','Nastavenie zariadenia',1),
	(25,'email','controller','Emailové šablóny',1),
	(32,'customAuth','controller','Systémoví užívatelia',1),
	(33,'properties','controller','Zariadenia - dev',1);

/*!40000 ALTER TABLE `controllers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `iso` varchar(2) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `nicename` varchar(80) DEFAULT NULL,
  `iso3` varchar(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`)
VALUES
	(1,'AF','AFGHANISTAN','Afghanistan','AFG',4,93),
	(2,'AL','ALBANIA','Albania','ALB',8,355),
	(3,'DZ','ALGERIA','Algeria','DZA',12,213),
	(4,'AS','AMERICAN SAMOA','American Samoa','ASM',16,1684),
	(5,'AD','ANDORRA','Andorra','AND',20,376),
	(6,'AO','ANGOLA','Angola','AGO',24,244),
	(7,'AI','ANGUILLA','Anguilla','AIA',660,1264),
	(8,'AQ','ANTARCTICA','Antarctica',NULL,NULL,0),
	(9,'AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1268),
	(10,'AR','ARGENTINA','Argentina','ARG',32,54),
	(11,'AM','ARMENIA','Armenia','ARM',51,374),
	(12,'AW','ARUBA','Aruba','ABW',533,297),
	(13,'AU','AUSTRALIA','Australia','AUS',36,61),
	(14,'AT','AUSTRIA','Austria','AUT',40,43),
	(15,'AZ','AZERBAIJAN','Azerbaijan','AZE',31,994),
	(16,'BS','BAHAMAS','Bahamas','BHS',44,1242),
	(17,'BH','BAHRAIN','Bahrain','BHR',48,973),
	(18,'BD','BANGLADESH','Bangladesh','BGD',50,880),
	(19,'BB','BARBADOS','Barbados','BRB',52,1246),
	(20,'BY','BELARUS','Belarus','BLR',112,375),
	(21,'BE','BELGIUM','Belgium','BEL',56,32),
	(22,'BZ','BELIZE','Belize','BLZ',84,501),
	(23,'BJ','BENIN','Benin','BEN',204,229),
	(24,'BM','BERMUDA','Bermuda','BMU',60,1441),
	(25,'BT','BHUTAN','Bhutan','BTN',64,975),
	(26,'BO','BOLIVIA','Bolivia','BOL',68,591),
	(27,'BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,387),
	(28,'BW','BOTSWANA','Botswana','BWA',72,267),
	(29,'BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0),
	(30,'BR','BRAZIL','Brazil','BRA',76,55),
	(31,'IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,246),
	(32,'BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,673),
	(33,'BG','BULGARIA','Bulgaria','BGR',100,359),
	(34,'BF','BURKINA FASO','Burkina Faso','BFA',854,226),
	(35,'BI','BURUNDI','Burundi','BDI',108,257),
	(36,'KH','CAMBODIA','Cambodia','KHM',116,855),
	(37,'CM','CAMEROON','Cameroon','CMR',120,237),
	(38,'CA','CANADA','Canada','CAN',124,1),
	(39,'CV','CAPE VERDE','Cape Verde','CPV',132,238),
	(40,'KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,1345),
	(41,'CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,236),
	(42,'TD','CHAD','Chad','TCD',148,235),
	(43,'CL','CHILE','Chile','CHL',152,56),
	(44,'CN','CHINA','China','CHN',156,86),
	(45,'CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,61),
	(46,'CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,672),
	(47,'CO','COLOMBIA','Colombia','COL',170,57),
	(48,'KM','COMOROS','Comoros','COM',174,269),
	(49,'CG','CONGO','Congo','COG',178,242),
	(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,242),
	(51,'CK','COOK ISLANDS','Cook Islands','COK',184,682),
	(52,'CR','COSTA RICA','Costa Rica','CRI',188,506),
	(53,'CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,225),
	(54,'HR','CROATIA','Croatia','HRV',191,385),
	(55,'CU','CUBA','Cuba','CUB',192,53),
	(56,'CY','CYPRUS','Cyprus','CYP',196,357),
	(57,'CZ','CZECH REPUBLIC','Czech Republic','CZE',203,420),
	(58,'DK','DENMARK','Denmark','DNK',208,45),
	(59,'DJ','DJIBOUTI','Djibouti','DJI',262,253),
	(60,'DM','DOMINICA','Dominica','DMA',212,1767),
	(61,'DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,1809),
	(62,'EC','ECUADOR','Ecuador','ECU',218,593),
	(63,'EG','EGYPT','Egypt','EGY',818,20),
	(64,'SV','EL SALVADOR','El Salvador','SLV',222,503),
	(65,'GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,240),
	(66,'ER','ERITREA','Eritrea','ERI',232,291),
	(67,'EE','ESTONIA','Estonia','EST',233,372),
	(68,'ET','ETHIOPIA','Ethiopia','ETH',231,251),
	(69,'FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,500),
	(70,'FO','FAROE ISLANDS','Faroe Islands','FRO',234,298),
	(71,'FJ','FIJI','Fiji','FJI',242,679),
	(72,'FI','FINLAND','Finland','FIN',246,358),
	(73,'FR','FRANCE','France','FRA',250,33),
	(74,'GF','FRENCH GUIANA','French Guiana','GUF',254,594),
	(75,'PF','FRENCH POLYNESIA','French Polynesia','PYF',258,689),
	(76,'TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0),
	(77,'GA','GABON','Gabon','GAB',266,241),
	(78,'GM','GAMBIA','Gambia','GMB',270,220),
	(79,'GE','GEORGIA','Georgia','GEO',268,995),
	(80,'DE','GERMANY','Germany','DEU',276,49),
	(81,'GH','GHANA','Ghana','GHA',288,233),
	(82,'GI','GIBRALTAR','Gibraltar','GIB',292,350),
	(83,'GR','GREECE','Greece','GRC',300,30),
	(84,'GL','GREENLAND','Greenland','GRL',304,299),
	(85,'GD','GRENADA','Grenada','GRD',308,1473),
	(86,'GP','GUADELOUPE','Guadeloupe','GLP',312,590),
	(87,'GU','GUAM','Guam','GUM',316,1671),
	(88,'GT','GUATEMALA','Guatemala','GTM',320,502),
	(89,'GN','GUINEA','Guinea','GIN',324,224),
	(90,'GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,245),
	(91,'GY','GUYANA','Guyana','GUY',328,592),
	(92,'HT','HAITI','Haiti','HTI',332,509),
	(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0),
	(94,'VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,39),
	(95,'HN','HONDURAS','Honduras','HND',340,504),
	(96,'HK','HONG KONG','Hong Kong','HKG',344,852),
	(97,'HU','HUNGARY','Hungary','HUN',348,36),
	(98,'IS','ICELAND','Iceland','ISL',352,354),
	(99,'IN','INDIA','India','IND',356,91),
	(100,'ID','INDONESIA','Indonesia','IDN',360,62),
	(101,'IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,98),
	(102,'IQ','IRAQ','Iraq','IRQ',368,964),
	(103,'IE','IRELAND','Ireland','IRL',372,353),
	(104,'IL','ISRAEL','Israel','ISR',376,972),
	(105,'IT','ITALY','Italy','ITA',380,39),
	(106,'JM','JAMAICA','Jamaica','JAM',388,1876),
	(107,'JP','JAPAN','Japan','JPN',392,81),
	(108,'JO','JORDAN','Jordan','JOR',400,962),
	(109,'KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,7),
	(110,'KE','KENYA','Kenya','KEN',404,254),
	(111,'KI','KIRIBATI','Kiribati','KIR',296,686),
	(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,850),
	(113,'KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,82),
	(114,'KW','KUWAIT','Kuwait','KWT',414,965),
	(115,'KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,996),
	(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,856),
	(117,'LV','LATVIA','Latvia','LVA',428,371),
	(118,'LB','LEBANON','Lebanon','LBN',422,961),
	(119,'LS','LESOTHO','Lesotho','LSO',426,266),
	(120,'LR','LIBERIA','Liberia','LBR',430,231),
	(121,'LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,218),
	(122,'LI','LIECHTENSTEIN','Liechtenstein','LIE',438,423),
	(123,'LT','LITHUANIA','Lithuania','LTU',440,370),
	(124,'LU','LUXEMBOURG','Luxembourg','LUX',442,352),
	(125,'MO','MACAO','Macao','MAC',446,853),
	(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,389),
	(127,'MG','MADAGASCAR','Madagascar','MDG',450,261),
	(128,'MW','MALAWI','Malawi','MWI',454,265),
	(129,'MY','MALAYSIA','Malaysia','MYS',458,60),
	(130,'MV','MALDIVES','Maldives','MDV',462,960),
	(131,'ML','MALI','Mali','MLI',466,223),
	(132,'MT','MALTA','Malta','MLT',470,356),
	(133,'MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,692),
	(134,'MQ','MARTINIQUE','Martinique','MTQ',474,596),
	(135,'MR','MAURITANIA','Mauritania','MRT',478,222),
	(136,'MU','MAURITIUS','Mauritius','MUS',480,230),
	(137,'YT','MAYOTTE','Mayotte',NULL,NULL,269),
	(138,'MX','MEXICO','Mexico','MEX',484,52),
	(139,'FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,691),
	(140,'MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,373),
	(141,'MC','MONACO','Monaco','MCO',492,377),
	(142,'MN','MONGOLIA','Mongolia','MNG',496,976),
	(143,'MS','MONTSERRAT','Montserrat','MSR',500,1664),
	(144,'MA','MOROCCO','Morocco','MAR',504,212),
	(145,'MZ','MOZAMBIQUE','Mozambique','MOZ',508,258),
	(146,'MM','MYANMAR','Myanmar','MMR',104,95),
	(147,'NA','NAMIBIA','Namibia','NAM',516,264),
	(148,'NR','NAURU','Nauru','NRU',520,674),
	(149,'NP','NEPAL','Nepal','NPL',524,977),
	(150,'NL','NETHERLANDS','Netherlands','NLD',528,31),
	(151,'AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,599),
	(152,'NC','NEW CALEDONIA','New Caledonia','NCL',540,687),
	(153,'NZ','NEW ZEALAND','New Zealand','NZL',554,64),
	(154,'NI','NICARAGUA','Nicaragua','NIC',558,505),
	(155,'NE','NIGER','Niger','NER',562,227),
	(156,'NG','NIGERIA','Nigeria','NGA',566,234),
	(157,'NU','NIUE','Niue','NIU',570,683),
	(158,'NF','NORFOLK ISLAND','Norfolk Island','NFK',574,672),
	(159,'MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,1670),
	(160,'NO','NORWAY','Norway','NOR',578,47),
	(161,'OM','OMAN','Oman','OMN',512,968),
	(162,'PK','PAKISTAN','Pakistan','PAK',586,92),
	(163,'PW','PALAU','Palau','PLW',585,680),
	(164,'PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,970),
	(165,'PA','PANAMA','Panama','PAN',591,507),
	(166,'PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,675),
	(167,'PY','PARAGUAY','Paraguay','PRY',600,595),
	(168,'PE','PERU','Peru','PER',604,51),
	(169,'PH','PHILIPPINES','Philippines','PHL',608,63),
	(170,'PN','PITCAIRN','Pitcairn','PCN',612,0),
	(171,'PL','POLAND','Poland','POL',616,48),
	(172,'PT','PORTUGAL','Portugal','PRT',620,351),
	(173,'PR','PUERTO RICO','Puerto Rico','PRI',630,1787),
	(174,'QA','QATAR','Qatar','QAT',634,974),
	(175,'RE','REUNION','Reunion','REU',638,262),
	(176,'RO','ROMANIA','Romania','ROM',642,40),
	(177,'RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,70),
	(178,'RW','RWANDA','Rwanda','RWA',646,250),
	(179,'SH','SAINT HELENA','Saint Helena','SHN',654,290),
	(180,'KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,1869),
	(181,'LC','SAINT LUCIA','Saint Lucia','LCA',662,1758),
	(182,'PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,508),
	(183,'VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,1784),
	(184,'WS','SAMOA','Samoa','WSM',882,684),
	(185,'SM','SAN MARINO','San Marino','SMR',674,378),
	(186,'ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,239),
	(187,'SA','SAUDI ARABIA','Saudi Arabia','SAU',682,966),
	(188,'SN','SENEGAL','Senegal','SEN',686,221),
	(189,'CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,381),
	(190,'SC','SEYCHELLES','Seychelles','SYC',690,248),
	(191,'SL','SIERRA LEONE','Sierra Leone','SLE',694,232),
	(192,'SG','SINGAPORE','Singapore','SGP',702,65),
	(193,'SK','SLOVAKIA','Slovakia','SVK',703,421),
	(194,'SI','SLOVENIA','Slovenia','SVN',705,386),
	(195,'SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,677),
	(196,'SO','SOMALIA','Somalia','SOM',706,252),
	(197,'ZA','SOUTH AFRICA','South Africa','ZAF',710,27),
	(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0),
	(199,'ES','SPAIN','Spain','ESP',724,34),
	(200,'LK','SRI LANKA','Sri Lanka','LKA',144,94),
	(201,'SD','SUDAN','Sudan','SDN',736,249),
	(202,'SR','SURINAME','Suriname','SUR',740,597),
	(203,'SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,47),
	(204,'SZ','SWAZILAND','Swaziland','SWZ',748,268),
	(205,'SE','SWEDEN','Sweden','SWE',752,46),
	(206,'CH','SWITZERLAND','Switzerland','CHE',756,41),
	(207,'SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,963),
	(208,'TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,886),
	(209,'TJ','TAJIKISTAN','Tajikistan','TJK',762,992),
	(210,'TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,255),
	(211,'TH','THAILAND','Thailand','THA',764,66),
	(212,'TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,670),
	(213,'TG','TOGO','Togo','TGO',768,228),
	(214,'TK','TOKELAU','Tokelau','TKL',772,690),
	(215,'TO','TONGA','Tonga','TON',776,676),
	(216,'TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,1868),
	(217,'TN','TUNISIA','Tunisia','TUN',788,216),
	(218,'TR','TURKEY','Turkey','TUR',792,90),
	(219,'TM','TURKMENISTAN','Turkmenistan','TKM',795,7370),
	(220,'TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,1649),
	(221,'TV','TUVALU','Tuvalu','TUV',798,688),
	(222,'UG','UGANDA','Uganda','UGA',800,256),
	(223,'UA','UKRAINE','Ukraine','UKR',804,380),
	(224,'AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,971),
	(225,'GB','UNITED KINGDOM','United Kingdom','GBR',826,44),
	(226,'US','UNITED STATES','United States','USA',840,1),
	(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,1),
	(228,'UY','URUGUAY','Uruguay','URY',858,598),
	(229,'UZ','UZBEKISTAN','Uzbekistan','UZB',860,998),
	(230,'VU','VANUATU','Vanuatu','VUT',548,678),
	(231,'VE','VENEZUELA','Venezuela','VEN',862,58),
	(232,'VN','VIET NAM','Viet Nam','VNM',704,84),
	(233,'VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,1284),
	(234,'VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,1340),
	(235,'WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,681),
	(236,'EH','WESTERN SAHARA','Western Sahara','ESH',732,212),
	(237,'YE','YEMEN','Yemen','YEM',887,967),
	(238,'ZM','ZAMBIA','Zambia','ZMB',894,260),
	(239,'ZW','ZIMBABWE','Zimbabwe','ZWE',716,263);

/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `template` longtext,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mjml` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;

INSERT INTO `email_templates` (`id`, `property_id`, `name`, `template`, `deleted`, `mjml`)
VALUES
	(16,1,'asd','\n    <!doctype html>\n    <html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n      <head>\n        <title>\n          \n        </title>\n        <!--[if !mso]><!-- -->\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n        <!--<![endif]-->\n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n        <style type=\"text/css\">\n          #outlook a { padding:0; }\n          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }\n          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }\n          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }\n          p { display:block;margin:13px 0; }\n        </style>\n        <!--[if mso]>\n        <xml>\n        <o:OfficeDocumentSettings>\n          <o:AllowPNG/>\n          <o:PixelsPerInch>96</o:PixelsPerInch>\n        </o:OfficeDocumentSettings>\n        </xml>\n        <![endif]-->\n        <!--[if lte mso 11]>\n        <style type=\"text/css\">\n          .outlook-group-fix { width:100% !important; }\n        </style>\n        <![endif]-->\n        \n        \n        <style type=\"text/css\">\n        \n        \n        </style>\n        \n        \n      </head>\n      <body>\n        \n        \n      <div\n         style=\"\"\n      >\n        \n      </div>\n    \n      </body>\n    </html>\n  ',1,'<mjml><mj-body>\n\n            </mj-body></mjml>');

/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `order` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`, `order`)
VALUES
	(1,'admin','Administrator',1),
	(2,'members','General Requester',0),
	(3,'developer','Developer',2);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table guestbook_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `guestbook_users`;

CREATE TABLE `guestbook_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `user_name` varchar(150) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `guestbook_users` WRITE;
/*!40000 ALTER TABLE `guestbook_users` DISABLE KEYS */;

INSERT INTO `guestbook_users` (`id`, `property_id`, `name`, `last_name`, `user_name`, `password`, `created`, `deleted`)
VALUES
	(1,1,'dev','eloper','developer','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',NULL,0),
	(2,11,'Supervisor','Fab','supervisorsfab@hotellomnica.sk','9769753a222b45ab6b886f95c02882ca8eba2331',NULL,0),
	(3,11,'dev','eleper','developer@lominca','375bbac4fa27035910372f6e7120adf85f6d8b53',NULL,0),
	(4,12,'staff','','alfaro','96327d5521a52aff95ad8066420f715fdd0d2561',NULL,0);

/*!40000 ALTER TABLE `guestbook_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table icons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `icons`;

CREATE TABLE `icons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `icons` WRITE;
/*!40000 ALTER TABLE `icons` DISABLE KEYS */;

INSERT INTO `icons` (`id`, `value`, `name`)
VALUES
	(1,'home','home'),
	(2,'home2','home2'),
	(3,'home5','home5'),
	(10,'magazine','magazine'),
	(12,'pencil','pencil'),
	(16,'pencil6','pencil6'),
	(17,'pencil7','pencil7'),
	(18,'eraser','eraser'),
	(19,'eraser2','eraser2'),
	(20,'eraser3','eraser3'),
	(27,'blog','blog'),
	(28,'pen6','pen6'),
	(31,'color-sampler','color-sampler'),
	(48,'image4','image4'),
	(49,'image5','image5'),
	(50,'camera','camera'),
	(51,'shutter','shutter'),
	(52,'headphones','headphones'),
	(53,'headset','headset'),
	(54,'music','music'),
	(55,'album','album'),
	(56,'tape','tape'),
	(57,'piano','piano'),
	(58,'speakers','speakers'),
	(59,'play','play'),
	(60,'clapboard-play','clapboard-play'),
	(61,'clapboard','clapboard'),
	(62,'media','media'),
	(63,'presentation','presentation'),
	(64,'movie','movie'),
	(65,'film','film'),
	(66,'film2','film2'),
	(67,'film3','film3'),
	(68,'film4','film4'),
	(69,'video-camera','video-camera'),
	(70,'video-camera2','video-camera2'),
	(71,'video-camera-slash','video-camera-slash'),
	(72,'video-camera3','video-camera3'),
	(73,'dice','dice'),
	(74,'chess-king','chess-king'),
	(75,'chess-queen','chess-queen'),
	(76,'chess','chess'),
	(77,'megaphone','megaphone'),
	(78,'new','new'),
	(79,'connection','connection'),
	(80,'station','station'),
	(81,'satellite-dish2','satellite-dish2'),
	(82,'feed','feed'),
	(83,'mic2','mic2'),
	(84,'mic-off2','mic-off2'),
	(85,'book','book'),
	(86,'book2','book2'),
	(87,'book-play','book-play'),
	(88,'book3','book3'),
	(89,'bookmark','bookmark'),
	(90,'books','books'),
	(91,'archive','archive'),
	(92,'reading','reading'),
	(93,'library2','library2'),
	(94,'graduation2','graduation2'),
	(95,'file-text','file-text'),
	(96,'profile','profile'),
	(97,'file-empty','file-empty'),
	(98,'file-empty2','file-empty2'),
	(99,'files-empty','files-empty'),
	(100,'files-empty2','files-empty2'),
	(101,'file-plus','file-plus'),
	(102,'file-plus2','file-plus2'),
	(103,'file-minus','file-minus'),
	(104,'file-minus2','file-minus2'),
	(105,'file-download','file-download'),
	(106,'file-download2','file-download2'),
	(107,'file-upload','file-upload'),
	(108,'file-upload2','file-upload2'),
	(109,'file-check','file-check'),
	(110,'file-check2','file-check2'),
	(111,'file-eye','file-eye'),
	(112,'file-eye2','file-eye2'),
	(113,'file-text2','file-text2'),
	(114,'file-text3','file-text3'),
	(115,'file-picture','file-picture'),
	(116,'file-picture2','file-picture2'),
	(117,'file-music','file-music'),
	(118,'file-music2','file-music2'),
	(119,'file-play','file-play'),
	(120,'file-play2','file-play2'),
	(121,'file-video','file-video'),
	(122,'file-video2','file-video2'),
	(123,'copy','copy'),
	(124,'copy2','copy2'),
	(125,'file-zip','file-zip'),
	(126,'file-zip2','file-zip2'),
	(127,'file-xml','file-xml'),
	(128,'file-xml2','file-xml2'),
	(129,'file-css','file-css'),
	(130,'file-css2','file-css2'),
	(131,'file-presentation','file-presentation'),
	(132,'file-presentation2','file-presentation2'),
	(133,'file-stats','file-stats'),
	(134,'file-stats2','file-stats2'),
	(135,'file-locked','file-locked'),
	(136,'file-locked2','file-locked2'),
	(137,'file-spreadsheet','file-spreadsheet'),
	(138,'file-spreadsheet2','file-spreadsheet2'),
	(139,'copy3','copy3'),
	(140,'copy4','copy4'),
	(141,'paste','paste'),
	(142,'paste2','paste2'),
	(143,'paste3','paste3'),
	(144,'paste4','paste4'),
	(145,'stack','stack'),
	(146,'stack2','stack2'),
	(147,'stack3','stack3'),
	(148,'folder','folder'),
	(149,'folder-search','folder-search'),
	(150,'folder-download','folder-download'),
	(151,'folder-upload','folder-upload'),
	(152,'folder-plus','folder-plus'),
	(153,'folder-plus2','folder-plus2'),
	(154,'folder-minus','folder-minus'),
	(155,'folder-minus2','folder-minus2'),
	(156,'folder-check','folder-check'),
	(157,'folder-heart','folder-heart'),
	(158,'folder-remove','folder-remove'),
	(159,'folder2','folder2'),
	(160,'folder-open','folder-open'),
	(161,'folder3','folder3'),
	(162,'folder4','folder4'),
	(163,'folder-plus3','folder-plus3'),
	(164,'folder-minus3','folder-minus3'),
	(165,'folder-plus4','folder-plus4'),
	(166,'folder-minus4','folder-minus4'),
	(167,'folder-download2','folder-download2'),
	(168,'folder-upload2','folder-upload2'),
	(169,'folder-download3','folder-download3'),
	(170,'folder-upload3','folder-upload3'),
	(171,'folder5','folder5'),
	(172,'folder-open2','folder-open2'),
	(173,'folder6','folder6'),
	(174,'folder-open3','folder-open3'),
	(175,'certificate','certificate'),
	(176,'cc','cc'),
	(177,'price-tag','price-tag'),
	(178,'price-tag2','price-tag2'),
	(179,'price-tags','price-tags'),
	(180,'price-tag3','price-tag3'),
	(181,'price-tags2','price-tags2'),
	(182,'barcode2','barcode2'),
	(183,'qrcode','qrcode'),
	(184,'ticket','ticket'),
	(185,'theater','theater'),
	(186,'store','store'),
	(187,'store2','store2'),
	(188,'cart','cart'),
	(189,'cart2','cart2'),
	(190,'cart4','cart4'),
	(191,'cart5','cart5'),
	(192,'cart-add','cart-add'),
	(193,'cart-add2','cart-add2'),
	(194,'cart-remove','cart-remove'),
	(195,'basket','basket'),
	(196,'bag','bag'),
	(197,'percent','percent'),
	(198,'coins','coins'),
	(199,'coin-dollar','coin-dollar'),
	(200,'coin-euro','coin-euro'),
	(201,'coin-pound','coin-pound'),
	(202,'coin-yen','coin-yen'),
	(203,'piggy-bank','piggy-bank'),
	(204,'wallet','wallet'),
	(205,'cash','cash'),
	(206,'cash2','cash2'),
	(207,'cash3','cash3'),
	(208,'cash4','cash4'),
	(209,'credit-card','credit-card'),
	(210,'credit-card2','credit-card2'),
	(211,'calculator4','calculator4'),
	(212,'calculator2','calculator2'),
	(213,'calculator3','calculator3'),
	(214,'chip','chip'),
	(215,'lifebuoy','lifebuoy'),
	(245,'reset','reset'),
	(246,'history','history'),
	(247,'watch','watch'),
	(248,'watch2','watch2'),
	(249,'alarm','alarm'),
	(250,'alarm-add','alarm-add'),
	(251,'alarm-check','alarm-check'),
	(252,'alarm-cancel','alarm-cancel'),
	(253,'bell2','bell2'),
	(254,'bell3','bell3'),
	(255,'bell-plus','bell-plus'),
	(256,'bell-minus','bell-minus'),
	(257,'bell-check','bell-check'),
	(258,'bell-cross','bell-cross'),
	(259,'calendar','calendar'),
	(260,'calendar2','calendar2'),
	(261,'calendar3','calendar3'),
	(262,'calendar52','calendar52'),
	(263,'printer','printer'),
	(264,'printer2','printer2'),
	(265,'printer4','printer4'),
	(266,'shredder','shredder'),
	(267,'mouse','mouse'),
	(268,'mouse-left','mouse-left'),
	(269,'mouse-right','mouse-right'),
	(270,'keyboard','keyboard'),
	(271,'typewriter','typewriter'),
	(272,'display','display'),
	(273,'display4','display4'),
	(274,'laptop','laptop'),
	(275,'mobile','mobile'),
	(276,'mobile2','mobile2'),
	(277,'tablet','tablet'),
	(278,'mobile3','mobile3'),
	(279,'tv','tv'),
	(280,'radio','radio'),
	(281,'cabinet','cabinet'),
	(282,'drawer','drawer'),
	(283,'drawer2','drawer2'),
	(284,'drawer-out','drawer-out'),
	(285,'drawer-in','drawer-in'),
	(286,'drawer3','drawer3'),
	(287,'box','box'),
	(288,'box-add','box-add'),
	(289,'box-remove','box-remove'),
	(290,'download','download'),
	(291,'upload','upload'),
	(292,'floppy-disk','floppy-disk'),
	(293,'floppy-disks','floppy-disks'),
	(294,'usb-stick','usb-stick'),
	(295,'drive','drive'),
	(296,'server','server'),
	(297,'database','database'),
	(298,'database2','database2'),
	(299,'database4','database4'),
	(300,'database-menu','database-menu'),
	(301,'database-add','database-add'),
	(302,'database-remove','database-remove'),
	(303,'database-insert','database-insert'),
	(304,'database-export','database-export'),
	(305,'database-upload','database-upload'),
	(306,'database-refresh','database-refresh'),
	(307,'database-diff','database-diff'),
	(308,'database-edit2','database-edit2'),
	(309,'database-check','database-check'),
	(310,'database-arrow','database-arrow'),
	(311,'database-time2','database-time2'),
	(312,'undo','undo'),
	(313,'redo','redo'),
	(314,'rotate-ccw','rotate-ccw'),
	(315,'rotate-cw','rotate-cw'),
	(316,'rotate-ccw2','rotate-ccw2'),
	(317,'rotate-cw2','rotate-cw2'),
	(318,'rotate-ccw3','rotate-ccw3'),
	(319,'rotate-cw3','rotate-cw3'),
	(320,'flip-vertical2','flip-vertical2'),
	(321,'flip-horizontal2','flip-horizontal2'),
	(322,'flip-vertical3','flip-vertical3'),
	(323,'flip-vertical4','flip-vertical4'),
	(324,'angle','angle'),
	(325,'shear','shear'),
	(326,'align-left','align-left'),
	(327,'align-right','align-right'),
	(328,'align-top','align-top'),
	(329,'align-bottom','align-bottom'),
	(330,'undo2','undo2'),
	(331,'redo2','redo2'),
	(332,'forward','forward'),
	(333,'reply','reply'),
	(334,'reply-all','reply-all'),
	(335,'bubble','bubble'),
	(336,'bubbles','bubbles'),
	(337,'bubbles2','bubbles2'),
	(338,'bubble2','bubble2'),
	(339,'bubbles3','bubbles3'),
	(340,'bubbles4','bubbles4'),
	(341,'bubble-notification','bubble-notification'),
	(342,'bubbles5','bubbles5'),
	(343,'bubbles6','bubbles6'),
	(344,'bubble6','bubble6'),
	(345,'bubbles7','bubbles7'),
	(346,'bubble7','bubble7'),
	(347,'bubbles8','bubbles8'),
	(348,'bubble8','bubble8'),
	(349,'bubble-dots3','bubble-dots3'),
	(350,'bubble-lines3','bubble-lines3'),
	(351,'bubble9','bubble9'),
	(352,'bubble-dots4','bubble-dots4'),
	(353,'bubble-lines4','bubble-lines4'),
	(354,'bubbles9','bubbles9'),
	(355,'bubbles10','bubbles10'),
	(356,'user','user'),
	(357,'users','users'),
	(358,'user-plus','user-plus'),
	(359,'user-minus','user-minus'),
	(360,'user-cancel','user-cancel'),
	(361,'user-block','user-block'),
	(362,'user-lock','user-lock'),
	(363,'user-check','user-check'),
	(364,'users2','users2'),
	(365,'users4','users4'),
	(366,'user-tie','user-tie'),
	(367,'collaboration','collaboration'),
	(368,'vcard','vcard'),
	(369,'hat','hat'),
	(370,'bowtie','bowtie'),
	(371,'quotes-left','quotes-left'),
	(372,'quotes-right','quotes-right'),
	(373,'quotes-left2','quotes-left2'),
	(374,'quotes-right2','quotes-right2'),
	(375,'hour-glass','hour-glass'),
	(376,'hour-glass2','hour-glass2'),
	(377,'hour-glass3','hour-glass3'),
	(378,'spinner','spinner'),
	(379,'spinner2','spinner2'),
	(380,'spinner3','spinner3'),
	(381,'spinner4','spinner4'),
	(382,'spinner6','spinner6'),
	(383,'spinner9','spinner9'),
	(384,'spinner10','spinner10'),
	(385,'spinner11','spinner11'),
	(386,'microscope','microscope'),
	(387,'enlarge','enlarge'),
	(388,'shrink','shrink'),
	(389,'enlarge3','enlarge3'),
	(390,'shrink3','shrink3'),
	(391,'enlarge5','enlarge5'),
	(392,'shrink5','shrink5'),
	(393,'enlarge6','enlarge6'),
	(394,'shrink6','shrink6'),
	(395,'enlarge7','enlarge7'),
	(396,'shrink7','shrink7'),
	(397,'key','key'),
	(398,'lock','lock'),
	(399,'lock2','lock2'),
	(400,'lock4','lock4'),
	(401,'unlocked','unlocked'),
	(402,'lock5','lock5'),
	(403,'unlocked2','unlocked2'),
	(404,'safe','safe'),
	(405,'wrench','wrench'),
	(406,'wrench2','wrench2'),
	(407,'wrench3','wrench3'),
	(408,'equalizer','equalizer'),
	(409,'equalizer2','equalizer2'),
	(410,'equalizer3','equalizer3'),
	(411,'equalizer4','equalizer4'),
	(412,'cog','cog'),
	(413,'cogs','cogs'),
	(414,'cog2','cog2'),
	(415,'cog3','cog3'),
	(416,'cog4','cog4'),
	(417,'cog52','cog52'),
	(418,'cog6','cog6'),
	(419,'cog7','cog7'),
	(420,'hammer','hammer'),
	(421,'hammer-wrench','hammer-wrench'),
	(422,'magic-wand','magic-wand'),
	(423,'magic-wand2','magic-wand2'),
	(424,'pulse2','pulse2'),
	(425,'aid-kit','aid-kit'),
	(426,'bug2','bug2'),
	(427,'construction','construction'),
	(428,'traffic-cone','traffic-cone'),
	(429,'traffic-lights','traffic-lights'),
	(430,'pie-chart','pie-chart'),
	(431,'pie-chart2','pie-chart2'),
	(432,'pie-chart3','pie-chart3'),
	(433,'pie-chart4','pie-chart4'),
	(434,'pie-chart5','pie-chart5'),
	(435,'pie-chart6','pie-chart6'),
	(436,'pie-chart7','pie-chart7'),
	(437,'stats-dots','stats-dots'),
	(438,'stats-bars','stats-bars'),
	(439,'pie-chart8','pie-chart8'),
	(440,'stats-bars2','stats-bars2'),
	(441,'stats-bars3','stats-bars3'),
	(442,'stats-bars4','stats-bars4'),
	(443,'chart','chart'),
	(444,'stats-growth','stats-growth'),
	(445,'stats-decline','stats-decline'),
	(446,'stats-growth2','stats-growth2'),
	(447,'stats-decline2','stats-decline2'),
	(448,'stairs-up','stairs-up'),
	(449,'stairs-down','stairs-down'),
	(450,'stairs','stairs'),
	(451,'ladder','ladder'),
	(452,'rating','rating'),
	(453,'rating2','rating2'),
	(454,'rating3','rating3'),
	(455,'podium','podium'),
	(456,'stars','stars'),
	(457,'medal-star','medal-star'),
	(458,'medal','medal'),
	(459,'medal2','medal2'),
	(460,'medal-first','medal-first'),
	(461,'medal-second','medal-second'),
	(462,'medal-third','medal-third'),
	(463,'crown','crown'),
	(464,'trophy2','trophy2'),
	(465,'trophy3','trophy3'),
	(466,'diamond','diamond'),
	(467,'trophy4','trophy4'),
	(468,'gift','gift'),
	(469,'pipe','pipe'),
	(470,'mustache','mustache'),
	(471,'cup2','cup2'),
	(472,'coffee','coffee'),
	(473,'paw','paw'),
	(474,'footprint','footprint'),
	(475,'rocket','rocket'),
	(476,'meter2','meter2'),
	(477,'meter-slow','meter-slow'),
	(478,'meter-fast','meter-fast'),
	(479,'hammer2','hammer2'),
	(480,'balance','balance'),
	(481,'fire','fire'),
	(482,'fire2','fire2'),
	(483,'lab','lab'),
	(484,'atom','atom'),
	(485,'atom2','atom2'),
	(486,'bin','bin'),
	(487,'bin2','bin2'),
	(488,'briefcase','briefcase'),
	(489,'briefcase3','briefcase3'),
	(490,'airplane2','airplane2'),
	(491,'airplane3','airplane3'),
	(492,'airplane4','airplane4'),
	(493,'paperplane','paperplane'),
	(494,'car','car'),
	(495,'steering-wheel','steering-wheel'),
	(496,'car2','car2'),
	(497,'gas','gas'),
	(498,'bus','bus'),
	(499,'truck','truck'),
	(500,'bike','bike'),
	(501,'road','road'),
	(502,'train','train'),
	(503,'train2','train2'),
	(504,'ship','ship'),
	(505,'boat','boat'),
	(506,'chopper','chopper'),
	(507,'cube','cube'),
	(508,'cube2','cube2'),
	(509,'cube3','cube3'),
	(510,'cube4','cube4'),
	(511,'pyramid','pyramid'),
	(512,'pyramid2','pyramid2'),
	(513,'package','package'),
	(514,'puzzle','puzzle'),
	(515,'puzzle2','puzzle2'),
	(516,'puzzle3','puzzle3'),
	(517,'puzzle4','puzzle4'),
	(518,'glasses-3d2','glasses-3d2'),
	(519,'brain','brain'),
	(520,'accessibility','accessibility'),
	(521,'accessibility2','accessibility2'),
	(522,'strategy','strategy'),
	(523,'target','target'),
	(524,'target2','target2'),
	(525,'shield-check','shield-check'),
	(526,'shield-notice','shield-notice'),
	(527,'shield2','shield2'),
	(530,'power2','power2'),
	(531,'power3','power3'),
	(532,'switch','switch'),
	(533,'switch22','switch22'),
	(534,'power-cord','power-cord'),
	(541,'playlist','playlist'),
	(542,'playlist-add','playlist-add'),
	(543,'list-numbered','list-numbered'),
	(544,'list','list'),
	(545,'list2','list2'),
	(546,'more','more'),
	(547,'more2','more2'),
	(548,'grid','grid'),
	(549,'grid2','grid2'),
	(550,'grid3','grid3'),
	(551,'grid4','grid4'),
	(552,'grid52','grid52'),
	(553,'grid6','grid6'),
	(554,'grid7','grid7'),
	(555,'tree5','tree5'),
	(556,'tree6','tree6'),
	(557,'tree7','tree7'),
	(558,'lan','lan'),
	(559,'lan2','lan2'),
	(560,'lan3','lan3'),
	(561,'menu','menu'),
	(562,'circle-small','circle-small'),
	(563,'menu2','menu2');

/*!40000 ALTER TABLE `icons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `code`, `name`, `language`)
VALUES
	(1,'sk','slovak','Slovenčina'),
	(2,'cz','czech','Čeština'),
	(3,'en','english','English');

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigation`;

CREATE TABLE `navigation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `icon` int(15) DEFAULT NULL,
  `type` varchar(12) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `active` int(1) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `placeholder` varchar(1) DEFAULT NULL,
  `link` int(1) DEFAULT NULL,
  `link_path` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `navigation` WRITE;
/*!40000 ALTER TABLE `navigation` DISABLE KEYS */;

INSERT INTO `navigation` (`id`, `name`, `controller`, `icon`, `type`, `order`, `active`, `deleted`, `parent`, `placeholder`, `link`, `link_path`)
VALUES
	(2,'Dashboard','1',2,'3',1,1,0,0,NULL,NULL,NULL),
	(3,'Developer zone','',145,'2',8,1,0,0,'1',NULL,NULL),
	(4,'Navigácia','2',545,'3',8,1,0,3,NULL,NULL,NULL),
	(5,'Administrácia','3',357,'3',9,1,0,3,'',NULL,NULL),
	(6,'Právomoci','4',415,'3',10,1,0,3,NULL,NULL,NULL),
	(7,'Nastavenia','',415,'2',16,1,1,0,'1',NULL,NULL),
	(8,'Otváracie hodiny','7',249,'3',16,1,1,0,NULL,NULL,NULL),
	(9,'Nastavenie zariadenia','8',10,'3',5,1,0,37,NULL,NULL,NULL),
	(10,'Čašníci','9',366,'3',18,1,1,0,NULL,NULL,NULL),
	(11,'Blokovanie časov','10',248,'3',17,1,1,0,NULL,NULL,NULL),
	(12,'Zákazníci','11',356,'3',3,1,1,0,NULL,NULL,NULL),
	(13,'Nastavenie zón','12',381,'3',5,1,1,0,NULL,NULL,NULL),
	(14,'Rezervácie','13',209,'3',2,1,0,0,NULL,NULL,NULL),
	(15,'Práva zariadení','14',80,'3',23,1,1,3,NULL,NULL,NULL),
	(16,'Rezervačná kniha','15',89,'3',1,1,1,0,NULL,NULL,NULL),
	(17,'Rezervačná kniha','15',89,'3',21,1,1,0,NULL,NULL,NULL),
	(18,'Zariadenia','16',93,'3',26,1,1,3,NULL,NULL,NULL),
	(19,'Hodnotenia','17',456,'3',5,1,1,0,NULL,NULL,NULL),
	(20,'Štatistiky','',437,'2',7,1,1,0,'1',NULL,NULL),
	(21,'Štatistiky - rezervácie','18',432,'3',10,1,1,0,NULL,NULL,NULL),
	(22,'Štatistiky - zákazníci','19',433,'3',12,1,1,0,NULL,NULL,NULL),
	(23,'Widget','20',272,'3',16,1,1,0,NULL,NULL,NULL),
	(24,'Kombinácie stolov','21',211,'3',6,1,1,0,NULL,NULL,NULL),
	(25,'Blokovanie stolov/zón','22',214,'3',7,1,1,0,NULL,NULL,NULL),
	(26,'Štatistiky - čašníci','23',364,'3',7,1,1,0,NULL,NULL,NULL),
	(27,'Doplnkové služby','24',146,'3',9,1,1,0,NULL,NULL,NULL),
	(28,'Emailové šablóny','25',145,'3',10,1,1,0,NULL,NULL,NULL),
	(30,'Platobné nastavenia','26',198,'3',17,1,1,0,NULL,NULL,NULL),
	(31,'Štatistiky - upsells','27',434,'3',13,1,1,0,NULL,NULL,NULL),
	(32,'Štatistiky - platby','28',440,'3',13,1,1,0,NULL,NULL,NULL),
	(33,'Štatistiky - zóny','29',439,'3',14,1,1,0,NULL,NULL,NULL),
	(34,'Služby','30',514,'3',8,1,1,0,NULL,NULL,NULL),
	(35,'Rýchla rezervácia','34',213,'3',2,1,1,0,NULL,NULL,NULL),
	(36,'Systémoví užívatelia','32',358,'3',6,1,0,37,NULL,NULL,NULL),
	(37,'Nastavenia','',415,'2',5,1,0,0,'1',NULL,NULL),
	(38,'Email','25',146,'3',1,1,1,0,NULL,NULL,NULL),
	(39,'Šablóny','25',146,'3',3,1,0,40,NULL,NULL,NULL),
	(40,'Marketing','',281,'2',3,1,0,0,'1',NULL,NULL),
	(41,'Zariadenia','',509,'2',2,1,1,0,'1',NULL,NULL),
	(42,'Zariadenia','33',509,'3',1,1,0,3,NULL,NULL,NULL),
	(43,'Zariadenia','33',554,'3',1,1,1,3,NULL,NULL,NULL);

/*!40000 ALTER TABLE `navigation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table navigation_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigation_type`;

CREATE TABLE `navigation_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `navigation_type` WRITE;
/*!40000 ALTER TABLE `navigation_type` DISABLE KEYS */;

INSERT INTO `navigation_type` (`id`, `name`, `value`)
VALUES
	(1,'Link','link'),
	(2,'Placeholder','placeholder'),
	(3,'Controller','controller');

/*!40000 ALTER TABLE `navigation_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table payment_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_log`;

CREATE TABLE `payment_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL,
  `response` longtext,
  `request` longtext,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table payment_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_types`;

CREATE TABLE `payment_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property`;

CREATE TABLE `property` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `postalcode` int(10) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `taxid` varchar(15) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `capacity` int(4) DEFAULT NULL,
  `token` varchar(20) DEFAULT NULL,
  `active_token` tinyint(1) DEFAULT '0',
  `iban` varchar(100) DEFAULT NULL,
  `swift` varchar(10) DEFAULT NULL,
  `event_email` varchar(100) DEFAULT NULL,
  `event_phone` varchar(18) DEFAULT NULL,
  `smsFarmCode` varchar(50) DEFAULT NULL,
  `integrationID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;

INSERT INTO `property` (`id`, `name`, `street`, `city`, `postalcode`, `telephone`, `email`, `taxid`, `url`, `capacity`, `token`, `active_token`, `iban`, `swift`, `event_email`, `event_phone`, `smsFarmCode`, `integrationID`)
VALUES
	(14,'Testovacia reštaurácia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table property_notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_notification`;

CREATE TABLE `property_notification` (
  `property_id` int(11) DEFAULT NULL,
  `property_color` varchar(10) DEFAULT NULL,
  `confirm_email` longtext,
  `pending_email` longtext,
  `cancel_email` longtext,
  `rating_email` longtext,
  `confirm_sms` varchar(300) DEFAULT NULL,
  `pending_sms` varchar(300) DEFAULT NULL,
  `cancel_sms` varchar(300) DEFAULT NULL,
  `accepted_email` longtext,
  `declined_email` longtext,
  `accepted_sms` varchar(300) DEFAULT NULL,
  `declined_sms` varchar(300) DEFAULT NULL,
  `success_pay_email` longtext,
  `unsuccess_pay_email` longtext,
  `request_pay_email` longtext,
  `success_pay_sms` varchar(300) DEFAULT NULL,
  `unsuccess_pay_sms` varchar(300) DEFAULT NULL,
  `request_pay_sms` varchar(300) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `property_notification` WRITE;
/*!40000 ALTER TABLE `property_notification` DISABLE KEYS */;

INSERT INTO `property_notification` (`property_id`, `property_color`, `confirm_email`, `pending_email`, `cancel_email`, `rating_email`, `confirm_sms`, `pending_sms`, `cancel_sms`, `accepted_email`, `declined_email`, `accepted_sms`, `declined_sms`, `success_pay_email`, `unsuccess_pay_email`, `request_pay_email`, `success_pay_sms`, `unsuccess_pay_sms`, `request_pay_sms`, `lang`)
VALUES
	(1,'','','','','','','','','','','','','','','','','','','sk'),
	(1,'','','','','','','','','','','','','','','','','','','en'),
	(11,'#263f58','<p style=\"text-align: center; font-size: 14px;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; white-space: pre-wrap;\"> {customerName}, ď</span><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">akujeme za Vašu rezerváciu.</span><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">​​​​​​​</span></span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Tešíme sa na Vašu návštevu dňa {reservationDate} o {reservationTimeStart}</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Ak si želáte zrušiť rezerváciu, kliknite, prosím, na odkaz nižšie.</span><span style=\"font-family: Arial; font-size: 14px;\"><br></span></p>','<p><br></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px; white-space: pre-wrap;\">{customerName}, ď</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px; white-space: pre-wrap;\">akujeme za Vašu rezerváciu.</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px; white-space: pre-wrap;\">​​​​​​​</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Rezervácia dňa {reservationDate} o {reservationTimeStart} čaká na schválenie.</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Ak si želáte zrušiť rezerváciu, kliknite, prosím, na odkaz nižšie.</span></p>','<div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">{customerName}, </span><span style=\"font-family: Arial; font-size: 14px;\">Vaša rezervácia dňa {reservationDate} o {reservationTimeStart} bola úspešne zrušená.</span></span><br></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px; white-space: pre-wrap;\" {customername}=\"\" {customerlastname},=\"\" veríme,=\"\" že=\"\" ste=\"\" si=\"\" chvíle=\"\" v=\"\" hoteli=\"\" lomnica=\"\" maximálne=\"\" vychutnali:)<=\"\" span=\"\"></span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">{customerName} </span><span style=\"font-family: Calibri, sans-serif; font-size: 11pt; text-align: left;\">veríme,\r\nže ste si chvíle v Hoteli Lomnica maximálne vychutnali. </span><span style=\"font-family: Arial; font-size: 14px;\">Budeme veľmi radi, ak sa s nami podelíte o dojmy a názory, ktoré pomôžu zdokonaľovať naše služby.</span></span></div>','{customerName}, ďakujeme za rezerváciu. Tešíme sa na Vašu návštevu dňa {reservationDate} o {reservationTimeStart}.','{customerName}, ďakujeme za Vašu rezerváciu. Rezervácia dňa {reservationDate} o {reservationTimeStart} čaká na schválenie. ','{customerName}, Vaša rezervácia dňa {reservationDate} o {reservationTimeStart} bola úspešne zrušená.','<p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap; color: rgb(255, 255, 255);\">{customerName}, ď</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px; white-space: pre-wrap;\">akujeme za Vašu rezerváciu.</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px; white-space: pre-wrap;\">​​​​​​​</span></p><p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 14px; color: rgb(255, 255, 255);\">Rezervácia dňa {reservationDate} o {reservationTimeStart} bola schválená.</span></p><p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 14px; color: rgb(255, 255, 255);\">Ak si želáte zrušiť rezerváciu, kliknite, prosím, na odkaz nižšie.</span></p>','<p style=\"text-align: center; font-size: 14px;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; white-space: pre-wrap;\">{customerName} ď</span><span style=\"font-family: Arial; white-space: pre-wrap;\">akujeme za Váš záujem. Žiaľ,</span><span style=\"font-family: Arial; font-size: 11px; white-space: pre-wrap;\">​​​​​​​</span><span style=\"font-family: Arial; font-size: 14px;\">Vaša rezervácia dňa {reservationDate} o {reservationTimeStart} bola zamietnutá.</span></span></p><p style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px;\">Pre zistenie dôvodov zamietnutia rezervácie nás, prosím, kontaktujte na telefónnom čísle </span></font><span style=\"text-align: left; font-size: 14px;\"><font color=\"#ffffff\" face=\"Arial\" style=\"\">{propertyPhone} alebo emailom</font></span><span style=\"text-align: left; font-size: 14px;\"><font color=\"#ffffff\" face=\"Arial\"> {propertyEmail}.</font></span></p>','{customerName}, ďakujeme za Vašu rezerváciu dňa {reservationDate} o {reservationTimeStart}. Rezervácia bola schválená. ','{customerName}, ďakujeme za Váš záujem. Žiaľ, Vaša rezervácia dňa {reservationDate} o {reservationTimeStart} bola zamietnutá. Pre zistenie dôvodov zamietnutia rezervácie nás, prosím, kontaktujte na telefónnom čísle {propertyPhone}alebo emailom {propertyEmail}.','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px; white-space: pre-wrap;\"> {customerName}, Vaša platba bola úspešne zaevidovaná.</span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Tešíme sa na Vašu návštevu. </span></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px; white-space: pre-wrap;\" {customername}=\"\" {customerlastname},=\"\" vaša=\"\" platba=\"\" nebola=\"\" spracovaná.=\"\" <=\"\" span=\"\"></span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">{customerName},</span><span style=\"text-align: left; font-family: Arial; font-size: 14px;\">Vaša platba nebola spracovaná.</span></span></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px; white-space: pre-wrap;\" {customername}=\"\" {customerlastname},=\"\" Žiaľ,=\"\" stále=\"\" neevidujeme=\"\" vašu=\"\" platbu.=\"\" pre=\"\" úspešné=\"\" dokončenie=\"\" rezervácie=\"\" vykonajte,=\"\" prosím,=\"\" <=\"\" span=\"\"></span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 14px; white-space: pre-wrap;\">{customerName}, </span><span style=\"text-align: left; font-family: Calibri, sans-serif; font-size: 14.6667px;\">stále neevidujeme Vašu platbu. <br>Pre úspešné dokončenie rezervácie</span><span style=\"font-family: Calibri, sans-serif; font-size: 14.6667px; text-align: left;\">, prosím,</span><span style=\"font-family: Calibri, sans-serif; font-size: 14.6667px; text-align: left;\"> </span><span style=\"text-align: left; font-family: Calibri, sans-serif; font-size: 14.6667px;\">vykonajte platbu.</span><span style=\"text-align: left; font-family: Calibri, sans-serif; font-size: 14.6667px;\"> </span><font face=\"Arial\" style=\"font-size: 0.8125rem;\"><span style=\"font-size: 14px;\">Vaša rezervácia je nepotvrdená.</span></font></span></div>','{customerName}, ďakujeme. Vaša platba bola úspešne zaevidovaná. Tešíme sa na návštevu.','{customerName}, Vaša platba nebola spracovaná. ','{customerName}, Žiaľ stále neevidujeme Vašu platbu. Pre úspešné dokončenie rezervácie vykonajte, prosím, platbu. Vaša rezervácia dňa {reservationDate} o {reservationTimeStart} nie je aktuálne potvrdená.','sk'),
	(11,'#263f58','<p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"font-family: Arial; font-size: 11px; white-space: pre-wrap;\">Thanks for you reservation {customerName} {customerLastName} !</span><span style=\"font-family: Arial; font-size: 11px; white-space: pre-wrap; background-color: rgb(255, 255, 0);\">​​​​​​​</span></span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">We\'re looking forward for visit at {reservationDate} o {reservationTimeStart}</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 8px;\">If you change your decision , click on button bellow!</span></p>','<p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">Thanks for your reservation {customerName} {customerLastName} !</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">​​​​​​​</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Reservation  {reservationDate} at {reservationTimeStart} is waiting for confirm!</span></p><p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 8px;\">If you change your </span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 8px;\">decision</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 8px;\"> , click on button bellow!</span><br></p>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 11px; white-space: pre-wrap;\">I\'m so sad for your decision {customerName} {customerLastName} ! :(</span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">Your reservation {reservationDate} at {reservationTimeStart} was succesfully canceled !</span></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 11px; white-space: pre-wrap;\">Dear {customerName} {customerLastName}, we hope than you enjoyed your visit ! :)</span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">For make it better, please rate ours services by click on button bellow!</span></div>','','','','<p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">Thanks for you reservation {customerName} {customerLastName} !</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">​​​​​​​</span></p><p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 14px; color: rgb(255, 255, 255);\">Reservation {reservationDate} at {reservationTimeStart} was confirmed!</span></p><p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 8px; color: rgb(255, 255, 255);\">If you change your decision, click on button bellow!</span></p>','<p style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">Thanks for your interest {customerName} {customerLastName}  ale</span><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 11px; white-space: pre-wrap;\">​​​​​​​</span></p><p style=\"text-align: center;\"><span style=\"font-family: Arial; font-size: 14px; color: rgb(255, 255, 255);\">reservation{reservationDate} at {reservationTimeStart} was declined!</span></p><p style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 8px;\">For more informations contact us!</span></font></p>','','','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 11px; white-space: pre-wrap;\">Dear {customerName} {customerLastName}, your payment was registred! :)</span></font></div><div style=\"text-align: center;\"><span style=\"color: rgb(255, 255, 255); font-family: Arial; font-size: 14px;\">We hope, you\'ll enjoy a visit!</span></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 11px; white-space: pre-wrap;\">Dear {customerName} {customerLastName}, your payment doesnt was a registered! :(</span></font></div><div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px;\">Hope a next time will succesfully done!</span></font></div>','<div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 11px; white-space: pre-wrap;\">Dear {customerName} {customerLastName}, for reaservation u\'ve to pay!</span></font></div><div style=\"text-align: center;\"><font color=\"#ffffff\" face=\"Arial\"><span style=\"font-size: 14px;\">Your reseservation is pending!</span></font></div>','','','','en'),
	(12,'','','','','','','','','','','','','','','','','','','sk'),
	(12,'','','','','','','','','','','','','','','','','','','en');

/*!40000 ALTER TABLE `property_notification` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table property_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_settings`;

CREATE TABLE `property_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `cron_email` varchar(700) DEFAULT NULL,
  `time_sequence` varchar(500) DEFAULT NULL,
  `primary_color` varchar(10) DEFAULT NULL,
  `secondary_color` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `property_settings` WRITE;
/*!40000 ALTER TABLE `property_settings` DISABLE KEYS */;

INSERT INTO `property_settings` (`id`, `property_id`, `cron_email`, `time_sequence`, `primary_color`, `secondary_color`)
VALUES
	(5,1,NULL,'01','#fcb334','#283950'),
	(6,11,'kajo.cibulka@gmail.com','09,10,14,17,20,22,11','#61173a','#5e1211'),
	(7,12,NULL,'01','#fcb334','#283950');

/*!40000 ALTER TABLE `property_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reservations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservations`;

CREATE TABLE `reservations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `source` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `condition` tinyint(1) DEFAULT '1',
  `timeFrom` timestamp NULL DEFAULT NULL,
  `timeTo` timestamp NULL DEFAULT NULL,
  `person_count` int(2) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL,
  `notificated` tinyint(1) DEFAULT '0',
  `rated` tinyint(1) DEFAULT '0',
  `notificatedbyemail` tinyint(1) DEFAULT '0',
  `combination_enabled` tinyint(4) DEFAULT '0',
  `pending` tinyint(1) DEFAULT '0',
  `childSeat` tinyint(1) DEFAULT '0',
  `waiter` int(10) DEFAULT NULL,
  `upsells` text,
  `required_payment` tinyint(1) DEFAULT '0',
  `payed` tinyint(1) DEFAULT '0',
  `price` varchar(10) DEFAULT '0',
  `payment_type` int(5) DEFAULT '5',
  `bank_response` longtext,
  `payed_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `price_payed` varchar(10) NOT NULL DEFAULT '0',
  `price_waiting` varchar(10) NOT NULL DEFAULT '0',
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table time_interval
# ------------------------------------------------------------

DROP TABLE IF EXISTS `time_interval`;

CREATE TABLE `time_interval` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `time_interval` WRITE;
/*!40000 ALTER TABLE `time_interval` DISABLE KEYS */;

INSERT INTO `time_interval` (`id`, `name`)
VALUES
	(1,'eachday'),
	(2,'eachmonday'),
	(3,'eachtuesday'),
	(4,'eachwednesday'),
	(5,'eachthursday'),
	(6,'eachfriday'),
	(7,'eachsaturday'),
	(8,'eachsunday'),
	(9,'eachweekend');

/*!40000 ALTER TABLE `time_interval` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) DEFAULT NULL,
  `controller_id` int(11) DEFAULT NULL,
  `show` tinyint(1) DEFAULT '0',
  `create` tinyint(1) DEFAULT '0',
  `edit` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;

INSERT INTO `user_permissions` (`id`, `user_group_id`, `controller_id`, `show`, `create`, `edit`, `delete`)
VALUES
	(1,1,1,1,1,1,1),
	(2,1,2,0,0,0,0),
	(3,1,3,0,0,0,0),
	(7,1,4,0,0,0,0),
	(50,1,7,1,1,1,1),
	(51,1,8,1,1,1,1),
	(52,1,9,1,1,1,1),
	(53,1,10,1,1,1,1),
	(54,1,11,1,1,1,1),
	(55,1,12,1,1,1,1),
	(56,1,13,1,1,1,1),
	(57,1,14,0,0,0,0),
	(58,1,15,1,1,1,1),
	(59,1,16,0,0,0,0),
	(60,1,17,1,1,1,1),
	(61,1,18,1,1,1,1),
	(62,1,19,1,1,1,1),
	(63,1,20,1,1,1,1),
	(64,1,21,1,1,1,1),
	(65,1,22,1,1,1,1),
	(66,1,23,1,1,1,1),
	(67,1,24,1,1,1,1),
	(68,1,25,1,1,1,1),
	(69,1,26,1,1,1,1),
	(70,3,1,1,1,1,1),
	(71,3,12,1,1,1,1),
	(72,3,14,1,1,1,1),
	(73,1,27,1,1,1,1),
	(74,1,28,1,1,1,1),
	(75,1,29,1,1,1,1),
	(76,3,2,1,1,1,1),
	(77,3,3,1,1,1,1),
	(78,3,4,1,1,1,1),
	(79,3,7,1,1,1,1),
	(80,3,8,1,1,1,1),
	(81,3,9,1,1,1,1),
	(82,3,10,1,1,1,1),
	(83,3,11,1,1,1,1),
	(84,3,13,1,1,1,1),
	(85,3,15,1,1,1,1),
	(86,3,16,1,1,1,1),
	(87,3,17,1,1,1,1),
	(88,3,18,1,1,1,1),
	(89,3,19,1,1,1,1),
	(90,3,20,1,1,1,1),
	(91,3,21,1,1,1,1),
	(92,3,22,1,1,1,1),
	(93,3,23,1,1,1,1),
	(94,3,24,1,1,1,1),
	(95,3,25,1,1,1,1),
	(96,3,26,1,1,1,1),
	(97,3,27,1,1,1,1),
	(98,3,28,1,1,1,1),
	(99,3,29,1,1,1,1),
	(100,1,30,1,1,1,1),
	(101,3,30,1,1,1,1),
	(102,1,31,1,1,1,1),
	(103,3,31,1,1,1,1),
	(104,1,32,1,1,1,1),
	(105,3,32,1,1,1,1),
	(106,3,33,1,1,1,1);

/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `lang` varchar(2) DEFAULT 'sk',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_email` (`email`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `salt`, `lang`)
VALUES
	(1,'127.0.0.1','administrator','$2y$08$3crWDvzv7eZtNTMAs5LCJ.8ODdXnIFSTye8VPy4y7Zu0nvv4/AgiG','admin@admin.com',NULL,'',NULL,NULL,NULL,NULL,'7REgRD0XHWPnJi/kLk05ju',1268889823,1582746716,1,'Admin','istrator','ADMIN','0','1','sk'),
	(8,'','kajo.cibulka@gmail.com','$2y$08$dVmv.s2lMMTSTvTWKYhK6uT3v9NQsBIswlpQxOUC1uu71JXjtX0.u','kajo.cibulka@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1585081625,1,'Karol','Cibulka','bc. karol cibulka','0904191800',NULL,'sk'),
	(15,'','support@each.sk','$2y$08$3crWDvzv7eZtNTMAs5LCJ.8ODdXnIFSTye8VPy4y7Zu0nvv4/AgiG','support@each.sk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1582626699,1582636580,1,'Karol','Cibulka',NULL,NULL,NULL,'sk');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(22,1,1),
	(23,1,2),
	(24,1,3),
	(20,8,1),
	(21,8,3),
	(72,15,1),
	(71,15,2),
	(73,15,3);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_properties`;

CREATE TABLE `users_properties` (
  `user_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  UNIQUE KEY `Index 1` (`user_id`,`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users_properties` WRITE;
/*!40000 ALTER TABLE `users_properties` DISABLE KEYS */;

INSERT INTO `users_properties` (`user_id`, `property_id`)
VALUES
	(1,14),
	(8,14),
	(13,8),
	(13,15),
	(14,1),
	(14,8),
	(14,15),
	(15,14);

/*!40000 ALTER TABLE `users_properties` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
