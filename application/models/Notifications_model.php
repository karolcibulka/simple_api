<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-09-26
 * Time: 15:46
 */

class Notifications_model extends CI_Model
{

    public function insertNotification($data){
        $this->db->insert('notifications',$data);
        return $this->db->insert_id();
    }

    public function insertNotificationLangs($data){
        $this->db->insert_batch('notifications_langs',$data);
    }

    public function insertNotificationDates($data){
        $this->db->insert_batch('notifications_dates',$data);
    }

    public function getNotifications($property_id){
        return $this->db->select('*')
            ->from('notifications as n')
            ->where('n.property_id',$property_id)
            ->where('n.deleted',0)
            ->order_by('n.order','asc')
            ->get()
            ->result_array();
    }

    public function updateNotification($id,$data){
        $this->db->where('id',$id)->update('notifications',$data);
        return $id;
    }

    public function deleteDates($id){
        $this->db->where('notification_id',$id)->delete('notifications_dates');
    }

    public function deleteLangs($id){
        $this->db->where('notification_id',$id)->delete('notifications_langs');
    }

    public function getNotification($id){
        $data = $this->db->select('*')
            ->from('notifications as n')
            ->where('n.id',$id)
            ->join('notifications_dates as nd','nd.notification_id = n.id','left')
            ->join('notifications_langs as nl','n.id = nl.notification_id','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $key => $d){
                $response['id'] = $d['id'];
                $response['visible_on'] = json_decode($d['visible_on'],true);
                $response['internal_name'] = $d['internal_name'];
                $response['color'] = $d['color'];
                $response['text_color'] = $d['text_color'];
                $response['web'] = $d['web'];
                $response['email'] = $d['email'];
                $response['dates'][$d['from'].'_'.$d['to']]['from'] = date('d.m.Y',strtotime($d['from']));
                $response['dates'][$d['from'].'_'.$d['to']]['to'] = date('d.m.Y',strtotime($d['to']));
                $response['languages'][$d['lang']]['name'] = $d['name'];
                $response['languages'][$d['lang']]['content'] = $d['content'];
            }
        }

        return $response;
    }

    public function getLang($reservationID){
        $lang = $this->db->select('lang')
            ->from('reservations as r')
            ->where('r.id',$reservationID)
            ->get()
            ->row_array();

        $lang = $lang['lang'];
        return $lang;
    }

    public function getProperty($propertyID){
        return $this->db->select('*')
            ->from('property')
            ->where('id',$propertyID)
            ->get()
            ->row_array();
    }

    public function getSubscriber($propertyID,$token){

    }

    public function getPropertySettingsSms($property_id){
        $data = $this->db->select('sms')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        if(isset($data['sms']) && !empty($data['sms'])){
            $smsSettings =  unserialize($data['sms']);
            if(isset($smsSettings['integration_id']) && !empty($smsSettings['integration_id']) && isset($smsSettings['sms_farm']) && !empty($smsSettings['sms_farm'])){
                $response = array(
                    'integration_id' => $smsSettings['integration_id'],
                    'code' => $smsSettings['sms_farm'],
                    'name' => isset($smsSettings['sender']) && !empty($smsSettings['sender']) ? $smsSettings['sender'] : 'BlueGastro',
                    'text' => isset($smsSettings['text']) && !empty($smsSettings['text']) ? $smsSettings['text'] : '',
                );

                return $response;
            }
        }

        return false;
    }

    public function getPropertySettingsNotificationEmail($property_id){
        $data = $this->db->select('notification_email')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        if(!empty($data['notification_email'])){
            return $data['notification_email'];
        }
        return false;

    }

    public function userExistAndIsNotActive($propertyID,$token){
        $data = $this->db->select('*')
            ->from('subscribers')
            ->where('token',$token)
            ->where('active','0')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        if(!empty($data)){
            return true;
        }
        return false;
    }

    public function updateSubscriber($propertyID,$token,$data){
        $this->db->where('token',$token)->where('property_id',$propertyID)->update('subscribers',$data);
    }

    public function userExistAndIsActive($propertyID,$token){
        $data = $this->db->select('*')
            ->from('subscribers')
            ->where('token',$token)
            ->where('active','1')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        if(!empty($data)){
            return true;
        }
        return false;
    }

    public function getTable($property_id,$table_token){
        return $this->db->select('*')
            ->from('tables')
            ->where('property_id',$property_id)
            ->where('token',$table_token)
            ->get()
            ->row_array();
    }

    public function getEmailNotifications($property_id,$lang){
        return $this->db->select('
            nl.name as name,
            nl.content as content,
            n.id as id
        ')->from('notifications as n')
            ->join('notifications_langs as nl','nl.notification_id = n.id','left')
            ->join('notifications_dates as nd','nd.notification_id = n.id','left')
            ->where('nl.lang',$lang)
            ->where('n.property_id',$property_id)
            ->where('DATE(nd.from)<=',date('Y-m-d'))
            ->where('DATE(nd.to)>=',date('Y-m-d'))
            ->where('n.email',1)
            ->where('n.active',1)
            ->where('n.deleted',0)
            ->order_by('n.order','asc')
            ->get()
            ->result_array();
    }

    public function getSubpages($property_id){
        return $this->db->select('
            
        ')->from('front_navigation')
            ->where('property_id',$property_id)
            ->where('deleted',0)
            ->get()
            ->result_array();
    }
}
