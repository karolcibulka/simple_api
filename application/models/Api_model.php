<?php


class Api_model extends CI_Model
{

    public function getControllers(){
        return $this->db->select('*')
            ->from('api_controllers as ac')
            ->where('ac.deleted',0)
            ->get()
            ->result_array();
    }

    public function getPermissions(){
        $data = $this->db->select('*')
            ->from('api_permissions as ap')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['api_group_id']][$d['api_controller_id']] = array(
                    'show' => $d['s'],
                    'edit' => $d['e'],
                    'create' => $d['c'],
                    'delete' => $d['d']
                );
            }
        }

        return $response;
    }

    public function getApiPermissions(){

        $controllers = $this->getControllers();
        $permissions = $this->getPermissions();

        $groups = $this->db->select('*')
            ->from('api_groups as ag')
            ->where('ag.deleted',0)
            ->order_by('ag.order','asc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($groups)){
            foreach($groups as $group_key => $group){
                if(!empty($controllers)){
                    foreach($controllers as $controller){
                        $response[$group['id']]['id'] = $group['id'];
                        $response[$group['id']]['name'] = $group['name'];
                        $response[$group['id']]['description'] = $group['description'];
                        $response[$group['id']]['controllers'][$controller['name']]['id'] = $controller['id'];
                        $response[$group['id']]['controllers'][$controller['name']]['name'] = $controller['name'];
                        $response[$group['id']]['controllers'][$controller['name']]['description'] = $controller['description'];
                        $response[$group['id']]['controllers'][$controller['name']]['permissions']['show'] = isset($permissions[$group['id']][$controller['id']]['show']) && !empty($permissions[$group['id']][$controller['id']]['show']) ? 1 : 0;
                        $response[$group['id']]['controllers'][$controller['name']]['permissions']['edit'] = isset($permissions[$group['id']][$controller['id']]['edit']) && !empty($permissions[$group['id']][$controller['id']]['edit']) ? 1 : 0;
                        $response[$group['id']]['controllers'][$controller['name']]['permissions']['create'] = isset($permissions[$group['id']][$controller['id']]['create']) && !empty($permissions[$group['id']][$controller['id']]['create']) ? 1 : 0;
                        $response[$group['id']]['controllers'][$controller['name']]['permissions']['delete'] = isset($permissions[$group['id']][$controller['id']]['delete']) && !empty($permissions[$group['id']][$controller['id']]['delete']) ? 1 : 0;
                    }
                }
            }
        }

        return $response;
    }

    public function storeController($data){
        $this->db->insert('api_controllers',$data);
    }

    public function storeGroup($data){
        $this->db->insert('api_groups',$data);
        return $this->db->insert_id();
    }

    public function updateGroup($id,$data){
        $this->db->where('id',$id)->update('api_groups',$data);
    }

    public function insertOrUpdatePermission($data,$method){

        $query = 'INSERT INTO api_permissions ('.implode(',',array_keys($data)).') values ('.implode(',',$data).') ON DUPLICATE KEY UPDATE '.$method.'='.$data[$method];
        $this->db->query($query);
    }

    public function getController($id){
        return $this->db->select('*')
            ->from('api_controllers')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function updateController($id,$data){
        $this->db->where('id',$id)->update('api_controllers',$data);
    }

    public function getApiGroup($id){
        return $this->db->select('*')
            ->from('api_groups')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function validateApiKey($api_key){
        $data = $this->db->select('*')
            ->from('api_keys as ak')
            ->where('ak.api_key',$api_key)
            ->join('api_key_groups as akg','akg.api_key_id = ak.id','left')
            ->join('api_groups as ag','ag.id = akg.api_group_id AND ag.deleted = 0','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                if(isset($d['api_group_id']) && !empty($d['api_group_id'])){
                    $response[$d['api_group_id']] = $d['api_group_id'];
                }
            }
        }

        return $response;
    }

    public function getApiKeys(){
        $data = $this->db->select('*')
            ->from('api_keys as ak')
            ->where('ak.active',1)
            ->where('ak.deleted',0)
            ->join('api_key_groups as akg','akg.api_key_id = ak.id','left')
            ->join('api_groups as ag','ag.id = akg.api_group_id AND ag.deleted = 0','left')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[$d['api_key']][$d['api_group_id']] = $d['api_group_id'];
            }
        }

        return $response;
    }

    public function getPermissionsImportant($groups){
        $data = $this->db->select('
            ap.*,
            ac.name as controller_name,
            ag.id as group_id
        ')
            ->from('api_groups as ag')
            ->where_in('ag.id',$groups)
            ->join('api_permissions as ap','ap.api_group_id = ag.id','left')
            ->join('api_controllers as ac','ac.id = ap.api_controller_id AND ac.deleted = 0','left')
            ->where('ag.deleted',0)
            ->order_by('ag.order','asc')
            ->get()
            ->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $group_key => $group){
                if(isset($group['controller_name']) && !empty($group['controller_name'])){
                    $response[$group['group_id']][$group['controller_name']]['show']   = $group['s'];
                    $response[$group['group_id']][$group['controller_name']]['edit']   = $group['e'];
                    $response[$group['group_id']][$group['controller_name']]['create'] = $group['c'];
                    $response[$group['group_id']][$group['controller_name']]['delete'] = $group['d'];
                }
            }
        }

        return $response;
    }
}