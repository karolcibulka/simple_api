<?php


class Geography_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->driver('cache',array('driver'=>'file','backup'=>'file'));

        if(!file_exists(APPPATH.'cache')){
            mkdir(APPPATH.'cache', 0777, true);
        }
    }

    public function getRegions(){
        if($this->cache->file->get('regions')){
            return $this->cache->file->get('regions');
        }
        else{
            $regions = $this->db->select('*')
                ->from('regions')
                ->get()
                ->result_array();

            $this->cache->file->save('regions',$regions,60*60*24*356);
            return $regions;
        }
    }

    public function getCities(){
        if($this->cache->file->get('cities')){
            return $this->cache->file->get('cities');
        }
        else {
            $cities = $this->db->select('*')
                ->from('cities')
                ->where('validTo', '')
                ->or_where('validTo>', date('Y-m-d H:i:s'))
                ->get()
                ->result_array();

            $regions = $this->getRegions();
            $regionsF = array();
            foreach($regions as $region){
                $regionsF[$region['regionCode']] = $region;
            }
            foreach($cities as $cityKey => $city){
                $code = substr($city['municipalityCode'], 0, 5);
                if(isset($regionsF[$code]) && !empty($regionsF[$code])){
                    $cities[$cityKey]['regionName'] = $regionsF[$code]['regionName'];
                }
            }

            $this->cache->file->save('cities',$cities,60*60*24*356);
            return $cities;
        }
    }

    public function getCitiesByCode($regionCode){
        if($this->cache->file->get('region_cities_'.$regionCode)){
            return $this->cache->file->get('region_cities_'.$regionCode);
        }
        else {
            $cities = $this->db->select('*')
                ->from('cities')
                ->group_start()
                ->where('validTo', '')
                ->or_where('validTo>', date('Y-m-d H:i:s'))
                ->group_end()
                ->like('municipalityCode',$regionCode)
                ->get()
                ->result_array();

            $this->cache->file->save('region_cities_'.$regionCode,$cities,60*60*24*356);
            return $cities;
        }
    }
}