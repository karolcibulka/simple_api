<?php


class Reservation_model extends CI_Model
{
    public function getPropertySettings($propertyID){
        return $this->db->select('*')
            ->from('property')
            ->where('id',$propertyID)
            ->get()
            ->row_array();
    }

    public function updateReservation($reservationID,$data){
        $this->db->where('id',$reservationID)->update('reservations',$data);
    }

    public function storeReservationLog($data){
        $this->db->insert('reservations_log',$data);
    }


    public function storePaymentLog($data){
        $this->db->insert('payment_log',$data);
    }

    public function getOrderTypeId($id,$property_id){
       return $this->db->select('external_id,external_plu_id,price')
            ->from('transportation')
            ->where('id',$id)
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

    }

    public function getPaymentTypeId($id,$property_id){

        //pre_r($id);exit;
        $payment = $this->db->select('external_id')
            ->from('payments')
            ->where('id',$id)
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        return $payment['external_id'];
    }

    public function getPaymentGate($payment_id,$property_id){
        $data = $this->db->select('pt.id,pt.type')
            ->from('payments as p')
            ->join('payment_types as pt','pt.id = p.payment_type_id')
            ->where('p.id',$payment_id)
            ->where('p.property_id',$property_id)
            ->get()
            ->row_array();

        if(isset($data['id']) && !empty($data['id'])){
            return $data;
        }
        return false;
    }

    public function getLang($reservationID){
        $lang = $this->db->select('lang')
            ->from('reservations as r')
            ->where('r.id',$reservationID)
            ->get()
            ->row_array();

        $lang = $lang['lang'];
        return $lang;
    }

    public function getLangToken($token){
        $lang = $this->db->select('lang')
            ->from('reservations as r')
            ->where('r.token',$token)
            ->get()
            ->row_array();

        $lang = $lang['lang'];
        return $lang;
    }

    public function getPaymentsDataForReservation($reservationID){
        $lang = $this->getLang($reservationID);

        $data = $this->db->select('
        r.payment_id as "paymentID",
        pt.type as "payment_type",
        pl.name as "payment_name",
        r.*,ri.*,
        r.note as "resNote",
        ri.type as "ri_type",
        ri.menu_type as "ri_menu_type",
        ri.daily_menu_internal_id as "ri_daily_menu_internal_id",
        ri.is_traceable as "ri_is_traceable",
        ri.count as "offerCount",
        ri.price as "offer_price",
        ri.price_with_upsells as "ri_price_with_upsells",
        ri.unit_price_with_upsells as "ri_unit_price_with_upsells",
        ri.note as "offerNote",
        riu.unit_price as "riu_unit_price",
        riu.count as "riu_count",
        riu.price as "riu_price",
        riu.upsell_id as "riu_upsell_id",
        riu.external_id as "riu_external_id",
        riu.upsell_data as "riu_upsell_data"
        
        ')
            ->from('reservations as r')
            ->where('r.id',$reservationID)
            ->join('reservations_item as ri','ri.reservation_id = r.id','left')
            ->join('payments as p','p.id = r.payment_id','left')
            ->join('payments_lang as pl','pl.payment_id = p.id','left')
            ->join('payment_types as pt','pt.id = p.payment_type_id','left')
            ->join('reservations_item_upsell as riu','riu.item_id = ri.id','left')
            ->where('pl.lang',$lang)
            ->get()
            ->result_array();


        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response['id'] = $d['id'];
                $response['property_id'] = $d['property_id'];
                $response['first_name'] = $d['first_name'];
                $response['last_name'] = $d['last_name'];
                $response['email'] = $d['email'];
                $response['phone'] = $d['phone'];
                $response['lang'] = $d['lang'];
                $response['note'] = $d['resNote'];
                $response['status'] = $d['status'];
                $response['total_price'] = $d['total_price'];
                $response['packing_price'] = $d['packing_price'];
                $response['delivery_price'] = $d['delivery_price'];
                $response['discount_price'] = $d['discount_price'];
                $response['items_price'] = $d['items_price'];
                $response['currency'] = $d['currency'];
                $response['payment_name'] = $d['payment_name'];
                $response['payment_type'] = $d['payment_type'];
                $response['payed_at'] = $d['payed_at'];
                $response['lang'] = $d['lang'];
                $response['transportation_id'] = $d['transportation_id'];
                $response['is_delivery'] = $d['is_delivery'];
                $response['created_at'] = $d['created_at'];
                $response['payed'] = $d['payed'];
                $response['city'] = $d['city'];
                $response['street'] = $d['street'];
                $response['number'] = $d['number'];
                $response['payment_id'] = $d['paymentID'];
                if($d['ri_type'] == 'products') {
                    $response['items'][$d['offer_id']]['offer_id']                                    = $d['offer_id'];
                    $response['items'][$d['offer_id']]['external_id']                                 = $d['external_id'];
                    $response['items'][$d['offer_id']]['count']                                       = $d['offerCount'];
                    $response['items'][$d['offer_id']]['unit_price']                                  = $d['unit_price'];
                    $response['items'][$d['offer_id']]['unit_price_with_upsells']                     = $d['ri_unit_price_with_upsells'];
                    $response['items'][$d['offer_id']]['note']                                        = $d['offerNote'];
                    $response['items'][$d['offer_id']]['price']                                       = $d['offer_price'];
                    $response['items'][$d['offer_id']]['ri_price_with_upsells']                       = $d['ri_price_with_upsells'];
                    $response['items'][$d['offer_id']]['offer_data']                                  = isset($d['offer_data']) && !empty($d['offer_data']) ? json_decode($d['offer_data'], true) : '';
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['upsell_id']   = $d['riu_upsell_id'];
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['external_id'] = $d['riu_external_id'];
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['price']       = $d['riu_price'];
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['count']       = $d['riu_count'];
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['unit_price']  = $d['riu_unit_price'];
                    $response['items'][$d['offer_id']]['upsells'][$d['riu_upsell_id']]['upsell_data'] = isset($d['riu_upsell_data']) && !empty($d['riu_upsell_data']) ? json_decode($d['riu_upsell_data'], true) : '';
                }
                elseif($d['ri_type'] == 'menu'){
                    $response['menu'][$d['ri_daily_menu_internal_id']]['count'] = $d['offerCount'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['note'] = $d['offerNote'];
                    if(isset($response['menu'][$d['ri_daily_menu_internal_id']]['price']) && !empty($response['menu'][$d['ri_daily_menu_internal_id']]['price'])){
                        if($d['ri_is_traceable'] == '0'){
                            $response['menu'][$d['ri_daily_menu_internal_id']]['price'] += formatNumApi($d['offerCount']*formatNumApi($d['unit_price']));
                        }
                    }
                    else{
                        if($d['ri_is_traceable'] == '0'){
                            $response['menu'][$d['ri_daily_menu_internal_id']]['price'] = formatNumApi($d['offerCount']*formatNumApi($d['unit_price']));
                        }
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['main']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['main'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['main'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink'] = array();
                    }

                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['traceable'] = $d['ri_is_traceable'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['price'] = $d['ri_is_traceable']=='1' ? '0.00' : formatNumApi($d['unit_price']);
                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['external_id'] = $d['external_id'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['id'] = $d['offer_id'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['count'] = $d['offerCount'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['daily_menu_data'] = isset($d['offer_data']) && !empty($d['offer_data']) ? json_decode($d['offer_data'],true) : '';
                }
            }
        }

        return $response;
    }

    public function getPaymentsDataForReservationToken($token){
        $lang = $this->getLangToken($token);

        $data = $this->db->select('
            r.payment_id as "paymentID",
            r.id as "reservation_id",
            ri.id as "ri_item_id",
            pt.type as "payment_type",
            pl.name as "payment_name",
            r.*,ri.*,
            ri.type as "ri_type",
            ri.menu_type as "ri_menu_type",
            ri.daily_menu_internal_id as "ri_daily_menu_internal_id",
            ri.is_traceable as "ri_is_traceable",
            r.note as "resNote",
            ri.count as "offerCount",
            ri.price as "offer_price",
            ri.price_with_upsells as "ri_price_with_upsells",
            ri.unit_price_with_upsells as "ri_unit_price_with_upsells",
            ri.note as "offerNote",
            riu.unit_price as "riu_unit_price",
            riu.count as "riu_count",
            riu.price as "riu_price",
            riu.upsell_id as "riu_upsell_id",
            riu.external_id as "riu_external_id",
            riu.upsell_data as "riu_upsell_data",
            riu.external_data as "riu_external_data",
            p.check_customer "p_check_customer"
        ')
            ->from('reservations as r')
            ->where('r.token',$token)
            ->join('reservations_item as ri','ri.reservation_id = r.id','left')
            ->join('payments as p','p.id = r.payment_id','left')
            ->join('payments_lang as pl','pl.payment_id = p.id','left')
            ->join('payment_types as pt','pt.id = p.payment_type_id','left')
            ->join('reservations_item_upsell as riu','riu.item_id = ri.id','left')
            ->where('pl.lang',$lang)
            ->get()
            ->result_array();


        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response['id'] = $d['reservation_id'];
                $response['token'] = $d['token'];
                $response['property_id'] = $d['property_id'];
                $response['first_name'] = $d['first_name'];
                $response['last_name'] = $d['last_name'];
                $response['discount_id'] = $d['discount_id'];
                $response['discount_price'] = $d['discount_price'];
                $response['discount_unit_price'] = $d['discount_unit_price'];
                $response['email'] = $d['email'];
                $response['phone'] = $d['phone'];
                $response['lang'] = $d['lang'];
                $response['note'] = $d['resNote'];
                $response['status'] = $d['status'];
                $response['total_price'] = $d['total_price'];
                $response['packing_price'] = $d['packing_price'];
                $response['delivery_price'] = $d['delivery_price'];
                $response['items_price'] = $d['items_price'];
                $response['currency'] = $d['currency'];
                $response['payment_name'] = $d['payment_name'];
                $response['payment_type'] = $d['payment_type'];
                $response['payed_at'] = $d['payed_at'];
                $response['lang'] = $d['lang'];
                $response['transportation_id'] = $d['transportation_id'];
                $response['is_delivery'] = $d['is_delivery'];
                $response['created_at'] = $d['created_at'];
                $response['payed'] = $d['payed'];
                $response['city'] = $d['city'];
                $response['street'] = $d['street'];
                $response['number'] = $d['number'];
                $response['payment_id'] = $d['paymentID'];
                $response['check_customer'] = $d['p_check_customer'];
                $response['table_token'] = $d['table_token'];
                $response['accommodated_guest_id'] = $d['accommodated_guest_id'];
                $response['room_nr'] = $d['room_nr'];
                if($d['ri_type'] == 'products') {
                    $response['items'][$d['ri_item_id']]['offer_id']                = $d['offer_id'];
                    $response['items'][$d['ri_item_id']]['external_id']             = $d['external_id'];
                    $response['items'][$d['ri_item_id']]['count']                   = $d['offerCount'];
                    $response['items'][$d['ri_item_id']]['unit_price']              = $d['unit_price'];
                    $response['items'][$d['ri_item_id']]['unit_price_with_upsells'] = $d['ri_unit_price_with_upsells'];
                    $response['items'][$d['ri_item_id']]['note']                    = $d['offerNote'];
                    $response['items'][$d['ri_item_id']]['price']                   = $d['offer_price'];
                    $response['items'][$d['ri_item_id']]['ri_price_with_upsells']   = $d['ri_price_with_upsells'];
                    $response['items'][$d['ri_item_id']]['offer_data']              = isset($d['offer_data']) && !empty($d['offer_data']) ? json_decode($d['offer_data'], true) : '';
                    if (isset($d['riu_upsell_id']) && !empty($d['riu_upsell_id'])) {
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['upsell_id']     = $d['riu_upsell_id'];
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['external_id']   = $d['riu_external_id'];
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['price']         = $d['riu_price'];
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['count']         = $d['riu_count'];
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['unit_price']    = $d['riu_unit_price'];
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['upsell_data']   = isset($d['riu_upsell_data']) && !empty($d['riu_upsell_data']) ? json_decode($d['riu_upsell_data'], true) : '';
                        $response['items'][$d['ri_item_id']]['upsells'][$d['riu_upsell_id']]['external_data'] = isset($d['riu_external_data']) && !empty($d['riu_external_data']) ? json_decode($d['riu_external_data'], true) : '';
                    }
                }
                elseif($d['ri_type'] == 'menu'){
                    $response['menu'][$d['ri_daily_menu_internal_id']]['count'] = $d['offerCount'];
                    $response['menu'][$d['ri_daily_menu_internal_id']]['note'] = $d['offerNote'];
                    if(isset($response['menu'][$d['ri_daily_menu_internal_id']]['price']) && !empty($response['menu'][$d['ri_daily_menu_internal_id']]['price'])){
                        if($d['ri_is_traceable'] == '0'){
                            $response['menu'][$d['ri_daily_menu_internal_id']]['price'] += formatNumApi($d['offerCount']*formatNumApi($d['unit_price']));
                        }
                    }
                    else{
                        if($d['ri_is_traceable'] == '0'){
                            $response['menu'][$d['ri_daily_menu_internal_id']]['price'] = formatNumApi($d['offerCount']*formatNumApi($d['unit_price']));
                        }
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['main']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['main'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['main'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['soup'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['desert'] = array();
                    }
                    if(!isset($response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink']) || empty($response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink'])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items']['drink'] = array();
                    }

                    if(isset($response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']])){
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['traceable'] = $d['ri_is_traceable'];
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['price'] = $d['ri_is_traceable']=='1' ? '0.00' : formatNumApi($d['unit_price']);
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['external_id'] = $d['external_id'];
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['id'] = $d['offer_id'];
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['count'] = $d['offerCount'];
                        $response['menu'][$d['ri_daily_menu_internal_id']]['items'][$d['ri_menu_type']]['daily_menu_data'] = isset($d['offer_data']) && !empty($d['offer_data']) ? json_decode($d['offer_data'],true) : '';
                    }
                }
            }
        }

        return $response;
    }

    public function getPropertyData($propertyID,$field = false){
        $data = $this->db->select('psw.*,c.id as "currencyID",c.name as "currencyName",c.code as "currencyCode",p.version')
            ->from('property_settings_web as psw')
            ->where('psw.property_id',$propertyID)
            ->join('property as p','p.id = psw.property_id')
            ->join('currencies as c','c.id = psw.currency','left')
            ->get()
            ->row_array();

        if($field){
            return $data[$field];
        }
        $response = array();
        if(!empty($data)){
            $language = getLangFromCode($data['default_lang']);
            $this->lang->load('global_lang', $language);
            $response['property_id'] = $data['property_id'];
            $response['default_lang'] = $data['default_lang'];
            $response['name'] = $data['name'];
            $response['version'] = $data['version'];
            $response['sms'] = isset($data['sms']) && !empty($data['sms']) ? unserialize($data['sms']) : '';
            $response['web'] = $data['web'];
            $response['currency']['id'] = $data['currencyID'];
            $response['currency']['name'] = $data['currencyName'];
            $response['currency']['code'] = $data['currencyCode'];
            if(isset($data['logo']) && !empty($data['logo'])){
                $response['logo'] = upload_url($propertyID.'/logo/'.$data['logo']);
            }
            else{
                $response['logo'] = '';
            }
            $response['default_language']['code'] = $data['default_lang'];

            $response['default_language']['name'] = lang('langshort.'.$data['default_lang']);
            $response['contact']['email'] = $data['email'];
            $response['contact']['phone'] = $data['phone'];
            $response['address']['country'] = $data['country'];
            $response['address']['street'] = $data['address'];
            $response['address']['zip'] = $data['zip'];
            $response['address']['city'] = $data['city'];
            $response['gps_coordinates']['longitude'] = $data['longitude'];
            $response['gps_coordinates']['latitude'] = $data['latitude'];
            $response['social']['facebook'] = $data['facebook'];
            $response['social']['instagram'] = $data['instagram'];
            $response['last_reservation_before_the_end'] = $data['last_reservation'];
        }
        return $response;
    }

    public function getDiscountExternalID($propertyID,$id){
        $data = $this->db->select('external_id')
            ->from('discounts')
            ->where('property_id',$propertyID)
            ->where('id',$id)
            ->get()
            ->row_array();

        return $data['external_id'];
    }

    public function iterateDiscount($id,$propertyID){
        $data = $this->db->select('used_count')
            ->from('discounts')
            ->where('property_id',$propertyID)
            ->where('id',$id)
            ->get()
            ->row_array();

        if(isset($data['used_count'])){
            $count = $data['used_count']+1;
            $this->db->where('property_id',$propertyID)->where('id',$id)->update('discounts',array('used_count'=>$count));
        }
    }

    public function getTable($property_id,$table_token){
        $data = $this->db->select('external_id,name,area_name')
            ->from('tables')
            ->where('property_id',$property_id)
            ->where('token',$table_token)
            ->get()
            ->row_array();

        if(!empty($data)){
            return $data;
        }
        return array();
    }

    public function getPropertySettingsByPropertyID($property_id){
        $data =  $this->db->select('psw.*')
            ->from('property_settings_web as psw')
            ->where('psw.property_id',$property_id)
            ->get()
            ->row_array();

        if(isset($data['logo']) && !empty($data['logo'])){
            $data['logo'] = upload_url($data['property_id'].'/logo/'.$data['logo']);
        }

        return $data;
    }

    //public function getPropertyVersion()
}