<?php
/**
 * Created by PhpStorm.
 * Requester: karol
 * Date: 2019-10-14
 * Time: 08:19
 */

class Log_model extends CI_Model
{
    public function saveResponse($data){
        $this->db->insert('payment_log',$data);
    }

    public function storeLog($data){
        $this->db->insert('dashboard_log',$data);
    }

    public function getLogs($start,$limit,$filters=array()){
        $data = $this->db->select('p.name as property,u.email as email,dl.item_id,dl.id,dl.method,dl.controller,dl.created_at,dl.request')
            ->from('dashboard_log as dl')
            ->join('property as p','p.id = dl.property_id','left')
            ->join('users as u','u.id = dl.user_id','left')
            ->limit($limit,$start);
            if(!empty($filters)){
                foreach($filters as $key => $val){
                    if($val !== ''){
                        if($key === 'created_at'){
                            $data->where('DATE(dl.created_at)',$val);
                        }
                        else{
                            $data->where('dl.'.$key,$val);
                        }
                    }
                }
            }
        return $data->order_by('created_at','desc')
            ->get()->result_array();

    }

    public function getLogsCount($filters=array()){
        $data = $this->db->select('dl.*')
            ->from('dashboard_log as dl')
            ->join('property as p','p.id = dl.property_id','left')
            ->join('users as u','u.id = dl.user_id','left');
            if(!empty($filters)){
                foreach($filters as $key => $val){
                    if($val !== ''){
                        if($key === 'created_at'){
                            $data->where('DATE(dl.created_at)',$val);
                        }
                        else{
                            $data->where('dl.'.$key,$val);
                        }
                    }
                }
            }
            return $data->get()->num_rows();
    }

    public function getProperties(){
        return $this->db->select('*')
            ->from('property')
            ->where('deleted','0')
            ->get()
            ->result_array();
    }

    public function getUsers(){
        return $this->db->select('*')
            ->from('users')
            ->get()
            ->result_array();
    }

    public function storeApiLog($data){
        $this->db->insert('api_log',$data);
    }

    public function getApiLogs($start,$limit,$filters=array()){
        $data = $this->db->select('p.name as property,al.item_id,al.id,al.method,al.controller,al.created_at,al.request')
            ->from('api_log as al')
            ->join('property as p','p.id = al.property_id','left')
            ->limit($limit,$start);
        if(!empty($filters)){
            foreach($filters as $key => $val){
                if($val !== ''){
                    if($key === 'created_at'){
                        $data->where('DATE(al.created_at)',$val);
                    }
                    else{
                        $data->where('al.'.$key,$val);
                    }
                }
            }
        }
        return $data->order_by('created_at','desc')
            ->get()->result_array();

    }

    public function getApiLogsCount($filters=array()){
        $data = $this->db->select('al.*')
            ->from('api_log as al')
            ->join('property as p','p.id = al.property_id','left');
        if(!empty($filters)){
            foreach($filters as $key => $val){
                if($val !== ''){
                    if($key === 'created_at'){
                        $data->where('DATE(al.created_at)',$val);
                    }
                    else{
                        $data->where('al.'.$key,$val);
                    }
                }
            }
        }
        return $data->get()->num_rows();
    }
}