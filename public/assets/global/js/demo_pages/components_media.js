/* ------------------------------------------------------------------------------
 *
 *  # Media objects
 *
 *  Demo JS code for components_media.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup modules
// ------------------------------

var Media = function () {


    //
    // Setup modules components
    //

    // Switchery
    var _componentSwitchery = function() {
        if (typeof Switchery == 'undefined') {
            console.warn('Warning - switchery.min.js is not loaded.');
            return;
        }

        // Initialize
        var elems = Array.prototype.slice.call(document.querySelectorAll('.form-control-switchery'));
        elems.forEach(function(html) {
            var switchery = new Switchery(html);
        });
    };

	// Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform();
    };


    //
    // Return objects assigned to modules
    //

    return {
        initComponents: function() {
            _componentSwitchery();
            _componentUniform();
        }
    }
}();


// Initialize modules
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    Media.initComponents();
});
