/* ------------------------------------------------------------------------------
 *
 *  # Job search - list
 *
 *  Demo JS code for job search page kit - list view
 *
 * ---------------------------------------------------------------------------- */


// Setup modules
// ------------------------------

var JobList = function () {


    //
    // Setup modules components
    //

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform();
    };


    //
    // Return objects assigned to modules
    //

    return {
        init: function() {
            _componentUniform();
        }
    }
}();


// Initialize modules
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    JobList.init();
});
