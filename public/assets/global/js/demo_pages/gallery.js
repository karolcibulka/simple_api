/* ------------------------------------------------------------------------------
 *
 *  # Media gallery
 *
 *  Demo JS code for gallery page kit
 *
 * ---------------------------------------------------------------------------- */


// Setup modules
// ------------------------------

var Gallery = function() {


    //
    // Setup modules components
    //

    // Lightbox
    var _componentFancybox = function() {
        if (!$().fancybox) {
            console.warn('Warning - fancybox.min.js is not loaded.');
            return;
        }

        // Image lightbox
        $('[data-popup="lightbox"]').fancybox({
            padding: 3
        });
    };


    //
    // Return objects assigned to modules
    //

    return {
        init: function() {
            _componentFancybox();
        }
    }
}();


// Initialize modules
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    Gallery.init();
});
