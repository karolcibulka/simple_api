/* ------------------------------------------------------------------------------
 *
 *  # Invoice grid
 *
 *  Demo JS code for invoice_grid.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup modules
// ------------------------------

var InvoiceGrid = function () {


    //
    // Setup modules components
    //

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform();
    };


    //
    // Return objects assigned to modules
    //

    return {
        initComponents: function() {
            _componentUniform();
        }
    }
}();


// Initialize modules
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    InvoiceGrid.initComponents();
});
