(function ($) {
 'use strict';

  function makeid(long) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < long; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }


  var intersect = function(a,b){
   return(
    (
     (
      ( a.x >= b.x && a.x <= b.x1 ) || ( a.x1 >= b.x && a.x1 <= b.x1  )
      ) && (
      ( a.y >= b.y && a.y <= b.y1 ) || ( a.y1 >= b.y && a.y1 <= b.y1 )
      )
      ) || (
      (
       ( b.x >= a.x && b.x <= a.x1 ) || ( b.x1 >= a.x && b.x1 <= a.x1  )
       ) && (
       ( b.y >= a.y && b.y <= a.y1 ) || ( b.y1 >= a.y && b.y1 <= a.y1 )
       )
       )
      ) || (
      (
       (
        ( a.x >= b.x && a.x <= b.x1 ) || ( a.x1 >= b.x && a.x1 <= b.x1  )
        ) && (
        ( b.y >= a.y && b.y <= a.y1 ) || ( b.y1 >= a.y && b.y1 <= a.y1 )
        )
        ) || (
        (
         ( b.x >= a.x && b.x <= a.x1 ) || ( b.x1 >= a.x && b.x1 <= a.x1  )
         ) && (
         ( a.y >= b.y && a.y <= b.y1 ) || ( a.y1 >= b.y && a.y1 <= b.y1 )
         )
         )
        );
  }


  function refreshCoordinates(){
    coordinates = new Array();
    elements = [];
    $('.vrr-draggable .vrr-element').each(function(e){
      elements.push($(this));
    });
    // for (var k in elements){
    for (var k = 0; k <= elements.length - 1; k++) {
      coordinates[k] = {
        x: $(elements[k]).offset().left,
        y: $(elements[k]).offset().top,
        x1: $(elements[k]).offset().left + $(elements[k])[0].clientWidth,
        y1: $(elements[k]).offset().top + $(elements[k])[0].clientHeight
      }
    }
    return coordinates;
  }

  function elementIdRefresh(){
    if($('.vrr-draggable .vrr-element').length > 0){
      var id = 0;
      $('.vrr-draggable .vrr-element').each(function(e){
        var tem_id = parseInt($(this).attr('data-id'));
        if(tem_id > id){
          id = tem_id;
        }
      });
      element_id = id;
    }
  }

  var coordinates = new Array(); // coordinates of all elemnts on canvas
  var new_coordinates = new Array(); // fake coordinates for delete curent
  var coordinates_with_curent = new Array();// coordinates of all elemnts on canvas and custom curent adding
  var elements = []; // Object of elements on canvas with out curent
  var new_elements = [];
  var draggableElementCoord = new Array(); // live position of dragged element
  var revert = null; // revet whene dragged from start to canvas
  var revert_exist = true; // rever whene dragged element on canvas
  var one_time_is_done = 0; // check for one time element of array delete
  var element_id = 0;
  var dropDraggableElementCoord = new Array();

  function initDraggble(){
    coordinates = new Array();
    elements = [];

    $('.vrr-draggable .vrr-element').each(function(e){
      elements.push($(this));
    });
    // for (var k in elements){
    for (var k = 0; k <= elements.length - 1; k++) {
      coordinates[k] = {
        x: $(elements[k]).offset().left,
        y: $(elements[k]).offset().top,
        x1: $(elements[k]).offset().left + $(elements[k])[0].clientWidth,
        y1: $(elements[k]).offset().top + $(elements[k])[0].clientHeight
      }
    }

    $(".vrr-element-sidebar .vrr-element").draggable({
      // grid: [20, 20],
      helper: 'clone',
      opacity: 0.9,
      cursor: "pointer",
      // snap: ".draggable",
      snapMode: "inner",
      scope: "this",
      cancel: ".vrr-element-delete, .vrr-element-edit",
      reverDuration: 200,
      // containment: '.draggable',
      drag: function( event, ui ) {
        
        draggableElementCoord = {
          x: ui.offset.left,
          y: ui.offset.top,
          x1: ui.offset.left + ui.helper[0].clientWidth,
          y1: ui.offset.top + ui.helper[0].clientHeight
        };

        draggableElementCoord['x'] = parseInt(draggableElementCoord['x']);
        draggableElementCoord['y'] = parseInt(draggableElementCoord['y']);
        draggableElementCoord['x1'] = parseInt(draggableElementCoord['x1']);
        draggableElementCoord['y1'] = parseInt(draggableElementCoord['y1']);

        dropDraggableElementCoord = draggableElementCoord;

        $('.vrr-draggable .vrr-element').removeClass('dragover');
        if(Object.keys(coordinates).length > 0){
          revert = true;
          for (var k in coordinates){
            if (intersect(coordinates[k], draggableElementCoord)){
              $(elements[k]).addClass('dragover');
              revert = false;
            } 
          }
        }
      }, 
      revert : function(event) {
        if(revert === true){
          return false;
        } else if(revert === false) {
          $('.vrr-draggable .vrr-element').removeClass('dragover');
          return true;
        } else if(revert === null){
          $('.vrr-draggable .vrr-element').removeClass('dragover');
          return true;
        }
      },// revert
      start: function(e, ui){
        $('.vrr-draggable .vrr-element').removeClass('opened');
        $( ".ui-draggable" ).not( ui.helper.css( "z-index", "3" ) ).css( "z-index", "2" );
        $(this).closest('.vrr-element-sidebar-wrap-all').find('.vrr-element-sidebar-wrap').css('z-index', '1');
        $(this).closest('.vrr-element-sidebar-wrap').css('z-index', '2');
      },
    }); 

    $('.vrr-tables-wrap').removeClass('vrr-loading');

  } // initDraggble



  function ititDroppable(){
    $(".vrr-draggable").droppable({
      scope: "this",
      tolerance: "fit",
      drop: function(e, ui) {
          var $draggable;
          $draggable = ui.helper.clone();
          $draggable.css('opacity', 1);
          $draggable.css('position', 'absolute');
          $draggable.removeClass('ui-draggable-dragging');

          if(revert === null){ // if first element on canvas
            ui.helper.remove();
          }

          coordinates_with_curent[0] = {
            x: ui.offset.left,
            y: ui.offset.top,
            x1: ui.offset.left + ui.draggable[0].clientWidth,
            y1: ui.offset.top + ui.draggable[0].clientHeight
          }

          elements = [];
          $('.vrr-draggable .vrr-element').each(function(e){
            elements.push($(this));
          });

          var canvasOffset = {
            'top': parseInt($(this).offset().top, 10) + parseInt($(this).css('border-top-width'), 10) + parseInt($(this).closest('.vrr-draggable-wrap').css('border-top-width'), 10),
            'left': parseInt($(this).offset().left, 10) + parseInt($(this).css('border-left-width'), 10) + parseInt($(this).closest('.vrr-draggable-wrap').css('border-left-width'), 10)
          }

          var offsetTop = dropDraggableElementCoord['y'] - canvasOffset.top;
          var offsetLeft = dropDraggableElementCoord['x'] - canvasOffset.left;

          $draggable.draggable({
            // grid: [20, 20],
            helper: 'original',
            opacity: 0.9,
            cancel: ".vrr-element-delete, .vrr-element-edit",
            containment: '.vrr-draggable',
            // snap: '.element',
            reverDuration: 200,
            //snapMode: "outside",
            snapTolerance: 5,
            create: function(event, ui){
              elements = [];
              $('.vrr-draggable .vrr-element').each(function(e){
                elements.push($(this));
              });
              // console.log(elements);
              if(revert === true || revert === null){
                coordinates = new Array();
                var i = 0;
                if(elements.length > 0){
                  // for (var k in elements){
                  for (var k = 0; k <= elements.length - 1; k++) {
                    coordinates[k] = {
                      x: $(elements[k]).offset().left,
                      y: $(elements[k]).offset().top,
                      x1: $(elements[k]).offset().left + $(elements[k])[0].clientWidth,
                      y1: $(elements[k]).offset().top + $(elements[k])[0].clientHeight
                    }
                    i = k;
                  }
                  coordinates[parseInt(i)+1] = coordinates_with_curent[0];
                } else {
                  coordinates[0] = coordinates_with_curent[0];
                }

                element_id++;
                var unique = makeid(10);
                $(this).find('.vrr-element-table').prepend('<div class="vrr-element-edit-rotate"></div>')
                  // $(this).find('.vrr-element-table').prepend('<div class="vrr-element-edit"></div>')
                  // $(this).find('.vrr-element-table').prepend('<div class="vrr-element-id" data-default="'+element_id+'">'+element_id+'</div>');
                $(this).attr('data-unique', unique);
                $(this).attr('data-id', element_id);
                $(this).find('.vrr-element-id').text(element_id);
                $(this).find('.vrr-element-id').attr('data-default', element_id);
              }
            },
            start: function(e, ui){
              $('.vrr-draggable .vrr-element').removeClass('opened');
              $( ".ui-draggable" ).not( ui.helper.css( "z-index", "3" ) ).css( "z-index", "2" );
              coordinates = refreshCoordinates();
              new_coordinates = new Array();
              new_elements = new Array();
              new_coordinates = coordinates;
              new_elements = elements;

            },
            drag: function( event, ui ) {
             
              draggableElementCoord = {
                x: ui.offset.left,
                y: ui.offset.top,
                x1: ui.offset.left + ui.helper[0].clientWidth,
                y1: ui.offset.top + ui.helper[0].clientHeight
              };

              var obj = {};
              obj = {0 : draggableElementCoord}

              var count = 0;
              for (var k  in coordinates){
                if(typeof new_coordinates[k] != "undefined"){

                  draggableElementCoord['x'] = parseInt(draggableElementCoord['x']);
                  new_coordinates[k]['x'] = parseInt(new_coordinates[k]['x']);
                  draggableElementCoord['y'] = parseInt(draggableElementCoord['y']);
                  new_coordinates[k]['y'] = parseInt(new_coordinates[k]['y']);

                  // if moved to 1px in any way then delet from fake array to let move forward(8 ways and static)
                  if(intersect(new_coordinates[k], draggableElementCoord) && one_time_is_done != 1){
                    if(one_time_is_done === 1){}else{
                        new_coordinates = new_coordinates.filter(function(item) {
                          return item !== new_coordinates[k];
                        });
                        new_elements = new_elements.filter(function(item) {
                          return item !== new_elements[k];
                        });
                        one_time_is_done = 1;
                      }
                  } else {
                    $('.vrr-draggable .vrr-element').removeClass('dragover');
                    revert_exist = true;
                    for (var k in new_coordinates){
                      if (intersect(new_coordinates[k], draggableElementCoord)){
                        $(new_elements[k]).addClass('dragover');
                        revert_exist = false;
                      } 
                    }
                  }
                  count++;
                }
              }
            },
            stop: function( event, ui ) {
              one_time_is_done = 0;
            },
            revert : function(event) {
              if(revert_exist === true){
                return false;
              } else if(revert_exist === false) {
                $('.vrr-draggable .vrr-element').removeClass('dragover');
                return true;
              } else if(revert_exist === null){
                $('.vrr-draggable .vrr-element').removeClass('dragover');
                return true;
              }
            },
          });
          
          if(revert === true || revert === null){
            $draggable.css({
              "top": offsetTop,
              "left": offsetLeft
            }).appendTo('.vrr-draggable');
          }

          elements = [];
          $('.vrr-draggable .vrr-element').each(function(e){
            elements.push($(this));
          });
        
      } // drop: function(){
    }); // $(".draggable").droppable({
  } // ititDroppable

  function initDraggbleLoad(){
    $('.vrr-draggable .vrr-element').draggable({
      // grid: [20, 20],
      helper: 'original',
      opacity: 0.9,
      cancel: ".vrr-element-delete, .vrr-element-edit",
      containment: '.vrr-draggable',
      // snap: '.draggable',
      reverDuration: 200,
      snapMode: "inside",
      snapTolerance: 5,
      create: function(event, ui){
        if(revert === true || revert === null){
          coordinates = new Array();
          var i = 0;
          // for (var k in elements){
          for (var k = 0; k <= elements.length - 1; k++) {
            coordinates[k] = {
              x: $(elements[k]).offset().left,
              y: $(elements[k]).offset().top,
              x1: $(elements[k]).offset().left + $(elements[k])[0].clientWidth,
              y1: $(elements[k]).offset().top + $(elements[k])[0].clientHeight
            }
            i = k;
          }
        }
      },
      start: function(e, ui){
        $('.vrr-draggable .vrr-element').removeClass('opened');

        $( ".ui-draggable" ).not( ui.helper.css( "z-index", "3" ) ).css( "z-index", "2" );
        coordinates = refreshCoordinates();
        new_coordinates = new Array();
        new_elements = new Array();
        new_coordinates = coordinates;
        new_elements = elements;
      },
      drag: function( event, ui ) {
        draggableElementCoord = {
          x: ui.offset.left,
          y: ui.offset.top,
          x1: ui.offset.left + ui.helper[0].clientWidth,
          y1: ui.offset.top + ui.helper[0].clientHeight
        };

       

        var count = 0;
        for (var k  in coordinates){
          if(typeof new_coordinates[k] != "undefined"){
            draggableElementCoord['x'] = parseInt(draggableElementCoord['x']);
            new_coordinates[k]['x'] = parseInt(new_coordinates[k]['x']);
            draggableElementCoord['y'] = parseInt(draggableElementCoord['y']);
            new_coordinates[k]['y'] = parseInt(new_coordinates[k]['y']);

            // if moved to 1px in any way then delet from fake array to let move forward(8 ways and static)
            if(intersect(new_coordinates[k], draggableElementCoord) && one_time_is_done != 1){
                // this need to do only one time, on first check
                if(one_time_is_done === 1){}else{
                  new_coordinates = new_coordinates.filter(function(item) {
                    return item !== new_coordinates[k];
                  });
                  new_elements = new_elements.filter(function(item) {
                    return item !== new_elements[k];
                  });
                  one_time_is_done = 1;
                }
            } else {
              $('.vrr-draggable .vrr-element').removeClass('dragover');
              revert_exist = true;
              for (var k in new_coordinates){
                if (intersect(new_coordinates[k], draggableElementCoord)){
                  $(new_elements[k]).addClass('dragover');
                  revert_exist = false;
                } 
              }
            }
            count++;
          }
        }
      },
      stop: function( event, ui ) {
        one_time_is_done = 0;
        coordinates = refreshCoordinates();
      },
      revert : function(event) {
        if(revert_exist === true){
          return false;
        } else if(revert_exist === false) {
          $('.vrr-draggable .vrr-element').removeClass('dragover');
          return true;
        } else if(revert_exist === null){
          $('.vrr-draggable .vrr-element').removeClass('dragover');
          return true;
        }
      },
    });
  }

  // Change Id
    // Rotate element
  $(document).on('click','.vrr-element-edit-rotate', function(){
    var element = $(this).closest('.vrr-element');
      var table = element.find('.vrr-element-table');
    var rotate = parseInt(element.attr('data-rotate'));
      var deg = 45;
      if(rotate == 1){
          element.attr('data-rotate',deg);
          table.css('transform', 'rotate(' + deg + 'deg)');
      }else{
          deg += parseInt(element.attr('data-rotate'));
          element.attr('data-rotate',deg);
          table.css('transform', 'rotate(' + deg + 'deg)');
      }
      table.find('.vrr-element-seats').css({'transform-origin':'top left','transform': 'rotate(' + -deg + 'deg) translateX(-50%) translateY(-50%)'});
  });

  // Delete element

  


  $(document).ready(function () {
    
    elementIdRefresh();
    initDraggble();
    ititDroppable();
    initDraggbleLoad();
    
    $('.vrr-element').each(function(){
      var element = $(this);
      var rotate = element.attr('data-rotate');
      var q = element.find('.vrr-element-seats-wrap .vrr-element-seat').length;
      // addCSStoDraw(q,element, rotate);
    });
    
    $('.vrr-tables-wrap').removeClass('vrr-loading');

  });






})(jQuery);